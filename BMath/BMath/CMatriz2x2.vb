﻿'Classe para operações com matrizes 2x2

' Previne erros de conversão de tipo em tempo de execução
Option Strict On

'Para padronizar a precisao utilizada
Imports Precisao = System.Double


Namespace Matriz

    Public Class Matriz2x2
        Implements ICloneable, IDisposable, IComparable



#Region "Declaracoes"

        Private privM11 As Precisao
        Private privM12 As Precisao
        Private privM21 As Precisao
        Private privM22 As Precisao

        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False

#End Region


#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub

#Region " IDisposable Support "

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub
#End Region



        Public Sub New()
            Me.privM11 = 0.0
            Me.privM12 = 0.0
            Me.privM21 = 0.0
            Me.privM22 = 0.0
        End Sub


        Public Sub New(ByVal M11 As Precisao, ByVal M12 As Precisao, ByVal M21 As Precisao, ByVal M22 As Precisao)
            Me.privM11 = M11
            Me.privM12 = M12
            Me.privM21 = M21
            Me.privM22 = M22
        End Sub

#End Region
        'Dispose, Finalize, New




#Region "Propriedades"



#Region "Matriz"

        Public Property M11() As Precisao
            Get
                Return Me.privM11
            End Get
            Set(value As Precisao)
                Me.privM11 = value
            End Set
        End Property

        Public Property M12() As Precisao
            Get
                Return Me.privM12
            End Get
            Set(value As Precisao)
                Me.privM12 = value
            End Set
        End Property

        Public Property M21() As Precisao
            Get
                Return Me.privM21
            End Get
            Set(value As Precisao)
                Me.privM21 = value
            End Set
        End Property

        Public Property M22() As Precisao
            Get
                Return Me.privM22
            End Get
            Set(value As Precisao)
                Me.privM22 = value
            End Set
        End Property

#End Region




#End Region
        ' ### Get - Set ###
        ' M11, M12, M21, M22
        '



#Region "Métodos"


#Region "Operações"



#Region "Multiplicação"


        'Multiplica Matrizes
        Public Shared Operator *(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Matriz2x2
            Dim MatrizSaida As New Matriz2x2()

            MatrizSaida.M11 = esquerda.M11 * direita.M11 + esquerda.M12 * direita.M21
            MatrizSaida.M12 = esquerda.M11 * direita.M12 + esquerda.M12 * direita.M22
            MatrizSaida.M21 = esquerda.M21 * direita.M11 + esquerda.M22 * direita.M21
            MatrizSaida.M22 = esquerda.M21 * direita.M12 + esquerda.M22 * direita.M22

            Return MatrizSaida

        End Operator

        'Multiplica matriz por constante
        Public Shared Operator *(ByVal matriz As Matriz2x2, ByVal constante As Precisao) As Matriz2x2

            Dim MatrizSaida As New Matriz2x2()
            MatrizSaida.M11 = matriz.M11 * constante
            MatrizSaida.M12 = matriz.M12 * constante
            MatrizSaida.M21 = matriz.M21 * constante
            MatrizSaida.M22 = matriz.M22 * constante

            Return MatrizSaida

        End Operator

        'Multiplica constante por matriz
        Public Shared Operator *(ByVal constante As Precisao, ByVal matriz As Matriz2x2) As Matriz2x2

            Dim MatrizSaida As New Matriz2x2()

            MatrizSaida.M11 = matriz.M11 * constante
            MatrizSaida.M12 = matriz.M12 * constante
            MatrizSaida.M21 = matriz.M21 * constante
            MatrizSaida.M22 = matriz.M22 * constante

            Return MatrizSaida

        End Operator


#End Region


#Region "Soma"

        Public Shared Operator +(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Matriz2x2

            Dim MatrizSaida As New Matriz2x2()

            MatrizSaida.M11 = esquerda.M11 + direita.M11
            MatrizSaida.M12 = esquerda.M12 + direita.M12
            MatrizSaida.M21 = esquerda.M21 + direita.M21
            MatrizSaida.M22 = esquerda.M22 + direita.M22

            Return MatrizSaida

        End Operator

#End Region


#Region "Subtração"

        Public Shared Operator -(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Matriz2x2

            Dim MatrizSaida As New Matriz2x2()

            MatrizSaida.M11 = esquerda.M11 - direita.M11
            MatrizSaida.M12 = esquerda.M12 - direita.M12
            MatrizSaida.M21 = esquerda.M21 - direita.M21
            MatrizSaida.M22 = esquerda.M22 - direita.M22

            Return MatrizSaida

        End Operator


        Public Shared Operator -(ByVal matriz As Matriz2x2) As Matriz2x2

            Dim MatrizSaida As New Matriz2x2()

            MatrizSaida.M11 = -matriz.M11
            MatrizSaida.M12 = -matriz.M12
            MatrizSaida.M21 = -matriz.M21
            MatrizSaida.M22 = -matriz.M22

            Return MatrizSaida

        End Operator


#End Region


#Region "Igualdade"

        Public Shared Operator =(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Boolean
            Return (esquerda.M11 = direita.M11 AndAlso _
                    esquerda.M12 = direita.M12 AndAlso _
                    esquerda.M21 = direita.M21 AndAlso _
                    esquerda.M22 = direita.M22)
        End Operator

#End Region



#Region "Diferença"

        Public Shared Operator <>(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Boolean
            Return Not (esquerda = direita)
        End Operator

#End Region


#Region "Divisão"

        'Divisão por constante
        Public Shared Operator /(ByVal esquerda As Matriz2x2, ByVal direita As Precisao) As Matriz2x2
            Return New Matriz2x2(esquerda.M11 / direita, esquerda.M12 / direita, _
                                 esquerda.M21 / direita, esquerda.M22 / direita)
        End Operator

        'Divisão por matriz (multiplicação pela inversa)
        Public Shared Operator /(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Matriz2x2
            Dim C As New Matriz2x2
            C = esquerda * direita.Inversa
            Return C
        End Operator


#End Region



#Region "Potenciação"

        'Potência "dot"(matlab). Termo a termo elevado a constante
        Public Shared Operator ^(ByVal esquerda As Matriz2x2, ByVal direita As Precisao) As Matriz2x2
            Dim C As New Matriz2x2(Math.Pow(esquerda.M11, direita), Math.Pow(esquerda.M12, direita), _
                                   Math.Pow(esquerda.M21, direita), Math.Pow(esquerda.M22, direita))
            Return C
        End Operator

#End Region





#End Region
        ' *, +, -, =, <>, /, ^



#Region "Funções Membro"



#Region "Cofator"

        Public Function Cofator() As Matriz2x2
            Dim MatrizSaida As New Matriz2x2()

            With MatrizSaida
                .M11 = Me.M22
                .M12 = -Me.M21
                .M21 = -Me.M12
                .M22 = Me.M11
            End With

            Return MatrizSaida

        End Function

#End Region


#Region "Determinante"

        ' Método de Laplace
        Public Function Determinante() As Precisao
            Return (Me.M11 * Me.Cofator.M11) + (Me.M12 * Me.Cofator.M12)
        End Function

#End Region


#Region "Transposta"

        Public Function Transposta() As Matriz2x2
            Return New Matriz2x2(Me.M11, Me.M21, Me.M12, Me.M22)
        End Function

#End Region


#Region "Adjunta"

        Public Function Adjunta() As Matriz2x2
            Return Me.Cofator.Transposta
        End Function

#End Region


#Region "Inversa"

        Public Function Inversa() As Matriz2x2
            'Console.WriteLine("Não é possível calcular a inversa, pois o determinante é zero.")
            'MsgBox("Não é possível calcular a inversa, pois o determinante é zero!")

            Return Me.Adjunta * (1.0 / Me.Determinante)
        End Function

#End Region


        ' Copia Matriz2x2.
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Matriz2x2(Me.M11, Me.M12, Me.M21, Me.M22)
        End Function



        Public Shared Function Identidade() As Matriz2x2
            Return New Matriz2x2(1, 0, 1, 0)
        End Function


        Public Shared Function Zeros() As Matriz2x2
            Return New Matriz2x2
        End Function



        Public Overloads Function Compara(ByVal obj As Object) As Boolean
            If TypeOf obj Is Matriz2x2 Then
                Return (Me = DirectCast(obj, Matriz2x2))
            Else
                Return False
            End If
        End Function



        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Matriz2x2) Then Return 0
            Dim matriz As Matriz2x2 = CType(obj, Matriz2x2)
            Return Me.CompareTo(matriz)
        End Function


        Public Shared Function Multiplica(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Matriz2x2
            Return esquerda * direita
        End Function



        Public Shared Function Multiplica(ByVal matriz As Matriz2x2, ByVal constante As Precisao) As Matriz2x2
            Return matriz * constante
        End Function



        Public Shared Function Multiplica(ByVal constante As Precisao, ByVal matriz As Matriz2x2) As Matriz2x2
            Return constante * matriz
        End Function



        Public Shared Function Soma(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Matriz2x2
            Return esquerda + direita
        End Function



        Public Shared Function Subtrai(ByVal esquerda As Matriz2x2, ByVal direita As Matriz2x2) As Matriz2x2
            Return esquerda - direita
        End Function



        Public Shared Function Oposta(ByVal matriz As Matriz2x2) As Matriz2x2
            Return -matriz
        End Function



        Public Sub CopiaArray(ByVal array As Precisao(), ByVal index As Integer)
            Me.M11 = array(index)
            Me.M12 = array(index + 1)
            Me.M21 = array(index + 3)
            Me.M22 = array(index + 4)
        End Sub


        Public Shared Function Array2Matriz(ByVal array As Precisao()) As Matriz2x2

            Dim MatrizSaida As Matriz2x2 = Identidade()

            MatrizSaida.CopiaArray(array, 0)

            Return MatrizSaida
        End Function



        Public Overloads Overrides Function ToString() As String

            Dim builder As New System.Text.StringBuilder()

            builder.AppendFormat(" | {0} {1} |" & vbLf, Me.M11, Me.M12)
            builder.AppendFormat(" | {0} {1} |" & vbLf, Me.M21, Me.M22)
            Return builder.ToString()

        End Function

        'Copia matrix
        Sub CopiaMatriz(ByVal MatSrc As Matriz2x2, ByRef MatDest As Matriz2x2)

            With MatDest
                .M11 = MatSrc.M11 : .M12 = MatSrc.M12
                .M21 = MatSrc.M21 : .M22 = MatSrc.M22
            End With

        End Sub


        'Troca (swap) matrizes
        Sub TrocaMatriz(ByVal MatA As Matriz2x2, ByVal MatB As Matriz2x2)

            Dim MatC As Matriz2x2 = Nothing
            CopiaMatriz(MatA, MatC)
            CopiaMatriz(MatB, MatA)
            CopiaMatriz(MatC, MatB)

        End Sub


#End Region
        ' Determinante, Transposta, 
        ' Cofator, Adjunta, Inversa
        ' Identidade, Zeros, Clone
        ' Compara, Multiplica, Soma, Subtrai, Oposta
        ' CopiaArray, Array2Matriz, ToString
        ' CopiaMatriz, TrocaMatriz



#End Region



    End Class



End Namespace


