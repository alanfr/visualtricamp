﻿'Operações com matrizes 4x4

' Previne erros de conversão de tipo em tempo de execução
Option Strict On

Imports BMath.Funcoes


Imports Precisao = System.Double


Namespace Matriz

    Public Class Matriz4x4

        Implements ICloneable, IDisposable, IComparable


#Region "Declarações"

        Private privM11, privM12, privM13, privM14 As Precisao
        Private privM21, privM22, privM23, privM24 As Precisao
        Private privM31, privM32, privM33, privM34 As Precisao
        Private privM41, privM42, privM43, privM44 As Precisao

        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False

#End Region


#Region "Construtores e Destrutores"


        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub



        Public Sub New()
            '
            ' | 0   0   0   0 |
            ' | 0   0   0   0 |
            ' | 0   0   0   0 |
            ' | 0   0   0   0 |
            '   
            With Me
                .M11 = 0 : .M12 = 0 : .M13 = 0 : .M14 = 0
                .M21 = 0 : .M22 = 0 : .M23 = 0 : .M24 = 0
                .M31 = 0 : .M32 = 0 : .M33 = 0 : .M34 = 0
                .M41 = 0 : .M42 = 0 : .M43 = 0 : .M44 = 0
            End With

        End Sub


        Public Sub New(ByVal m11 As Precisao, ByVal m12 As Precisao, ByVal m13 As Precisao, ByVal m14 As Precisao, _
                       ByVal m21 As Precisao, ByVal m22 As Precisao, ByVal m23 As Precisao, ByVal m24 As Precisao, _
                       ByVal m31 As Precisao, ByVal m32 As Precisao, ByVal m33 As Precisao, ByVal m34 As Precisao, _
                       ByVal m41 As Precisao, ByVal m42 As Precisao, ByVal m43 As Precisao, ByVal m44 As Precisao)

            With Me
                .M11 = m11 : .M12 = m12 : .M13 = m13 : .M14 = m14
                .M21 = m21 : .M22 = m22 : .M23 = m23 : .M24 = m24
                .M31 = m31 : .M32 = m32 : .M33 = m33 : .M34 = m34
                .M41 = m41 : .M42 = m42 : .M43 = m43 : .M44 = m44
            End With

        End Sub


#End Region
        'Dispose, Finalize, New



#Region "Propriedades"


        Public Property M11() As Precisao
            Get
                Return Me.privM11
            End Get
            Set(value As Precisao)
                Me.privM11 = value
            End Set
        End Property

        Public Property M12() As Precisao
            Get
                Return Me.privM12
            End Get
            Set(value As Precisao)
                Me.privM12 = value
            End Set
        End Property

        Public Property M13() As Precisao
            Get
                Return Me.privM13
            End Get
            Set(value As Precisao)
                Me.privM13 = value
            End Set
        End Property

        Public Property M14() As Precisao
            Get
                Return Me.privM14
            End Get
            Set(value As Precisao)
                Me.privM14 = value
            End Set
        End Property



        Public Property M21() As Precisao
            Get
                Return Me.privM21
            End Get
            Set(value As Precisao)
                Me.privM21 = value
            End Set
        End Property

        Public Property M22() As Precisao
            Get
                Return Me.privM22
            End Get
            Set(value As Precisao)
                Me.privM22 = value
            End Set
        End Property

        Public Property M23() As Precisao
            Get
                Return Me.privM23
            End Get
            Set(value As Precisao)
                Me.privM23 = value
            End Set
        End Property


        Public Property M24() As Precisao
            Get
                Return Me.privM24
            End Get
            Set(value As Precisao)
                Me.privM24 = value
            End Set
        End Property





        Public Property M31() As Precisao
            Get
                Return Me.privM31
            End Get
            Set(value As Precisao)
                Me.privM31 = value
            End Set
        End Property

        Public Property M32() As Precisao
            Get
                Return Me.privM32
            End Get
            Set(value As Precisao)
                Me.privM32 = value
            End Set
        End Property

        Public Property M33() As Precisao
            Get
                Return Me.privM33
            End Get
            Set(value As Precisao)
                Me.privM33 = value
            End Set
        End Property

        Public Property M34() As Precisao
            Get
                Return Me.privM34
            End Get
            Set(value As Precisao)
                Me.privM34 = value
            End Set
        End Property





        Public Property M41() As Precisao
            Get
                Return Me.privM41
            End Get
            Set(value As Precisao)
                Me.privM41 = value
            End Set
        End Property

        Public Property M42() As Precisao
            Get
                Return Me.privM42
            End Get
            Set(value As Precisao)
                Me.privM42 = value
            End Set
        End Property

        Public Property M43() As Precisao
            Get
                Return Me.privM43
            End Get
            Set(value As Precisao)
                Me.privM43 = value
            End Set
        End Property

        Public Property M44() As Precisao
            Get
                Return Me.privM44
            End Get
            Set(value As Precisao)
                Me.privM44 = value
            End Set
        End Property


#End Region
        ' Get - Set
        ' M11, M12, M13, M14
        ' M21, M22, M23, M24
        ' M31, M32, M33, M34
        ' M41, M42, M43, M44




#Region "Métodos"


#Region "Operações"


#Region "Multiplicação"


        Public Shared Operator *(ByVal A As Matriz4x4, ByVal B As Matriz4x4) As Matriz4x4
            Dim M As New Matriz4x4()
            'If two matrices A & B, gives different effects (for example rotation and scale),
            'we use MatrixMultiply to give boths transformations in a single matrix, in other
            'words, matrix multiplication 'combine two matrices, in condition that A*B <> B*A
            With M
                .M11 = (A.M11 * B.M11) + (A.M12 * B.M21) + (A.M13 * B.M31) + (A.M14 * B.M41)
                .M12 = (A.M11 * B.M12) + (A.M12 * B.M22) + (A.M13 * B.M32) + (A.M14 * B.M42)
                .M13 = (A.M11 * B.M13) + (A.M12 * B.M23) + (A.M13 * B.M33) + (A.M14 * B.M43)
                .M14 = (A.M11 * B.M14) + (A.M12 * B.M24) + (A.M13 * B.M34) + (A.M14 * B.M44)

                .M21 = (A.M21 * B.M11) + (A.M22 * B.M21) + (A.M23 * B.M31) + (A.M24 * B.M41)
                .M22 = (A.M21 * B.M12) + (A.M22 * B.M22) + (A.M23 * B.M32) + (A.M24 * B.M42)
                .M23 = (A.M21 * B.M13) + (A.M22 * B.M23) + (A.M23 * B.M33) + (A.M24 * B.M43)
                .M24 = (A.M21 * B.M14) + (A.M22 * B.M24) + (A.M23 * B.M34) + (A.M24 * B.M44)

                .M31 = (A.M31 * B.M11) + (A.M32 * B.M21) + (A.M33 * B.M31) + (A.M34 * B.M41)
                .M32 = (A.M31 * B.M12) + (A.M32 * B.M22) + (A.M33 * B.M32) + (A.M34 * B.M42)
                .M33 = (A.M31 * B.M13) + (A.M32 * B.M23) + (A.M33 * B.M33) + (A.M34 * B.M43)
                .M34 = (A.M31 * B.M14) + (A.M32 * B.M24) + (A.M33 * B.M34) + (A.M34 * B.M44)

                .M41 = (A.M41 * B.M11) + (A.M42 * B.M21) + (A.M43 * B.M31) + (A.M44 * B.M41)
                .M42 = (A.M41 * B.M12) + (A.M42 * B.M22) + (A.M43 * B.M32) + (A.M44 * B.M42)
                .M43 = (A.M41 * B.M13) + (A.M42 * B.M23) + (A.M43 * B.M33) + (A.M44 * B.M43)
                .M44 = (A.M41 * B.M14) + (A.M42 * B.M24) + (A.M43 * B.M34) + (A.M44 * B.M44)
            End With
            Return M
        End Operator

        Public Shared Operator *(ByVal A As Matriz4x4, ByVal Precisao As Precisao) As Matriz4x4
            Dim M As New Matriz4x4()
            With M
                .M11 = A.M11 * Precisao
                .M12 = A.M12 * Precisao
                .M13 = A.M13 * Precisao
                .M14 = A.M14 * Precisao

                .M21 = A.M21 * Precisao
                .M22 = A.M22 * Precisao
                .M23 = A.M23 * Precisao
                .M24 = A.M24 * Precisao

                .M31 = A.M31 * Precisao
                .M32 = A.M32 * Precisao
                .M33 = A.M33 * Precisao
                .M34 = A.M34 * Precisao

                .M41 = A.M41 * Precisao
                .M42 = A.M42 * Precisao
                .M43 = A.M43 * Precisao
                .M44 = A.M44 * Precisao
            End With
            Return M
        End Operator

        Public Shared Operator *(ByVal Precisao As Precisao, ByVal matrix As Matriz4x4) As Matriz4x4
            Return matrix * Precisao
        End Operator



#End Region




#Region "Soma"



        Public Shared Operator +(ByVal A As Matriz4x4, ByVal B As Matriz4x4) As Matriz4x4
            Dim M As New Matriz4x4()

            With M
                .M11 = A.M11 + B.M11
                .M12 = A.M12 + B.M12
                .M13 = A.M13 + B.M13
                .M14 = A.M14 + B.M14

                .M21 = A.M21 + B.M21
                .M22 = A.M22 + B.M22
                .M23 = A.M23 + B.M23
                .M24 = A.M24 + B.M24

                .M31 = A.M31 + B.M31
                .M32 = A.M32 + B.M32
                .M33 = A.M33 + B.M33
                .M34 = A.M34 + B.M34

                .M41 = A.M41 + B.M41
                .M42 = A.M42 + B.M42
                .M43 = A.M43 + B.M43
                .M44 = A.M44 + B.M44
            End With

            Return M
        End Operator


#End Region



#Region "Subtração"



        Public Shared Operator -(ByVal A As Matriz4x4, ByVal B As Matriz4x4) As Matriz4x4
            Dim M As New Matriz4x4()

            With M
                .M11 = A.M11 - B.M11
                .M12 = A.M12 - B.M12
                .M13 = A.M13 - B.M13
                .M14 = A.M14 - B.M14
                .M21 = A.M21 - B.M21
                .M22 = A.M22 - B.M22
                .M23 = A.M23 - B.M23
                .M24 = A.M24 - B.M24
                .M31 = A.M31 - B.M31
                .M32 = A.M32 - B.M32
                .M33 = A.M33 - B.M33
                .M34 = A.M34 - B.M34
                .M41 = A.M41 - B.M41
                .M42 = A.M42 - B.M42
                .M43 = A.M43 - B.M43
                .M44 = A.M44 - B.M44
            End With

            Return M
        End Operator
        Public Shared Operator -(ByVal matrix As Matriz4x4) As Matriz4x4
            Return matrix * (-1)
        End Operator


#End Region


#Region "Igualdade"

        Public Shared Operator =(ByVal A As Matriz4x4, ByVal B As Matriz4x4) As Boolean
            Return (A.M11 = B.M11 AndAlso A.M12 = B.M12 AndAlso A.M13 = B.M13 AndAlso A.M14 = B.M14 AndAlso _
                    A.M21 = B.M21 AndAlso A.M22 = B.M22 AndAlso A.M23 = B.M23 AndAlso A.M24 = B.M24 AndAlso _
                    A.M31 = B.M31 AndAlso A.M32 = B.M32 AndAlso A.M33 = B.M33 AndAlso A.M34 = B.M34 AndAlso _
                    A.M41 = B.M41 AndAlso A.M42 = B.M42 AndAlso A.M43 = B.M43 AndAlso A.M44 = B.M44)
        End Operator

#End Region


#Region "Diferença"


        Public Shared Operator <>(ByVal A As Matriz4x4, ByVal B As Matriz4x4) As Boolean
            Return Not (A = B)
        End Operator


#End Region




#Region "Divisão"

        'Divisão por constante
        Public Shared Operator /(ByVal A As Matriz4x4, ByVal B As Precisao) As Matriz4x4
            Return New Matriz4x4(A.M11 / B, A.M12 / B, A.M13 / B, A.M14 / B, _
                                 A.M21 / B, A.M22 / B, A.M23 / B, A.M24 / B, _
                                 A.M31 / B, A.M32 / B, A.M33 / B, A.M34 / B, _
                                 A.M41 / B, A.M42 / B, A.M43 / B, A.M44 / B)
        End Operator

        'Divisão por matriz (multiplicação pela inversa)
        Public Shared Operator /(ByVal A As Matriz4x4, ByVal B As Matriz4x4) As Matriz4x4
            Return (A * B.Inversa)
        End Operator

#End Region


#Region "Potenciação"

        'Potência "dot"(matlab). Termo a termo elevado a constante
        Public Shared Operator ^(ByVal esquerda As Matriz4x4, ByVal direita As Precisao) As Matriz4x4
            Dim C As New Matriz4x4(Math.Pow(esquerda.M11, direita), Math.Pow(esquerda.M12, direita), Math.Pow(esquerda.M13, direita), Math.Pow(esquerda.M14, direita), _
                                   Math.Pow(esquerda.M21, direita), Math.Pow(esquerda.M22, direita), Math.Pow(esquerda.M23, direita), Math.Pow(esquerda.M24, direita), _
                                   Math.Pow(esquerda.M31, direita), Math.Pow(esquerda.M32, direita), Math.Pow(esquerda.M33, direita), Math.Pow(esquerda.M34, direita), _
                                   Math.Pow(esquerda.M41, direita), Math.Pow(esquerda.M42, direita), Math.Pow(esquerda.M43, direita), Math.Pow(esquerda.M44, direita))
            Return C
        End Operator

#End Region



#End Region
        '+ ,- ,* ,= ,<>



#Region "Cofator"


        Public Function Cofator() As Matriz4x4

            Dim A11 As Precisao = Det3x3(Me.M22, Me.M23, Me.M24, Me.M32, Me.M33, Me.M34, Me.M42, Me.M43, Me.M44)
            Dim A12 As Precisao = -Det3x3(Me.M21, Me.M23, Me.M24, Me.M31, Me.M33, Me.M34, Me.M41, Me.M43, Me.M44)
            Dim A13 As Precisao = Det3x3(Me.M21, Me.M22, Me.M24, Me.M31, Me.M32, Me.M34, Me.M41, Me.M42, Me.M44)
            Dim A14 As Precisao = -Det3x3(Me.M21, Me.M22, Me.M23, Me.M31, Me.M32, Me.M33, Me.M41, Me.M42, Me.M43)

            Dim A21 As Precisao = -Det3x3(Me.M12, Me.M13, Me.M14, Me.M32, Me.M33, Me.M34, Me.M42, Me.M43, Me.M44)
            Dim A22 As Precisao = Det3x3(Me.M11, Me.M13, Me.M14, Me.M31, Me.M33, Me.M34, Me.M41, Me.M43, Me.M44)
            Dim A23 As Precisao = -Det3x3(Me.M11, Me.M12, Me.M14, Me.M31, Me.M32, Me.M34, Me.M41, Me.M42, Me.M44)
            Dim A24 As Precisao = Det3x3(Me.M11, Me.M12, Me.M13, Me.M31, Me.M32, Me.M33, Me.M41, Me.M42, Me.M43)

            Dim A31 As Precisao = Det3x3(Me.M12, Me.M13, Me.M14, Me.M22, Me.M23, Me.M24, Me.M42, Me.M43, Me.M44)
            Dim A32 As Precisao = -Det3x3(Me.M11, Me.M13, Me.M14, Me.M21, Me.M23, Me.M24, Me.M41, Me.M43, Me.M44)
            Dim A33 As Precisao = Det3x3(Me.M11, Me.M12, Me.M14, Me.M21, Me.M22, Me.M24, Me.M41, Me.M42, Me.M44)
            Dim A34 As Precisao = -Det3x3(Me.M11, Me.M12, Me.M13, Me.M21, Me.M22, Me.M23, Me.M41, Me.M42, Me.M43)

            Dim A41 As Precisao = -Det3x3(Me.M12, Me.M13, Me.M14, Me.M22, Me.M23, Me.M24, Me.M32, Me.M33, Me.M34)
            Dim A42 As Precisao = Det3x3(Me.M11, Me.M13, Me.M14, Me.M21, Me.M23, Me.M24, Me.M31, Me.M33, Me.M34)
            Dim A43 As Precisao = -Det3x3(Me.M11, Me.M12, Me.M14, Me.M21, Me.M22, Me.M24, Me.M31, Me.M32, Me.M34)
            Dim A44 As Precisao = Det3x3(Me.M11, Me.M12, Me.M13, Me.M21, Me.M22, Me.M23, Me.M31, Me.M32, Me.M33)

            Dim MatrizSaida As New Matriz4x4(A11, A12, A13, A14, _
                                             A21, A22, A23, A24, _
                                             A31, A32, A33, A34, _
                                             A41, A42, A43, A44)

            Return MatrizSaida

        End Function


#End Region



#Region "Determinante"

        Public Function Determinante() As Precisao

            Dim MatrizSaida As Precisao

            MatrizSaida = (Me.M11 * Me.Cofator.M11) + (Me.M12 * Me.Cofator.M12) + (Me.M13 * Me.Cofator.M13) + (Me.M14 * Me.Cofator.M14)

            Return MatrizSaida

        End Function



#End Region


#Region "Transposta"

        Public Function Transposta() As Matriz4x4
            'Swap a 4x4 matrix from rows mode, to colmuns mode (and vise-versa)

            Return New Matriz4x4(Me.M11, Me.M21, Me.M31, Me.M41, _
                                 Me.M12, Me.M22, Me.M32, Me.M42, _
                                 Me.M13, Me.M23, Me.M33, Me.M43, _
                                 Me.M14, Me.M24, Me.M34, Me.M44)
        End Function



#End Region




#Region "Adjunta"

        Public Function Adjunta() As Matriz4x4

            Return Me.Cofator.Transposta

        End Function

#End Region



#Region "Inversa"


        Public Function Inversa() As Matriz4x4

            Return Me.Adjunta * (1.0 / Me.Determinante)

        End Function




#End Region
        ' Get
        ' Cofator, Determinante, Transposta 
        ' Adjunta, Inversa




#Region "Métodos"


        ' Copia a Matriz 4x4
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Matriz4x4(Me.M11, Me.M12, Me.M13, Me.M14, _
                                 Me.M21, Me.M22, Me.M23, Me.M24, _
                                 Me.M31, Me.M32, Me.M33, Me.M34, _
                                 Me.M41, Me.M42, Me.M43, Me.M44)
        End Function



        Public Shared Function Identidade() As Matriz4x4
            ' Matriz padrão para as transformações:
            ' (Matriz * Identidade) = Matriz
            '
            ' | 1   0   0   0 |
            ' | 0   1   0   0 |
            ' | 0   0   1   0 |
            ' | 0   0   0   1 |
            ' 
            Dim matriz As New Matriz4x4

            With matriz
                .M11 = 1 : .M12 = 0 : .M13 = 0 : .M14 = 0
                .M21 = 0 : .M22 = 1 : .M23 = 0 : .M24 = 0
                .M31 = 0 : .M32 = 0 : .M33 = 1 : .M34 = 0
                .M41 = 0 : .M42 = 0 : .M43 = 0 : .M44 = 1
            End With

            Return matriz

        End Function

        Public Shared Function Unidade() As Matriz4x4
            ' Matriz padrão para as transformações:
            ' (Matriz * Identidade) = Matriz
            '
            ' | 1   1   1   1 |
            ' | 1   1   1   1 |
            ' | 1   1   1   1 |
            ' | 1   1   1   1 |
            ' 
            Dim matriz As New Matriz4x4

            With matriz
                .M11 = 1 : .M12 = 1 : .M13 = 1 : .M14 = 1
                .M21 = 1 : .M22 = 1 : .M23 = 1 : .M24 = 1
                .M31 = 1 : .M32 = 1 : .M33 = 1 : .M34 = 1
                .M41 = 1 : .M42 = 1 : .M43 = 1 : .M44 = 1
            End With

            Return matriz

        End Function


        Public Shared Function Zeros() As Matriz4x4
            Return New Matriz4x4
        End Function



        Public Overloads Function Compara(ByVal obj As Object) As Boolean
            If TypeOf obj Is Matriz4x4 Then
                Return (Me = DirectCast(obj, Matriz4x4))
            Else
                Return False
            End If
        End Function


        Public Shared Function Multiplica(ByVal left As Matriz4x4, ByVal right As Matriz4x4) As Matriz4x4
            Return left * right
        End Function


        Public Shared Function Multiplica(ByVal matrix As Matriz4x4, ByVal constante As Precisao) As Matriz4x4
            Return matrix * constante
        End Function


        Public Shared Function Multiplica(ByVal constante As Precisao, ByVal matrix As Matriz4x4) As Matriz4x4
            Return constante * matrix
        End Function

        Public Shared Function Soma(ByVal left As Matriz4x4, ByVal right As Matriz4x4) As Matriz4x4
            Return left + right
        End Function

        Public Shared Function Subtrai(ByVal left As Matriz4x4, ByVal right As Matriz4x4) As Matriz4x4
            Return left - right
        End Function

        Public Shared Function Oposta(ByVal matrix As Matriz4x4) As Matriz4x4
            Return -matrix
        End Function


        Public Shared Function CopiaArray(ByVal array As Precisao()) As Matriz4x4
            Dim returnvalue As Matriz4x4 = Identidade()
            returnvalue.Array2Matriz(array, 0)
            Return returnvalue
        End Function



        Public Sub Array2Matriz(ByVal array As Precisao(), ByVal index As Integer)

            With Me
                .M11 = array(index)
                .M12 = array(index + 1)
                .M13 = array(index + 2)
                .M14 = array(index + 3)

                .M21 = array(index + 4)
                .M22 = array(index + 5)
                .M23 = array(index + 6)
                .M24 = array(index + 7)

                .M31 = array(index + 8)
                .M32 = array(index + 9)
                .M33 = array(index + 10)
                .M34 = array(index + 11)

                .M41 = array(index + 12)
                .M42 = array(index + 13)
                .M43 = array(index + 14)
                .M44 = array(index + 15)
            End With

        End Sub





        Public Overloads Overrides Function ToString() As String
            Dim builder As New System.Text.StringBuilder()
            builder.AppendFormat(" | {0} {1} {2} {3} |" & vbLf, Me.M11, Me.M12, Me.M13, Me.M14)
            builder.AppendFormat(" | {0} {1} {2} {3} |" & vbLf, Me.M21, Me.M22, Me.M23, Me.M24)
            builder.AppendFormat(" | {0} {1} {2} {3} |" & vbLf, Me.M31, Me.M32, Me.M33, Me.M34)
            builder.AppendFormat(" | {0} {1} {2} {3} |" & vbLf, Me.M41, Me.M42, Me.M43, Me.M44)
            Return builder.ToString()
        End Function



        'Copia matriz
        Sub CopiaMatriz(ByVal MatSrc As Matriz4x4, ByRef MatDest As Matriz4x4)

            With MatDest
                .M11 = MatSrc.M11 : .M12 = MatSrc.M12 : .M13 = MatSrc.M13 : .M14 = MatSrc.M14
                .M21 = MatSrc.M21 : .M22 = MatSrc.M22 : .M23 = MatSrc.M23 : .M24 = MatSrc.M24
                .M31 = MatSrc.M31 : .M32 = MatSrc.M32 : .M33 = MatSrc.M33 : .M34 = MatSrc.M34
                .M41 = MatSrc.M41 : .M42 = MatSrc.M42 : .M43 = MatSrc.M43 : .M44 = MatSrc.M44
            End With

        End Sub

        'Troca (swap) matrizes
        Sub TrocaMatriz(ByVal MatA As Matriz4x4, ByVal MatB As Matriz4x4)

            Dim MatC As Matriz4x4 = Nothing
            CopiaMatriz(MatA, MatC)
            CopiaMatriz(MatB, MatA)
            CopiaMatriz(MatC, MatB)

        End Sub


        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Matriz4x4) Then Return 0
            Dim matriz As Matriz4x4 = CType(obj, Matriz4x4)
            Return Me.CompareTo(matriz)
        End Function



#End Region
        ' Identidade, Zeros, Clone
        ' Compara, Multiplica, Soma, Subtrai, Oposta
        ' CopiaArray, Array2Matriz, ToString
        ' CopiaMatriz, TrocaMatriz


#End Region



    End Class


End Namespace