﻿'Operações com matrizes 3x3

' Previne erros de conversão de tipo em tempo de execução
Option Strict On

Imports BMath.Funcoes

Imports Precisao = System.Double


Namespace Matriz



    Public Class Matriz3x3
        Implements ICloneable, IDisposable, IComparable


#Region "Objetos"


#Region "Declaracoes"

        Private protM11 As Precisao
        Private protM12 As Precisao
        Private protM13 As Precisao

        Private protM21 As Precisao
        Private protM22 As Precisao
        Private protM23 As Precisao

        Private protM31 As Precisao
        Private protM32 As Precisao
        Private protM33 As Precisao

        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False

#End Region


#Region "Construtores e Destrutores"


        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub

#Region " IDisposable Support "

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub
#End Region





        Public Sub New()
            Me.protM11 = 0.0
            Me.protM12 = 0.0
            Me.protM13 = 0.0

            Me.protM21 = 0.0
            Me.protM22 = 0.0
            Me.protM23 = 0.0

            Me.protM31 = 0.0
            Me.protM32 = 0.0
            Me.protM33 = 0.0
        End Sub


        Public Sub New(ByVal m11 As Precisao, ByVal m12 As Precisao, ByVal m13 As Precisao, _
                       ByVal m21 As Precisao, ByVal m22 As Precisao, ByVal m23 As Precisao, _
                       ByVal m31 As Precisao, ByVal m32 As Precisao, ByVal m33 As Precisao)

            Me.protM11 = m11
            Me.protM12 = m12
            Me.protM13 = m13
            Me.protM21 = m21
            Me.protM22 = m22
            Me.protM23 = m23
            Me.protM31 = m31
            Me.protM32 = m32
            Me.protM33 = m33

        End Sub



#End Region
        'Dispose, Finalize, New


#End Region


#Region "Propriedades"

#Region "Matriz"


        Public Property M11() As Precisao
            Get
                Return Me.protM11
            End Get
            Set(value As Precisao)
                Me.protM11 = value
            End Set
        End Property

        Public Property M12() As Precisao
            Get
                Return Me.protM12
            End Get
            Set(value As Precisao)
                Me.protM12 = value
            End Set
        End Property

        Public Property M13() As Precisao
            Get
                Return Me.protM13
            End Get
            Set(value As Precisao)
                Me.protM13 = value
            End Set
        End Property

        Public Property M21() As Precisao
            Get
                Return Me.protM21
            End Get
            Set(value As Precisao)
                Me.protM21 = value
            End Set
        End Property

        Public Property M22() As Precisao
            Get
                Return Me.protM22
            End Get
            Set(value As Precisao)
                Me.protM22 = value
            End Set
        End Property

        Public Property M23() As Precisao
            Get
                Return Me.protM23
            End Get
            Set(value As Precisao)
                Me.protM23 = value
            End Set
        End Property

        Public Property M31() As Precisao
            Get
                Return Me.protM31
            End Get
            Set(value As Precisao)
                Me.protM31 = value
            End Set
        End Property

        Public Property M32() As Precisao
            Get
                Return Me.protM32
            End Get
            Set(value As Precisao)
                Me.protM32 = value
            End Set
        End Property

        Public Property M33() As Precisao
            Get
                Return Me.protM33
            End Get
            Set(value As Precisao)
                Me.protM33 = value
            End Set
        End Property


#End Region
        'Get - Set
        'M11, M12, M13
        'M21, M22, M23
        'M31, M32, M33



#End Region


#Region "Métodos"


#Region "Operações"


#Region "Multiplicação"


        'Multiplica matrizes
        Public Shared Operator *(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Matriz3x3

            Dim MatrizSaida As New Matriz3x3()
            MatrizSaida.M11 = esquerda.M11 * direita.M11 + esquerda.M12 * direita.M21 + esquerda.M13 * direita.M31
            MatrizSaida.M12 = esquerda.M11 * direita.M12 + esquerda.M12 * direita.M22 + esquerda.M13 * direita.M32
            MatrizSaida.M13 = esquerda.M11 * direita.M13 + esquerda.M12 * direita.M23 + esquerda.M13 * direita.M33

            MatrizSaida.M21 = esquerda.M21 * direita.M11 + esquerda.M22 * direita.M21 + esquerda.M23 * direita.M31
            MatrizSaida.M22 = esquerda.M21 * direita.M12 + esquerda.M22 * direita.M22 + esquerda.M23 * direita.M32
            MatrizSaida.M23 = esquerda.M21 * direita.M13 + esquerda.M22 * direita.M23 + esquerda.M23 * direita.M33

            MatrizSaida.M31 = esquerda.M31 * direita.M11 + esquerda.M32 * direita.M21 + esquerda.M33 * direita.M31
            MatrizSaida.M32 = esquerda.M31 * direita.M12 + esquerda.M32 * direita.M22 + esquerda.M33 * direita.M32
            MatrizSaida.M33 = esquerda.M31 * direita.M13 + esquerda.M32 * direita.M23 + esquerda.M33 * direita.M33

            Return MatrizSaida

        End Operator

        'Matriz por constante
        Public Shared Operator *(ByVal matriz As Matriz3x3, ByVal constante As Precisao) As Matriz3x3

            Dim MatrizSaida As New Matriz3x3()

            MatrizSaida.M11 = matriz.M11 * constante
            MatrizSaida.M12 = matriz.M12 * constante
            MatrizSaida.M13 = matriz.M13 * constante
            MatrizSaida.M21 = matriz.M21 * constante
            MatrizSaida.M22 = matriz.M22 * constante
            MatrizSaida.M23 = matriz.M23 * constante
            MatrizSaida.M31 = matriz.M31 * constante
            MatrizSaida.M32 = matriz.M32 * constante
            MatrizSaida.M33 = matriz.M33 * constante

            Return MatrizSaida

        End Operator


        'Constante por matriz
        Public Shared Operator *(ByVal constante As Precisao, ByVal matriz As Matriz3x3) As Matriz3x3

            Dim MatrizSaida As New Matriz3x3()

            MatrizSaida.M11 = matriz.M11 * constante
            MatrizSaida.M12 = matriz.M12 * constante
            MatrizSaida.M13 = matriz.M13 * constante
            MatrizSaida.M21 = matriz.M21 * constante
            MatrizSaida.M22 = matriz.M22 * constante
            MatrizSaida.M23 = matriz.M23 * constante
            MatrizSaida.M31 = matriz.M31 * constante
            MatrizSaida.M32 = matriz.M32 * constante
            MatrizSaida.M33 = matriz.M33 * constante

            Return MatrizSaida

        End Operator


#End Region



#Region "Soma"


        'Soma matrizes
        Public Shared Operator +(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Matriz3x3

            Dim MatrizSaida As New Matriz3x3()

            MatrizSaida.M11 = esquerda.M11 + direita.M11
            MatrizSaida.M12 = esquerda.M12 + direita.M12
            MatrizSaida.M13 = esquerda.M13 + direita.M13
            MatrizSaida.M21 = esquerda.M21 + direita.M21
            MatrizSaida.M22 = esquerda.M22 + direita.M22
            MatrizSaida.M23 = esquerda.M23 + direita.M23
            MatrizSaida.M31 = esquerda.M31 + direita.M31
            MatrizSaida.M32 = esquerda.M32 + direita.M32
            MatrizSaida.M33 = esquerda.M33 + direita.M33

            Return MatrizSaida

        End Operator

#End Region



#Region "Subtração"


        'Subtrai matrizes
        Public Shared Operator -(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Matriz3x3

            Dim MatrizSaida As New Matriz3x3()

            MatrizSaida.M11 = esquerda.M11 - direita.M11
            MatrizSaida.M12 = esquerda.M12 - direita.M12
            MatrizSaida.M13 = esquerda.M13 - direita.M13
            MatrizSaida.M21 = esquerda.M21 - direita.M21
            MatrizSaida.M22 = esquerda.M22 - direita.M22
            MatrizSaida.M23 = esquerda.M23 - direita.M23
            MatrizSaida.M31 = esquerda.M31 - direita.M31
            MatrizSaida.M32 = esquerda.M32 - direita.M32
            MatrizSaida.M33 = esquerda.M33 - direita.M33

            Return MatrizSaida

        End Operator



        Public Shared Operator -(ByVal matriz As Matriz3x3) As Matriz3x3

            Dim MatrizSaida As New Matriz3x3()

            MatrizSaida.M11 = -matriz.M11
            MatrizSaida.M12 = -matriz.M12
            MatrizSaida.M13 = -matriz.M13
            MatrizSaida.M21 = -matriz.M21
            MatrizSaida.M22 = -matriz.M22
            MatrizSaida.M23 = -matriz.M23
            MatrizSaida.M31 = -matriz.M31
            MatrizSaida.M32 = -matriz.M32
            MatrizSaida.M33 = -matriz.M33

            Return MatrizSaida

        End Operator

#End Region


#Region "Igualdade"

        Public Shared Operator =(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Boolean
            Return (esquerda.M11 = direita.M11 AndAlso _
                    esquerda.M12 = direita.M12 AndAlso _
                    esquerda.M13 = direita.M13 AndAlso _
                    esquerda.M21 = direita.M21 AndAlso _
                    esquerda.M22 = direita.M22 AndAlso _
                    esquerda.M23 = direita.M23 AndAlso _
                    esquerda.M31 = direita.M31 AndAlso _
                    esquerda.M32 = direita.M32 AndAlso _
                    esquerda.M33 = direita.M33)
        End Operator


        Public Shared Operator <>(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Boolean
            Return Not (esquerda = direita)
        End Operator

#End Region


#Region "Divisão"

        'Divisão por constante
        Public Shared Operator /(ByVal esquerda As Matriz3x3, ByVal direita As Precisao) As Matriz3x3
            Return New Matriz3x3(esquerda.M11 / direita, esquerda.M12 / direita, esquerda.M13 / direita, _
                                 esquerda.M21 / direita, esquerda.M22 / direita, esquerda.M23 / direita, _
                                 esquerda.M31 / direita, esquerda.M32 / direita, esquerda.M33 / direita)
        End Operator

        'Divisão por matriz (multiplicação pela inversa)
        Public Shared Operator /(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Matriz3x3
            Dim C As New Matriz3x3
            C = esquerda * direita.Inversa
            Return C
        End Operator

#End Region


#Region "Potenciação"

        'Potência "dot"(matlab). Termo a termo elevado a constante
        Public Shared Operator ^(ByVal esquerda As Matriz3x3, ByVal direita As Precisao) As Matriz3x3
            Dim C As New Matriz3x3(Math.Pow(esquerda.M11, direita), Math.Pow(esquerda.M12, direita), Math.Pow(esquerda.M13, direita), _
                                   Math.Pow(esquerda.M21, direita), Math.Pow(esquerda.M22, direita), Math.Pow(esquerda.M23, direita), _
                                   Math.Pow(esquerda.M31, direita), Math.Pow(esquerda.M32, direita), Math.Pow(esquerda.M33, direita))
            Return C
        End Operator

#End Region


#End Region
        ' *, +, -, =, <>, /, ^



#Region "Funções Membro"



#Region "Cofator"

        Public Function Cofator() As Matriz3x3
            Dim A11 As Precisao = Det2x2(Me.M22, Me.M23, Me.M32, Me.M33)
            Dim A12 As Precisao = -(Det2x2(Me.M21, Me.M23, Me.M31, Me.M33))
            Dim A13 As Precisao = Det2x2(Me.M21, Me.M22, Me.M31, Me.M32)

            Dim A21 As Precisao = -(Det2x2(Me.M12, Me.M13, Me.M32, Me.M33))
            Dim A22 As Precisao = Det2x2(Me.M11, Me.M13, Me.M31, Me.M33)
            Dim A23 As Precisao = -(Det2x2(Me.M11, Me.M12, Me.M31, Me.M32))

            Dim A31 As Precisao = Det2x2(Me.M12, Me.M13, Me.M22, Me.M23)
            Dim A32 As Precisao = -(Det2x2(Me.M11, Me.M13, Me.M21, Me.M23))
            Dim A33 As Precisao = Det2x2(Me.M11, Me.M12, Me.M21, Me.M22)

            Dim MatrizSaida As New Matriz3x3(A11, A12, A13, _
                                             A21, A22, A23, _
                                             A31, A32, A33)

            Return MatrizSaida

        End Function


#End Region


#Region "Determinante"

        Public Function Determinante() As Precisao

            Dim MatrizSaida As Precisao
            MatrizSaida = (Me.M11 * Me.Cofator.M11) + (Me.M12 * Me.Cofator.M12) + (Me.M13 * Me.Cofator.M13)
            Return MatrizSaida

        End Function


#End Region


#Region "Transposta"

        Public Function Transposta() As Matriz3x3
            Return New Matriz3x3(Me.M11, Me.M21, Me.M31, _
                                 Me.M12, Me.M22, Me.M32, _
                                 Me.M13, Me.M23, Me.M33)
        End Function


#End Region


#Region "Adjunta"


        Public Function Adjunta() As Matriz3x3
            Return Me.Cofator.Transposta
        End Function


#End Region


#Region "Inversa"


        Public Function Inversa() As Matriz3x3
            Return Me.Adjunta * (1.0 / Me.Determinante)
        End Function

#End Region





        Public Shared Function Identidade() As Matriz3x3

            Return New Matriz3x3(1, 0, 0, _
                                 0, 1, 0, _
                                 0, 0, 1)
        End Function


        Public Shared Function Zeros() As Matriz3x3
            Return New Matriz3x3
        End Function


        ' Copia a Matriz3x3.
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Matriz3x3(Me.M11, Me.M12, Me.M13, _
                                 Me.M21, Me.M22, Me.M23, _
                                 Me.M31, Me.M32, Me.M33)
        End Function


        Public Overloads Function Compara(ByVal obj As Object) As Boolean
            If TypeOf obj Is Matriz3x3 Then
                Return (Me = DirectCast(obj, Matriz3x3))
            Else
                Return False
            End If
        End Function


        Public Shared Function Multiplica(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Matriz3x3
            Return esquerda * direita
        End Function


        Public Shared Function Multiplica(ByVal matriz As Matriz3x3, ByVal Precisao As Precisao) As Matriz3x3
            Return matriz * Precisao
        End Function


        Public Shared Function Multiplica(ByVal Precisao As Precisao, ByVal matriz As Matriz3x3) As Matriz3x3
            Return Precisao * matriz
        End Function


        Public Shared Function Soma(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Matriz3x3
            Return esquerda + direita
        End Function


        Public Shared Function Subtrai(ByVal esquerda As Matriz3x3, ByVal direita As Matriz3x3) As Matriz3x3
            Return esquerda - direita
        End Function


        Public Shared Function Oposta(ByVal matriz As Matriz3x3) As Matriz3x3
            Return -matriz
        End Function


        Public Sub CopiaArray(ByVal array As Precisao(), ByVal index As Integer)

            With Me
                .M11 = array(index)
                .M12 = array(index + 1)
                .M13 = array(index + 2)

                .M21 = array(index + 3)
                .M22 = array(index + 4)
                .M23 = array(index + 5)

                .M31 = array(index + 6)
                .M32 = array(index + 7)
                .M33 = array(index + 8)
            End With

        End Sub



        Public Shared Function Array2Matriz(ByVal array As Precisao()) As Matriz3x3

            Dim MatrizSaida As Matriz3x3 = Identidade()

            MatrizSaida.CopiaArray(array, 0)

            Return MatrizSaida

        End Function


        Public Overloads Overrides Function ToString() As String
            Dim builder As New System.Text.StringBuilder()
            builder.AppendFormat(" | {0} {1} {2} |" & vbLf, Me.M11, Me.M12, Me.M13)
            builder.AppendFormat(" | {0} {1} {2} |" & vbLf, Me.M21, Me.M22, Me.M23)
            builder.AppendFormat(" | {0} {1} {2} |", Me.M31, Me.M32, Me.M33)
            Return builder.ToString()
        End Function


        'Copia matrix
        Sub CopiaMatriz(ByVal MatSrc As Matriz3x3, ByRef MatDest As Matriz3x3)

            With MatDest
                .M11 = MatSrc.M11 : .M12 = MatSrc.M12 : .M13 = MatSrc.M13
                .M21 = MatSrc.M21 : .M22 = MatSrc.M22 : .M23 = MatSrc.M23
                .M31 = MatSrc.M31 : .M32 = MatSrc.M32 : .M33 = MatSrc.M33
            End With

        End Sub


        'Troca (swap) matrizes
        Sub TrocaMatriz(ByVal MatA As Matriz3x3, ByVal MatB As Matriz3x3)

            Dim MatC As Matriz3x3 = Nothing
            CopiaMatriz(MatA, MatC)
            CopiaMatriz(MatB, MatA)
            CopiaMatriz(MatC, MatB)

        End Sub


        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Matriz3x3) Then Return 0
            Dim matriz As Matriz3x3 = CType(obj, Matriz3x3)
            Return Me.CompareTo(matriz)
        End Function


#End Region
        ' Cofator, Determinante, Transposta, Adjunta, Inversa 
        ' Identidade, Zeros, Clone
        ' Compara, Multiplica, Soma, Subtrai, Oposta
        ' CopiaArray, Array2Matriz, ToString
        ' CopiaMatriz, TrocaMatriz


#End Region



    End Class




End Namespace



