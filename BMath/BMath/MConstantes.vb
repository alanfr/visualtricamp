﻿Option Strict On

Namespace Constantes

    Public Module Constantes


#Region "Constantes"

        'Do LIBBOOST - C++ - 1.60
        Public Const HALF = 0.5
        Public Const THIRD = 0.33333333333333331
        Public Const TWOTHIRDS = 0.66666666666666663
        Public Const TWO_THIRDS = 0.66666666666666663
        Public Const THREE_QUARTERS = 0.75
        Public Const ROOT_TWO = 1.4142135623730951
        Public Const ROOT_THREE = 1.7320508075688772
        Public Const HALF_ROOT_TWO = 0.70710678118654757
        Public Const LN_TWO = 0.69314718055994529
        Public Const LN_LN_TWO = -0.36651292058166435
        Public Const ROOT_LN_FOUR = 1.1774100225154747
        Public Const ONE_DIV_ROOT_TWO = 0.70710678118654757
        Public Const PI = 3.1415926535897931
        Public Const HALF_PI = 1.5707963267948966
        Public Const THIRD_PI = 1.0471975511965979
        Public Const SIXTH_PI = 0.52359877559829893
        Public Const TWO_PI = 6.2831853071795862
        Public Const TWO_THIRDS_PI = 2.0943951023931957
        Public Const THREE_QUARTERS_PI = 2.3561944901923448
        Public Const QUARTER_PI = 0.78539816339744828
        Public Const FOUR_THIRDS_PI = 4.1887902047863914
        Public Const ONE_DIV_TWO_PI = 0.70710678118654757
        Public Const ONE_DIV_ROOT_TWO_PI = 0.3989422804014327
        Public Const ROOT_PI = 1.7724538509055161
        Public Const ROOT_HALF_PI = 1.2533141373155003
        Public Const ROOT_TWO_PI = 2.5066282746310007
        Public Const LOG_ROOT_TWO_PI = 0.91893853320467278
        Public Const ONE_DIV_ROOT_PI = 0.56418958354775628
        Public Const ROOT_ONE_DIV_PI = 0.56418958354775628
        Public Const PI_MINUS_THREE = 0.14159265358979323
        Public Const FOUR_MINUS_PI = 0.85840734641020677
        Public Const POW23_FOUR_MINUS_PI = 0.79531676737159751
        Public Const PI_POW_E = 22.459157718361045
        Public Const PI_SQR = 9.869604401089358
        Public Const PI_SQR_DIV_SIX = 1.6449340668482264
        Public Const PI_CUBED = 31.00627668029982
        Public Const EXP_MINUS_HALF = 0.60653065971263342
        Public Const E_POW_PI = 23.14069263277927
        Public Const ROOT_E = 1.6487212707001282
        Public Const LOG10_E = 0.43429448190325182
        Public Const ONE_DIV_LOG10_E = 2.3025850929940459
        Public Const LN_TEN = 2.3025850929940459

        Public Const DEGREE = 0.017453292519943295
        Public Const RADIAN = 57.295779513082323

        Public Const SIN_ONE = 0.8414709848078965
        Public Const COS_ONE = 0.54030230586813977
        Public Const SINH_ONE = 1.1752011936438014
        Public Const COSH_ONE = 1.5430806348152437
        Public Const PHI = 1.6180339887498949
        Public Const LN_PHI = 0.48121182505960347
        Public Const ONE_DIV_LN_PHI = 2.0780869212350277
        Public Const EULER = 0.57721566490153287

        Public Const ONE_DIV_EULER = 1.7324547146006335
        Public Const EULER_SQR = 0.33317792380771866
        Public Const ZETA_TWO = 1.6449340668482264
        Public Const ZETA_THREE = 1.2020569031595942
        Public Const CATALAN = 0.915965594177219
        Public Const GLAISHER = 1.2824271291006226
        Public Const KHINCHIN = 2.6854520010653062
        Public Const EXTREME_VALUE_SKEWNESS = 1.1395470994046486
        Public Const RAYLEIGH_SKEWNESS = 0.63111065781893716
        Public Const RAYLEIGH_KURTOSIS = 3.245089300687638
        Public Const RAYLEIGH_KURTOSIS_EXCESS = 0.24508930068763807
        Public Const TWO_DIV_PI = 0.63661977236758138
        Public Const ROOT_TWO_DIV_PI = 0.79788456080286541
        Public Const NEPER = 2.7182818284590451

        Public Const RADIANO_2_GRAU = DEGREE
        Public Const GRAU_2_RADIANO = RADIAN

        Public Const ZERO = 0.00001
        Public Const INFINITO = 1.0E+15
        Public Const COORDZERO = ZERO

#End Region


    End Module


End Namespace

