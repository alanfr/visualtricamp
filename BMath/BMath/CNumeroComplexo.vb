﻿' Operações com números complexos

' Previne erros de conversão de tipo em tempo de execução
Option Strict On

Imports System.Numerics
Imports Precisao = System.Double

Namespace Complexo


#Region "NumeroComplexo"

    Public Class NumeroComplexo

        'Para copia e comparação de complexos
        Implements ICloneable, IComparable, IDisposable


#Region "Declaracoes"

        ' Componentes real e imaginária.
        Private Real As Precisao
        Private Imaginario As Precisao

        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False

#End Region


#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub

#Region " IDisposable Support "

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub
#End Region


        ' Cria um numero complexo.
        Public Sub New()
            Me.Real = 0.0
            Me.Imaginario = 0.0
        End Sub

        ' Cria um numero complexo.
        Public Sub New(ByVal real As Precisao, ByVal imaginario As Precisao)
            Me.Real = real
            Me.Imaginario = imaginario
        End Sub



#End Region


#Region "Operacoes"


#Region "Reciproco"

        'Existe um único w tal que z*w=(1,0) -> w = reciproco de z
        Public Shared Function Reciproco(ByVal a As NumeroComplexo) As NumeroComplexo
            Dim divisor As Precisao

            divisor = a.Real * a.Real + a.Imaginario * a.Imaginario

            If (divisor = 0.0#) Then
                Throw New DivideByZeroException
            End If

            Return New NumeroComplexo(a.Real / divisor, -a.Imaginario / divisor)
        End Function


#End Region


#Region "Soma"

        '' Adiciona um numero complexo ao atual.
        Public Function Soma(ByVal complexo As NumeroComplexo) As NumeroComplexo
            Return New NumeroComplexo(Me.Real + complexo.Real, Me.Imaginario + complexo.Imaginario)
        End Function

        ' Adiciona um numero real ao complexo atual.
        Public Function Soma(ByVal real As Precisao) As NumeroComplexo
            Return New NumeroComplexo(Me.Real + real, Me.Imaginario)
        End Function


#End Region


#Region "Subtração"

        ' Subtrai um numero complexo do atual.
        Public Function Subtrai(ByVal complexo As NumeroComplexo) As NumeroComplexo
            Return New NumeroComplexo(Me.Real - complexo.Real, Me.Imaginario - complexo.Imaginario)
        End Function

        ' Subtrai um numero real do complexo atual.
        Public Function Subtrai(ByVal real As Precisao) As NumeroComplexo
            Return New NumeroComplexo(Me.Real - real, Me.Imaginario)
        End Function


#End Region


#Region "Multiplicação"


        ' Multiplica um complexo pelo atual.
        Public Function Multiplica(ByVal complexo As NumeroComplexo) As NumeroComplexo
            Dim x, y, u, v As Precisao
            x = Me.Real : y = Me.Imaginario
            u = complexo.Real : v = complexo.Imaginario
            Return New NumeroComplexo(x * u - y * v, x * v + y * u)
        End Function

        ' Multiplica um numero real ao complexo atual.
        Public Function Multiplica(ByVal real As Precisao) As NumeroComplexo
            Return New NumeroComplexo(Me.Real * real, Me.Imaginario * real)
        End Function



#End Region


#Region "Divisão"

        ' Divide o complexo por outro complexo.
        Public Function Divide(ByVal complexo As NumeroComplexo) As NumeroComplexo
            Dim x, y, u, v As Precisao
            x = Me.Real : y = Me.Imaginario
            u = complexo.Real : v = complexo.Imaginario
            Dim Soma As Precisao = u * u + v * v
            Return New NumeroComplexo((x * u + y * v) / Soma, (y * u - x * v) / Soma)
        End Function

        ' Divide o complexo atual por um numero real.
        Public Function Divide(ByVal real As Precisao) As NumeroComplexo
            Return New NumeroComplexo(Me.Real / real, Me.Imaginario / real)
        End Function


#End Region



#End Region





#Region "Operadores"



#Region "Multiplicação"

        'Mutiplica dois numeros complexos
        Public Shared Operator *(ByVal a As NumeroComplexo, ByVal b As NumeroComplexo) As NumeroComplexo
            Return New NumeroComplexo(a.Real * b.Real - a.Imaginario * b.Imaginario, a.Real * b.Imaginario + a.Imaginario * b.Real)
        End Operator

        'Multiplica real por complexo
        Public Shared Operator *(ByVal real As Precisao, ByVal complexo As NumeroComplexo) As NumeroComplexo
            Return New NumeroComplexo(real * complexo.Real, real * complexo.Imaginario)
        End Operator

        'Multiplica complexo por real
        Public Shared Operator *(ByVal complexo As NumeroComplexo, ByVal real As Precisao) As NumeroComplexo
            Return New NumeroComplexo(real * complexo.Real, real * complexo.Imaginario)
        End Operator


#End Region


#Region "Soma"

        ' Sobrecarga do operador +
        Public Shared Operator +(ByVal a As NumeroComplexo, ByVal b As NumeroComplexo) As NumeroComplexo
            Return New NumeroComplexo(a.Real + b.Real, a.Imaginario + b.Imaginario)
        End Operator

        ' Sobrecarga do operador +
        Public Shared Operator +(ByVal real As Precisao, ByVal b As NumeroComplexo) As NumeroComplexo
            Return New NumeroComplexo(real + b.Real, b.Imaginario)
        End Operator

        ' Sobrecarga do operador +
        Public Shared Operator +(ByVal b As NumeroComplexo, ByVal real As Precisao) As NumeroComplexo
            Return New NumeroComplexo(b.Real + real, b.Imaginario)
        End Operator



#End Region


#Region "Subtração"

        'Sobrecarrega operador
        Public Shared Operator -(ByVal a As NumeroComplexo, ByVal b As NumeroComplexo) As NumeroComplexo
            Return New NumeroComplexo(a.Real - b.Real, a.Imaginario - b.Imaginario)
        End Operator

        'Sobrecarrega operador
        Public Shared Operator -(ByVal real As Precisao, ByVal b As NumeroComplexo) As NumeroComplexo
            Return New NumeroComplexo(real - b.Real, b.Imaginario)
        End Operator

        'Sobrecarrega operador
        Public Shared Operator -(ByVal b As NumeroComplexo, ByVal real As Precisao) As NumeroComplexo
            Return New NumeroComplexo(b.Real - real, b.Imaginario)
        End Operator


#End Region


#Region "Igualdade"

        Public Shared Operator =(ByVal esquerda As NumeroComplexo, ByVal direita As NumeroComplexo) As Boolean
            Return (esquerda.Real = direita.Real AndAlso _
                    esquerda.Imaginario = direita.Imaginario)
        End Operator

#End Region



#Region "Diferença"

        Public Shared Operator <>(ByVal esquerda As NumeroComplexo, ByVal direita As NumeroComplexo) As Boolean
            Return Not (esquerda = direita)
        End Operator

#End Region


#Region "Divisão"

        'http://www.clarku.edu/~djoyce/complex/mult.html
        Public Shared Operator /(ByVal a As NumeroComplexo, ByVal b As NumeroComplexo) As NumeroComplexo
            Return a * Reciproco(b)
        End Operator

        Public Shared Operator /(ByVal real As Precisao, ByVal b As NumeroComplexo) As NumeroComplexo
            Dim C As New NumeroComplexo(real, 0.0)

            Return C / b
        End Operator

        Public Shared Operator /(ByVal b As NumeroComplexo, ByVal real As Precisao) As NumeroComplexo
            Dim C As New NumeroComplexo(real, 0.0)

            Return b / C
        End Operator

#End Region



#Region "Potenciação"

        'Potência entre dois números complexos
        Public Shared Operator ^(ByVal a As NumeroComplexo, ByVal b As NumeroComplexo) As NumeroComplexo
            Dim a1 As New Complex(a.Real, a.Imaginario)
            Dim b1 As New Complex(b.Real, b.Imaginario)
            Dim c As New Complex
            Dim d As New NumeroComplexo

            c = Complex.Pow(a1, b1)
            d.Real = c.Real
            d.Imaginario = c.Imaginary

            Return d
        End Operator


        'Potência entre dois números complexos
        Public Shared Operator ^(ByVal real As Precisao, ByVal b As NumeroComplexo) As NumeroComplexo
            Dim b1 As New Complex(b.Real, b.Imaginario)
            Dim c As New Complex
            Dim d As New NumeroComplexo

            c = Complex.Pow(real, b1)
            d.Real = c.Real
            d.Imaginario = c.Imaginary

            Return d
        End Operator

        'Potência entre dois números complexos
        Public Shared Operator ^(ByVal b As NumeroComplexo, ByVal real As Precisao) As NumeroComplexo
            Dim b1 As New Complex(b.Real, b.Imaginario)
            Dim c As New Complex
            Dim d As New NumeroComplexo

            c = Complex.Pow(b1, real)
            d.Real = c.Real
            d.Imaginario = c.Imaginary

            Return d
        End Operator

#End Region





#End Region
        ' *, +, -, =, <>, /, ^






#Region "Testes"

        ' Testa se dois números complexos são iguais.
        Public Overloads Shared Function Compara(ByVal objA As Object, ByVal objB As Object) As Boolean
            If Not (TypeOf objA Is NumeroComplexo) Or Not (TypeOf objB Is NumeroComplexo) Then Return False
            Dim ComplexA As NumeroComplexo = CType(objA, NumeroComplexo)
            Dim ComplexB As NumeroComplexo = CType(objB, NumeroComplexo)
            Return (ComplexA.Real = ComplexB.Real And ComplexA.Imaginario = ComplexB.Imaginario)
        End Function


        ' Testa se dois números complexos são iguais.
        Public Overloads Function Compara(ByVal obj As Object) As Boolean
            If Not TypeOf obj Is NumeroComplexo Then Return False
            Dim comp As NumeroComplexo = CType(obj, NumeroComplexo)
            Return (Me.Real = comp.Real And Me.Imaginario = comp.Imaginario)
        End Function



#End Region



#Region "Métodos"


        'Definição de métodos.

        Public Function Modulo() As Precisao
            Return Math.Sqrt(Me.Real ^ 2 + Me.Imaginario ^ 2)
        End Function


        Public Function ModuloQuadrado() As Precisao
            Return Me.Real ^ 2 + Me.Imaginario ^ 2
        End Function


        Public Function Argumento() As Precisao
            Return Math.Atan2(Me.Imaginario, Me.Real)
        End Function

        Public Function Conjugado() As NumeroComplexo
            Return New NumeroComplexo(Me.Real, -Me.Imaginario)
        End Function



        ' Converte para string
        Public Overrides Function ToString() As String
            Return Me.Real.ToString() & ", " & Me.Imaginario.ToString() & "i"
        End Function


        ' Copia um numero complexo.
        Public Function Clona() As Object Implements System.ICloneable.Clone
            Return New NumeroComplexo(Me.Real, Me.Imaginario)
        End Function


        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is NumeroComplexo) Then Return 0
            Dim Compare As NumeroComplexo = CType(obj, NumeroComplexo)
            Return Me.Modulo.CompareTo(Compare.Modulo)
        End Function

#End Region


    End Class



#End Region


End Namespace


