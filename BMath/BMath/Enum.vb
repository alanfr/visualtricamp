﻿Option Strict On

Namespace Enumera

#Region "Enumeração"

    Public Enum TiposDeSolucao
        UNICA_SOLUCAO
        SEM_SOLUCAO
        INFINITAS_SOLUCOES
    End Enum


    Public Enum Result3D
        UNICO_RESULTADO
        NAO_EXISTE
        INFINITOS_RESULTADOS
    End Enum


#End Region


End Namespace






