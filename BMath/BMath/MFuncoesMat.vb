﻿'Funções matemáticas básicas

' Previne erros de conversão de tipo em tempo de execução
Option Strict On

'Namespaces utilizados
Imports BMath.Enumera
Imports BMath.Constantes
Imports BMath.Matriz


'Para padronizar a precisao utilizada
Imports Precisao = System.Double



Namespace Funcoes

    Public Module FuncoesMat


#Region "Funcoes"

#Region "Entrada e saida em graus"

        'G=Grau Funcoes que possuem entrada e saida em graus
        Public Function AcosG(ByVal AnguloGraus As Precisao) As Precisao
            Return (Math.Acos(AnguloGraus) * GRAU_2_RADIANO)
        End Function

        Public Function AsenG(ByVal AnguloGraus As Precisao) As Precisao
            Return (Math.Asin(AnguloGraus) * GRAU_2_RADIANO)
        End Function

        Public Function AtanG(ByVal AnguloGraus As Precisao) As Precisao
            Return (Math.Atan(AnguloGraus) * GRAU_2_RADIANO)
        End Function

        Public Function Atan2G(ByVal y As Precisao, ByVal x As Precisao) As Precisao
            Return (Math.Atan2(y, x))
        End Function

        Public Function CosG(ByVal AnguloGraus As Precisao) As Precisao
            Return (Math.Cos(AnguloGraus * RADIANO_2_GRAU))
        End Function

        Public Function SenG(ByVal AnguloGraus As Precisao) As Precisao
            Return (Math.Sin(AnguloGraus * RADIANO_2_GRAU))
        End Function

        Public Function TanG(ByVal AnguloGraus As Precisao) As Precisao
            Return (Math.Tan(AnguloGraus * RADIANO_2_GRAU))
        End Function

#End Region


#Region "Entrada e saida em radianos"


        'Funcoes que possuem entrada e saida em radianos
        Public Function Acos(ByVal d As Precisao) As Precisao
            Return Math.Acos(d)
        End Function

        Public Function Asen(ByVal d As Precisao) As Precisao
            Return (Math.Asin(d))
        End Function

        Public Function Atan(ByVal d As Precisao) As Precisao
            Return (Math.Atan(d))
        End Function

        Public Function Atan2(ByVal y As Precisao, ByVal x As Precisao) As Precisao
            Return (Math.Atan2(y, x))
        End Function

        Public Function Cos(ByVal d As Precisao) As Precisao
            Return (Math.Cos(d))
        End Function

        Public Function Cosh(ByVal value As Precisao) As Precisao
            Return (Math.Cosh(value))
        End Function

        Public Function Sen(ByVal a As Precisao) As Precisao
            Return (Math.Sin(a))
        End Function

        Public Function Senh(ByVal value As Precisao) As Precisao
            Return (Math.Sinh(value))
        End Function

        Public Function Tan(ByVal a As Precisao) As Precisao
            Return (Math.Tan(a))
        End Function

        Public Function Tanh(ByVal value As Precisao) As Precisao
            Return (Math.Tanh(value))
        End Function

#End Region



#Region "Funcoes Matemáticas"


        'Funções matemáticas
        Public Function Abs(ByVal d As Precisao) As Precisao
            Return Math.Abs(d)
        End Function

        Public Function ArredondaCima(ByVal a As Precisao) As Precisao
            Return CLng((Math.Ceiling(a)))
        End Function

        Public Function Exp(ByVal d As Precisao) As Precisao
            Return (Math.Exp(d))
        End Function

        Public Function ArredondaBaixo(ByVal d As Precisao) As Precisao
            Return CLng((Math.Floor(d)))
        End Function

        Public Function IEEERemainder(ByVal x As Precisao, ByVal y As Precisao) As Precisao
            Return (Math.IEEERemainder(x, y))
        End Function

        Public Function Log(ByVal d As Precisao) As Precisao
            Return (Math.Log(d))
        End Function

        Public Function Log(ByVal a As Precisao, ByVal b As Precisao) As Precisao
            Return (Math.Log(a, b))
        End Function

        Public Function Log10(ByVal d As Precisao) As Precisao
            Return (Math.Log10(d))
        End Function

        Public Function Pot(ByVal a As Precisao, ByVal b As Precisao) As Precisao
            Return (Math.Pow(a, b))
        End Function

        Public Function Arredondar(ByVal a As Precisao) As Precisao
            Return (Math.Round(a))
        End Function

        Public Function Arredondar(ByVal valor As Precisao, ByVal digitos As Integer) As Precisao
            Return (Math.Round(valor, digitos))
        End Function

        Public Function Arredondar(ByVal valor As Precisao, ByVal modo As MidpointRounding) As Precisao
            Return (Math.Round(valor, modo))
        End Function

        Public Function Arredondar(ByVal valor As Precisao, ByVal digitos As Precisao, ByVal modo As MidpointRounding) As Precisao
            Return (Math.Round(valor, CInt(digitos), modo))
        End Function

        Public Function Raiz(ByVal d As Precisao) As Precisao
            Return (Math.Sqrt(d))
        End Function

        Public Function Truncar(ByVal d As Precisao) As Precisao
            Return (Math.Truncate(d))
        End Function


        Public Function Max(ByVal val1 As Precisao, ByVal val2 As Precisao) As Precisao
            If val1 > val2 Then
                Return val1
            End If
            If Precisao.IsNaN(val1) Then
                Return val1
            End If
            Return val2
        End Function

        Public Function Min(ByVal val1 As Precisao, ByVal val2 As Precisao) As Precisao
            If val1 < val2 Then
                Return val1
            End If
            If Precisao.IsNaN(val1) Then
                Return val1
            End If
            Return val2
        End Function

        Public Function IgualarPrecisao(ByVal a As Precisao, ByVal b As Precisao, ByVal tolerancia As Precisao) As Boolean
            If Math.Abs(b - a) <= tolerancia Then
                Return True
            End If
            Return False
        End Function

        Public Function IgualarPrecisao(ByVal a As Precisao, ByVal b As Precisao) As Boolean
            Return IgualarPrecisao(a, b, 0.00001F)
        End Function

        Public Function Grau2Radiano(ByVal degrees As Precisao) As Precisao
            Return degrees * RADIANO_2_GRAU
        End Function

        Public Function Radiano2Grau(ByVal radians As Precisao) As Precisao
            Return radians * GRAU_2_RADIANO
        End Function


#End Region



#Region "Sistemas lineares"


        'Resolve 2 equações lineares na forma
        'Ax + By = C
        'Pela regra de Kramer
        Public Function Linear2(ByVal a1 As Precisao, ByVal b1 As Precisao, ByVal c1 As Precisao, _
                                ByVal a2 As Precisao, ByVal b2 As Precisao, ByVal c2 As Precisao, _
                                ByRef x As Precisao, ByRef y As Precisao) As TiposDeSolucao

            Dim M, XM, YM As Matriz2x2
            M = New Matriz2x2(a1, b1, a2, b2)
            XM = New Matriz2x2(c1, b1, c2, b2)
            YM = New Matriz2x2(a1, c1, a2, c2)

            If M.Determinante = 0 Then
                If XM.Determinante = 0 And YM.Determinante = 0 Then
                    Return TiposDeSolucao.INFINITAS_SOLUCOES
                    Exit Function
                Else
                    Return TiposDeSolucao.SEM_SOLUCAO
                    Exit Function
                End If
            End If

            x = XM.Determinante / M.Determinante
            y = YM.Determinante / M.Determinante
            Return TiposDeSolucao.SEM_SOLUCAO

        End Function

        'Resolve 3 equações lineares na forma
        'ax+by+cz=d
        Public Function Linear3(ByVal a1 As Precisao, ByVal b1 As Precisao, ByVal c1 As Precisao, ByVal d1 As Precisao, _
                                ByVal a2 As Precisao, ByVal b2 As Precisao, ByVal c2 As Precisao, ByVal d2 As Precisao, _
                                ByVal a3 As Precisao, ByVal b3 As Precisao, ByVal c3 As Precisao, ByVal d3 As Precisao, _
                                ByRef x As Precisao, ByRef y As Precisao, ByRef z As Precisao) As TiposDeSolucao

            Dim M, XM, YM, ZM As Matriz3x3
            M = New Matriz3x3(a1, b1, c1, a2, b2, c2, a3, b3, c3)
            XM = New Matriz3x3(d1, b1, c1, d2, b2, c2, d3, b3, c3)
            YM = New Matriz3x3(a1, d1, c1, a2, d2, c2, a3, d3, c3)
            ZM = New Matriz3x3(a1, b1, d1, a2, b2, d2, a3, b3, d3)

            If M.Determinante = 0 Then
                If XM.Determinante = 0 And YM.Determinante = 0 And ZM.Determinante = 0 Then
                    Return TiposDeSolucao.INFINITAS_SOLUCOES
                    Exit Function
                Else
                    Return TiposDeSolucao.SEM_SOLUCAO
                    Exit Function
                End If
            End If

            x = XM.Determinante / M.Determinante
            y = YM.Determinante / M.Determinante
            z = ZM.Determinante / M.Determinante
            Return TiposDeSolucao.SEM_SOLUCAO

        End Function



        'Resolve 4 equações lineares na forma
        'ax+by+cz+dw=k
        Public Function Linear4(ByVal a1 As Precisao, ByVal b1 As Precisao, ByVal c1 As Precisao, ByVal d1 As Precisao, ByVal k1 As Precisao, _
                                ByVal a2 As Precisao, ByVal b2 As Precisao, ByVal c2 As Precisao, ByVal d2 As Precisao, ByVal k2 As Precisao, _
                                ByVal a3 As Precisao, ByVal b3 As Precisao, ByVal c3 As Precisao, ByVal d3 As Precisao, ByVal k3 As Precisao, _
                                ByVal a4 As Precisao, ByVal b4 As Precisao, ByVal c4 As Precisao, ByVal d4 As Precisao, ByVal k4 As Precisao, _
                                ByRef x As Precisao, ByRef y As Precisao, ByRef z As Precisao, ByRef w As Precisao) As TiposDeSolucao

            Dim M, XM, YM, ZM, WM As Matriz4x4
            M = New Matriz4x4(a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3, a4, b4, c4, d4)
            XM = New Matriz4x4(k1, b1, c1, d1, k2, b2, c2, d2, k3, b3, c3, d3, k4, b4, c4, d4)
            YM = New Matriz4x4(a1, k1, c1, d1, a2, k2, c2, d2, a3, k3, c3, d3, a4, k4, c4, d4)
            ZM = New Matriz4x4(a1, b1, k1, d1, a2, b2, k2, d2, a3, b3, k3, d3, a4, b4, k4, d4)
            WM = New Matriz4x4(a1, b1, c1, k1, a2, b2, c2, k2, a3, b3, c3, k3, a4, b4, c4, k4)

            If M.Determinante = 0 Then
                If XM.Determinante = 0 And YM.Determinante = 0 And ZM.Determinante = 0 And WM.Determinante = 0 Then
                    Return TiposDeSolucao.INFINITAS_SOLUCOES
                    Exit Function
                Else
                    Return TiposDeSolucao.SEM_SOLUCAO
                    Exit Function
                End If
            End If

            x = XM.Determinante / M.Determinante
            y = YM.Determinante / M.Determinante
            z = ZM.Determinante / M.Determinante
            w = WM.Determinante / M.Determinante

            Return TiposDeSolucao.SEM_SOLUCAO

        End Function


#End Region



#Region "Determinante"



        Public Function Det2x2(ByVal m11 As Precisao, ByVal m12 As Precisao, _
                               ByVal m21 As Precisao, ByVal m22 As Precisao) As Precisao

            Dim saida As Precisao = (m11 * m22) - (m12 * m21)
            Return saida

        End Function

        'Determinante por Laplace
        Public Function Det3x3(ByVal m11 As Precisao, ByVal m12 As Precisao, ByVal m13 As Precisao, _
                               ByVal m21 As Precisao, ByVal m22 As Precisao, ByVal m23 As Precisao, _
                               ByVal m31 As Precisao, ByVal m32 As Precisao, ByVal m33 As Precisao) As Precisao

            Dim saida As Precisao = (m11 * Det2x2(m22, m23, m32, m33)) + _
                                    (-m12 * Det2x2(m21, m23, m31, m33)) + _
                                    (m13 * Det2x2(m21, m22, m31, m32))
            Return saida

        End Function

        'Determinante por Laplace
        Public Function Det4x4(ByVal m11 As Precisao, ByVal m12 As Precisao, ByVal m13 As Precisao, ByVal m14 As Precisao, _
                               ByVal m21 As Precisao, ByVal m22 As Precisao, ByVal m23 As Precisao, ByVal m24 As Precisao, _
                               ByVal m31 As Precisao, ByVal m32 As Precisao, ByVal m33 As Precisao, ByVal m34 As Precisao, _
                               ByVal m41 As Precisao, ByVal m42 As Precisao, ByVal m43 As Precisao, ByVal m44 As Precisao) As Precisao

            Dim saida As Precisao = (m11 * Det3x3(m22, m23, m24, m32, m33, m34, m42, m43, m44)) + _
                                    (-m12 * Det3x3(m21, m23, m24, m31, m33, m34, m41, m43, m44)) + _
                                    (m13 * Det3x3(m21, m22, m24, m31, m32, m34, m41, m42, m44)) + _
                                    (-m14 * Det3x3(m21, m22, m23, m31, m32, m33, m41, m42, m43))
            Return saida

        End Function


#End Region




#End Region
        ' AcosG, AsenG, AtanG, Atan2G
        ' CosG, SinG, TanG
        ' Abs, Acos, Asen, Atan, Atan2
        ' ArredondaCima, Cos, Cosh, Exp, ArredondaBaixo
        ' IEEERemainder, Log, Log10, Pot 
        ' Arredonda, Sen, Senh, Raiz, Tan, Tanh
        ' Truncar, Max, Min, IgualarPrecisao
        ' Grau2Radiano, Radiano2Grau
        ' Linear2, Linear3, Linear4





    End Module


End Namespace
