﻿
Imports BMath.Matriz
Imports BMath.Constantes
Imports BMath.Funcoes

Imports BGeom.Transformacoes


Namespace Casca3D



    Public Class Toroide3D

        Implements IDisposable ', ICloneable, IComparable

        Private pMT As Matriz4x4
        Private pCoord(,) As Double
        Private pElementos(,) As Double
        Private pFronteira() As Double
        Private pNN, pNE, pNNF As Integer
        Private pSENTIDO As Integer
        Private pNome As Integer
        Private pstrNome As String
        Private pTipoELEM As Integer


        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False



#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub



        Public Sub New()
            With Me
                .MT = Matriz4x4.Zeros
                .Coord = Nothing
                .Elementos = Nothing
                .Fronteira = Nothing
                .NN = 0
                .NE = 0
                .NNF = 0
                .SENTIDO = 0
                .Nome = 0
                .TipoELEM = 0
                .Nomestr = ""
            End With

        End Sub


#End Region




#Region "Propriedades"


        Public Property MT() As Matriz4x4
            Get
                Return Me.pMT
            End Get
            Set(value As Matriz4x4)
                Me.pMT = value
            End Set
        End Property

        Public Property Coord() As Double(,)
            Get
                Return Me.pCoord
            End Get
            Set(value As Double(,))
                Me.pCoord = value
            End Set
        End Property


        Public Property Elementos() As Double(,)
            Get
                Return Me.pElementos
            End Get
            Set(value As Double(,))
                Me.pElementos = value
            End Set
        End Property


        Public Property Fronteira() As Double()
            Get
                Return Me.pFronteira
            End Get
            Set(value As Double())
                Me.pFronteira = value
            End Set
        End Property

        Public Property NN() As Double
            Get
                Return Me.pNN
            End Get
            Set(value As Double)
                Me.pNN = value
            End Set
        End Property

        Public Property NE() As Double
            Get
                Return Me.pNE
            End Get
            Set(value As Double)
                Me.pNE = value
            End Set
        End Property

        Public Property NNF() As Double
            Get
                Return Me.pNNF
            End Get
            Set(value As Double)
                Me.pNNF = value
            End Set
        End Property

        Public Property SENTIDO() As Integer
            Get
                Return Me.pSENTIDO
            End Get
            Set(value As Integer)
                Me.pSENTIDO = value
            End Set
        End Property


        Public Property Nome() As Integer
            Get
                Return Me.pNome
            End Get
            Set(value As Integer)
                Me.pNome = value
            End Set
        End Property


        Public Property Nomestr() As String
            Get
                Return Me.pstrNome
            End Get
            Set(value As String)
                Me.pstrNome = value
            End Set
        End Property


        Public Property TipoELEM() As Integer
            Get
                Return Me.pTipoELEM
            End Get
            Set(value As Integer)
                Me.pTipoELEM = value
            End Set
        End Property



#End Region




#Region "Esvazia Casca 3D"


        Public Sub EsvaziaCasca3D()
            With Me
                .MT = Matriz4x4.Zeros
                .Coord = Nothing
                .Elementos = Nothing
                .Fronteira = Nothing
                .NN = 0
                .NE = 0
                .NNF = 0
                .SENTIDO = 0
                .Nome = 0
                .TipoELEM = 0
                .Nomestr = ""
            End With
        End Sub


#End Region



        '        Interface dToroide
        '  MODULE PROCEDURE dquadToroideBASICO
        '  MODULE PROCEDURE dquadARCOTOROIDE
        '  MODULE PROCEDURE dquadARCOTOROIDEPG
        '  MODULE PROCEDURE dquadSETOR_ARCOTOROIDE
        '  MODULE PROCEDURE dquadSETOR_ARCOTOROIDEPG
        'END INTERFACE


        'CONTAINS


        '! =============================================================================

        Public Sub ToroideBASICO(ByVal DivComp As Integer, _
                                 ByVal DivCirc As Integer, _
                                 ByVal RaioToro As Double, _
                                 ByVal RaioTubo As Double)

            Dim NL, NR As Integer
            Dim RToro, RTubo As Double

            Dim Vet(,) As Double
            Dim MatRot As Matriz4x4
            Dim DAlfa As Double
            Dim i, j, k, nnL, nnR, nn, ne As Integer
            Dim indx(2) As Integer


            NL = DivComp
            NR = DivCirc
            RToro = RaioToro
            RTubo = RaioTubo

            ReDim Vet(4, 2 * NR)

            nnR = NR
            nnL = NL

            If (nnL < toroideMinimoDivComprimentro) Then nnL = toroideMinimoDivComprimentro
            If (nnR < toroideMinimoDivRaio) Then nnR = toroideMinimoDivRaio

            DAlfa = PI / nnL '! DAlfa - numero de cortes no tubo =  2*PI / 2*NL 

            ne = nnL * nnR
            nn = (2 * nnR) * (2 * nnL)


            '!  LIMPA O OBJETO ANTERIOR E REDIMENSION-O PARA O NOVO - "ZERADO"
            EsvaziaCasca3D()

            With Me

                .MT = Matriz4x4.Identidade
                .NN = nn
                .NE = ne
                .NNF = 0    '! NAO TEM EXREMIDADES

                ReDim .Coord(4, .NN)
                ReDim .Elementos(9, .NE)
                ReDim .Fronteira(.NNF)

                'Objeto%Coord(4,:) = 1.0D0
                For j = 0 To UBound(.Coord, 2)
                    .Coord(4, j) = 1.0
                Next

                nnR = 2 * nnR
                nnL = 2 * nnL

                MatRot = MatrizRotacionaZ(DAlfa)

                '!  MARCACAO DOS NOS DE FRONTEIRA
                .Fronteira(NNF) = 0

                '!  GERACAO DOS NOS DO PRIMEIRO "DISCO" - DISCO BASICO

                '   CALL dPtosCircunferencia(Vet, nnR, RTubo, 1)

                '!  TRANSLACAO DOS PONTOS DO DISCO BASE (PRIMEIRO) EM RToro  NA DIRECAO DO EIXO Y

                For j = 0 To UBound(Vet, 2)
                    Vet(2, j) = Vet(2, j) + RToro
                Next


                '!  TRANSFERENCIA DO PRIMEIRO DISCO DE COORDENADAS PARA O OBJETO

                For i = 1 To 4
                    For j = 1 To nnR
                        .Coord(i, j) = Vet(i, j)
                    Next
                Next

                indx(0) = 1
                nn = nnR
                ne = 0

                For i = 1 To NL - 1 '   DO i = 1, (NL-1)

                    '!    ROTACAO DO DISCO EM RELACAO A Z PARA AS 2 PROXIMAS POSICOES 
                    '!    PARA GERAR O i-esimo ANEL DE ELEMENTOS

                    For j = 1 To 2
                        indx(j) = nn + 1

                        '!      POSICIONAMENTO DO DISCO DE PONTOS
                        For i = 1 To 4
                            For k = 1 To nnR
                                Vet(i, k) = MatRot * Vet(i, k)
                            Next
                        Next

                        '         
                        '       END DO
                    Next




                    '!      TRANSFERENCIA DO DISCO DE COORDENADAS PARA O OBJETO

                    '       k  = nn + 1 
                    '       nn = nn + nnR

                    '       Objeto%Coord(1:4,k:nn) = Vet(1:4,:)

                    '     END DO

                    '!    GERACAO DOS ELEMENTOS

                    '     k = 0

                    '     DO j = 1, NR-1
                    '       ne = ne + 1
                    '       Objeto%Elementos(0,ne) = 9
                    '       Objeto%Elementos(2,ne) = k + indx(0)
                    '       Objeto%Elementos(5,ne) = k + indx(1)
                    '       Objeto%Elementos(1,ne) = k + indx(2)
                    '       Objeto%Elementos(8,ne) = k + indx(2) + 1
                    '       Objeto%Elementos(4,ne) = k + indx(2) + 2
                    '       Objeto%Elementos(7,ne) = k + indx(1) + 2
                    '       Objeto%Elementos(3,ne) = k + indx(0) + 2
                    '       Objeto%Elementos(6,ne) = k + indx(0) + 1
                    '       Objeto%Elementos(9,ne) = k + indx(1) + 1
                    '       k = k + 2
                    '     END DO               

                    '     ne = ne + 1
                    '     Objeto%Elementos(0,ne) = 9
                    '     Objeto%Elementos(2,ne) = k + indx(0)
                    '     Objeto%Elementos(5,ne) = k + indx(1)
                    '     Objeto%Elementos(1,ne) = k + indx(2)
                    '     Objeto%Elementos(8,ne) = k + indx(2) + 1
                    '     Objeto%Elementos(4,ne) = indx(2)
                    '     Objeto%Elementos(7,ne) = indx(1)
                    '     Objeto%Elementos(3,ne) = indx(0)
                    '     Objeto%Elementos(6,ne) = k + indx(0) + 1
                    '     Objeto%Elementos(9,ne) = k + indx(1) + 1

                    '     indx(0) = indx(2)

                    '   END DO 

                    '!  GERACAO DO ULTIMO ANEL

                    '   indx(2) = 1 ! aponta para o primeiro anel

                    '   indx(1) = nn + 1

                    '!      POSICIONAMENTO DO DISCO DE PONTOS

                    '   DO k = 1, nnR
                    '     Vet(:,k) = MATMUL(MatRot,Vet(:,k))
                    '   END DO

                    '!  TRANSFERENCIA DO DISCO DE COORDENADAS PARA O OBJETO

                    '   k  = nn + 1 
                    '   nn = nn + nnR

                    '   Objeto%Coord(:,k:nn) = Vet

                    '   k = 0

                    '   DO j = 1, NR-1
                    '     ne = ne + 1
                    '     Objeto%Elementos(0,ne) = 9
                    '     Objeto%Elementos(2,ne) = k + indx(0)
                    '     Objeto%Elementos(5,ne) = k + indx(1)
                    '     Objeto%Elementos(1,ne) = k + indx(2)
                    '     Objeto%Elementos(8,ne) = k + indx(2) + 1
                    '     Objeto%Elementos(4,ne) = k + indx(2) + 2
                    '     Objeto%Elementos(7,ne) = k + indx(1) + 2
                    '     Objeto%Elementos(3,ne) = k + indx(0) + 2
                    '     Objeto%Elementos(6,ne) = k + indx(0) + 1
                    '     Objeto%Elementos(9,ne) = k + indx(1) + 1
                    '     k = k + 2
                    '   END DO               

                    '     ne = ne + 1
                    '     Objeto%Elementos(0,ne) = 9
                    '     Objeto%Elementos(2,ne) = k + indx(0)
                    '     Objeto%Elementos(5,ne) = k + indx(1)
                    '     Objeto%Elementos(1,ne) = k + indx(2)
                    '     Objeto%Elementos(8,ne) = k + indx(2) + 1
                    '     Objeto%Elementos(4,ne) = indx(2)
                    '     Objeto%Elementos(7,ne) = indx(1)
                    '     Objeto%Elementos(3,ne) = indx(0)
                    '     Objeto%Elementos(6,ne) = k + indx(0) + 1
                    '     Objeto%Elementos(9,ne) = k + indx(1) + 1

                    '   RETURN 

                    ' END SUBROUTINE dquadToroideBASICO

            End With

        End Sub


    End Class


End Namespace
