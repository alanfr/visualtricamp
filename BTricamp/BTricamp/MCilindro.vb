﻿

Imports BMath.Matriz
Imports BMath.Constantes
Imports BMath.Funcoes


Namespace Casca



    Public Module MCilindro

        Public Const cilindroMinimoDivComprimenro As Integer = 2
        Public Const cilindroMinimoDivRaio As Integer = 2


        Public Structure Casca3D

            Public MT As Matriz4x4
            Public Coord(,) As Double
            Public Elementos(,) As Double
            Public Fronteira() As Double
            Public NN, NE, NNF As Integer
            Public SENTIDO As Integer
            Public Nome As Integer
            Public strNome As String
            Public TipoELEM As Integer


            Public Sub EsvaziaCasca3D(ByRef Objeto As Casca3D)
                With Objeto
                    .MT = Matriz4x4.Zeros
                    .Coord = Nothing
                    .Elementos = Nothing
                    .Fronteira = Nothing
                    .NN = 0
                    .NE = 0
                    .NNF = 0
                    .SENTIDO = 0
                    .Nome = 0
                    .TipoELEM = 0
                    .strNome = ""
                End With
            End Sub



            Public Sub CilindroBASICO(ByVal Objeto As Casca3D, _
                                      ByVal sTipo As Integer, _
                                      ByVal NL As Integer, _
                                      ByVal NR As Integer, _
                                      ByVal L As Double, _
                                      ByVal Raio As Double) 'Cilinro "reto" - basico

                Dim DL As Double = 0.0
                Dim DAlfa As Double = 0.0
                Dim Alfa As Double = 0.0
                Dim Dist As Double = 0.0

                Dim i As Integer = 0
                Dim j As Integer = 0
                Dim k As Integer = 0
                Dim nnL As Integer = 0
                Dim nnR As Integer = 0
                Dim nn As Integer = 0
                Dim ne As Integer = 0
                Dim kk As Integer = 0
                Dim Status As Integer = 0
                Dim indx(2) As Integer


                Dim ERRO As Boolean = False
                Dim strERRO As String = ""


                nnR = NR
                nnL = NL

                If (nnL < cilindroMinimoDivComprimenro) Then
                    nnL = cilindroMinimoDivComprimenro
                End If

                If (nnR < cilindroMinimoDivRaio) Then
                    nnR = cilindroMinimoDivRaio
                End If

                DAlfa = PI / (1.0 * nnR) 'DAlfa - numero de cortes no tubo =  2*PI / 2*NR 
                DL = L / (2.0 * nnL)
                nn = (2 * nnR) * (2 * nnL + 1)
                ne = nnR * nnL

                EsvaziaCasca3D(Objeto)

                With Objeto
                    .MT = Matriz4x4.Identidade
                    .NN = nn
                    .NE = ne
                    .NNF = nnR * 4  '2 * (2 * nnR)
                End With

                Select Case (sTipo) ' RESERVA ESPACO PARA AS POSSIVEIS TAMPAS
                    Case 0
                        kk = 0
                    Case 1, 2
                        kk = 1
                    Case 3
                        kk = 2
                    Case Else
                        kk = 0
                End Select

                With Objeto
                    .NE = .NE + kk
                    .NN = .NN + kk * 9
                    .NNF = .NNF + kk * 8
                End With

                'ALLOCATE
                ReDim Objeto.Coord(4, Objeto.NN)
                ReDim Objeto.Elementos(9, Objeto.NE)
                ReDim Objeto.Fronteira(Objeto.NNF)

                'Objeto%Coord(4,:) = 1.0D0
                For j = 0 To Objeto.Coord.Rank
                    Objeto.Coord(4, j) = 1.0
                Next

                nnR = 2 * nnR

                '!   MARCACAO DOS NOS DE FRONTEIRA E GERACAO DOS NOS DAS TAMPAS

                For i = 1 To nnR
                    k = nn - nnR + i
                    Objeto.Fronteira(i) = i
                    Objeto.Fronteira(nnR + i) = k
                Next

                If (sTipo <> 0) Then
                    k = 2 * nnR
                    j = 0
                    If ((sTipo = 1) Or (sTipo = 3)) Then

                        For i = 1 To 8
                            With Objeto
                                .Fronteira(k + i) = nn + i
                                Alfa = (1 - i) * FOURTH_PI
                                .Coord(1, nn + i) = 0.0              '!X
                                .Coord(2, nn + i) = Raio * Cos(Alfa) '!Y
                                .Coord(3, nn + i) = Raio * Sen(Alfa) '!Z
                            End With
                        Next

                        With Objeto
                            .Coord(1, nn + 9) = 0.0 '! X
                            .Coord(2, nn + 9) = 0.0 '!Y
                            .Coord(3, nn + 9) = 0.0 '!Z
                        End With
                        j = 8

                    End If 'sTipo = 1 ou sTipo = 3


                    If (sTipo = 2) Then
                        For i = 1 To 8
                            With Objeto
                                .Fronteira(k + i + j) = nn + i + j
                                Alfa = (i - 1) * FOURTH_PI
                                .Coord(1, nn + i + j) = L                       '!X
                                .Coord(2, nn + i + j) = Raio * Cos(Alfa)        '!Y
                                .Coord(3, nn + i + j) = Raio * Sen(Alfa) '!Z
                            End With
                        Next
                        Objeto.Coord(1, nn + 9) = L     '!X
                        Objeto.Coord(2, nn + 9) = 0D    '!Y
                        Objeto.Coord(3, nn + 9) = 0.0   '! Z
                    End If 'sTipo = 2

                    If (sTipo = 3) Then
                        For i = 1 To 8
                            With Objeto
                                .Fronteira(k + i + j) = nn + i + j + 1
                                Alfa = (i - 1) * FOURTH_PI
                                .Coord(1, nn + i + j + 1) = L                '!X
                                .Coord(2, nn + i + j + 1) = Raio * Cos(Alfa) '!Y
                                .Coord(3, nn + i + j + 1) = Raio * Sen(Alfa) '!Z
                            End With

                        Next

                        Objeto.Coord(1, nn + 18) = L   '!X
                        Objeto.Coord(2, nn + 18) = 0.0 '! Y
                        Objeto.Coord(3, nn + 18) = 0.0 '! Z
                    End If 'sTipo = 3
                Else
                    Exit Sub
                End If 'sTipo <> 0

                '!   GERACAO DOS NOS DO PRIMEIRO "DISCO" 

                For i = 1 To nnR
                    With Objeto
                        Alfa = (i - 1) * DAlfa
                        .Coord(1, i) = 0.0              '!X
                        .Coord(2, i) = Raio * Cos(Alfa) '!Y
                        .Coord(3, i) = Raio * Sen(Alfa) '!Z
                    End With
                Next
                indx(0) = 1
                nn = nnR
                ne = 0
                nnL = 0


                '!     GERACAO DOS NOS DOS "DISCOS" 
                With Objeto
                    For i = 1 To NL
                        For j = 1 To 2
                            indx(j) = nn + 1
                            nnL = nnL + 1
                            Dist = nnL * DL
                            For k = 1 To nnR
                                nn = nn + 1
                                Alfa = (k - 1) * DAlfa
                                .Coord(1, nn) = Dist             '!X
                                .Coord(2, nn) = Raio * Cos(Alfa) '!Y
                                .Coord(3, nn) = Raio * Sen(Alfa) '!Z
                            Next
                        Next

                        '!     GERACAO DOS ELEMENTOS
                        k = 0
                        For j = 1 To NR - 1
                            ne = ne + 1
                            .Elementos(0, ne) = 9
                            .Elementos(1, ne) = k + indx(0)
                            .Elementos(5, ne) = k + indx(1)
                            .Elementos(2, ne) = k + indx(2)
                            .Elementos(6, ne) = k + indx(2) + 1
                            .Elementos(3, ne) = k + indx(2) + 2
                            .Elementos(7, ne) = k + indx(1) + 2
                            .Elementos(4, ne) = k + indx(0) + 2
                            .Elementos(8, ne) = k + indx(0) + 1
                            .Elementos(9, ne) = k + indx(1) + 1
                            k = k + 2
                        Next 'j = 1 To NR - 1
                        ne = ne + 1
                        .Elementos(0, ne) = 9
                        .Elementos(1, ne) = k + indx(0)
                        .Elementos(5, ne) = k + indx(1)
                        .Elementos(2, ne) = k + indx(2)
                        .Elementos(6, ne) = k + indx(2) + 1
                        .Elementos(3, ne) = indx(2)
                        .Elementos(7, ne) = indx(1)
                        .Elementos(4, ne) = indx(0)
                        .Elementos(8, ne) = k + indx(0) + 1
                        .Elementos(9, ne) = k + indx(1) + 1
                        indx(0) = indx(2)
                    Next ' i = 1 To NL

                    nn = .NN - kk * 9
                    If ((sTipo = 1) Or (sTipo = 3)) Then
                        ne = ne + 1
                        .Elementos(0, ne) = 9
                        .Elementos(8, ne) = nn + 1
                        .Elementos(4, ne) = nn + 2
                        .Elementos(7, ne) = nn + 3
                        .Elementos(3, ne) = nn + 4
                        .Elementos(6, ne) = nn + 5
                        .Elementos(2, ne) = nn + 6
                        .Elementos(5, ne) = nn + 7
                        .Elementos(1, ne) = nn + 8
                        .Elementos(9, ne) = nn + 9
                        nn = nn + 9
                    End If


                    If ((sTipo = 2) Or (sTipo = 3)) Then
                        ne = ne + 1
                        .Elementos(0, ne) = 9
                        .Elementos(8, ne) = nn + 1
                        .Elementos(4, ne) = nn + 2
                        .Elementos(7, ne) = nn + 3
                        .Elementos(3, ne) = nn + 4
                        .Elementos(6, ne) = nn + 5
                        .Elementos(2, ne) = nn + 6
                        .Elementos(5, ne) = nn + 7
                        .Elementos(1, ne) = nn + 8
                        .Elementos(9, ne) = nn + 9
                    End If

                End With


            End Sub

        End Structure

    End Module


End Namespace

