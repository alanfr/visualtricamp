﻿'Cilindro Básico

Imports BMath.Matriz
Imports BMath.Constantes
Imports BMath.Funcoes



Namespace Casca3D


    Public Class Cilindro3D

        Implements IDisposable ', ICloneable, IComparable

        Private pMT As Matriz4x4
        Private pCoord(,) As Double
        Private pElementos(,) As Double
        Private pFronteira() As Double
        Private pNN, pNE, pNNF As Integer
        Private pSENTIDO As Integer
        Private pNome As Integer
        Private pstrNome As String
        Private pTipoELEM As Integer


        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False



#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub



        Public Sub New()
            With Me
                .MT = Matriz4x4.Zeros
                .Coord = Nothing
                .Elementos = Nothing
                .Fronteira = Nothing
                .NN = 0
                .NE = 0
                .NNF = 0
                .SENTIDO = 0
                .Nome = 0
                .TipoELEM = 0
                .Nomestr = ""
            End With

        End Sub


#End Region




#Region "Propriedades"


        Public Property MT() As Matriz4x4
            Get
                Return Me.pMT
            End Get
            Set(value As Matriz4x4)
                Me.pMT = value
            End Set
        End Property

        Public Property Coord() As Double(,)
            Get
                Return Me.pCoord
            End Get
            Set(value As Double(,))
                Me.pCoord = value
            End Set
        End Property


        Public Property Elementos() As Double(,)
            Get
                Return Me.pElementos
            End Get
            Set(value As Double(,))
                Me.pElementos = value
            End Set
        End Property


        Public Property Fronteira() As Double()
            Get
                Return Me.pFronteira
            End Get
            Set(value As Double())
                Me.pFronteira = value
            End Set
        End Property

        Public Property NN() As Double
            Get
                Return Me.pNN
            End Get
            Set(value As Double)
                Me.pNN = value
            End Set
        End Property

        Public Property NE() As Double
            Get
                Return Me.pNE
            End Get
            Set(value As Double)
                Me.pNE = value
            End Set
        End Property

        Public Property NNF() As Double
            Get
                Return Me.pNNF
            End Get
            Set(value As Double)
                Me.pNNF = value
            End Set
        End Property

        Public Property SENTIDO() As Integer
            Get
                Return Me.pSENTIDO
            End Get
            Set(value As Integer)
                Me.pSENTIDO = value
            End Set
        End Property


        Public Property Nome() As Integer
            Get
                Return Me.pNome
            End Get
            Set(value As Integer)
                Me.pNome = value
            End Set
        End Property


        Public Property Nomestr() As String
            Get
                Return Me.pstrNome
            End Get
            Set(value As String)
                Me.pstrNome = value
            End Set
        End Property


        Public Property TipoELEM() As Integer
            Get
                Return Me.pTipoELEM
            End Get
            Set(value As Integer)
                Me.pTipoELEM = value
            End Set
        End Property



#End Region




#Region "Esvazia Casca 3D"


        Public Sub EsvaziaCasca3D()
            With Me
                .MT = Matriz4x4.Zeros
                .Coord = Nothing
                .Elementos = Nothing
                .Fronteira = Nothing
                .NN = 0
                .NE = 0
                .NNF = 0
                .SENTIDO = 0
                .Nome = 0
                .TipoELEM = 0
                .Nomestr = ""
            End With
        End Sub


#End Region



#Region "Cilindro Basico"

        'sTipo = Tipo de Superfície
        'NL = Número de divisões no comprimento L (fatias)
        'NR = Número de divisões da circunferência de raio R
        'L = Comprimento do cilindro
        'R = Raio do Cilindro
        Public Sub CilindroBASICO(ByVal sTipo As Integer, _
                                  ByVal NL As Integer, _
                                  ByVal NR As Integer, _
                                  ByVal L As Double, _
                                  ByVal Raio As Double) 'As Integer 'Cilindro "reto" - basico

            Dim DL As Double = 0.0
            Dim DAlfa As Double = 0.0
            Dim Alfa As Double = 0.0
            Dim Dist As Double = 0.0

            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim k As Integer = 0
            Dim nnL As Integer = 0
            Dim nnR As Integer = 0
            Dim nn As Integer = 0
            Dim ne As Integer = 0
            Dim kk As Integer = 0
            Dim Status As Integer = 0
            Dim indx(2) As Integer


            Dim ERRO As Boolean = False
            Dim strERRO As String = ""


            nnR = NR
            nnL = NL

            If (nnL < cilindroMinimoDivComprimentro) Then
                nnL = cilindroMinimoDivComprimentro
            End If

            If (nnR < cilindroMinimoDivRaio) Then
                nnR = cilindroMinimoDivRaio
            End If

            DAlfa = PI / (1.0 * nnR) 'DAlfa - numero de cortes no tubo =  2*PI / 2*NR 
            DL = L / (2.0 * nnL)
            nn = (2 * nnR) * (2 * nnL + 1)
            ne = nnR * nnL

            Me.EsvaziaCasca3D()

            With Me
                .MT = Matriz4x4.Identidade
                .NN = nn
                .NE = ne
                .NNF = nnR * 4  '2 * (2 * nnR)


                Select Case (sTipo) ' RESERVA ESPACO PARA AS POSSIVEIS TAMPAS
                    Case 0
                        kk = 0
                    Case 1, 2
                        kk = 1
                    Case 3
                        kk = 2
                    Case Else
                        kk = 0
                End Select

                With Me
                    .NE = .NE + kk
                    .NN = .NN + kk * 9
                    .NNF = .NNF + kk * 8
                End With

                'ALLOCATE
                ReDim Me.Coord(4, Me.NN)
                ReDim Me.Elementos(9, Me.NE)
                ReDim Me.Fronteira(Me.NNF)

                'Objeto%Coord(4,:) = 1.0D0
                For j = 0 To UBound(Me.Coord, 2)
                    Me.Coord(4, j) = 1.0
                Next

                nnR = 2 * nnR

                '!   MARCACAO DOS NOS DE FRONTEIRA E GERACAO DOS NOS DAS TAMPAS

                For i = 1 To nnR
                    k = nn - nnR + i
                    Me.Fronteira(i) = i
                    Me.Fronteira(nnR + i) = k
                Next

                If (sTipo <> 0) Then
                    k = 2 * nnR
                    j = 0

                    If ((sTipo = 1) Or (sTipo = 3)) Then

                        For i = 1 To 8
                            .Fronteira(k + i) = nn + i
                            Alfa = (1 - i) * QUARTER_PI
                            .Coord(1, nn + i) = 0.0              '!X
                            .Coord(2, nn + i) = Raio * Cos(Alfa) '!Y
                            .Coord(3, nn + i) = Raio * Sen(Alfa) '!Z
                        Next
                        .Coord(1, nn + 9) = 0.0 '! X
                        .Coord(2, nn + 9) = 0.0 '!Y
                        .Coord(3, nn + 9) = 0.0 '!Z
                        j = 8
                    End If 'sTipo = 1 ou sTipo = 3


                    If (sTipo = 2) Then
                        For i = 1 To 8

                            .Fronteira(k + i + j) = nn + i + j
                            Alfa = (i - 1) * QUARTER_PI
                            .Coord(1, nn + i + j) = L                       '!X
                            .Coord(2, nn + i + j) = Raio * Cos(Alfa)        '!Y
                            .Coord(3, nn + i + j) = Raio * Sen(Alfa) '!Z

                        Next
                        .Coord(1, nn + 9) = L     '!X
                        .Coord(2, nn + 9) = 0D    '!Y
                        .Coord(3, nn + 9) = 0.0   '! Z
                    End If 'sTipo = 2

                    If (sTipo = 3) Then
                        For i = 1 To 8
                            .Fronteira(k + i + j) = nn + i + j + 1
                            Alfa = (i - 1) * QUARTER_PI
                            .Coord(1, nn + i + j + 1) = L                '!X
                            .Coord(2, nn + i + j + 1) = Raio * Cos(Alfa) '!Y
                            .Coord(3, nn + i + j + 1) = Raio * Sen(Alfa) '!Z
                        Next
                        .Coord(1, nn + 18) = L   '!X
                        .Coord(2, nn + 18) = 0.0 '! Y
                        .Coord(3, nn + 18) = 0.0 '! Z
                    End If 'sTipo = 3
                Else
                    'Return 101 'Erro
                    Exit Sub
                End If 'sTipo <> 0

                '!   GERACAO DOS NOS DO PRIMEIRO "DISCO" 

                For i = 1 To nnR
                    Alfa = (i - 1) * DAlfa
                    .Coord(1, i) = 0.0              '!X
                    .Coord(2, i) = Raio * Cos(Alfa) '!Y
                    .Coord(3, i) = Raio * Sen(Alfa) '!Z
                Next

                indx(0) = 1
                nn = nnR
                ne = 0
                nnL = 0


                '!     GERACAO DOS NOS DOS "DISCOS" 

                For i = 1 To NL
                    For j = 1 To 2
                        indx(j) = nn + 1
                        nnL = nnL + 1
                        Dist = nnL * DL
                        For k = 1 To nnR
                            nn = nn + 1
                            Alfa = (k - 1) * DAlfa
                            .Coord(1, nn) = Dist             '!X
                            .Coord(2, nn) = Raio * Cos(Alfa) '!Y
                            .Coord(3, nn) = Raio * Sen(Alfa) '!Z
                        Next
                    Next

                    '!     GERACAO DOS ELEMENTOS
                    k = 0
                    For j = 1 To NR - 1
                        ne = ne + 1
                        .Elementos(0, ne) = 9
                        .Elementos(1, ne) = k + indx(0)
                        .Elementos(5, ne) = k + indx(1)
                        .Elementos(2, ne) = k + indx(2)
                        .Elementos(6, ne) = k + indx(2) + 1
                        .Elementos(3, ne) = k + indx(2) + 2
                        .Elementos(7, ne) = k + indx(1) + 2
                        .Elementos(4, ne) = k + indx(0) + 2
                        .Elementos(8, ne) = k + indx(0) + 1
                        .Elementos(9, ne) = k + indx(1) + 1
                        k = k + 2
                    Next 'j = 1 To NR - 1
                    ne = ne + 1
                    .Elementos(0, ne) = 9
                    .Elementos(1, ne) = k + indx(0)
                    .Elementos(5, ne) = k + indx(1)
                    .Elementos(2, ne) = k + indx(2)
                    .Elementos(6, ne) = k + indx(2) + 1
                    .Elementos(3, ne) = indx(2)
                    .Elementos(7, ne) = indx(1)
                    .Elementos(4, ne) = indx(0)
                    .Elementos(8, ne) = k + indx(0) + 1
                    .Elementos(9, ne) = k + indx(1) + 1
                    indx(0) = indx(2)
                Next ' i = 1 To NL

                nn = .NN - kk * 9
                If ((sTipo = 1) Or (sTipo = 3)) Then
                    ne = ne + 1
                    .Elementos(0, ne) = 9
                    .Elementos(8, ne) = nn + 1
                    .Elementos(4, ne) = nn + 2
                    .Elementos(7, ne) = nn + 3
                    .Elementos(3, ne) = nn + 4
                    .Elementos(6, ne) = nn + 5
                    .Elementos(2, ne) = nn + 6
                    .Elementos(5, ne) = nn + 7
                    .Elementos(1, ne) = nn + 8
                    .Elementos(9, ne) = nn + 9
                    nn = nn + 9
                End If


                If ((sTipo = 2) Or (sTipo = 3)) Then
                    ne = ne + 1
                    .Elementos(0, ne) = 9
                    .Elementos(8, ne) = nn + 1
                    .Elementos(4, ne) = nn + 2
                    .Elementos(7, ne) = nn + 3
                    .Elementos(3, ne) = nn + 4
                    .Elementos(6, ne) = nn + 5
                    .Elementos(2, ne) = nn + 6
                    .Elementos(5, ne) = nn + 7
                    .Elementos(1, ne) = nn + 8
                    .Elementos(9, ne) = nn + 9
                End If

            End With

            ' Return 0

        End Sub

#End Region



    End Class






End Namespace


