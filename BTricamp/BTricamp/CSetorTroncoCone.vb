﻿'Cone

Imports BMath.Matriz
Imports BMath.Constantes
Imports BMath.Funcoes



Namespace Casca3D


    Public Class SetorTroncoCone3D

        Implements IDisposable ', ICloneable, IComparable

        Private pMT As Matriz4x4
        Private pCoord(,) As Double
        Private pElementos(,) As Double
        Private pFronteira() As Double
        Private pNN, pNE, pNNF As Integer
        Private pSENTIDO As Integer
        Private pNome As Integer
        Private pstrNome As String
        Private pTipoELEM As Integer



        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False




#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub



        Public Sub New()
            With Me
                .MT = Matriz4x4.Zeros
                .Coord = Nothing
                .Elementos = Nothing
                .Fronteira = Nothing
                .NN = 0
                .NE = 0
                .NNF = 0
                .SENTIDO = 0
                .Nome = 0
                .TipoELEM = 0
                .Nomestr = ""
            End With

        End Sub


#End Region



#Region "Propriedades"


        Public Property MT() As Matriz4x4
            Get
                Return Me.pMT
            End Get
            Set(value As Matriz4x4)
                Me.pMT = value
            End Set
        End Property

        Public Property Coord() As Double(,)
            Get
                Return Me.pCoord
            End Get
            Set(value As Double(,))
                Me.pCoord = value
            End Set
        End Property


        Public Property Elementos() As Double(,)
            Get
                Return Me.pElementos
            End Get
            Set(value As Double(,))
                Me.pElementos = value
            End Set
        End Property


        Public Property Fronteira() As Double()
            Get
                Return Me.pFronteira
            End Get
            Set(value As Double())
                Me.pFronteira = value
            End Set
        End Property

        Public Property NN() As Double
            Get
                Return Me.pNN
            End Get
            Set(value As Double)
                Me.pNN = value
            End Set
        End Property

        Public Property NE() As Double
            Get
                Return Me.pNE
            End Get
            Set(value As Double)
                Me.pNE = value
            End Set
        End Property

        Public Property NNF() As Double
            Get
                Return Me.pNNF
            End Get
            Set(value As Double)
                Me.pNNF = value
            End Set
        End Property

        Public Property SENTIDO() As Integer
            Get
                Return Me.pSENTIDO
            End Get
            Set(value As Integer)
                Me.pSENTIDO = value
            End Set
        End Property


        Public Property Nome() As Integer
            Get
                Return Me.pNome
            End Get
            Set(value As Integer)
                Me.pNome = value
            End Set
        End Property


        Public Property Nomestr() As String
            Get
                Return Me.pstrNome
            End Get
            Set(value As String)
                Me.pstrNome = value
            End Set
        End Property


        Public Property TipoELEM() As Integer
            Get
                Return Me.pTipoELEM
            End Get
            Set(value As Integer)
                Me.pTipoELEM = value
            End Set
        End Property



#End Region



#Region "Esvazia Casca 3D"


        Public Sub EsvaziaCasca3D()
            With Me
                .MT = Matriz4x4.Zeros
                .Coord = Nothing
                .Elementos = Nothing
                .Fronteira = Nothing
                .NN = 0
                .NE = 0
                .NNF = 0
                .SENTIDO = 0
                .Nome = 0
                .TipoELEM = 0
                .Nomestr = ""
            End With
        End Sub


#End Region



        'NL = Número de divisões no comprimento L (fatias)
        'NR = Número de divisões da circunferência de raio R
        'L = Comprimento do cilindro
        'R1 = Raio do Cilindro
        'R2 = Raio do Cilindro
        Public Sub SetorTroncoCone(ByVal NL As Integer, _
                                   ByVal NR As Integer, _
                                   ByVal L As Double, _
                                   ByVal R1 As Double, _
                                   ByVal R2 As Double, _
                                   ByVal A1 As Double, _
                                   ByVal A2 As Double) '! Cone / tronco de cone 




            Dim DL As Double = 0.0
            Dim DAlfa As Double = 0.0
            Dim Alfa As Double = 0.0
            Dim Dist As Double = 0.0
            Dim Raio As Double = 0.0
            Dim DR As Double = 0.0
            Dim DA As Double = 0.0



            Dim i As Integer = 0
            Dim j As Integer = 0
            Dim k As Integer = 0
            Dim nnL As Integer = 0
            Dim nnR As Integer = 0
            Dim nn As Integer = 0
            Dim ne As Integer = 0
            Dim innF As Integer = 0
            Dim Status As Integer = 0
            Dim indx(2) As Integer

            Dim ERRO As Boolean = False
            Dim strERRO As String = ""



            If (Abs(R1) <= ZERO) Then
                ERRO = 111
                strERRO = "ERRO(<dquadSETOR_TRONCOCONE>): SETOR TRONCO DE CONE COM RAIO INICIAL = O"
                Return
            End If
            If (Abs(R2) <= ZERO) Then
                ERRO = 112
                strERRO = "ERRO(<dquadSETOR_TRONCOCONE>): SETOR TRONCO DE CONE COM RAIO FINAL = O"
                Return
            End If
            If (R1 < 0.0) Then
                ERRO = 113
                strERRO = "ERRO(<dquadSETOR_TRONCOCONE>): SETOR TRONCO DE CONE COM RAIO INICIAL NEGATIVO"
                Return
            End If
            If (R2 < 0.0) Then
                ERRO = 114
                strERRO = "ERRO(<dquadSETOR_TRONCOCONE>): SETOR TRONCO DE CONE COM RAIO FINAL NEGATIVO"
                Return
            End If

            DA = A2 - A1

            If (Abs(Sen(DA)) <= ZERO) Then
                ERRO = 115
                MsgBox("ERRO(<SetorTroncoCone>): SETOR TRONCO DE CONE ANGULO FINAL = INICIAL")
                Exit Sub
            End If

            nnR = NR
            nnL = NL

            DR = (R2 - R1) / (NL * 2.0)

            If (nnL < cilindroMinimoDivComprimentro) Then nnL = cilindroMinimoDivComprimentro
            If (nnR < cilindroMinimoDivRaio) Then nnR = cilindroMinimoDivRaio

            DL = L / (2.0 * nnL)
            DAlfa = DA / (2.0 * nnR)

            nn = (2 * nnR + 1) * (2 * nnL + 1)
            ne = nnR * nnL


            '!   LIMPA O OBJETO ANTERIOR E REDIMENSIONA-O PARA O NOVO - "ZERADO"

            EsvaziaCasca3D()


            With Me

                .MT = Matriz4x4.Identidade
                .NN = nn
                .NE = ne
                .NNF = (nnL * nnR) * 4 '! = 2(2nnR+1) + 2(2nnL-1)

                'ALLOCATE(Objeto%Coord(4,Objeto%NN),STAT=status)
                Try
                    ReDim .Coord(4, .NN)
                Catch ex As Exception
                    ERRO = 116
                    'Write(strERRO,'("Cilindro: ",I6,", NNos = ",I6," ]")')Objeto%Nome,Objeto%NN
                    strERRO = "ERRO(<dquadTRONCOCONE>): ALOCACAO coordenadas [ " '// strERRO
                End Try


                '    ALLOCATE(Objeto%Elementos(0:9,Objeto%NE),STAT=status) 
                Try
                    ReDim .Elementos(9, .NE)
                Catch ex As Exception
                    ERRO = 117
                    'Write(strERRO,'("Cilindro: ",I6,", NELem = ",I6," ]")')Objeto%Nome,Objeto%NE
                    strERRO = "ERRO(<dquadTRONCOCONE>): ALOCACAO: elementos [ " '// strERRO
                End Try

                '    ALLOCATE(Objeto%Fronteira(Objeto%NNF),STAT=status) 
                Try
                    ReDim .Fronteira(.NNF)
                Catch ex As Exception
                    ERRO = 118
                    '         Write(strERRO,'("Cilindro: ",I6,", Nos Front. = ",I6," ]")')Objeto%Nome,Objeto%NNF
                    strERRO = "ERRO(<dquadTRONCOCONE>): ALOCACAO Nos das fronteiras [ " '// strERRO
                End Try


                '    Objeto%Coord(4,:) = 1.0D0
                For j = 0 To UBound(Me.Coord, 2)
                    Me.Coord(4, j) = 1.0
                Next

                nnR = 2 * nnR + 1


                '!   MARCACAO DOS NOS DE FRONTEIRA
                For i = 1 To nnR
                    k = nn - nnR + i
                    .Fronteira(i) = i
                    .Fronteira(k) = k
                Next

                innF = 2 * nnR + 1 '! proxima posicao do vetor de nos na fronteira

                '!   GERACAO DOS NOS DO PRIMEIRO "SEMI-DISCO" 
                Raio = R1
                For i = 1 To nnR
                    Alfa = A1 + (i - 1) * DAlfa
                    .Coord(1, i) = 0.0              '!X
                    .Coord(2, i) = Raio * Cos(Alfa) '!Y
                    .Coord(3, i) = Raio * Sen(Alfa) '!Z
                Next

                indx(0) = 1
                nn = nnR
                ne = 0
                nnL = 0


                For i = 1 To NL

                    '!   GERACAO DOS NOS DOS "SEMI-DISCOS" 

                    For j = 1 To 2
                        indx(j) = nn + 1
                        nnL = nnL + 1
                        Dist = nnL * DL
                        Raio = R1 + ((i - 1) * 2 + j) * DR

                        '!       MARCACAO DOS NOS EXTREMOS COMO FRONTEIRAS

                        .Fronteira(innF) = nn + 1
                        .Fronteira(innF + 1) = nn + 1 + nnR

                        innF = innF + 2

                        For k = 1 To nnR
                            nn = nn + 1
                            Alfa = A1 + (i - 1) * DAlfa
                            .Coord(1, nn) = Dist              '!X
                            .Coord(2, nn) = Raio * Cos(Alfa) '!Y
                            .Coord(3, nn) = Raio * Sen(Alfa) '!Z
                        Next 'k = 1 To nnR

                    Next 'j = 1 To 2

                    '!     GERACAO DOS ELEMENTOS

                    k = 0
                    For j = 1 To NR
                        ne = ne + 1
                        .Elementos(0, ne) = 9
                        .Elementos(1, ne) = k + indx(0)
                        .Elementos(5, ne) = k + indx(1)
                        .Elementos(2, ne) = k + indx(2)
                        .Elementos(6, ne) = k + indx(2) + 1
                        .Elementos(3, ne) = k + indx(2) + 2
                        .Elementos(7, ne) = k + indx(1) + 2
                        .Elementos(4, ne) = k + indx(0) + 2
                        .Elementos(8, ne) = k + indx(0) + 1
                        .Elementos(9, ne) = k + indx(1) + 1
                        k = k + 2
                    Next

                    indx(0) = indx(2)


                Next 'i = 1 To NL


            End With

        End Sub



    End Class


End Namespace
