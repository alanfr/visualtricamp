﻿Imports System.IO
Imports System.Windows.Forms
Imports System.Text.RegularExpressions

Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes
Imports BMath.Matriz
Imports BMath.Constantes


Module Module1

    Private Structure trixl
        Dim Linha() As Integer
        Dim Px() As Double
        Dim Py() As Double
        Dim Pz() As Double
        Dim Ez() As Double
        Public Sub Define(ByVal d As Integer)
            ReDim Linha(d)
            ReDim Px(d)
            ReDim Py(d)
            ReDim Pz(d)
            ReDim Ez(d)
        End Sub
    End Structure


    Sub Main()

        Dim PL() As trixl
        Dim cperfil As Integer = 0
        Dim clinha As Integer = 0

        Dim TextoStream As Stream = Nothing
        Dim AbrirArquivo As New OpenFileDialog()
        Dim file_num As Integer = FreeFile()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim kin As Integer = 0



        AbrirArquivo.InitialDirectory = "" 'Abre o ultimo diretorio utilizado
        AbrirArquivo.Filter = "Trixl (trixl.out)|trixl.out"
        AbrirArquivo.FilterIndex = 0
        AbrirArquivo.RestoreDirectory = True


        If AbrirArquivo.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                TextoStream = AbrirArquivo.OpenFile()
                If (TextoStream IsNot Nothing) Then
                    FileOpen(file_num, AbrirArquivo.FileName, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

                    Dim input() As String = File.ReadAllLines(AbrirArquivo.FileName)

                    'Le o arquivo e checa quantidade de perfis
                    cperfil = 0
                    For Each m In input
                        'trixl.out
                        If (Mid(m, 3, 17).Trim = "INICIO DO PERFIL-") Then
                            cperfil += 1 'Conta perfis
                        End If
                    Next

                    'Define a quantidade de perfis
                    ReDim PL(cperfil - 1)

                    cperfil = 0
                    clinha = 0
                    'Dimensiona arrays para o numero de linhas que cada perfil possui
                    'Forma burra funcional emergencial
                    For Each m In input
                        If (Mid(m, 3, 17).Trim = "INICIO DO PERFIL-") Then
                            cperfil += 1
                            clinha = 0
                        Else
                            clinha += 1
                            PL(cperfil - 1).Define(clinha - 1)
                        End If
                    Next

                    cperfil = 0
                    clinha = 0
                    i = 1
                    For Each m In input
                        If (Mid(m, 3, 17).Trim = "INICIO DO PERFIL-") Then
                            cperfil += 1
                            clinha = 0
                        Else
                            PL(cperfil - 1).Linha(clinha) = i
                            PL(cperfil - 1).Px(clinha) = Val(Mid(m, 1, 12).Trim)
                            PL(cperfil - 1).Py(clinha) = Val(Mid(m, 16, 12).Trim)
                            PL(cperfil - 1).Pz(clinha) = Val(Mid(m, 31, 12).Trim)
                            PL(cperfil - 1).Ez(clinha) = Val(Mid(m, 46, 12).Trim)
                            clinha += 1
                        End If
                        i += 1
                    Next

                    For i = 0 To (PL.Length - 1)
                        For j = 0 To (PL(i).Linha.Length - 1)
                            Console.WriteLine("Linha {0}: ( {1}, {2}, {3} ) = {4}", PL(i).Linha(j), PL(i).Px(j), PL(i).Py(j), PL(i).Pz(j), PL(i).Ez(j))
                        Next
                    Next



                    ' Close the file.
                    FileClose(file_num)
                    Console.ReadKey()
                End If
            Catch Ex As Exception
                MessageBox.Show("Não é possivel ler o arquivo. Original error: " & Ex.Message)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
                If (TextoStream IsNot Nothing) Then
                    TextoStream.Close()
                End If
            End Try
        End If



    End Sub

End Module
