﻿Imports System.Drawing
Imports System.Windows.Forms

Imports BMath.Matriz
Imports BMath.Funcoes

Imports BGeom.Transformacoes
Imports BGeom.Vetor
Imports BGeom.Ponto

Imports BGraph.Desenha3D



Public Class _Canvas3D
    Private g As Graphics
    Dim C As Collection
    'Variable(s) Use For QSort sub
    Private Const CS_upNumQSort = 2000
    Private finishP(CS_upNumQSort) As Long, lowP(CS_upNumQSort) As Long, cline(CS_upNumQSort) As Byte
    Public DontSortPages As Boolean
    Private FleshAxis(2) As Pagina3D
    Private TextXdraw, TextYdraw, TextZdraw As Poligono3D
    Dim mousepoint As Point
    Dim Axis3D As Objeto3D
    Public object1 As New Objeto3D
    Dim Zbuffer(,) As Single
    Const INFINITY = -10000
    Dim ordered_faces As New Collection
    Dim faces As New Collection
    Private optShape As Long       ' Shape index
    Dim light As New Luz3D
    Public Light_Sources As New Collection
    Dim PrevColor As Color
    'Dim sc As New MSScriptControl.ScriptControl
    Public Ambience As Integer
    Dim shift As Integer
    Private Timer_dThetaX, Timer_dThetaY As Single
    Dim dx, dy As Single
    Public zoomfactor As Single = 1
    Private Structure View2DParam
        Dim Centre As Point
        Dim XScale As Single
        Dim YScale As Single
    End Structure

    Private View2D As View2DParam
    Public Cam3D As New Camera3D
    Public Property graphicsObj() As Graphics
        Get
            Return g
        End Get
        Set(ByVal value As Graphics)
            g = value
        End Set
    End Property
#Region "PROJECTION"
    Private Function Point2Dof3D(ByVal p1 As Ponto3D) As Point
        Dim p As Ponto3D
        p = Cam3D.Projeta3DEm2D(New Ponto3D(p1.x, p1.y, p1.z))
        Return New Point(View2D.Centre.X + zoomfactor * p.x, View2D.Centre.Y - zoomfactor * p.y)
    End Function

    Private Function ZHeight3D(ByVal p1 As Pagina3D) As Single
        'Return z (Height) of a 3d point as seem from view point
        'when change view Teta,Fi this value change as objects and points rotate in reverse angles
        'This is Important to find Drawing order (from far objects to near objects)
        Dim i As Integer
        ZHeight3D = p1.TransformaPonto(0).z
        For i = 1 To p1.NPontos
            ' If ZHeight3D < p1.transPoint(i).z Then ZHeight3D = p1.transPoint(i).z
            ' ZHeight3D = ZHeight3D + p1.transPoint(i).z
        Next
        'Dim p As New Geometry3D.Point3D(p1.x, p1.y, p1.z)
        'ZHeight3D = Cam3D.Project3Dto2D(p).z
    End Function
    'Quick-Sort Algoritm:
    Private Sub QSort(ByVal a1() As Single, ByVal b1() As Long, ByVal start1 As Long, ByVal finish1 As Long)
        'a1() -> Main Data
        'b1() -> Sorted Index base on Initial values.
        '        This is needed for find order of drawing pages/objects

        Dim start As Long, finish As Long
        Dim low As Long, middle As Long, p As Long, high As Long
        Dim v1 As Single, i1 As Long
        Dim check As Single

        start = start1 : finish = finish1
        If finish <= start Then Exit Sub

        p = 0
        finishP(p) = finish

QSort_st:  '------------------------------------------------------------------
        finish = finishP(p)
        low = start : high = finish
        middle = (low + high) \ 2

        check = a1(middle)

        Do

            Do While a1(low) < check
                low = low + 1
            Loop

            Do While a1(high) > check
                high = high - 1
            Loop

            If low <= high Then
                'SWAP a1(low), a1(high):
                v1 = a1(low) : a1(low) = a1(high) : a1(high) = v1
                i1 = b1(low) : b1(low) = b1(high) : b1(high) = i1
                low = low + 1
                high = high - 1
            End If
        Loop Until low > high

        lowP(p) = low


        If start < high Then 'CALL QSort(start, high)
            cline(p) = 0
            p = p + 1 : finishP(p) = high : GoTo QSort_st
        End If

QSort_1:
        If low < finish Then 'CALL QSort(low, finish)
            cline(p) = 1
            p = p + 1 : start = low : finishP(p) = finish : GoTo QSort_st
        End If

QSort_END:
        p = p - 1
        If p >= 0 Then If cline(p) = 0 Then low = lowP(p) : finish = finishP(p) : GoTo QSort_1 Else GoTo QSort_END

    End Sub

#End Region

#Region "EVENTS"

    Private Sub _3DCanvas_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick
        Dim cube3d As New Objeto3D
        cube3d = MakeCube(100, 100, 100)
        object1 = cube3d
    End Sub

    Private Sub _3DCanvas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        shift = e.KeyCode
    End Sub

    Private Sub _3DCanvas_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        shift = 0
    End Sub

    Private Sub _3DCanvas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        g = Me.CreateGraphics
        'Enable double buffering
        ' sc.Language = "vbscript"
        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        object1.drawMode = eDesenho.SOLIDOFRAME
        Initialize3D()
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        Static bDrawing As Boolean
        If bDrawing = True Then Exit Sub
        bDrawing = True 'Flag 

        If Light_Sources.Count = 0 Then Exit Sub
        e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        g = e.Graphics
        g.Clear(Color.FromArgb(242, 242, 242))

        Dim cube3d As New Objeto3D
        cube3d = MakeCube(10, 10, 10)
        ReDim Zbuffer(Me.Width, Me.Height)
        For i = 0 To Me.Width - 1
            For j = 0 To Me.Height - 1
                Zbuffer(i, j) = -INFINITY
            Next
        Next
        cube3d.drawMode = eDesenho.SOLIDOPLANO
        Dim m As Matriz4x4
        m = MatrizTranslacao3D(Light_Sources(1).Pos3D.x, Light_Sources(1).Pos3D.y, Light_Sources(1).Pos3D.z)
        cube3d.Transforma(m)
        Light_Sources(1).tPos3D = Cam3D.Projeta3DEm2D(Light_Sources(1).Pos3D) 'applytransform(light.Pos3D, M)

        For i = 0 To 2
            ' testing of z buffer ...> DrawLine3D(Cam3D.Project3Dto2D(FleshAxis(i).Point(1)), Cam3D.Project3Dto2D(FleshAxis(i).Point(2)), FleshAxis(i).Edgecolor)
            FleshAxis(i).DesenhaBorda = True
            DrawPage3D(FleshAxis(i), eDesenho.SOLIDOPLANO)
        Next

        DrawPolyDraw3D(TextXdraw, True) 'X text
        DrawPolyDraw3D(TextYdraw, True) 'Y text
        DrawPolyDraw3D(TextZdraw, True) 'Z text
        DrawObject3D(object1)
        DrawObject3D(cube3d)
        bDrawing = False
        Exit Sub

        'object1 = MakeCube(90, 80, 100)
        'Axis3D = ObjTest()
        'object1.drawMode = DrawStyle.SOLIDFLAT
        'object1.drawMode = DrawStyle.SOLID
        'object1 = ObjTest() ' MakeCube(100, 100, 100)
        'object1 = cube3d
        'Dim o As Object3D
        'o = AddObject3D(Axis3D, object1)

        'o.drawMode = DrawStyle.SOLIDFLAT
        'DrawObject3D(o)
        'Axis3D.drawMode = DrawStyle.SOLIDFLAT
        'DrawObject3D(Axis3D)
        'cube3d.drawMode = DrawStyle.SOLIDFRAME
        'DrawObject3D(cube3d)
    End Sub

    Private Sub _3DCanvas_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        mousepoint.X = e.X
        mousepoint.Y = e.Y
        StopAutorotation()
    End Sub

    Private Sub _3DCanvas_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        If (Math.Abs(dx) >= 2) Or (Math.Abs(dy) >= 2) Then
            If dx > 2 Then dx = 3 : If dx <= -2 Then dx = -2
            If dy > 2 Then dy = 3 : If dy <= -2 Then dy = -2
            If Abs(dx) <= 1 Then dx = 0
            If Abs(dy) <= 1 Then dy = 0
            StartAutorotation(-dx * 0.8, -dy * 0.8)
            dx = 0
            dy = 0
        End If
    End Sub

    Private Sub _3DCanvas_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove

        dx = e.X - mousepoint.X
        dy = e.Y - mousepoint.Y
        If Math.Abs(dx) + Math.Abs(dy) = 0 Then Exit Sub
        If e.Button = Windows.Forms.MouseButtons.Left Then Debug.Print(shift)

        If e.Button = Windows.Forms.MouseButtons.Left Then
            If shift = 88 Then '"X" is pressed: rotate about X axis
                Cam3D.RotacionaCamera(-dy * 1.2, 0, 0)
            ElseIf shift = 89 Then '"y" is pressed: rotate about y axis
                Cam3D.RotacionaCamera(0, -dx * 1.2, 0)
            ElseIf shift = 90 Then '"z" is pressed: rotate about z axis
                Cam3D.RotacionaCamera(0, 0, -dx * 1.2)
            ElseIf shift = 17 Then '"Ctrl" is pressed: rotate Light Source
                Light_Sources(1).rotate(150, dx * 0.6, -dy * 0.5)
            ElseIf shift = 16 Then '"Shift" is pressed: rotate Object
                object1.Rotaciona(-dx * 1.2, -dy * 0.9)
            Else
                Cam3D.RotacionaCamera(-dx * 1.2, -dy * 0.9)
            End If
            mousepoint.X = e.X
            mousepoint.Y = e.Y
            Me.Invalidate()
            Exit Sub
        ElseIf e.Button = Windows.Forms.MouseButtons.Right Then
            If shift = 0 Then 'translate Graph
                View2D.Centre.Y = View2D.Centre.Y + dy
                View2D.Centre.X = View2D.Centre.X + dx
            ElseIf shift = 16 Then 'Translate Object
                'object1.tr
            End If

            Me.Invalidate()
            mousepoint.X = e.X
            mousepoint.Y = e.Y
            Exit Sub
        End If

        mousepoint.X = e.X
        mousepoint.Y = e.Y

    End Sub

#End Region

#Region "DRAWING"
    Private Sub DrawLine3D(ByVal L1 As TLinha3D)

        With L1
            If .ePontilhada = False Then
                g.DrawLine(New Pen(.eCor, .eLargura), Point2Dof3D(.eP1), Point2Dof3D(.eP2))
            Else
                Dim ppen As Pen
                ppen = New Pen(Color.FromArgb(70, 120, 130, 190))
                ppen.DashStyle = Drawing2D.DashStyle.Dash
                g.DrawLine(ppen, Point2Dof3D(.eP1), Point2Dof3D(.eP2))
            End If
        End With

    End Sub

    Private Sub DrawPolyDraw3D(ByVal PDraw As Poligono3D, Optional ByVal iRelativeMode As Boolean = True)
        '- this sub begin from first point and
        '  continue in Relative or Absolute Mode
        '  as is determined iRelativeMode
        'PDraw.Point contain 3D points
        'PDraw.CommandStr: such as "MLLLML.."
        'M=MoveTo   L=LineTo

        Dim p As New Point, p3D As New Ponto3D, p0 As New Ponto3D
        Dim p1, p2 As Point

        Dim ch As String, cmd As String, i As Long

        With PDraw
            cmd = UCase(.CommandStr)
            For i = 0 To Len(cmd) - 1
                p3D = .Ponto(i)
                If i <> 0 And iRelativeMode Then
                    p3D.x = p3D.x + p0.x : p3D.y = p3D.y + p0.y : p3D.z = p3D.z + p0.z
                End If
                p = Point2Dof3D(p3D)
                ch = Mid(cmd, i + 1, 1)
                Select Case ch
                    Case "M" 'MoveTo
                        'MoveToEx(gdi_BufferHdc, p.X, p.Y, p)
                        p1 = p
                    Case "L" 'LineTo
                        p2 = p
                        'LineTo(gdi_BufferHdc, p.x, p.y)
                        g.DrawLine(New Pen(.Cor), p1, p2)
                        p1 = p
                End Select
                p0 = p3D
            Next
        End With

    End Sub

    Private Sub DrawPage3D(ByVal page1 As Pagina3D, Optional ByVal drawStyle1 As eDesenho = eDesenho.SOLIDOPLANO)
        Dim i, n As Long

        With page1
            n = .NPontos
            If n <= 1 Then Exit Sub
            Dim p2() As Point
            ReDim p2(n - 1)

            For i = 1 To n
                p2(i - 1) = Point2Dof3D(.Ponto(i))
            Next



            Select Case drawStyle1
                Case eDesenho.WIREFRAME
                    .Preenchimento = True
                    .DesenhaBorda = True
                    If .CorBorda = Nothing Then .CorBorda = Color.Black
                    .CorFace = Color.FromArgb(242, 242, 242) ' Me.BackColor
                Case eDesenho.SOLIDO
                    .Preenchimento = True
                    .CorFace = .SuperficieCor1(Light_Sources, Ambience, Cam3D.TempCamPos)
                    .DesenhaBorda = True
                    .CorBorda = Color.Black ' Color.FromArgb(70, 240, 240, 240)
                Case eDesenho.SOLIDOPLANO
                    .Preenchimento = True
                    If .CorFace = Nothing Then .CorFace = .Ponto(0).Cor

                    '////zBuffer Testing
                    ' If .NumPoints <= 2 Then Exit Sub
                    ' drawtriangle(.transPoint(1), .transPoint(2), .transPoint(3), .faceColor)
                    'If .NumPoints > 3 Then drawtriangle(.transPoint(3), .transPoint(4), .transPoint(1), .faceColor)
                    'g.DrawPolygon(New Pen(Color.Black), p2)
                    'Exit Sub
                Case eDesenho.SOLIDOFRAME
                    .Preenchimento = True
                    .DesenhaBorda = True
                    .CorBorda = Color.Black ' Color.FromArgb(70, 240, 240, 240)
                    If .CorFace = Nothing And .Ponto(0).Cor = Nothing Then .CorFace = .SuperficieCor1(Light_Sources, Ambience, Cam3D.TempCamPos)
                    If .Ponto(0).Cor <> Nothing Then .CorFace = .Ponto(0).Cor
                Case 7 'DrawStyle.SOLID
                    .Preenchimento = True
                    .CorFace = .Ponto(1).Cor
                Case eDesenho.SOLIDOGRADIENTE
                    ' Make a path gradient brush.
                    .Preenchimento = True
                    Dim edge_colors() As Color
                    ReDim edge_colors(n - 1)
                    Dim TempPt As Point
                    Dim bExists As Boolean
                    Dim k As Integer
                    p2(0) = Point2Dof3D(.Ponto(1))
                    edge_colors(0) = .Ponto(1).Cor
                    k = 1

                    'add those points only which are not already there
                    For i = 2 To n
                        bExists = False
                        TempPt = Point2Dof3D(.Ponto(i))
                        For j As Integer = 0 To i - 2
                            If p2(j).X = TempPt.X And p2(j).Y = TempPt.Y Then
                                bExists = True
                                Exit For
                            End If
                        Next
                        If Not bExists Then k = k + 1 : p2(k - 1) = TempPt : edge_colors(k - 1) = .Ponto(i).Cor
                    Next
                    ReDim Preserve p2(k - 1)
                    ReDim Preserve edge_colors(k - 1)
                    n = k

                    Dim IsVertical, IsHorizontal As Boolean
                    IsHorizontal = True : IsVertical = True
                    For i = 0 To n - 3 '
                        If p2(i).X <> p2(i + 1).X Then IsHorizontal = False
                        If p2(i).Y <> p2(i + 1).Y Then IsVertical = False
                    Next
                    'chech if line is vertical to avoid crash
                    If IsVertical Or IsHorizontal Then
                        g.FillPolygon(New SolidBrush(.Ponto(0).Cor), p2)
                        Exit Sub
                    Else
                        g.SmoothingMode = Drawing2D.SmoothingMode.HighSpeed
                        ' Make a path gradient brush.
                        Dim path_brush As New Drawing2D.PathGradientBrush(p2)
                        path_brush.CenterColor = .Ponto(0).Cor
                        path_brush.SurroundColors = edge_colors
                        ' Fill the hexagon.

                        g.FillPolygon(path_brush, p2)
                        path_brush.Dispose()

                        Dim ppp As New Pen(Color.Blue, 1)
                        ppp.Alignment = Drawing2D.PenAlignment.Inset
                        ppp.Dispose()
                        Exit Sub
                    End If

            End Select

            If .Preenchimento = True Then g.FillPolygon(New SolidBrush(.CorFace), p2)
            If .CorBorda <> Nothing And .DesenhaBorda = True Then g.DrawPolygon(New Pen(.CorBorda), p2)
        End With

    End Sub


    Private Sub DrawObject3D(ByVal Obj1 As Objeto3D)
        Dim i As Long, n As Long, a() As Single, b() As Long
        'Dim iCode As Integer, NoEdge As Boolean

        With Obj1
            n = .NPaginas - 1
            If n < 0 Then Exit Sub
            Obj1.Projeta(Cam3D)

            faces = New Collection
            'preparing to sort:
            ReDim a(n), b(n)
            For i = 0 To n
                a(i) = ZHeight3D(.Pagina(i))  'point(0)=Center Point Of page
                b(i) = i
            Next

            QSort(a, b, 0, n)
            ' SingleSort(a, b, 2, n - 1)
            For i = 0 To Obj1.NPaginas - 1
                faces.Add(.Pagina(b(n - i)), i)
                'faces.Add(.Page(b(i)), i)
            Next
            ' Debug.Print("===================")

            Dim light1 As Luz3D
            For Each light1 In Light_Sources
                light.ScaleIntensityForDepth(faces)
            Next

            For i = 0 To n - 1
                '.Page(i).faceColor = .Page(i).SurfaceColor1(Light_Sources, Ambience, Cam3D.TempCamPos)
            Next

            Dim graph1 As New FuncoesCartesianas

            '**
            'If .drawMode = DrawStyle.SOLID Then
            '    For i = 1 To n - 1
            '        ' Debug.Print(.Page(i).Point(1).x, .Page(i).Point(1).y)
            '        If doTest Then
            '            '        Debug.Print(.Page(i).Point(1).Normal.tostring())
            '            .Page(i).Point(1).col = .Page(i).VertexColor(.Page(i).Point(1).Normal, Light_Sources, Ambience, Cam3D.TempCamPos)
            '            .Page(i).Point(2).col = .Page(i).VertexColor(.Page(i).Point(2).Normal, Light_Sources, Ambience, Cam3D.TempCamPos)
            '            .Page(i).Point(3).col = .Page(i).VertexColor(.Page(i).Point(3).Normal, Light_Sources, Ambience, Cam3D.TempCamPos)
            '            .Page(i).Point(4).col = .Page(i).VertexColor(.Page(i).Point(4).Normal, Light_Sources, Ambience, Cam3D.TempCamPos)
            '        Else
            '            .Page(i).Point(1).col = .Page(i).VertexColor(graph1.DyDx(12, .Page(i).Point(1).x, .Page(i).Point(1).y), Light_Sources, Ambience, Cam3D.TempCamPos)
            '            .Page(i).Point(2).col = .Page(i).VertexColor(graph1.DyDx(12, .Page(i).Point(2).x, .Page(i).Point(2).y), Light_Sources, Ambience, Cam3D.TempCamPos)
            '            .Page(i).Point(3).col = .Page(i).VertexColor(graph1.DyDx(12, .Page(i).Point(3).x, .Page(i).Point(3).y), Light_Sources, Ambience, Cam3D.TempCamPos)
            '            .Page(i).Point(4).col = .Page(i).VertexColor(graph1.DyDx(12, .Page(i).Point(4).x, .Page(i).Point(4).y), Light_Sources, Ambience, Cam3D.TempCamPos)
            '        End If
            '        .Page(i).Point(0).col = ColorAverage(ColorAverage(.Page(i).Point(1).col, .Page(i).Point(2).col), _
            '       ColorAverage(.Page(i).Point(3).col, .Page(i).Point(4).col))
            '    Next
            '    ''*
            'End If


            Dim pg As Pagina3D
            For Each pg In faces
                DrawPage3D(pg, .drawMode)
            Next

        End With
    End Sub
#End Region

    '/////////////////Testing purpose code
    Public Sub SortFaces(ByRef Obj As Objeto3D)
        ' Sort the faces so those with the largest
        ' transformed Z coordinates come first.
        ' 
        ' As we switch faces around, we keep track of the
        ' number of switches we have made. If it's clear we
        ' are stuck in an infinite loop, just move the
        ' first face to the ordered_faces collection so we
        ' can continue.
        Dim face_1 As New Pagina3D
        Dim face_i As New Pagina3D
        Dim i As Integer
        Dim Xmin As Single
        Dim xmax As Single
        Dim ymin As Single
        Dim ymax As Single
        Dim zmin As Single
        Dim zmax As Single
        Dim xmini As Single
        Dim xmaxi As Single
        Dim ymini As Single
        Dim ymaxi As Single
        Dim zmini As Single
        Dim zmaxi As Single
        Dim overlap As Boolean
        Dim switches As Integer
        Dim max_switches As Integer
        ordered_faces = New Collection
        ' Pull out any that are culled. These are not
        ' drawn so we can put them at the front of' the ordered_faces collection.
        For i = faces.Count To 1 Step -1
            If faces(i).IsCulled Then
                ordered_faces.Add(faces(i))
                faces.Remove(i)
            End If
        Next i

        ' Order the remaining faces.
        max_switches = faces.Count
        Do While faces.Count > 0
            ' Get the first item's extent.
            face_1 = faces(1)
            face_1.GetExtent(Xmin, xmax, ymin, ymax, zmin, zmax)
            ' Compare this face to the others.
            overlap = False ' In case Face.Count = 0.
            For i = 2 To faces.Count
                face_i = faces(i)
                ' Get item i's extent.
                face_i.GetExtent(xmini, xmaxi, _
                ymini, ymaxi, zmini, zmaxi)
                overlap = True
                If xmaxi <= Xmin Or xmini >= xmax Or _
                ymaxi <= ymin Or ymini >= ymax Or _
                zmini >= zmax Then
                    ' The extents do not overlap.
                    overlap = False
                ElseIf face_i.SeAcima(face_1) Then
                    ' Face i is all above the plane
                    ' of face 1.
                    overlap = False
                ElseIf face_1.SeAbaixo(face_i) Then
                    ' Face 1 is all beneath the plane
                    ' of face i.
                    overlap = False
                ElseIf Not face_1.Ofuscar(face_i) Then
                    ' If face_1 does not lie partly above
                    ' face_i, then there is no problem.
                    overlap = False
                End If
                If overlap Then Exit For
            Next i
            If overlap And switches < max_switches Then
                ' There's overlap, move face_i to the
                ' top of the list.
                faces.Remove(i)
                faces.Add(face_i, , 1) ' Before position 1.

                switches = switches + 1
            Else
                ' There's no overlap. Move face 1 to
                ' the ordered_faces collection.
                ordered_faces.Add(face_1)
                faces.Remove(1)
                max_switches = faces.Count
                switches = 0
            End If
        Loop ' Loop until we've ordered all the faces.
        ' Replace the Faces collection with the
        ' ordered_faces collection.
        ' faces = ordered_faces
    End Sub

    Private Function MakeHorizRectangle(ByVal dx As Single, ByVal dy As Single) As Pagina3D
        Dim temp As New Pagina3D
        With temp
            .NPontos = 4
            .CorFace = Color.Pink
            ReDim .Ponto(4)
            .Ponto(0) = New Ponto3D(dx / 2, dy / 2, 0) 'Center
            .Ponto(1) = New Ponto3D(0, 0, 0)
            .Ponto(2) = New Ponto3D(dx, 0, 0)
            .Ponto(3) = New Ponto3D(dx, dy, 0)
            .Ponto(4) = New Ponto3D(0, dy, 0)
        End With
        Return temp
    End Function

    Private Function MakeCube(ByVal dx As Single, ByVal dy As Single, ByVal dz As Single, Optional ByVal ColRGB As Long = &HFFFFFF, Optional ByVal ColEdge As Long = 0, Optional ByVal iCode As Integer = 0) As Objeto3D

        Dim temp As New Objeto3D
        Dim alpha As Long
        alpha = 255
        With temp
            .NPaginas = 6
            .iCod = iCode
            .CorBorda = ColEdge
            .PontoCentral = New Ponto3D(dx / 2, dy / 2, dz / 2)
            ReDim .Pagina(6 - 1)
            Dim pg As Pagina3D
            pg = MakeHorizRectangle(dx, dy)
            .Pagina(0) = pg
            .Pagina(0).CorFace = Color.FromArgb(alpha, 255, 0, 0)
            .Pagina(0).Preenchimento = True
            pg = MakeHorizRectangle(dx, dy)
            .Pagina(1) = getRotacionaPagina(pg, eEixo.EIXO_X, 90)
            .Pagina(1).CorFace = Color.FromArgb(alpha, 0, 255, 0)
            .Pagina(1).Preenchimento = True
            pg = MakeHorizRectangle(dx, dy)
            .Pagina(2) = getRotacionaPagina(pg, eEixo.EIXO_Y, -90)
            .Pagina(2).CorFace = Color.FromArgb(alpha, 0, 0, 255)
            .Pagina(2).Preenchimento = True

            .Pagina(3) = TransferePagina3D(.Pagina(0), New Vetor3D(0, 0, dz))
            .Pagina(3).CorFace = Color.FromArgb(alpha, 255, 255, 0)
            .Pagina(3).Preenchimento = True
            .Pagina(4) = TransferePagina3D(.Pagina(1), New Vetor3D(0, dy, 0))
            .Pagina(4).CorFace = Color.FromArgb(alpha, 255, 0, 255)
            .Pagina(4).Preenchimento = True
            .Pagina(5) = TransferePagina3D(.Pagina(2), New Vetor3D(dx, 0, 0))
            .Pagina(5).CorFace = Color.FromArgb(alpha, 0, 255, 255)
            .Pagina(5).Preenchimento = True
        End With
        Return temp
    End Function

    Public Sub ObjTest() 'As Object3D
        Dim temp As New Objeto3D
        With temp
            .NPaginas = 2
            ReDim .Pagina(1)
            With .Pagina(0)
                .CorFace = Color.Red
                .NPontos = 4
                ReDim .Ponto(4)
                .Ponto(1) = New Ponto3D(160, 50, 0)
                .Ponto(2) = New Ponto3D(0, 50, 0)
                .Ponto(3) = New Ponto3D(0, 50, 120)
                .Ponto(4) = New Ponto3D(160, 50, 120)
            End With
            With .Pagina(1)
                .NPontos = 4
                .CorFace = Color.Blue
                ReDim .Ponto(4)
                .Ponto(1) = New Ponto3D(0, 0, 50)
                .Ponto(2) = New Ponto3D(160, 0, 50)
                .Ponto(3) = New Ponto3D(160, 100, 50)
                .Ponto(4) = New Ponto3D(0, 100, 50)
            End With

        End With
    End Sub

    Public Sub CreateAxis()
        FleshAxis(0) = MakeXArrow(170 * Me.Width / 600, 8, Color.Red)
        FleshAxis(1) = MakeXArrow(170 * Me.Width / 600, 8, Color.Green)
        FleshAxis(1) = getRotacionaPagina(FleshAxis(1), eEixo.EIXO_Z, 90)
        FleshAxis(2) = MakeXArrow(170 * Me.Width / 600, 8, Color.Blue)
        FleshAxis(2) = getRotacionaPagina(FleshAxis(2), eEixo.EIXO_Y, -90)
        PrevColor = Color.Blue
        With TextXdraw
            .CommandStr = "MLML" 'M:Move , L:Line
            .Cor = Color.White
            ReDim .Ponto(3)
            .Ponto(0) = New Ponto3D(170 * Me.Width / 600, 3, 0)
            .Ponto(1) = New Ponto3D(-6, -6, 0)
            .Ponto(2) = New Ponto3D(0, 6, 0)
            .Ponto(3) = New Ponto3D(6, -6, 0)
        End With
        With TextYdraw
            .CommandStr = "MLML"
            .Cor = Color.White
            ReDim .Ponto(3)
            .Ponto(0) = New Ponto3D(3, 170 * Me.Width / 600, 0)
            .Ponto(1) = New Ponto3D(-6, -6, 0)
            .Ponto(2) = New Ponto3D(6, 0, 0)
            .Ponto(3) = New Ponto3D(-3, 3, 0)
        End With

        With TextZdraw
            .CommandStr = "MLLL"
            .Cor = Color.White
            ReDim .Ponto(3)
            .Ponto(0) = New Ponto3D(0, -2, 170 * Me.Width / 600)
            .Ponto(1) = New Ponto3D(0, 4, 0)
            .Ponto(2) = New Ponto3D(0, -4, -4)
            .Ponto(3) = New Ponto3D(0, 4, 0)
        End With
    End Sub
    Private Sub Initialize3D()

        CreateAxis()
        light.Ir = 80
        light.Ib = 80
        light.Ig = 95
        light.Pos3D = New Ponto3D(100, 0, 0)
        light.LightSourceType = eFonteLuz.PONTO
        Ponto3D.RThetaPhiDePonto3D(light.Pos3D, light.Radius, light.Theta, light.Fi)

        Light_Sources.Add(light)
        Dim light1 As New Luz3D
        light1.Ir = 30
        light1.Ib = 30
        light1.Ig = 40
        light1.Pos3D = New Ponto3D(10, 20, 200)

        '  Light_Sources.Add(light1)
        Cam3D.CameraPosicaoCentro = New Ponto3D(0, 0, 500)
        Cam3D.Perspectiva = True
        View2D.Centre.X = Me.Width / 2
        View2D.Centre.Y = Me.Height / 2
        Dim t As Integer
        t = 25
        With Axis3D
            .NPaginas = 300
            ReDim .Pagina(299)
            For i = 0 To 9
                For j = 0 To 9
                    With .Pagina(10 * i + j)
                        ReDim .Ponto(4)
                        .CorFace = Color.FromArgb(200, 150, 10, 120)
                        'If i = 9 And j = 9 Then .faceColor = Color.Brown
                        .CorBorda = Color.Pink
                        .NPontos = 4
                        .Ponto(1) = New Ponto3D((i) * t, (j) * t, 0)
                        .Ponto(2) = New Ponto3D((i + 1) * t, (j) * t, 0)
                        .Ponto(3) = New Ponto3D((i + 1) * t, (j + 1) * t, 0)
                        .Ponto(4) = New Ponto3D((i) * t, (j + 1) * t, 0)
                        .Preenchimento = True
                        .Ponto(0) = Ponto3D.Ponto3DMedio(.Ponto(1), .Ponto(3))
                    End With
                Next
            Next

            For i = 0 To 9
                For j = 0 To 9
                    With .Pagina(100 + 10 * i + j)
                        ReDim .Ponto(4)
                        .CorFace = Color.FromArgb(200, 10, 200, 150)
                        .CorBorda = Color.SlateBlue
                        If i = 9 And j = 9 Then .CorFace = Color.Brown
                        '.Edgecolor = Color.White
                        .NPontos = 4
                        .Ponto(1) = New Ponto3D(0, (i) * t, (j) * t)
                        .Ponto(2) = New Ponto3D(0, (i + 1) * t, (j) * t)
                        .Ponto(3) = New Ponto3D(0, (i + 1) * t, (j + 1) * t)
                        .Ponto(4) = New Ponto3D(0, (i) * t, (j + 1) * t)
                        .Preenchimento = True
                        .Ponto(0) = Ponto3D.Ponto3DMedio(.Ponto(1), .Ponto(3))
                    End With
                Next
            Next
            For i = 0 To 9
                For j = 0 To 9
                    With .Pagina(200 + 10 * i + j)
                        ReDim .Ponto(4)
                        .CorFace = Color.FromArgb(200, 200, 150, 0)
                        If i = 9 And j = 9 Then .CorFace = Color.Brown
                        .CorBorda = Color.SlateGray
                        .NPontos = 4
                        .Ponto(1) = New Ponto3D((i) * t, 0, (j) * t)
                        .Ponto(2) = New Ponto3D((i + 1) * t, 0, (j) * t)
                        .Ponto(3) = New Ponto3D((i + 1) * t, 0, (j + 1) * t)
                        .Ponto(4) = New Ponto3D((i) * t, 0, (j + 1) * t)
                        .Preenchimento = True
                        .Ponto(0) = Ponto3D.Ponto3DMedio(.Ponto(1), .Ponto(3))
                    End With
                Next
            Next

        End With

        object1 = FORMAS(5)
        object1.drawMode = eDesenho.SOLIDOFRAME
    End Sub

    Private Sub _3DCanvas_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        View2D.Centre.X = Me.Width / 2
        View2D.Centre.Y = Me.Height / 2
        CreateAxis()
        ReDim Zbuffer(Me.Width, Me.Height)
        For i = 0 To Me.Width - 1
            For j = 0 To Me.Height - 1
                Zbuffer(i, j) = INFINITY
            Next
        Next
        Me.Invalidate()
    End Sub
    'Triabgle draw using zBuffer algo
    Private Sub drawLine3D(ByVal v1 As Ponto3D, ByVal v2 As Ponto3D, Optional ByVal col As Color = Nothing)
        Dim x1, x2, y1, y2, z1, z2 As Single
        Dim x, y, xP, yp As Integer
        Dim z As Single
        'Dim a, d, b As Single
        y1 = v1.y
        y2 = v2.y
        x1 = v1.x
        x2 = v2.x
        z1 = v1.z
        z2 = v2.z
        'make x1 smaller
        If x2 < x1 Then SingleSwap(x1, x2) : SingleSwap(y1, y2) : SingleSwap(z1, z2)
        Dim dt As Single
        Dim dz As Single
        If col = Nothing Then col = Color.Blue
        Dim p As New Pen(col)
        If (x2 - x1) < 0.001 Then
            'make y1 smaller
            If y1 > y2 Then IntegerSwap(y1, y2) : SingleSwap(z1, z2)
            x = x1
            If Abs(y2 - y1) < 0.001 Then Exit Sub
            ' dz = (z2 - z1) / (y2 - y1)
            dz = (v2.z - v1.z) / (v2.y - v1.y)
            For y = y1 To y2
                z = z1 + (y - y1) * dz
                xP = View2D.Centre.X + x
                yp = View2D.Centre.Y + y
                If xP >= Me.Width Or yp >= Me.Height Then Exit Sub
                If xP <= 0 Or yp <= 0 Then Exit Sub
                If z < Zbuffer(xP, yp) Then
                    Zbuffer(xP, yp) = z
                    g.DrawLine(p, New Point(xP, yp), New Point(xP, yp + 1))
                End If
            Next
        Else
            Try
                dt = (v2.y - v1.y) / (v2.x - v1.x)
                dz = (v2.z - v1.z) / (v2.x - v1.x) '(z2 - z1) / (x2 - x1)
                For i As Integer = 0 To CInt(v2.x - v1.x) + 1 Step 1
                    x = x1 + i
                    y = y1 + i * dt
                    z = z1 + i * dz
                    xP = View2D.Centre.X + x
                    yp = View2D.Centre.Y + y
                    If xP >= Me.Width Or yp >= Me.Height Then Exit Sub
                    If xP <= 0 Or yp <= 0 Then Exit Sub
                    If z < Zbuffer(xP, yp) Then
                        Zbuffer(xP, yp) = z
                        g.DrawLine(p, New Point(xP, yp), New Point(xP + 1, yp + 1))
                    End If
                Next
            Catch ex As Exception
                Debug.Print("mahesh" & (View2D.Centre.X + x).ToString & (View2D.Centre.Y + y).ToString)
            End Try
        End If
    End Sub

    'Triabgle draw using zBuffer algo
    Private Sub drawtriangle(ByVal v1 As Ponto3D, ByVal v2 As Ponto3D, ByVal v3 As Ponto3D, Optional ByVal col As Color = Nothing)
        Dim x1, x2, x3, y1, y2, y3 As Single
        Dim minx, miny, maxx, maxy, x, y As Integer
        Dim z As Single
        Dim d, a, b As Single
        y1 = v1.y
        y2 = v2.y
        y3 = v3.y
        On Error GoTo 0
        x1 = v1.x
        x2 = v2.x
        x3 = v3.x
        Linear3(v1.x, v1.y, -1, -v1.z, v2.x, v2.y, -1, -v2.z, v3.x, v3.y, -1, -v3.z, a, b, d)
        'Bounding(Rectangle)b,d
        minx = CInt(Min(Min(x1, x2), x3))
        maxx = CInt(Max(Max(x1, x2), x3))
        miny = CInt(Min(Min(y1, y2), y3))
        maxy = CInt(Max(Max(y1, y2), y3))

        If col = Nothing Then col = Color.Red
        '(char*&)colorBuffer += miny * stride;
        Dim p As New Pen(col)

        ' // Scan through bounding rectangle
        ' for(int y = miny; y < maxy; y++)
        ' Dim bz, az, dz As Single
        For y = miny To maxy
            'bz = (y - miny) * y
            'z = d - b
            For x = minx To maxx
                If ((x1 - x2) * (y - y1) - (y1 - y2) * (x - x1)) >= 0 And ((x2 - x3) * (y - y2) - (y2 - y3) * (x - x2)) >= 0 And _
                   ((x3 - x1) * (y - y3) - (y3 - y1) * (x - x3)) >= 0 _
                   Or ((x1 - x2) * (y - y1) - (y1 - y2) * (x - x1)) <= 0 And ((x2 - x3) * (y - y2) - (y2 - y3) * (x - x2)) <= 0 And _
                   ((x3 - x1) * (y - y3) - (y3 - y1) * (x - x3)) <= 0 Then
                    z = d - a * x - b * y
                    If View2D.Centre.X + x >= Me.Width Or View2D.Centre.Y + y >= Me.Height Then Exit Sub
                    If View2D.Centre.X + x <= 0 Or View2D.Centre.Y + y <= 0 Then Exit Sub
                    If z < Zbuffer(View2D.Centre.X + x, View2D.Centre.Y + y) Then
                        Zbuffer(View2D.Centre.X + x, View2D.Centre.Y + y) = z
                        g.DrawLine(p, New Point(View2D.Centre.X + x, View2D.Centre.Y - y), New Point(View2D.Centre.X + x + 1, View2D.Centre.Y - y))
                        'g.DrawRectangle(p, View2D.Centre.X + x, View2D.Centre.Y - y, 1, 1)
                    End If
                    'g.DrawLine(p, New Point(x, y), New Point(x + 1, y + 1))
                End If
            Next
        Next
0:
    End Sub

    Public Function MakeObjByExtrude(ByVal Y_as_fX As String, ByVal xMin As Single, ByVal xMax As Single, _
                                ByVal ndx As Long, ByVal LengthZ As Single, Optional ByVal nLengthZ As Long = 1, Optional ByVal iCode As Long = 0, _
                                Optional ByVal NameStr As String = "UserExtrude", Optional ByVal ColEdge As Long = &HFFFFFF, Optional ByVal ColRGB As Long = &HCCCCCC, Optional ByVal InSideOrOutSide As Integer = 0) As Objeto3D
        'VnInsideOutside: determine normal vector side of pages (think for y=x^2, inside=upward , outside=downward)
        '                 0=Inside , 1=Outside

        Dim x As Single, y As Single, x1 As Single, y1 As Single, z As Single, z1 As Single
        Dim dx As Single, dz As Single
        Dim ix As Long, iZ As Long, c As Long
        Dim ReturnObj As New Objeto3D
        dx = (xMax - xMin) / ndx
        dz = LengthZ / nLengthZ
        z1 = -LengthZ / 2
        Y_as_fX = UCase(Y_as_fX)

        If InSideOrOutSide = 0 Then dz = -dz : z1 = -z1

        With ReturnObj

            .NomeStr = NameStr
            .NPaginas = ndx * nLengthZ
            .iCod = iCode
            .CorBorda = ColEdge
            ReDim .Pagina(.NPaginas - 1)

            c = 0
            For ix = 0 To ndx - 1
                x = xMin + ix * dx : z = z1
                'y = sc.Eval(Replace(Y_as_fX, "X", x))
                'x1 = x : y1 = y : x = x + dx : y = sc.Eval(Replace(Y_as_fX, "X", x))
                For iZ = 0 To nLengthZ - 1
                    z = z1 + iZ * dz
                    With .Pagina(c)
                        '.ColRGB = RGB(40, 50, 10 * ix Mod 255)
                        .CorBorda = Color.Red
                        .CorFace = Color.FromArgb(150, 255 - Math.Abs(z), (Math.Abs(2 * x)) Mod 255)
                        .Preenchimento = True
                        .NPontos = 4
                        ReDim .Ponto(4)
                        .Ponto(1) = New Ponto3D(x1, y1, z)
                        .Ponto(2) = New Ponto3D(x, y, z)
                        .Ponto(3) = New Ponto3D(x, y, z + dz)
                        .Ponto(4) = New Ponto3D(x1, y1, z + dz)
                        .Ponto(0) = Ponto3D.Ponto3DMedio(.Ponto(1), .Ponto(3)) 'Center point of page
                    End With
                    c = c + 1
                Next
            Next
        End With

        Return ReturnObj
        Exit Function
    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Cam3D.RotacionaCamera(Timer_dThetaX, Timer_dThetaY)
        Me.Invalidate()
    End Sub

    'Purpose: to autoRoate 3d Graph
    'Parameters: 
    ' dx-angle abt x axis graph roates in one counter/tick
    ' dy-angle abt y axis graph roates in one counter
    ' dz-angle abt z axis graph roates in one counter
    Public Sub StartAutorotation(ByVal dThetaX As Single, ByVal dThetay As Single)
        Timer_dThetaX = dThetaX
        Timer_dThetaY = dThetay
        Timer1.Enabled = True
    End Sub

    Public Sub StopAutorotation()
        Timer1.Enabled = False
    End Sub

    Private Sub _3DCanvas_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseWheel
        If e.Delta > 0 Then
            If zoomfactor >= 3 Then zoomfactor = 3 : Exit Sub
            zoomfactor = zoomfactor + 0.1
            Me.Invalidate()
        ElseIf e.Delta < 0 Then
            If zoomfactor <= 0.3 Then zoomfactor = 0.3 : Exit Sub
            zoomfactor = zoomfactor - 0.1
            Me.Invalidate()
        End If
    End Sub
End Class
