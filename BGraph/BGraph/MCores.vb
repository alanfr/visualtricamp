﻿Option Strict On


Imports System.Drawing
Imports Precisao = System.Double


Namespace Desenha3D

    Public Module Cores


        ' Define the name of this class/module for error-trap reporting.
        ' Hue, Saturation, Value space

        Private Const privNomeModulo As String = "CoresHSVRGB"

        Structure CorRGB
            Public R As Precisao
            Public G As Precisao
            Public B As Precisao
        End Structure


        Structure CorHSV
            Public H As Precisao
            Public S As Precisao
            Public V As Precisao
        End Structure


        Function CorSoma(ByVal A As Color, ByVal B As Color) As Color
            Return Color.FromArgb(A.R + B.R, _
                                  A.G + B.G, _
                                  A.B + B.B)
        End Function


        Function CorSubtrai(ByVal A As Color, ByVal B As Color) As Color
            Return Color.FromArgb(A.R - B.R, _
                                  A.G - B.G, _
                                  A.B - B.B)
        End Function



        Function CorEscala(ByVal A As Color, ByVal B As Precisao) As Color
            Return Color.FromArgb(CInt(A.R * B), _
                                  CInt(A.G * B), _
                                  CInt(A.B * B))
        End Function


        Function CorMedia(ByVal A As Color, ByVal B As Color) As Color
            Return Color.FromArgb(CInt(A.R / 2 + B.R / 2), _
                                  CInt(A.G / 2 + B.G / 2), _
                                  CInt(A.B / 2 + B.B / 2))
        End Function


        Public Function CorMedia(ByVal A As Color) As Color
            Dim B As Precisao
            B = (A.R + A.G + A.B) / 3
            Return Color.FromArgb(CInt(B), CInt(B), CInt(B))
        End Function



        Function CorHex2Double(ByVal A As String) As Precisao
            Dim rv As Color
            rv = Color.FromArgb(CInt((CLng(A) And &HFF&)), _
                                CInt((CLng(A) And &HFF00&) / &H100&), _
                                CInt((CLng(A) And &HFF0000) / &H10000))
            Return rv.GetHashCode
        End Function


        Function CorHex2RGB(ByVal A As String) As Color
            Return Color.FromArgb(CInt((CLng(A) And &HFF&)), _
                                  CInt((CLng(A) And &HFF00&) / &H100&), _
                                  CInt((CLng(A) And &HFF0000) / &H10000))
        End Function



        Public Function CorInterpola(ByVal A As Color, ByVal B As Color, ByVal Alpha As Precisao) As Color
            Return Color.FromArgb(CInt((A.R * (1 - Alpha) + Alpha * B.R)), _
                                  CInt((A.G * (1 - Alpha) + Alpha * B.G)), _
                                  CInt((A.B * (1 - Alpha) + Alpha * B.B)))
            '(255, (A.R + Alpha * (B.R - A.R)), (A.G + Alpha * (B.G - A.G)), (A.B + Alpha * (B.B - A.B)))
        End Function


        Function CorDouble2Hex(ByVal A As Precisao) As String
            Dim s As String
            s = Hex(A)
            Return s
        End Function



        Function CorDouble2RGB(ByVal A As Precisao) As Color
            Return Color.FromArgb(CInt(CLng(A) And &HFF&), _
                                 CInt((CLng(A) And &HFF00&) / &H100&), _
                                 CInt((CLng(A) And &HFF0000) / &H10000))
        End Function


        Function CorCompara(ByVal A As Color, ByVal B As Color) As Boolean
            If A.R = B.R And A.G = B.G And A.B = B.B Then
                Return True
            Else
                Return False
            End If
        End Function


        Function CorInverte(ByVal A As Color) As Color
            Return Color.FromArgb(A.A, 255 - A.R, 255 - A.G, 255 - A.B)
        End Function


        Function CorRGB2Hex(ByVal A As Color) As String
            Dim temp As String
            temp = Hex(RGB(A.R, A.G, A.B))

            Return temp
        End Function


        Function CorRGB2Double(ByVal A As Color) As Precisao
            Dim temp As Precisao
            temp = A.GetHashCode

            Return temp
        End Function


        Function CorMonocromatico(ByVal A As Precisao) As Color
            Return Color.FromArgb(CInt(A), CInt(A), CInt(A))
        End Function


        Function CorRand() As Color
            Randomize()
            Return Color.FromArgb(CInt(255 * Rnd()), CInt(255 * Rnd()), CInt(255 * Rnd()))
        End Function



        Function CorUniao(ByVal A As Color, ByVal B As Color) As Color
            Dim R, G, Bl As Precisao
            R = CDbl(IIf(B.R > A.R, B.R, A.R))
            G = CDbl(IIf(B.G > A.G, B.G, A.G))
            Bl = CDbl(IIf(B.B > A.B, B.B, A.B))
            Return Color.FromArgb(CInt(R), CInt(G), CInt(Bl))
        End Function



        Function CorIntersecao(ByVal A As CorRGB, ByVal B As CorRGB) As Color
            Dim R, G, Bl As Precisao
            R = CDbl(IIf(B.R < A.R, B.R, A.R))
            G = CDbl(IIf(B.G < A.G, B.G, A.G))
            Bl = CDbl(IIf(B.B < A.B, B.B, A.B))
            Return Color.FromArgb(CInt(R), CInt(G), CInt(Bl))
        End Function



        Function CorDifusa(ByVal A1 As Color, ByVal B1 As Color) As Color
            ' When use diffuse lighting, for example,
            '  if we make a red sphere, and a blue light source,
            '   and the ambiant light is disable, we get a BLACK
            '    color (no light) on the sphere.
            ' (you can test this in any 3D software)
            '
            ' The ColorDiffuse function do the same thing,
            '  this is helpful when we compute the diffuse value.
            Dim R, G, B As Precisao
            R = (A1.R * (B1.R / 255))
            G = (A1.G * (B1.G / 255))
            B = (A1.B * (B1.B / 255))
            Return Color.FromArgb(CInt(R), CInt(G), CInt(B))
        End Function


        Public Function HSV2RGB(Optional ByVal Matiz As Precisao = -1, _
                                Optional ByVal Saturacao As Precisao = 1, _
                                Optional ByVal Brilho As Precisao = 1) As Color

            ' =================================================================================================
            ' Given a Hue, Saturation and Brightness, return the Red-Green_Blue equivalent as a Long data type.
            ' This funtion is intended to replace VB's RGB function.
            '
            ' Ranges:
            '   Hue -1 (no hue)
            '       or
            '   Hue 0 to 360
            '
            '   Saturation 0 to 1
            '   Brightness 0 to 1
            '
            ' ie. Bright-RED = (Hue=0, Saturation=1, Brightness=1)
            '
            ' Example:
            '   Picture1.ForeColor = HSV(0,1,1)
            '
            ' ==============================================================================================
            Dim Red As Precisao
            Dim Green As Precisao
            Dim Blue As Precisao

            Dim i As Precisao
            Dim f As Precisao
            Dim p As Precisao
            Dim q As Precisao
            Dim t As Precisao

            If Saturacao = 0 Then  '   The colour is on the black-and-white center line.
                If Matiz = -1 Then    '   Achromatic color: There is no hue.
                    Red = Brilho
                    Green = Brilho
                    Blue = Brilho
                Else
                    ' *** Make sure you've turned on 'Break on unhandled Errors' ***
                    Err.Raise(vbObjectError + 1000, "HSV2RGB", "A Matiz foi adicionada sem a saturação.")
                End If
            Else
                Matiz = (Matiz Mod 360) / 60
                i = Int(Matiz)    ' Return largest integer
                f = Matiz - i     ' f is the fractional part of Hue
                p = Brilho * (1 - Saturacao)
                q = Brilho * (1 - (Saturacao * f))
                t = Brilho * (1 - (Saturacao * (1 - f)))
                Select Case i
                    Case 0
                        Red = Brilho
                        Green = t
                        Blue = p
                    Case 1
                        Red = q
                        Green = Brilho
                        Blue = p
                    Case 2
                        Red = p
                        Green = Brilho
                        Blue = t
                    Case 3
                        Red = p
                        Green = q
                        Blue = Brilho
                    Case 4
                        Red = t
                        Green = p
                        Blue = Brilho
                    Case 5
                        Red = Brilho
                        Green = p
                        Blue = q
                End Select
            End If

            Return Color.FromArgb(CInt(255 * Red), CInt(255 * Green), CInt(255 * Blue))

        End Function



        'Public Function HSV2Double(ByRef Red As Precisao, _
        '                           ByRef Green As Precisao, _
        '                           ByRef Blue As Precisao, _
        '                           Optional ByVal Matiz As Precisao = -1, _
        '                           Optional ByVal Saturacao As Precisao = 1, _
        '                           Optional ByVal Brilho As Precisao = 1) As Precisao

        '    ' ==============================================================================================
        '    ' Given a Hue, Saturation and Brightness, return the separate Red, Green and Blue values having
        '    ' ranges between 0 and 1.
        '    '
        '    ' Ranges:
        '    '   Hue -1 (no hue)
        '    '       or
        '    '   Hue 0 to 360
        '    '
        '    '   Saturation 0 to 1
        '    '   Brightness 0 to 1
        '    '
        '    ' ie. Bright-RED = (Hue=0, Saturation=1, Brightness=1)
        '    '       returns
        '    '     Red=1, Green=0, Blue=0
        '    '
        '    ' Example:
        '    '
        '    '   Dim myRed As Single, myGreen As Single, myBlue As Single
        '    '   Call HSV2(myRed, myGreen, myBlue, 0, 1, 1)
        '    '   Picture1.ForeColour = RGB(255*myRed, 255*myGreen, 255*myBlue)
        '    '
        '    ' ==============================================================================================

        '    Dim i As Precisao
        '    Dim f As Precisao
        '    Dim p As Precisao
        '    Dim q As Precisao
        '    Dim t As Precisao

        '    If Saturacao = 0 Then  '   The colour is on the black-and-white center line.
        '        If Matiz = -1 Then    '   Achromatic color: There is no hue.
        '            Red = Brilho
        '            Green = Brilho
        '            Blue = Brilho
        '        Else
        '            Err.Raise(vbObjectError + 1000, "HSV2Double", "Matiz foi adicionada sem saturação.")
        '        End If
        '    Else
        '        Matiz = (Matiz Mod 360) / 60
        '        i = Int(Matiz)    ' Return largest integer
        '        f = Matiz - i     ' f is the fractional part of Hue
        '        p = Brilho * (1 - Saturacao)
        '        q = Brilho * (1 - (Saturacao * f))
        '        t = Brilho * (1 - (Saturacao * (1 - f)))
        '        Select Case i
        '            Case 0
        '                Red = Brilho
        '                Green = t
        '                Blue = p
        '            Case 1
        '                Red = q
        '                Green = Brilho
        '                Blue = p
        '            Case 2
        '                Red = p
        '                Green = Brilho
        '                Blue = t
        '            Case 3
        '                Red = p
        '                Green = q
        '                Blue = Brilho
        '            Case 4
        '                Red = t
        '                Green = p
        '                Blue = Brilho
        '            Case 5
        '                Red = Brilho
        '                Green = p
        '                Blue = q
        '        End Select
        '    End If

        'End Function

        Public Function RGB2HSV(ByVal R As Precisao, _
                                ByVal G As Precisao, _
                                ByVal B As Precisao) As CorHSV

            Dim cor As Color = Color.FromArgb(CInt(R), CInt(G), CInt(B))
            Dim hsv As CorHSV

            hsv.H = cor.GetHue()
            hsv.S = cor.GetSaturation()
            hsv.V = cor.GetBrightness()

            Return hsv

        End Function





        Public Function RGB2HSV(ByVal Red As Precisao, _
                                ByVal Green As Precisao, _
                                ByVal Blue As Precisao, _
                                ByVal Matiz As Precisao, _
                                ByVal Saturacao As Precisao, _
                                ByVal Brilho As Precisao) As CorHSV

            ' ======================================================================
            ' Converts Red, Green & Blue back into a Hue, Saturation and Brightness.
            ' ======================================================================

            Dim R As Precisao
            Dim G As Precisao
            Dim B As Precisao
            Dim BrilhoMax As Precisao
            Dim BrilhoMin As Precisao
            Dim Delta As Precisao

            ' Clamp to safe values.
            ' =====================
            If Red > 255 Then Red = 255
            If Green > 255 Then Green = 255
            If Blue > 255 Then Blue = 255
            If Red < 0 Then Red = 0
            If Green < 0 Then Green = 0
            If Blue < 0 Then Blue = 0


            ' Convert values from the range 0-255 to 0-1
            ' ==========================================
            R = Red / 255
            G = Green / 255
            B = Blue / 255

            ' Find the Min & Max Brightness values.
            ' ====================================
            BrilhoMax = 0
            If R > BrilhoMax Then BrilhoMax = R
            If G > BrilhoMax Then BrilhoMax = G
            If B > BrilhoMax Then BrilhoMax = B
            Brilho = BrilhoMax

            BrilhoMin = 1
            If R < BrilhoMin Then BrilhoMin = R
            If G < BrilhoMin Then BrilhoMin = G
            If B < BrilhoMin Then BrilhoMin = B


            ' Calculate Saturation
            ' ====================
            If BrilhoMax = 0 Then
                Saturacao = 0                                      ' << Saturation is 0 if Red, Green and Blue are all zero.
            Else
                Saturacao = (BrilhoMax - BrilhoMin) / BrilhoMax
            End If


            ' Calculate Hue.
            ' ==============
            If Saturacao = 0 Then
                Matiz = -1 ' Undefined.
            Else
                Delta = (BrilhoMax - BrilhoMin)

                If R = BrilhoMax Then
                    Matiz = (G - B) / Delta           '   << Resulting colour is between yellow and magenta.
                ElseIf G = BrilhoMax Then
                    Matiz = 2 + (B - R) / Delta         '   << Resulting colour is between cyan and yellow.
                ElseIf B = BrilhoMax Then
                    Matiz = 4 + (R - G) / Delta        '   << Resulting colour is between magenta and cyan.
                End If

                Matiz = Matiz * 60                                      '   << Convert Hue to degrees in the range 0 to 360.

                If Matiz < 0 Then Matiz = Matiz + 360 '   << Make sure Hue is non-negative.

            End If ' Is Chromatic?

        End Function



    End Module

End Namespace

