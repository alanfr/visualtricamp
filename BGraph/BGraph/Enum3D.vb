﻿Option Strict On

Imports BMath.Matriz
Imports BMath.Constantes
Imports BMath.Funcoes

Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports System.Drawing
Imports Precisao = System.Double

Namespace Desenha3D

    Public Module Enum3D

        Public Teste As Boolean = False


#Region "Enum"


        Public Enum eDesenho
            WIREFRAME = 0
            SOLIDO = 1
            SOLIDOPLANO = 2
            SOLIDOFRAME = 3
            SOLIDOGRADIENTE = 4
            CAIXA = 5
        End Enum

        Public Enum eProjecao
            PERSPECTIVA = 0
            ORTOGRAFICA = 1
            CAVALEIRA = 3
        End Enum


        Public Enum eFonteLuz
            PONTO = 0
            INFINITO = 1
        End Enum


        Public Enum eEixo
            EIXO_X = 0
            EIXO_Y = 1
            EIXO_Z = 2
        End Enum



        Public Enum eObjetosPredefinidos
            ESFERA = 0
            CILINDRO = 1
            PLANO = 2
            TOROIDE = 3
            PARABOLOIDE = 4
            SEMI_EFERA = 5
        End Enum


        'Public Enum eObjetoCartesiano

        'End Enum

        Public Enum eObjetoCilindrico
            ESFERA = 0
            ANEL = 1
            CONCHA = 2
        End Enum

        Public Enum eObjetoEsferico
            ELIPSOIDE = 0
            CILINDRO = 1
            DOIS_ELIPSOIDES = 2
            TROFEU = 3
            DOISCONES = 4
            TACA_DE_VINHO1 = 5
            TACA_DE_VINHO2 = 6
            TACA_DE_VINHO3 = 7
            HIPERBOLOIDE = 8
            SUPERFICIE_PLANA = 9
            SENO = 10
            LAMINADOBRADA = 11
            PARABOLOIDE = 12
            SEMIESFERA = 13
            TORO = 14
            RODA = 15
            PI = 16
        End Enum

#End Region

        'Stores sin cos etc so trig functions for rotating vector eg. Sub Rotepoint3D(e. theta phi)
        'SinCos3AngType
        Public Structure TSenCosTresAngulos
            Dim SenX As Precisao
            Dim CosX As Precisao
            Dim SenY As Precisao
            Dim CosY As Precisao
            Dim SenZ As Precisao
            Dim CosZ As Precisao
        End Structure


        'Stores line joining 2 points p1 and p2
        'Line3D
        Public Structure TLinha3D
            Dim eP1 As Ponto3D
            Dim eP2 As Ponto3D
            Dim eCor As Color
            Dim eLargura As Precisao
            Dim ePontilhada As Boolean

            'returns LineStructure through two points
            Public Sub New(ByVal p1 As Ponto3D, _
                           ByVal p2 As Ponto3D, _
                           Optional ByVal cor As Color = Nothing, _
                           Optional ByVal largura As Precisao = 1.0, _
                           Optional ByVal pontilhada As Boolean = False)

                If (cor = Nothing) Then
                    cor = Color.White
                End If
                eP1 = p1
                eP2 = p2
                eLargura = largura
                eCor = cor
                ePontilhada = pontilhada
            End Sub

        End Structure



        'Defines polygon through different points
        'PolyDraw3D
        Public Structure Poligono3D

            Dim CommandStr As String ' tells about order of draw MLML moveto Lineto etc
            Dim Ponto() As Ponto3D
            Dim Cor As Color

            Public Sub Transforma(ByVal T As Matriz4x4)
                If UBound(Ponto) = 0 Then Exit Sub
                Dim pt As Ponto3D
                For Each pt In Ponto
                    pt.Transforma(T)
                Next
            End Sub
        End Structure



        'defines plane containing points
        'Page3D
        Public Structure Pagina3D
            Public CorFace As Color
            Public CorBorda As Color
            Public NPontos As Precisao
            Public Ponto() As Ponto3D 'Point(0)=Center Of Page , Point(1 to NumPoints)=Points
            Public TransformaPonto() As Ponto3D
            Public Normal As Vetor3D
            Public Preenchimento As Boolean
            Public Vizinhos() As Integer
            Public DesenhaBorda As Boolean
            Public SeVisivel As Boolean

            Dim SpValue As Precisao 'any arbitrary value such as magnitude of Radiation on this page
            Public Shared KVermelho As Precisao = 0.8
            Public Shared KVerde As Precisao = 0.5
            Public Shared KAzul As Precisao = 0.3
            Public Shared EspecularK As Precisao = 0.2
            Public Shared EspecularN As Precisao = 2

            Public Sub Transforma(ByVal T As Matriz4x4)
                If NPontos = 0 Or SeVisivel Then Exit Sub
                For i = 0 To NPontos
                    Me.Ponto(CInt(i)).Transforma(T)
                Next
                ' If Normal <> Nothing Then
                '  Matrix4x4.applytransform(Normal, T)
                ' End If
            End Sub

            Public Sub Projecao(ByVal camera As Camera3D)
                If NPontos = 0 Or SeVisivel Then Exit Sub
                ReDim TransformaPonto(CInt(NPontos))
                For i = 0 To NPontos
                    TransformaPonto(CInt(i)) = camera.Projeta3DEm2D(Ponto(CInt(i)))
                Next
            End Sub

            'returns normal vector of a plane specifiesd by page3dtype in 3D
            Public Function getNormal() As Vetor3D
                'return normal vector of a page using Cross product of first two vectors of plate(points 0,1,2)
                Dim N As New Vetor3D(0, 0, 0)
                Dim v1 As Vetor3D
                Dim v2 As Vetor3D

                With Me
                    '.Point(0) is Center! not a corner point
                    If (.NPontos < 3) Then
                        getNormal = N  'Valor padrao = 0
                        Exit Function
                    End If

                    v1 = Vetor3D.ConvertePonto2Vetor3D(.TransformaPonto(1), .TransformaPonto(2))
                    v2 = Vetor3D.ConvertePonto2Vetor3D(.TransformaPonto(3), .TransformaPonto(2))
                    If (v1 * v2).getComprimento <> 0 Then
                        N = (v1 * v2).getNormaliza
                    End If
                End With

                Return N

            End Function

            Public Sub AdicionaPontos(ByVal ParamArray Pts() As Ponto3D)
                Dim num_pts As Precisao
                Dim i As Precisao
                Dim pt As Precisao
                num_pts = UBound(Pts)
                ReDim Preserve Ponto(CInt(NPontos + num_pts))

                pt = 0
                For i = 1 To num_pts
                    Ponto(CInt(NPontos + i)) = Pts(CInt(i))
                Next i
                NPontos = NPontos + num_pts
                ReDim TransformaPonto(CInt(NPontos))
            End Sub


            Public Sub AdicionaPonto(ByVal Pt As Ponto3D)
                If NPontos = 0 Then
                    NPontos = 1
                    ReDim Ponto(1)
                    ReDim TransformaPonto(1)
                    Ponto(1) = Pt
                    Ponto(0) = Pt
                    'Point(0).COR = Pt.COR
                    Exit Sub
                End If

                If Teste Then 'False
                    NPontos = NPontos
                End If
                For i = 1 To NPontos
                    'Skip if point is already there
                    '  If (Point(i).x = Pt.x) And (Point(i).y = Pt.y) And (Point(i).z = Pt.z) Then Exit Sub
                Next

                'Add point since it is not there
                NPontos = NPontos + 1
                ReDim Preserve Ponto(CInt(NPontos))
                ReDim Preserve TransformaPonto(CInt(NPontos))
                Ponto(CInt(NPontos)) = Pt

                'calculate new midpoint
                'Point(0).X = ((NumPoints - 1) * Point(0).X + Pt.X) / NumPoints
                'Point(0).Y = ((NumPoints - 1) * Point(0).Y + Pt.Y) / NumPoints
                'Point(0).Z = ((NumPoints - 1) * Point(0).Z + Pt.Z) / NumPoints
                'If doTest Then Point(0).Normal = New Vector3D(Pt.Normal.x, Pt.Normal.y, Pt.Normal.z)
                'Point(0).COR = ColorInterpolate(Point(0).COR, Pt.COR, 1 / (NumPoints))
                'redimension projected points

            End Sub


            'returns normal vector of a plane specifiesd by page3dtype in 3D
            Public Sub GetExtent(ByRef xmin As Precisao, _
                                 ByRef xmax As Precisao, _
                                 ByRef ymin As Precisao, _
                                 ByRef ymax As Precisao, _
                                 ByRef zmin As Precisao, _
                                 ByRef zmax As Precisao)

                xmin = TransformaPonto(0).x
                xmax = TransformaPonto(0).x
                ymin = TransformaPonto(0).y
                ymax = TransformaPonto(0).y
                zmin = TransformaPonto(0).z
                zmax = TransformaPonto(0).z

                For i = 0 To NPontos
                    If xmin > TransformaPonto(CInt(i)).x Then xmin = TransformaPonto(CInt(i)).x
                    If ymin > TransformaPonto(CInt(i)).y Then ymin = TransformaPonto(CInt(i)).y
                    If zmin > TransformaPonto(CInt(i)).z Then zmin = TransformaPonto(CInt(i)).z
                    If xmax < TransformaPonto(CInt(i)).x Then xmax = TransformaPonto(CInt(i)).x
                    If ymax < TransformaPonto(CInt(i)).y Then ymax = TransformaPonto(CInt(i)).y
                    If zmax < TransformaPonto(CInt(i)).z Then zmax = TransformaPonto(CInt(i)).z
                Next i
            End Sub



            ' Compute a normal vector for this polygon.
            Public Sub VetorNormal(ByRef Nx As Precisao, _
                                   ByRef Ny As Precisao, _
                                   ByRef Nz As Precisao)

                Dim n As Vetor3D
                n = getNormal()
                Nx = n.X
                Ny = n.Y
                Nz = n.Z
            End Sub


            ' Return True if this polygon is completely above
            ' the plane containing target.
            Public Function SeAcima(ByVal target As Pagina3D) As Boolean

                Dim nx As Precisao
                Dim ny As Precisao
                Dim nz As Precisao
                Dim px As Precisao
                Dim py As Precisao
                Dim pz As Precisao
                Dim dx As Precisao
                Dim dy As Precisao
                Dim dz As Precisao

                ' Compute an upward pointing normal to the plane.
                target.VetorNormal(nx, ny, nz)
                If nz > 0 Then 'make sure vector is in +ve z
                    nx = -nx
                    ny = -ny
                    nz = -nz
                End If

                ' Get a point on the plane.
                px = target.TransformaPonto(0).x
                py = target.TransformaPonto(0).y
                pz = target.TransformaPonto(0).z
                ' See if the points in this polygon all lie
                ' above the plane containing target.
                For i = 1 To NPontos
                    ' Get the vector from plane to point.
                    dx = TransformaPonto(CInt(i)).x - px
                    dy = TransformaPonto(CInt(i)).y - py
                    dz = TransformaPonto(CInt(i)).z - pz
                    ' If the dot product < 0, the point is
                    ' below the plane.
                    If dx * nx + dy * ny + dz * nz < -0.01 Then
                        SeAcima = False
                        Exit Function
                    End If
                Next i
                SeAcima = True
            End Function



            ' Return True if this polygon is completely above
            ' the plane containing target.
            Public Function SeAbaixo(ByVal target As Pagina3D) As Boolean

                Dim nx As Precisao
                Dim ny As Precisao
                Dim nz As Precisao
                Dim px As Precisao
                Dim py As Precisao
                Dim pz As Precisao
                Dim dx As Precisao
                Dim dy As Precisao
                Dim dz As Precisao
                ' Compute an upward pointing normal to the plane.
                target.VetorNormal(nx, ny, nz)
                If nz > 0 Then
                    nx = -nx
                    ny = -ny
                    nz = -nz
                End If

                ' Get a point on the plane.
                px = target.TransformaPonto(0).x
                py = target.TransformaPonto(0).y
                pz = target.TransformaPonto(0).z

                ' See if the points in this polygon all lie
                ' above the plane containing target.
                For i = 1 To NPontos
                    ' Get the vector from plane to point.
                    dx = TransformaPonto(CInt(i)).x - px
                    dy = TransformaPonto(CInt(i)).y - py
                    dz = TransformaPonto(CInt(i)).z - pz


                    ' If the dot product < 0, the point is
                    ' below the plane.
                    If dx * nx + dy * ny + dz * nz < -0.01 Then
                        SeAbaixo = False
                        Exit Function
                    End If
                Next i
                SeAbaixo = True
            End Function




            Public Sub getPontoModificado(ByVal i As Precisao, _
                                           ByRef px As Precisao, _
                                           ByRef py As Precisao, _
                                           ByRef pz As Precisao)

                px = TransformaPonto(CInt(i)).x
                py = TransformaPonto(CInt(i)).y
                pz = TransformaPonto(CInt(i)).z
            End Sub



            ' Return True if the point projection lies within
            ' this polygon's projection.
            Public Function PontoInterno(ByVal X As Precisao, ByVal Y As Precisao) As Boolean
                Dim i As Integer
                Dim theta1 As Double
                Dim theta2 As Double
                Dim dtheta As Double
                Dim dx As Double
                Dim dy As Double
                Dim angles As Double
                dx = TransformaPonto(CInt(NPontos)).x - X
                dy = TransformaPonto(CInt(NPontos)).y - Y
                theta1 = Atan2(CSng(dy), CSng(dx))
                If theta1 < 0 Then theta1 = theta1 + 2 * PI
                For i = 1 To CInt(NPontos)
                    dx = TransformaPonto(i).x - X
                    dy = TransformaPonto(i).y - Y
                    theta2 = Atan2(CSng(dy), CSng(dx))
                    If theta2 < 0 Then theta2 = theta2 + 2 * PI
                    dtheta = theta2 - theta1
                    If dtheta > PI Then dtheta = dtheta - 2 * PI
                    If dtheta < -PI Then dtheta = dtheta + 2 * PI
                    angles = angles + dtheta
                    theta1 = theta2
                Next i
                PontoInterno = (Abs(angles) > 0.001) '#################### ZERO ?
            End Function




            ' Return True if this polygon partially obscures
            ' (has greater Z value than) polygon target.
            '
            ' We assume one polygon may obscure the other, but
            ' they cannot obscure each other.
            '
            ' This check is executed by seeing where the
            ' projections of the edges of the polygons cross.
            ' Where they cross, see if one Z value is greater
            ' than the other.
            '
            ' If no edges cross, see if one polygon contains
            ' the other. If so, there is an overlap.
            Public Function Ofuscar(ByVal target As Pagina3D) As Boolean

                Dim num As Precisao
                Dim i As Precisao
                Dim j As Precisao
                Dim xi1 As Precisao
                Dim yi1 As Precisao
                Dim zi1 As Precisao
                Dim xi2 As Precisao
                Dim yi2 As Precisao
                Dim zi2 As Precisao
                Dim xj1 As Precisao
                Dim yj1 As Precisao
                Dim zj1 As Precisao
                Dim xj2 As Precisao

                ' See where the projections of two segments cross.
                ' Return true if the segments cross, false
                ' otherwise.
                Dim yj2 As Precisao
                Dim zj2 As Precisao
                Dim X As Precisao
                Dim Y As Precisao
                Dim z1 As Precisao
                Dim z2 As Precisao

                num = target.NPontos
                ' Check each edge in this polygon.
                getPontoModificado(NPontos, xi1, yi1, zi1)
                For i = 1 To NPontos
                    getPontoModificado(i, xi2, yi2, zi2)
                    ' Compare with each edge in the other.
                    target.getPontoModificado(num, xj1, yj1, zj1)
                    For j = 1 To num
                        target.getPontoModificado(j, xj2, yj2, zj2)

                        ' See if the segments cross.
                        If SeCruza(xi1, yi1, zi1, _
                                   xi2, yi2, zi2, _
                                   xj1, yj1, zj1, _
                                   xj2, yj2, zj2, _
                                   X, Y, z1, z2) Then

                            If z2 - z1 > 0.01 Then 'If z1 - z2 > 0.01 Then #################### ZERO
                                ' z1 > z2. We obscure it.
                                Ofuscar = True
                                Exit Function
                            End If
                            If z1 - z2 > 0.01 Then 'If z2 - z1 > 0.01 Then #################### ZERO
                                ' z2 > z1. It obscures us.
                                Ofuscar = False
                                Exit Function
                            End If
                        End If
                        xj1 = xj2
                        yj1 = yj2
                        zj1 = zj2
                    Next j
                    xi1 = xi2
                    yi1 = yi2
                    zi1 = zi2
                Next i
                ' No edges cross. See if one polygon contains
                ' the other.
                '
                ' If any points of one polygon are inside the
                ' other, then they must all be. Since the
                ' IsAbove tests were inconclusive, some points
                ' in one polygon are on the "bad" side of the
                ' other. In that case there is an overlap.
                ' See if this polygon is inside the other.
                getPontoModificado(1, xi1, yi1, zi1)
                If target.PontoInterno(xi1, yi1) Then
                    Ofuscar = True
                    Exit Function
                End If
                ' See if the other polygon is inside this one.
                target.getPontoModificado(1, xi1, yi1, zi1)
                If PontoInterno(xi1, yi1) Then
                    Ofuscar = True
                    Exit Function
                End If
                Ofuscar = False
            End Function

            Private Function SeCruza(ByVal ax1 As Precisao, _
                                     ByVal ay1 As Precisao, _
                                     ByVal az1 As Precisao, _
                                     ByVal ax2 As Precisao, _
                                     ByVal ay2 As Precisao, _
                                     ByVal az2 As Precisao, _
                                     ByVal bx1 As Precisao, _
                                     ByVal by1 As Precisao, _
                                     ByVal bz1 As Precisao, _
                                     ByVal bx2 As Precisao, _
                                     ByVal by2 As Precisao, _
                                     ByVal bz2 As Precisao, _
                                     ByRef X As Precisao, _
                                     ByRef Y As Precisao, _
                                     ByRef z1 As Precisao, _
                                     ByRef z2 As Precisao) As Boolean

                Dim dxa As Precisao
                Dim dya As Precisao
                Dim dza As Precisao
                Dim dxb As Precisao
                Dim dyb As Precisao
                Dim dzb As Precisao
                Dim t1 As Precisao
                Dim t2 As Precisao
                Dim denom As Precisao

                dxa = ax2 - ax1
                dya = ay2 - ay1
                dxb = bx2 - bx1
                dyb = by2 - by1

                SeCruza = False
                denom = dxb * dya - dyb * dxa

                ' If the segments are parallel, stop.
                If denom < 0.01 And denom > -0.01 Then Exit Function
                t2 = (ax1 * dya - ay1 * dxa - bx1 * dya + by1 * dxa) / denom

                If t2 < 0 Or t2 > 1 Then Exit Function
                t1 = (ax1 * dyb - ay1 * dxb - bx1 * dyb + by1 * dxb) / denom

                If t1 < 0 Or t1 > 1 Then Exit Function
                ' Compute the points of overlap.
                X = ax1 + t1 * dxa
                Y = ay1 + t1 * dya
                dza = az2 - az1
                dzb = bz2 - bz1
                z1 = az1 + t1 * dza
                z2 = bz1 + t2 * dzb
                SeCruza = True
            End Function


            ' Return the proper shade for this face
            ' due to the indicated light source.
            Public Function SuperficieCor1(ByVal light_Sources As Collection, _
                                           ByVal ambient_light As Precisao, _
                                           ByVal eyePos As Ponto3D) As Color

                'Dim light As Light3D

                Dim L As Vetor3D
                Dim length As Precisao
                Dim N As Vetor3D
                Dim NdotL As Precisao
                Dim R As Precisao
                Dim G As Precisao
                Dim B As Precisao
                Dim distance_factor As Precisao
                Dim diffuse_factor As Precisao
                Dim V As Vetor3D
                Dim MV As Vetor3D 'mirror vector
                Dim RdotV As Precisao
                Dim specular_factor As Precisao
                Dim light As Luz3D
                If ambient_light > 255 Then ambient_light = 255
                If ambient_light < 0 Then ambient_light = 0

                If Ponto(0).Cor <> Nothing Then
                    KVermelho = Ponto(0).Cor.R / 255
                    KVerde = Ponto(0).Cor.G / 255
                    KAzul = Ponto(0).Cor.B / 255
                    R = Ponto(0).Cor.R
                    G = Ponto(0).Cor.G
                    B = Ponto(0).Cor.B
                ElseIf CorFace <> Nothing Then
                    KVermelho = CorFace.R / 255
                    KVerde = CorFace.G / 255
                    KAzul = CorFace.B / 255
                Else
                    KVermelho = 0.8
                    KVerde = 0.1
                    KAzul = 0.3
                End If

                ' Find the unit surface normal. It is the
                ' same for all the light sources.
                ' NormalVector(Nx, Ny, Nz)
                N = getNormal()
                If N.getComprimento > ZERO Then
                    For Each light In light_Sources
                        ' **********************
                        ' * Diffuse Reflection *
                        ' **************************
                        ' Find the unit vector pointing towards the light.
                        If light.Enabled = False Then Continue For
                        If light.LightSourceType = eFonteLuz.INFINITO Then
                            L = New Vetor3D(SenG(light.Fi) * CosG(light.Theta), SenG(light.Fi) * SenG(light.Theta), CosG(light.Fi))
                        Else
                            L = Vetor3D.ConvertePonto2Vetor3D(TransformaPonto(0), light.tPos3D)
                            length = L.getComprimento
                            If L.getComprimento = 0 Then Return CorFace : Exit Function
                            L = L.getNormaliza
                        End If

                        ' See how intense to make the color.
                        NdotL = (Vetor3D.ProdutoEscalar(N, L))
                        'Debug.Print(NdotL)
                        ' The light does not hit the top of the
                        ' surface if NdotL <= 0.
                        If NdotL > 0 Then
                            If light.LightSourceType = eFonteLuz.INFINITO Then
                                distance_factor = 1
                            Else
                                distance_factor = (light.Rmin + light.Kdist) / (length + light.Kdist)
                            End If

                            diffuse_factor = Abs(NdotL) * distance_factor
                            R = R + light.Ir * KVermelho * diffuse_factor
                            G = G + light.Ig * KVerde * diffuse_factor
                            B = B + light.Ib * KAzul * diffuse_factor
                            ' ***********************
                            ' * Specular Reflection *
                            ' ***********************
                            ' Find the unit vector V from the surface
                            ' to the viewing position.
                            If NdotL > 0 Then
                                V = Vetor3D.ConvertePonto2Vetor3D(TransformaPonto(0), eyePos).getNormaliza
                                ' Find the mirror vector R.
                                MV = (2 * N * (Abs(NdotL)) - L)
                                RdotV = (Vetor3D.ProdutoEscalar(MV, V))
                                'Calculate the specular component.When n is small, Cosn(f)=(r.v)^n does not decrease as quickly when f increases. That allows more reflected light at
                                'larger angles f. This produces a larger, less intense highlight on the surface.
                                '0 If RdotV > 0 Then
                                specular_factor = EspecularK * (RdotV) ^ EspecularN
                                R = R + light.Ir * specular_factor
                                G = G + light.Ig * specular_factor
                                B = B + light.Ib * specular_factor
                                'End If
                            End If ' End if NdotL > 0 . . .
                        End If
                    Next light
                End If
                ' Add the ambient term.
                R = R + ambient_light * KVermelho
                G = G + ambient_light * KVerde
                B = B + ambient_light * KAzul

                ' Keep the color components <= 255.
                If R > 255 Then R = 255
                If G > 255 Then G = 255
                If B > 255 Then B = 255

                Return Color.FromArgb(CInt(R), CInt(G), CInt(B)) 'RGB(R, G, B)
            End Function


            ' Return the proper shade for this face
            ' due to the indicated light source.
            Public Function CorVertice(ByVal N As Vetor3D, _
                                       ByVal light_Sources As Collection, _
                                       ByVal ambient_light As Precisao, _
                                       ByVal eyePos As Ponto3D) As Color
                Dim L As Vetor3D
                'Dim length As Precisao
                Dim NdotL As Precisao
                Dim R As Precisao
                Dim G As Precisao
                Dim B As Precisao
                'Dim distance_factor As Precisao
                Dim diffuse_factor As Precisao
                Dim V As Vetor3D
                Dim MV As Vetor3D 'mirror vector
                Dim RdotV As Precisao
                Dim specular_factor As Precisao
                Dim AmbientKr As Precisao = 0.8
                Dim AmbientKg As Precisao = 0.3
                Dim AmbientKb As Precisao = 0.1
                Dim light As Luz3D
                If ambient_light > 255 Then ambient_light = 255
                If ambient_light < 0 Then ambient_light = 0
                ambient_light = 90
                EspecularN = 10
                'If faceColor <> Nothing Then
                '   diffuseKr = faceColor.R / 255
                '    diffuseKg = faceColor.G / 255
                '  diffuseKb = faceColor.B / 255
                '  Else
                KVermelho = 0.8
                KVerde = 0.1
                KAzul = 0.3
                ' End If
                ' Find the unit surface normal. It is the
                ' same for all the light sources.
                ' NormalVector(Nx, Ny, Nz)

                For Each light In light_Sources
                    ' **********************
                    ' * Diffuse Reflection *
                    ' **************************
                    ' Find the unit vector pointing towards the light.
                    L = New Vetor3D(SenG(light.Fi) * CosG(light.Theta), SenG(light.Fi) * SenG(light.Theta), CosG(light.Fi)) '1, 1, -1).Normalize '
                    ' L = VectorOf_2Points3D(Point(0), light.Pos3D).Normalize

                    'L = New Vector3D(0, 0, 1)
                    ' See how intense to make the color.
                    NdotL = (Vetor3D.ProdutoEscalar(N, L))
                    'Debug.Print(NdotL)
                    If NdotL > 0 Then

                        'Debug.Print(N.tostring)
                        ' Debug.Print(NdotL)
                        diffuse_factor = Abs(NdotL)
                        R = R + light.Ir * KVermelho * diffuse_factor
                        G = G + light.Ig * KVerde * diffuse_factor
                    End If
                    ' ***********************
                    ' * Specular Reflection *
                    ' ***********************
                    ' Find the unit vector V from the surface
                    ' to the viewing position.
                    If NdotL > 0 Then
                        V = Vetor3D.ConvertePonto2Vetor3D(TransformaPonto(0), eyePos).getNormaliza
                        ' Find the mirror vector R.
                        MV = (2 * N * (Abs(NdotL)) - L)
                        RdotV = (Vetor3D.ProdutoEscalar(MV, V))
                        ' Calculate the specular component.When n is small, Cosn(f)=(r.v)^n does not decrease as quickly when f increases. That allows more reflected light at
                        ' larger angles f. This produces a larger, less intense highlight on the surface.
                        If RdotV > 0 Then
                            specular_factor = EspecularK * (RdotV) ^ EspecularN
                            R = R + light.Ir * specular_factor
                            G = G + light.Ig * specular_factor
                            B = B + light.Ib * specular_factor
                        End If
                    End If
                    'End If ' End if NdotL > 0 . . .
                    ' End If
                Next light
                ' Add the ambient term.
                R = R + ambient_light * KVermelho
                G = G + ambient_light * KVerde
                B = B + ambient_light * KAzul

                ' Keep the color components <= 255.
                If R > 255 Then R = 255
                If G > 255 Then G = 255
                If B > 255 Then B = 255

                CorVertice = Color.FromArgb(CInt(R), CInt(G), CInt(B)) 'RGB(R, G, B)
            End Function

        End Structure


        'Apply matrix Transform to Vector3D (rotation etc)
        Public Function TransformaPonto(ByVal P As Ponto3D, ByVal T As Matriz4x4) As Ponto3D
            Dim Tx, Ty, Tz As Precisao
            Tx = P.x * T.M11 + P.y * T.M21 + P.z * T.M31 + T.M41
            Ty = P.x * T.M12 + P.y * T.M22 + P.z * T.M32 + T.M42
            Tz = P.x * T.M13 + P.y * T.M23 + P.z * T.M33 + T.M43
            Return New Ponto3D(Tx, Ty, Tz)
        End Function
        'Defines object containg different pages3Dtype/Planes in 3D





        Public Structure Objeto3D
            Dim NPaginas As Precisao
            Dim Pagina() As Pagina3D
            Dim PontoCentral As Ponto3D
            Dim NomeStr As String

            Dim iCod As Precisao   '1=Sphere / 2=SpObject-Tower / 4=Find Radiation / 8=Can Make Shadow
            '16=semiSphere / 32=Dome / 64=SunObject(=>Radius=SpValues(0))

            Dim CorBorda As Precisao  'color for Edges (-1 = NoEdge)
            Dim SpValues() As Precisao 'array for Special Values such as Temperature or G_Radiation
            Public drawMode As eDesenho

            Public Sub Transforma(ByVal T As Matriz4x4)
                If NPaginas = 0 Then Exit Sub
                Dim Pg As Pagina3D
                For Each Pg In Pagina
                    Pg.Transforma(T)
                Next
            End Sub

            Public Sub Escala(ByVal fatordeescala As Precisao)
                If fatordeescala = 0 Then Exit Sub
                Dim m As Matriz4x4
                m = MatrizEscala3D(fatordeescala, fatordeescala, fatordeescala)
                Transforma(m)
            End Sub

            Public Sub Rotaciona(ByVal theta_D As Precisao, ByVal Phi_D As Precisao)
                Dim m As Matriz4x4
                'Create 4x4matric to rotate object by theta(about z axis) and then phi(about x axis)
                m = MatrizRotacionaZ(theta_D) * MatrizRotacionaX(Phi_D)
                Transforma(m)
            End Sub

            Public Sub Projeta(ByVal camera As Camera3D)
                If NPaginas = 0 Then Exit Sub
                Dim i, j As Integer
                For i = 0 To CInt(NPaginas - 1)
                    ReDim Pagina(i).TransformaPonto(CInt(Pagina(i).NPontos))
                    For j = 0 To CInt(Pagina(i).NPontos)
                        Pagina(i).TransformaPonto(j) = camera.Projeta3DEm2D(Pagina(i).Ponto(j))
                    Next
                Next

            End Sub
        End Structure




        'Defines scene containg objects
        Public Structure TipoDeCena3D
            Dim NomeStr As String
            Dim Cam3D As Camera3D
            Dim PontoCentral As Ponto3D
            Dim NObjetos As Precisao
            Dim Obj3D() As Objeto3D
            Dim SpValues() As Single 'array for Special Values
            Public Sub Transforma(ByVal T As Matriz4x4)
                If NObjetos = 0 Then Exit Sub
                Dim Obj As Objeto3D
                For Each Obj In Obj3D
                    Obj.Transforma(T)
                Next
            End Sub

        End Structure





        Public Structure tCamera
            Public cFrom As Vetor3D
            Public cTo As Vetor3D
            Public cUp As Vetor3D
            Public Theta As Single
            Public fi As Single
            Public Zoom As Single
            Public NearPlane As Single
            Public FarPlane As Single
            Public Projection As eProjecao
        End Structure





        Structure Orientacao
            Public xPitch As Precisao  'theta
            Public yYaw As Precisao    '-fi
            Public zRoll As Precisao

            Public Overrides Function ToString() As String
                Return zRoll & vbTab & xPitch & vbTab & yYaw
            End Function

            Public Function OrientacaoSoma(ByVal A As Orientacao, ByVal B As Orientacao) As Orientacao
                OrientacaoSoma.zRoll = A.zRoll + B.zRoll
                OrientacaoSoma.xPitch = A.xPitch + B.xPitch
                OrientacaoSoma.yYaw = A.yYaw + B.yYaw
            End Function

            Public Function OrientacaoSubtrai(ByVal A As Orientacao, ByVal B As Orientacao) As Orientacao
                OrientacaoSubtrai.zRoll = A.zRoll - B.zRoll
                OrientacaoSubtrai.xPitch = A.xPitch - B.xPitch
                OrientacaoSubtrai.yYaw = A.yYaw - B.yYaw
            End Function

            Public Function OrientacaoInverte(ByVal A As Orientacao) As Orientacao
                OrientacaoInverte.zRoll = 1 / A.zRoll
                OrientacaoInverte.xPitch = 1 / A.xPitch
                OrientacaoInverte.yYaw = 1 / A.yYaw
            End Function


            Function OrientacaoAleatoria() As Orientacao
                Randomize()
                OrientacaoAleatoria.zRoll = (2 * Rnd() - 1) * PI
                OrientacaoAleatoria.xPitch = (2 * Rnd() - 1) * PI
                OrientacaoAleatoria.yYaw = (2 * Rnd() - 1) * PI
            End Function


            Function OrientacaoEscala(ByVal A As Orientacao, ByVal B As Precisao) As Orientacao
                OrientacaoEscala.zRoll = A.zRoll * B
                OrientacaoEscala.xPitch = A.xPitch * B
                OrientacaoEscala.yYaw = A.yYaw * B
            End Function


            Function OrientacaoEntrada(ByVal Roll As Precisao, ByVal Pitch As Precisao, ByVal Yaw As Precisao) As Orientacao
                OrientacaoEntrada.zRoll = zRoll
                OrientacaoEntrada.xPitch = xPitch
                OrientacaoEntrada.yYaw = yYaw
            End Function


            Function Vetor2Orientacao(ByVal A As Vetor3D) As Orientacao
                Vetor2Orientacao.yYaw = Acos(Vetor3D.AnguloVetor(New Vetor3D(A.X, 0, A.Z), New Vetor3D(0, 0, 1)))
                Vetor2Orientacao.xPitch = Acos(Vetor3D.AnguloVetor(New Vetor3D(A.X, A.Y, A.Z), New Vetor3D(A.X, 0, A.Z)))
                If A.X < 0 Then Vetor2Orientacao.yYaw = -Vetor2Orientacao.yYaw
                If A.Y > 0 Then Vetor2Orientacao.xPitch = -Vetor2Orientacao.xPitch
            End Function


            'Function Orientacao2Vetor(ByVal A As Orientacao) As Vetor3D
            '    ''INCOMPLETO
            '    Dim Transformacao As Matriz4x4 = Matriz4x4.Identidade
            'End Function


            Function OrientacaoZero() As Orientacao
                OrientacaoZero = OrientacaoEntrada(0, 0, 0)
            End Function

            'Compares 2 orientations
            Function OrientacaoCompara(ByVal A As Orientacao, ByVal B As Orientacao) As Boolean
                If A.zRoll = B.zRoll And A.xPitch = B.xPitch And A.yYaw = B.yYaw Then
                    Return True
                Else
                    Return False
                End If
            End Function
        End Structure



        'Structure TLight
        '    Public Color As Color
        '    Public BrightRange As Single
        '    Public DarkRange As Single
        '    Public Angles As Orientation
        '    Public Origin As Vetor3D
        '    Public Hotspot As Single
        '    Public Falloff As Single
        '    Public Enabled As Boolean
        '    Public Tag As String
        'End Structure


    End Module

End Namespace

