﻿Option Strict On

Imports BMath.Matriz


Imports BGeom.Ponto
Imports BGeom.Transformacoes

Imports Precisao = System.Double


Namespace Desenha3D

    Public Class Luz3D
        Public Pos3D As Ponto3D
        Public tPos3D As Ponto3D
        Public Radius As Precisao
        Public Theta, Fi As Precisao  'angles in degree
        Public Ir As Precisao
        Public Ig As Precisao
        Public Ib As Precisao
        'Public BrightRange As Single
        'Public DarkRange As Single
        Private sFalloff As Precisao = 0.5
        Private bEnabled As Boolean = True
        Public Kdist As Precisao '= 500
        Public Rmin As Precisao  '= 100
        Public LightSourceType As eFonteLuz = eFonteLuz.PONTO



        Public Property Falloff() As Precisao
            Get
                Return sFalloff
            End Get
            Set(ByVal value As Precisao)
                If value >= 0.9 Then sFalloff = 0.9
                If value <= 0 Then sFalloff = 0
                sFalloff = value
            End Set
        End Property



        Public Property Enabled() As Boolean
            Get
                Return bEnabled
            End Get
            Set(ByVal value As Boolean)
                bEnabled = value
            End Set
        End Property



        ' Set this light source's Kdist and Rmin values.
        Public Sub ScaleIntensityForDepth(ByVal faces As Collection)
            'To make the differences in intensity between different polygons noticeable, you also need to select kdist
            'carefully. You need to make (rmin + kdist) reasonably sized compared with the differences in the distances
            'from the polygons to the light source.
            'If kdist is very small, the difference between (rmin + kdist) and (rmax + kdist) is relatively large. In that case,
            'the intensity at polygons further from the light source is very small, and those polygons are too dark.
            'On the other hand, if kdist is very large, the difference between (rmin + kdist) and (rmax + kdist) is relatively
            'small. In that case, the intensity at polygons far from the light source is almost the same as at those that are
            'c'loser. This makes it difficult to notice the differences in the intensities.
            'Often you can get good results by making the intensity drop by roughly 50 percent across the scene. To do
            'that, you need to satisfy the equation:
            '(rmin + kdist) / (rmax + kdist) = 0.5
            'Solving for kdist gives:
            'Kdist = Rmax - 2 * Rmin
            Dim face As Pagina3D
            Dim Rmax As Precisao
            Dim r As Precisao
            Rmin = 1.0E+30
            Rmax = -1.0E+30
            For Each face In faces
                r = Ponto3D.DistanciaDePonto3D(face.Ponto(0), Pos3D)
                If Rmin > r Then Rmin = r
                If Rmax < r Then Rmax = r
            Next face
            Kdist = (sFalloff * Rmax - Rmin) / (1 - sFalloff) 'Rmax - 2 * Rmin '
            'If Rmin=Rmax then 
        End Sub



        Public Sub rotate(ByVal OrbitRadius As Precisao, ByVal dTheta As Precisao, ByVal dFi As Precisao)
            'Dim M As Matrix4x4
            Theta = Theta + dTheta
            Fi = Fi + dFi
            Pos3D = Ponto3D.PontoDeRThetaPhi(OrbitRadius, Theta, Fi)
            'Create 4x4matric to rotate Ligth by dtheta(about z axis) and then by dfi(about y axis)
            'M = Matrix4x4.MatrixRotateZ(dTheta) * Matrix4x4.MatrixRotateX(dFi)
            'Rotate camera by theta ,fi by transforming camPos and CamUppos accordingly, Save these in temp  Variables 
            'Pos3D = applytransform(Pos3D, M)
        End Sub



        Public Sub transform(ByVal m As Matriz4x4)
            Pos3D = AplicaTransformacao3D(Pos3D, m)
        End Sub

    End Class

End Namespace

