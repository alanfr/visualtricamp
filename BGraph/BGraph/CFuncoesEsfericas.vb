﻿Option Strict On

Imports BMath.Constantes
Imports BMath.Funcoes

Imports BGeom.Ponto

Imports System.Drawing
Imports Precisao = System.Double


Namespace Desenha3D


    Public Class FuncoesEsfericas

        Public Nome As String = "Definido pelo usuário"
        Public min_theta As Precisao = 0
        Public max_theta As Precisao = 2 * PI
        Public min_Phi As Precisao = 0
        Public max_Phi As Precisao = 2 * PI
        Public zLimMax As Precisao = -300
        Public zLimMin As Precisao = 300
        Public zRad As Precisao = 100
        Public thetaPoints As Precisao = 20
        Public phiPoints As Precisao = 20

        Public xText, yText, zText As String
        Public RText, GText, BText As String

        Public Points(,) As Ponto3D

        Public Sub Calcular(ByVal optshape As Precisao, _
                            ByVal ScaletoX As Precisao, _
                            ByVal ScaletoY As Precisao)

            '  Public Function SHAPES(ByVal optShape As Integer, Optional ByVal Lstep As Single = 18, Optional ByVal zAspect As Single = 1, Optional ByVal zRad As Single = 100) As Object3D
            Dim i As Precisao
            Dim j As Precisao
            Dim ztheta As Precisao
            Dim zphi As Precisao
            Dim zz As Precisao
            Dim zp As Precisao
            Dim z As Precisao
            Dim x As Precisao
            Dim y As Precisao
            Dim zzrad As Precisao
            ' Dim grxhi, gryhi As Single
            Dim zAspect As Precisao = 1
            Dim dTheta As Precisao
            Dim dPhi As Precisao
            'dtr# degree to radian
            thetaPoints = 20
            phiPoints = 20
            ' grxhi = (1 + 360 \ thetaPoints) : gryhi = (1 + 360 \ phiPoints)

            dTheta = 360 / (thetaPoints - 1)
            dPhi = 360 / (phiPoints - 1)

            zRad = 100 ' Basic radius
            'Dim Shape3d(,) As Point3D
            ReDim Points(CInt(thetaPoints), CInt(phiPoints))
            j = -1
            Select Case optshape
                Case 0
                    Nome = "ELLIPSOID"
                    zText = "zAspect * zRad * (Cos(Theta / 2))"
                    xText = " zRad * Sin(Theta / 2) * Cos(Phi)"
                    yText = " = zRad * Sin(Theta / 2) * Sin(Phi)"
                Case 1
                    Nome = "CYLINDER"
                    zText = " (Theta - 180) / 2 "
                    xText = "zRad * Cos(Phi)"
                    yText = "zAspect * zRad * Sin(Phi)"
                Case 2
                    Nome = "TWO ELLIPSOIDS"
                    zText = "(Theta - 180) / 2"
                    xText = "zAspect * zRad * Sin(Theta) * Cos(Phi)"
                    yText = "zRad * Sin(Theta) * Sin(Phi)"
                Case 3
                    Nome = "TWO HALF & ONE WHOLE SPHERE,  Trophy"
                    zText = "zRad * Cos(Theta / 2)"
                    xText = "zRad * Cos(Theta / 2) * Cos(Phi)"
                    yText = " zAspect * (Theta - 180) / 2 * Sin(Phi)"
                Case 4
                    Nome = "2 LINEAR CONES"
                    zText = "(Theta - 180) / 2"
                    xText = "zRad * z / 100 * Cos(Phi)"
                    yText = "zAspect * Name =  * Sin(Phi)"
                Case 5
                    Nome = " WINE GLASS & REFLECTION"
                    zText = "(Theta - 180) / 2 "
                    '  Thetarad = zRad * Sqrt(Abs(z)) / 20
                    ' If Thetarad = 0 Then
                    'Thetarad = zRad
                    ' ElseIf Thetarad < 30 Then
                    'Thetarad = 0
                    ' End If
                    xText = " zRad * Sqrt(Abs(z)) / 20 * Cos(Phi)"
                    yText = "zAspect *  zRad * Sqrt(Abs(z)) / 20 * Sin(Phi)"
                Case 6   ' 
                    Nome = "WINE GLASS"
                    zText = "(Theta - 180) / 2" 'Make origin at zero
                    ' If z >= 0 Then
                    'Thetarad = zRad * Sqrt(z) / 15
                    ' ElseIf z > -80 Then
                    ' Thetarad = 2
                    ' Else
                    ' Thetarad = zRad / 2
                    ' End If
                    xText = "Thetarad * Cos(Phi)"
                    yText = "zAspect * Thetarad * Sin(Phi)"
                Case 7   ' 
                    Nome = "HYPERBOLOID"
                    zText = "(Theta - 180) / 2" 'Make origin at zero
                    'Thetarad = 25 + zRad * z * z / 10000
                    xText = "Thetarad * Cos(Phi)"
                    yText = "zAspect * Thetarad * Sin(Phi)"
                Case 8   ' 
                    Nome = "FLAT SURFACE"
                    xText = "15 * (i - CInt(thetaPoints / 2))"
                    zText = "-20" ' * (j - 10)
                    yText = "15 * (j - CInt(phiPoints / 2))" ' 25 * Cos_D(((Sqrt(Shape3d(i, j).x * Shape3d(i, j).x + Shape3d(i, j).z * Shape3d(i, j).z) / 155) * 800)) ' 8 * (j - 10)

                Case 9   '
                    Nome = "SINE WAVE"

                Case 10  '
                    Nome = "BENDY BLADES"
                    zText = "zAspect * zRad * Cos(Theta) ^ 2"
                    xText = "zRad * Sin(Theta) * Cos(Phi) / 2"
                    yText = "zRad * Sin(Theta)"
                Case 11   '
                    Nome = "PARABLOID"
                    zText = "zRad + -zAspect * zRad * Cos(Theta / 4) * Cos(Theta / 4)"
                    xText = "zRad * Sin(Theta / 4) * Cos(Phi)"
                    yText = "zRad * Sin(Theta / 4) * Sin(Phi)"
                Case 12   '
                    Nome = "SEMI-SPHERE"
                    zText = "zRad + -zAspect * zRad * (Cos(Theta / 4))"
                    xText = "zRad * Sin(Theta / 4) * Cos(Phi)"
                    yText = "zRad * Sin(Theta / 4) * Sin(Phi)"
                Case 13  ' 
                    Nome = "TORUS"
                    zText = "zAspect * zRad * Sin(Phi) / 2"
                    xText = " zRad * Cos(Theta) * (1 + Cos(Phi) / 2)"
                    yText = "zRad * Sin(Theta) * (1 + Cos(Phi) / 2)"
                Case 14  '
                    Nome = "WHEEL"
                    zText = " -zAspect * zRad * Cos(Theta / 2) * Sin(Theta / 2) ^ 3"
                    xText = "zRad * Sin(Theta / 2) * Cos(Phi)"
                    yText = "zRad * Sin(Theta / 2) * Sin(Phi)"
                Case 15 '
                    Nome = "PI"
                    zText = "zAspect * zRad * Cos(Theta) * Sin(Theta) ^ 3"
                    xText = "zRad * Sin(Theta) * Cos(Phi)"
                    yText = "zRad * Sin(Theta) * Sin(Phi)"

            End Select
            '  yeye = 150
            For ztheta = 0 To 360 + dTheta Step dTheta
                i = -1
                j = j + 1
                zz = ztheta * RADIANO_2_GRAU
                For zphi = 0 To 360 + dPhi Step dPhi
                    i = i + 1
                    zp = zphi * RADIANO_2_GRAU

                    Select Case optshape
                        Case 0   ' ELLIPSOID
                            z = zAspect * zRad * (Cos(zz / 2))
                            x = zRad * Sen(zz / 2) * Cos(zp)
                            y = zRad * Sen(zz / 2) * Sen(zp)
                        Case 1   ' CYLINDER
                            z = (ztheta - 180) / 2 'Make origin at zero
                            x = 0.6 * zRad * Cos(zp)
                            y = 0.6 * zAspect * zRad * Sen(zp)
                        Case 2   ' TWO ELLIPSOIDS
                            z = (ztheta - 180) / 2 'Make origin at zero
                            x = zAspect * zRad * Sen(zz) * Cos(zp)
                            y = zRad * Sen(zz) * Sen(zp)
                        Case 3   ' TWO HALF & ONE WHOLE SPHERE,  Trophy
                            z = (ztheta - 180) / 2 'Make origin at zero
                            zzrad = zRad * Cos(zz / 2)
                            x = zzrad * Cos(zp)
                            y = zAspect * zzrad * Sen(zp)
                        Case 4   ' 2 LINEAR CONES
                            z = (ztheta - 180) / 2 'Make origin at zero
                            zzrad = zRad * z / 100
                            x = zzrad * Cos(zp)
                            y = zAspect * zzrad * Sen(zp)
                        Case 5   ' WINE GLASS & REFLECTION
                            z = (ztheta - 180) / 2 'Make origin at zero
                            zzrad = zRad * Raiz(Abs(z)) / 20
                            If zzrad = 0 Then
                                zzrad = zRad
                            ElseIf zzrad < 30 Then
                                zzrad = 0
                            End If
                            x = zzrad * Cos(zp)
                            y = zAspect * zzrad * Sen(zp)
                        Case 6   ' WINE GLASS
                            z = (ztheta - 180) / 2 'Make origin at zero
                            If z >= 0 Then
                                zzrad = zRad * Raiz(z) / 15
                            ElseIf z > -80 Then
                                zzrad = 2
                            Else
                                zzrad = zRad / 2
                            End If
                            x = zzrad * Cos(zp)
                            y = zAspect * zzrad * Sen(zp)
                        Case 7   ' HYPERBOLOID
                            z = (ztheta - 180) / 2 'Make origin at zero
                            zzrad = 25 + zRad * z * z / 10000
                            x = zzrad * Cos(zp)
                            y = zAspect * zzrad * Sen(zp)
                        Case 8   ' FLAT SURFACE
                            For j = 0 To phiPoints
                                For i = 0 To thetaPoints
                                    Points(CInt(i), CInt(j)).X = 15 * (i - CInt(thetaPoints / 2))
                                    Points(CInt(i), CInt(j)).Z = -20 ' * (j - 10)
                                    Points(CInt(i), CInt(j)).Y = 15 * (j - CInt(phiPoints / 2)) ' 25 * Cos_D(((Sqrt(Shape3d(i, j).x * Shape3d(i, j).x + Shape3d(i, j).z * Shape3d(i, j).z) / 155) * 800)) ' 8 * (j - 10)
                                    Points(CInt(i), CInt(j)).Cor = CorInterpola(Color.FromArgb(180, 90, 210), Color.FromArgb(226, 250, 255), 0.1 * zphi / (360 + dPhi) + 0.8 * Abs(ztheta - 180) / (180))
                                Next i
                            Next j
                            Exit For
                        Case 9   ' SINE WAVE
                            z = zAspect / 2 * zRad * (Cos(zz / 2))
                            x = 0.63 * zRad * Sen(zz / 2) * Cos(zp)
                            y = 1.2 * zRad * Sen(zz / 2) * Sen(zp)
                        Case 10  ' BENDY BLADES
                            z = zAspect * zRad * Cos(zz / 2) ^ 2
                            x = zRad * Sen(zz / 2) * Cos(zp) / 2
                            y = zRad * Sen(zp)
                        Case 11   ' PARABLOID
                            z = zRad + -zAspect * zRad * Cos(zz / 4) * Cos(zz / 4)
                            x = zRad * Sen(zz / 4) * Cos(zp)
                            y = zRad * Sen(zz / 4) * Sen(zp)
                        Case 12   ' SEMI-SPHERE
                            z = zRad + -zAspect * zRad * (Cos(zz / 4))
                            x = zRad * Sen(zz / 4) * Cos(zp)
                            y = zRad * Sen(zz / 4) * Sen(zp)
                        Case 13  ' TORUS
                            z = zAspect * zRad * Sen(zp) / 2
                            x = zRad * Cos(zz) * (1 + Cos(zp) / 2)
                            y = zRad * Sen(zz) * (1 + Cos(zp) / 2)
                        Case 14  ' WHEEL
                            z = -zAspect * zRad * Cos(zz / 2) * Sen(zz / 2) ^ 3
                            x = zRad * Sen(zz / 2) * Cos(zp)
                            y = zRad * Sen(zz / 2) * Sen(zp)
                        Case 15 'pi
                            z = zAspect * zRad * Cos(zz) * Sen(zz) ^ 3
                            x = zRad * Sen(zz) * Cos(zp)
                            y = zRad * Sen(zz) * Sen(zp)
                    End Select

                    Points(CInt(i), CInt(j)).X = x
                    Points(CInt(i), CInt(j)).Y = y
                    Points(CInt(i), CInt(j)).Z = z
                    Points(CInt(i), CInt(j)).Cor = CorInterpola(Color.FromArgb(180, 90, 210), Color.FromArgb(226, 250, 255), 0.1 * zphi / (360 + dPhi) + 0.8 * Abs(ztheta - 180) / (180))

                Next zphi
                If optshape = 8 Then Exit For
            Next ztheta

        End Sub





        Public Function getObject3D(ByVal optCode As Precisao, ByVal ScaleX As Precisao, ByVal ScaleY As Precisao) As Objeto3D
            Calcular(optCode, ScaleX, ScaleY)
            Dim Obj As New Objeto3D
            With Obj
                Obj.NPaginas = (thetaPoints - 1) * (phiPoints - 1)
                ReDim Obj.Pagina(CInt(Obj.NPaginas - 1))
                .CorBorda = Color.White.ToArgb
                For i = 0 To thetaPoints - 2
                    For j = 0 To phiPoints - 2
                        With .Pagina(CInt(i * (thetaPoints - 1) + j))
                            '.Edgecolor = Color.Green
                            '.faceColor = Color.Black 'IIf(i = 0, Color.Red, Color.FromArgb(150, 255 - Math.Abs(z), (Math.Abs(2 * i)) Mod 255))
                            '.filled = True
                            .AdicionaPonto(Points(CInt(j), CInt(i)))
                            .AdicionaPonto(Points(CInt(j + 1), CInt(i)))
                            .AdicionaPonto(Points(CInt(j + 1), CInt(i + 1)))
                            .AdicionaPonto(Points(CInt(j), CInt(i + 1)))
                        End With
                    Next
                Next
            End With
            Return Obj
        End Function


    End Class

End Namespace

