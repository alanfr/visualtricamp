﻿Option Strict On

Imports BMath.Funcoes
Imports BMath.Matriz
Imports BMath.Constantes

Imports BGeom.Vetor
Imports BGeom.Ponto
Imports BGeom.Transformacoes

Imports System.Drawing

Imports Precisao = System.Double


Namespace Desenha3D '3DDrawing


    Public Module Desenha


#Region "Transformacoes"

        'Retorna a estrutura TSenCosTresAngulos com os angulos diretores.
        'Retorna a estrutura para os eixos em 3D
        Public Function SenCosTresAngulos(ByVal ax As Precisao, _
                                          ByVal ay As Precisao, _
                                          ByVal Az As Precisao, _
                                          Optional ByVal Radiano As Boolean = False) As TSenCosTresAngulos

            Dim tmp As TSenCosTresAngulos

            If (Radiano = False) Then
                'Retorna o valor em graus
                With tmp
                    .SenX = SenG(ax)
                    .CosX = CosG(ax)
                    .SenY = SenG(ay)
                    .CosY = CosG(ay)
                    .SenZ = SenG(Az)
                    .CosZ = CosG(Az)
                End With
            ElseIf (Radiano = True) Then
                'Retorna o valor em radianos
                With tmp
                    .SenX = Sen(ax)
                    .CosX = Cos(ax)
                    .SenY = Sen(ay)
                    .CosY = Cos(ay)
                    .SenZ = Sen(Az)
                    .CosZ = Cos(Az)
                End With
            End If

            Return tmp
        End Function



        'Rotaciona PV de um ponto em 3D por um angulo (Primeiro em relação ao eixo X e depois em relação a y)
        Public Function RotacionaPonto3D(ByRef p1 As Ponto3D, ByVal SenCos3A As TSenCosTresAngulos) As Ponto3D
            'SelfMode=True => return none, p1 rotate itself (answer return in p1 already)
            'this sub USE THIS ORDER: Ax then Ay then Az
            'IT IS DIFFERENT THAT ROTATE A POINT OR OBJECT (FISRT ABOUT x AXIS THEN ABOUT y,Z) OR IN OTHER ORDER!
            'public var 'DoRotateZY' -> if true=> rotate about z and then y. (no rotation about x)
            Dim tmp As New Ponto3D
            Dim x As Precisao, y As Precisao, z As Precisao
            Dim x2 As Precisao, y2 As Precisao, z2 As Precisao

            'With p1
            '    x = .X : y = .Y : z = .Z
            'End With

            With tmp
                x2 = x
                y2 = y * SenCos3A.CosX - z * SenCos3A.SenX
                z2 = y * SenCos3A.SenX + z * SenCos3A.CosX
                x2 = z2 * SenCos3A.SenY + x * SenCos3A.CosY

                tmp.X = x2 * SenCos3A.CosZ - y2 * SenCos3A.SenZ
                tmp.Y = x2 * SenCos3A.SenZ + y2 * SenCos3A.CosZ
                tmp.Z = z2 * SenCos3A.CosY - x * SenCos3A.SenY 'x not x2
            End With

            Return tmp
        End Function



        'rotates page by specific angle (FISRT ABOUT x AXIS THEN ABOUT y) in 3D (using vector croos product)
        Public Function getRotacionaPagina(ByVal Pg As Pagina3D, ByVal eixo As eEixo, ByVal Ang_D As Precisao) As Pagina3D
            Dim M As Matriz4x4
            Dim pagina As Pagina3D
            pagina = Pg

            Select Case eixo
                Case eEixo.EIXO_X
                    M = MatrizRotacionaX(Ang_D)
                Case eEixo.EIXO_Y
                    M = MatrizRotacionaY(Ang_D)
                Case eEixo.EIXO_Z
                    M = MatrizRotacionaZ(Ang_D)
                Case Else
                    M = Matriz4x4.Identidade
            End Select

            pagina.Transforma(M)
            Return pagina
        End Function



        Public Function AreaPagina(ByVal pagina1 As Pagina3D) As Precisao
            'Return area of a page (polygon) by dividing it to n-2 threeangles
            Dim i As Precisao = 0.0
            Dim s As Precisao = 0.0

            With pagina1
                For i = 2 To .NPontos - 1
                    s = s + Ponto3D.getAreaTriangulo(.Ponto(1), .Ponto(CInt(i)), .Ponto(CInt(i + 1)))
                Next
            End With

            Return s
        End Function



        Public Function TransferePonto3D(ByVal p1 As Ponto3D, ByVal v As Vetor3D) As Ponto3D
            Dim ponto As New Ponto3D

            With ponto
                .X = p1.X + v.X
                .Y = p1.Y + v.Y
                .Z = p1.Z + v.Z
            End With

            Return ponto
        End Function



        Public Function TransferePagina3D(ByVal pg1 As Pagina3D, ByVal v As Vetor3D) As Pagina3D
            Dim n As Precisao, i As Precisao
            Dim tmp As New Pagina3D

            With tmp
                n = pg1.NPontos
                .CorFace = pg1.CorFace
                .CorBorda = pg1.CorBorda
                .NPontos = n
                ReDim .Ponto(CInt(n))
                For i = 0 To n
                    .Ponto(CInt(i)) = TransferePonto3D(pg1.Ponto(CInt(i)), v)
                Next
            End With

            Return tmp

        End Function




        Public Function TransfereObjeto3D(ByVal obj1 As Objeto3D, ByVal v As Vetor3D) As Objeto3D
            Dim i As Precisao
            Dim rv As Objeto3D = obj1

            With rv
                .PontoCentral = TransferePonto3D(.PontoCentral, v)
                For i = 0 To .NPaginas - 1
                    .Pagina(CInt(i)) = TransferePagina3D(.Pagina(CInt(i)), v)
                Next
            End With

            Return rv
        End Function


        Private Sub AdicionaPagina2Obj(ByVal pg1 As Pagina3D, ByRef obj1 As Objeto3D)
            'add a page to end of pages of obj1
            Dim n As Precisao
            With obj1
                n = .NPaginas
                n = n + 1
                .NPaginas = n
                ReDim Preserve .Pagina(CInt(n - 1))
                .Pagina(CInt(n - 1)) = pg1
            End With
        End Sub


        Public Function AdicionaObjeto3D(ByVal obj1 As Objeto3D, ByVal obj2 As Objeto3D) As Objeto3D
            Dim tmp As New Objeto3D
            tmp = obj1
            For i = 0 To obj2.NPaginas - 1
                AdicionaPagina2Obj(obj2.Pagina(CInt(i)), tmp)
            Next

            Return tmp
        End Function
#End Region




        'SHAPES
        Public Function FORMAS(ByVal optShape As Precisao, _
                               Optional ByVal Lstep As Precisao = 18, _
                               Optional ByVal zAspect As Precisao = 1, _
                               Optional ByVal zRad As Precisao = 100) As Objeto3D


            Dim i As Precisao, j As Precisao
            Dim ztheta As Precisao
            Dim zphi As Precisao
            Dim zz As Precisao
            Dim zp As Precisao
            Dim z As Precisao ', x As Precisao, y As Precisao
            Dim zzrad As Precisao
            Dim grxhi, gryhi As Precisao

            'dtr# degree to radian
            grxhi = (1 + 360 \ CLng(Lstep)) : gryhi = (1 + 360 \ CLng(Lstep))

            'Dim Shape3d(,) As Point3D
            Dim Shape3d(CInt(grxhi), CInt(gryhi)) As Ponto3D
            j = -1
            ' Basic radius
            zRad = 100

            zAspect = 1
            '  yeye = 150

            For ztheta = 0 To 360 Step Lstep
                i = -1
                j = j + 1
                zz = ztheta * RADIANO_2_GRAU
                For zphi = 0 To 360 Step Lstep
                    i = i + 1
                    zp = zphi * RADIANO_2_GRAU

                    With Shape3d(CInt(i), CInt(j))
                        Select Case optShape
                            Case 0   ' ELLIPSOID
                                .Z = zAspect * zRad * (Cos(zz / 2))
                                .X = zRad * Sen(zz / 2) * Cos(zp)
                                .Y = zRad * Sen(zz / 2) * Sen(zp)
                            Case 1   ' CYLINDER
                                .Z = (ztheta - 180) / 2 'Make origin at zero
                                .X = zRad * Cos(zp)
                                .Y = zAspect * zRad * Sen(zp)
                            Case 2   ' TWO ELLIPSOIDS
                                .Z = (ztheta - 180) / 2 'Make origin at zero
                                .X = zAspect * zRad * Sen(zz) * Cos(zp)
                                .Y = zRad * Sen(zz) * Sen(zp)
                            Case 3   ' TWO HALF & ONE WHOLE SPHERE,  Trophy
                                .Z = (ztheta - 180) / 2 'Make origin at zero
                                zzrad = zRad * Cos(zz / 2)
                                .X = zzrad * Cos(zp)
                                .Y = zAspect * zzrad * Sen(zp)
                            Case 4   ' 2 LINEAR CONES
                                .Z = (ztheta - 180) / 2 'Make origin at zero
                                zzrad = zRad * z / 100
                                .X = zzrad * Cos(zp)
                                .Y = zAspect * zzrad * Sen(zp)
                                'Case 5   ' WINE GLASS & REFLECTION
                                '    .Z = (ztheta - 180) / 2 'Make origin at zero
                                '    zzrad = zRad * Raiz(Abs(z)) / 20
                                '    If zzrad = 0 Then
                                '        zzrad = zRad
                                '    ElseIf zzrad < 30 Then
                                '        zzrad = 0
                                '    End If
                                '    .X = zzrad * Cos(zp)
                                '    .Y = zAspect * zzrad * Sen(zp)
                            Case 6   ' WINE GLASS
                                .Z = (ztheta - 180) / 2 'Make origin at zero
                                If z >= 0 Then
                                    zzrad = zRad * Raiz(z) / 15
                                ElseIf z > -80 Then
                                    zzrad = 2
                                Else
                                    zzrad = zRad / 2
                                End If
                                .X = zzrad * Cos(zp)
                                .Y = zAspect * zzrad * Sen(zp)
                            Case 7   ' HYPERBOLOID
                                .Z = (ztheta - 180) / 2 'Make origin at zero
                                zzrad = 25 + zRad * z * z / 10000
                                .X = zzrad * Cos(zp)
                                .Y = zAspect * zzrad * Sen(zp)
                            Case 8   ' FLAT SURFACE
                                For j = 0 To gryhi
                                    For i = 0 To grxhi
                                        .X = 15 * (i - 10)
                                        .Z = -20 ' * (j - 10)
                                        .Y = 15 * (j - 10) ' 25 * Cos_D(((Sqrt(Shape3d(i, j).x * Shape3d(i, j).x + Shape3d(i, j).z * Shape3d(i, j).z) / 155) * 800)) ' 8 * (j - 10)
                                    Next i
                                Next j
                                Exit For
                            Case 9   ' SINE WAVE
                                For j = 0 To gryhi
                                    For i = 0 To grxhi
                                        .X = 20 * (i - 8)
                                        .Y = 20 * (j - 8) ' 25 * Cos_D(((Sqrt(Shape3d(i, j).x * Shape3d(i, j).x + Shape3d(i, j).z * Shape3d(i, j).z) / 155) * 800)) ' 8 * (j - 10)
                                        .Z = 20 * Sen(Raiz((Shape3d(CInt(i), CInt(j)).X / 6) ^ 2 + (Shape3d(CInt(i), CInt(j)).Y / 6) ^ 2)) '10 * Sin(Abs(Shape3d(i, j).x) + Abs(Shape3d(i, j).y))
                                    Next i
                                Next j
                                Exit For
                            Case 10  ' BENDY BLADES
                                .Z = zAspect * zRad * Cos(zz) ^ 2
                                .X = zRad * Sen(zz) * Cos(zp) / 2
                                .Y = zRad * Sen(zz)
                            Case 11   ' PARABLOID
                                .Z = zRad + -zAspect * zRad * Cos(zz / 4) * Cos(zz / 4)
                                .X = zRad * Sen(zz / 4) * Cos(zp)
                                .Y = zRad * Sen(zz / 4) * Sen(zp)
                            Case 12   ' SEMI-SPHERE
                                .Z = zRad + -zAspect * zRad * (Cos(zz / 4))
                                .X = zRad * Sen(zz / 4) * Cos(zp)
                                .Y = zRad * Sen(zz / 4) * Sen(zp)
                            Case 13  ' TORUS
                                .Z = zAspect * zRad * Sen(zp) / 2
                                .X = zRad * Cos(zz) * (1 + Cos(zp) / 2)
                                .Y = zRad * Sen(zz) * (1 + Cos(zp) / 2)
                            Case 14  ' WHEEL
                                .Z = -zAspect * zRad * Cos(zz / 2) * Sen(zz / 2) ^ 3
                                .X = zRad * Sen(zz / 2) * Cos(zp)
                                .Y = zRad * Sen(zz / 2) * Sen(zp)
                            Case 15 'pi
                                .Z = zAspect * zRad * Cos(zz) * Sen(zz) ^ 3
                                .X = zRad * Sen(zz) * Cos(zp)
                                .Y = zRad * Sen(zz) * Sen(zp)

                        End Select
                    End With


                    'Shape3d(i, j).X = x
                    'Shape3d(i, j).Y = y
                    'Shape3d(i, j).Z = z

                Next zphi
                If optShape = 8 Then Exit For
            Next ztheta

            Dim Obj As New Objeto3D
            With Obj
                Obj.NPaginas = (grxhi - 1) * (gryhi - 1)
                ReDim Obj.Pagina(CInt(Obj.NPaginas - 1))
                .CorBorda = System.Drawing.Color.White.ToArgb
                For i = 0 To grxhi - 2
                    For j = 0 To gryhi - 2
                        With .Pagina(CInt(i * (gryhi - 1) + j))
                            '.Edgecolor = Color.Green
                            '.faceColor = Color.Black 'IIf(i = 0, Color.Red, Color.FromArgb(150, 255 - Math.Abs(z), (Math.Abs(2 * i)) Mod 255))
                            '.filled = True
                            .AdicionaPonto(Shape3d(CInt(j), CInt(i)))
                            .AdicionaPonto(Shape3d(CInt(j + 1), CInt(i)))
                            .AdicionaPonto(Shape3d(CInt(j + 1), CInt(i + 1)))
                            .AdicionaPonto(Shape3d(CInt(j), CInt(i + 1)))
                        End With
                    Next
                Next
            End With

            Return Obj

        End Function



        Public Function DesenhaReferencia3D(ByVal OptCode As Precisao) As Objeto3D

            'Dim tempS() As String
            Dim grxhi, gryhi As Precisao
            Dim zlim, ploti As Precisao
            Dim Svrx(,), Svry(,), Svrz(,) As Precisao
            Dim shape3d(,) As Ponto3D
            Dim cString As String = Nothing
            Dim scx, scy As Precisao
            'Dim ilx, ihx, ily, ihy, ilz, ihz As Precisao
            Dim zpl, zpH As Precisao
            Dim scaleX, scaleY As Precisao ', sCalez As Precisao
            'zpH = 0.8 * picDisplay.ScaleWidth
            Dim minX, miny, maxX, maxY As Precisao
            'Dim YH(0 To 0), YL(0 To 0), XH(0 To 0), XL(0 To 0), ZL(0 To 0), PD(0 To 0)
            Dim X, Y As Precisao

            zlim = 400.0
            ploti = 10.0
            grxhi = 20.0
            gryhi = 20.0
            minX = 0.0
            maxX = 6.0
            miny = 0.0
            maxY = 6.0

            'Original function points
            ReDim Svrx(CInt(grxhi), CInt(gryhi))
            ReDim Svry(CInt(grxhi), CInt(gryhi))
            ReDim Svrz(CInt(grxhi), CInt(gryhi))
            ReDim shape3d(CInt(grxhi), CInt(gryhi))

            'Scale factors for converting grid points to x,y values
            scx = (maxX - minX) / (grxhi - 1)
            scy = (maxY - miny) / (gryhi - 1)
            zpl = 1.8 '* Me.Width / (grxhi - 1)
            zpH = 1.2 ' * Me.Width
            scaleX = 20.5 ' * Me.Width / (maxX - minX)
            scaleY = 20.5 '* Canvas3D.Height / (maxY - miny)
            ' Dim scfx, scfy As Single
            ' scfx = (zpH - zpl) / (Xmax - Xmin)
            For J = 0 To gryhi
                For I = 0 To grxhi
                    Svrx(CInt(I), CInt(J)) = (scx * (I - 1) + minX) '* 200
                    Svry(CInt(I), CInt(J)) = (scy * (J - 1) + miny) '* 20
                    shape3d(CInt(I), CInt(J)).X = (scx * (I - 1) + minX) * scaleX
                    shape3d(CInt(I), CInt(J)).Y = (scy * (J - 1) + miny) * scaleY
                Next I
            Next J
            Dim eString As String
            'Fill svrz(i,j) with Func(x,y)
            For J = 0 To gryhi
                For I = 0 To grxhi
                    X = Svrx(CInt(I), CInt(J))
                    Y = Svry(CInt(I), CInt(J))

                    Select Case OptCode
                        Case 0 'Put Perspec On & roll into ball  & do Y-rot only
                            Svrz(CInt(I), CInt(J)) = (X) ^ 2 + (Y) ^ 2
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 6 - 60 '* 20 ' scaleX
                        Case 1 'Saddle
                            Svrz(CInt(I), CInt(J)) = X ^ 2 - Y ^ 2
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 5 ' scaleX
                        Case 2 'Balloon Perspec On & Y-rot only
                            Svrz(CInt(I), CInt(J)) = (X ^ 2 + Y ^ 2) ^ 2
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 3 ' scaleX
                        Case 3 'Butterfly 1
                            Svrz(CInt(I), CInt(J)) = Raiz(Abs((X - 6) ^ 2 - (Y - 6) ^ 2))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 6 ' scaleX
                        Case 4 'Butterfly 2
                            Svrz(CInt(I), CInt(J)) = (X + Y) ^ 2
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 35 ' scaleX
                        Case 5 'Bent strip 1
                            Svrz(CInt(I), CInt(J)) = X ^ 2 + Abs(Y) ^ 0.5
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 9 ' scaleX
                        Case 6 'Bent strip 2
                            Svrz(CInt(I), CInt(J)) = Y ^ 4 - Y ^ 2 - X ^ 2
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 8 ' scaleX
                        Case 7 'cornered cone
                            Svrz(CInt(I), CInt(J)) = Raiz(X ^ 2 + Y ^ 2)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 15 ' scaleX
                        Case 8 'Cubic twisted strip
                            Svrz(CInt(I), CInt(J)) = X ^ 3 + 2 * X * Y + Y ^ 2
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 6 ' scaleX
                        Case 9 'Edge spike
                            Svrz(CInt(I), CInt(J)) = Exp(-(X ^ 2 + Y ^ 2))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 100 ' scaleX
                        Case 10 'Center spike 1
                            Svrz(CInt(I), CInt(J)) = 2 * Sen(Raiz((X / 4) ^ 2 + (Y / 4) ^ 2))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 50 ' scaleX
                        Case 11 'Center spike 2
                            Svrz(CInt(I), CInt(J)) = Tanh((2 * X) ^ 2 + (2 * Y) ^ 2)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 120 ' scaleX
                        Case 12 'Concentric rings
                            Svrz(CInt(I), CInt(J)) = Sen(Raiz(X ^ 2 + Y ^ 2))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 20 ' scaleX
                        Case 13 'Trig hills Egg holder
                            Svrz(CInt(I), CInt(J)) = Sen(X) + Cos(Y)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 12 ' scaleX
                        Case 14 'ledge
                            Svrz(CInt(I), CInt(J)) = Senh(X + Y)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 12 ' scaleX
                        Case 15 ' Bat wing
                            Svrz(CInt(I), CInt(J)) = Cosh(X + Y)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 12 ' scaleX
                        Case 16 'Flying sheet
                            Svrz(CInt(I), CInt(J)) = Tanh(X + Y)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 50 ' scaleX
                        Case 17 'Stingray
                            Svrz(CInt(I), CInt(J)) = Raiz(Abs(Tanh(X + Y)))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 120 ' scaleX
                        Case 18 'Sheet with bent corner
                            Svrz(CInt(I), CInt(J)) = 2 * Acos(X / 22 + Y / 22)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 80 - 100 ' scaleX
                        Case 19 ' Log sheet(ignore error)
                            Svrz(CInt(I), CInt(J)) = CDbl(IIf(X + Y > 0, Log10(X + Y), 0))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 12 ' scaleX
                        Case 20 'Seat
                            Svrz(CInt(I), CInt(J)) = Senh(X) + Cosh(Y)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) '* 4 ' scaleX
                        Case 21 'Corrugated sheet
                            Svrz(CInt(I), CInt(J)) = (X - Sen(X)) + (Y - Cos(X))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 20 ' scaleX
                        Case 22 'Shaver head
                            Svrz(CInt(I), CInt(J)) = Cos(X) ^ 3 + Sen(X) ^ 3
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 20 ' scaleX
                        Case 23 'Wicker chairs
                            Svrz(CInt(I), CInt(J)) = (X ^ 3 + Y ^ 3) - 12 * X * Y
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 0.6 ' scaleX
                        Case 24 'Escalators, 
                            Svrz(CInt(I), CInt(J)) = X * Cos(Y)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 10 ' scaleX
                        Case 25 '3D corner
                            Svrz(CInt(I), CInt(J)) = Exp(-2 * X * Y)
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 30 ' scaleX
                            shape3d(CInt(I), CInt(J)).X = shape3d(CInt(I), CInt(J)).X + 180
                            shape3d(CInt(I), CInt(J)).Y = shape3d(CInt(I), CInt(J)).Y + 140
                        Case 26 ' Spiked waves
                            Svrz(CInt(I), CInt(J)) = Exp(-Abs(Cos(X + Y)))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 50 ' scaleX
                            shape3d(CInt(I), CInt(J)).X = shape3d(CInt(I), CInt(J)).X + 260
                            shape3d(CInt(I), CInt(J)).Y = shape3d(CInt(I), CInt(J)).Y + 190
                        Case 27 'Domed surface
                            Svrz(CInt(I), CInt(J)) = Exp(-Abs(Cos(X) + Sen(Y)))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = -Svrz(CInt(I), CInt(J)) * 120 + 110 ' scaleX
                            shape3d(CInt(I), CInt(J)).X = shape3d(CInt(I), CInt(J)).X + 260
                            shape3d(CInt(I), CInt(J)).Y = shape3d(CInt(I), CInt(J)).Y + 190
                        Case 28 'Wavy sheet
                            Svrz(CInt(I), CInt(J)) = Atan(Cos(X + Y) + Sen(X + Y))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 80 ' scaleX
                        Case 29 'Origami?
                            Svrz(CInt(I), CInt(J)) = CInt(Cos(X + Y) + Sen(X + Y))
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 40 ' scaleX

                        Case Else
                            eString$ = cString
                            ReplaceXY(eString$, CStr(X), CStr(Y))
                            '---------------------------------------------
                            Svrz(CInt(I), CInt(J)) = Sen(X) + Cos(Y) 'sc.Eval(eString$)  'MS Script EvalCInt(Cos(X + Y) + Sin(X + Y)) 'Atan(Cos(X + Y) + Sin(X + Y)) '4 '
                            '---------------------------------------------
                            If Svrz(CInt(I), CInt(J)) < -zlim Then Svrz(CInt(I), CInt(J)) = -zlim
                            If Svrz(CInt(I), CInt(J)) > zlim Then Svrz(CInt(I), CInt(J)) = zlim
                            shape3d(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 20 ' scaleX
                    End Select
                Next I
            Next J

            'Exit Sub
            'Used with scaling factors
            'zpL = 0.2 * picDisplay.ScaleWidth
            'z'pH = 0.8 * picDisplay.ScaleWidth



            Dim Obj As New Objeto3D

            With Obj
                Obj.NPaginas = (grxhi - 1) * (gryhi - 1)
                ReDim Obj.Pagina(CInt(Obj.NPaginas - 1))
                .CorBorda = Color.White.ToArgb
                For i = 0 To grxhi - 2
                    For j = 0 To gryhi - 2
                        With .Pagina(CInt(i * (gryhi - 1) + j))
                            .CorBorda = Color.Blue
                            .CorFace = Color.Black 'IIf(i = 0, Color.Red, Color.FromArgb(150, 255 - Math.Abs(z), (Math.Abs(2 * i)) Mod 255))
                            .Preenchimento = True
                            .AdicionaPonto(shape3d(CInt(j), CInt(i)))
                            .AdicionaPonto(shape3d(CInt(j + 1), CInt(i)))
                            .AdicionaPonto(shape3d(CInt(j + 1), CInt(i + 1)))
                            .AdicionaPonto(shape3d(CInt(j), CInt(i + 1)))
                            '.NumPoints = 4
                            ' ReDim .Point(4)
                            '.Point(1) = Shape3d(j, i) 'Point3D(Shape3D(, y1, z)
                            ' .Point(2) = Shape3d(j + 1, i) 'Point3D(x, y, z)
                            '.Point(3) = Shape3d(j + 1, i + 1) 'Point3D(x, y, z + dZ)
                            '.Point(4) = Shape3d(j, i + 1) 'Point3D(x1, y1, z + dZ)
                            '.Point(0) = midPoint3D(midPoint3D(.Point(1), .Point(3)), midPoint3D(.Point(2), .Point(4))) 'Center point of page
                        End With
                    Next
                Next
            End With

            Return Obj
        End Function


        Public Function MakeXArrow(ByVal L1 As Precisao, ByVal d As Precisao, ByVal cor As Color) As Pagina3D
            'draws axis with 3D arrow at end of axis
            Dim d2 As Precisao
            Dim X As New Pagina3D

            d2 = CLng(d) \ CLng(2.0)
            With X
                .NPontos = 6
                ReDim .Ponto(6)
                .CorFace = cor
                .CorBorda = cor
                .DesenhaBorda = True
                '.filled = False
                .Preenchimento = True
                .Ponto(0) = New Ponto3D(CLng(L1) \ 2, 0, 0) 'Center Point
                .Ponto(1) = New Ponto3D(0, 0, 0)
                .Ponto(2) = New Ponto3D(L1 - d2, 0, 0)
                .Ponto(3) = New Ponto3D(L1 - d, -d2, 0)
                .Ponto(4) = New Ponto3D(L1, 0, 0)
                .Ponto(5) = New Ponto3D(L1 - d, d2, 0)
                .Ponto(6) = .Ponto(2)
            End With

            Return X

        End Function

    End Module


End Namespace

