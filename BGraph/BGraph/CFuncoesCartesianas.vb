﻿Option Strict On


Imports BMath.Funcoes

Imports BGeom.Ponto
Imports BGeom.Vetor

Imports System.Drawing
Imports Precisao = System.Double

Namespace Desenha3D


    Public Class FuncoesCartesianas 'CartesianFunction

        Private Nome As String = "Definido pelo usuário"
        Private Xmin As Precisao = 0.0
        Private Ymin As Precisao = 0.0
        Private Xmax As Precisao = 10.0
        Private Ymax As Precisao = 10.0

        Private ZLimMax As Precisao = -50.0
        Private ZLimMin As Precisao = 50.0
        Private NPontosX As Precisao = 20.0
        Private NPontosY As Precisao = 20.0
        Private Pontos(,) As Ponto3D



        Public Sub Calcular(ByVal optshape As Precisao, _
                            ByVal EscX As Precisao, _
                            ByVal EscY As Precisao, _
                            ByVal sfunc As String)

            Dim tmpS() As String
            Dim Svrx(,), Svry(,), Svrz(,) As Precisao
            Dim scx, scy As Precisao
            Dim escalaX, escalaY As Precisao
            'zpH = 0.8 * picDisplay.ScaleWidth
            Dim ZLim As Precisao
            'Dim YH(0 To 0), YL(0 To 0), XH(0 To 0), XL(0 To 0), ZL(0 To 0), PD(0 To 0)

            Dim X, Y As Precisao
            tmpS = Split(sfunc, ",")
            Xmin = Val(tmpS(2))
            Xmax = Val(tmpS(3))
            Ymin = Val(tmpS(4))
            Ymax = Val(tmpS(5))
            ZLim = Val(tmpS(6))
            '   NuPlotInterval.Value = Val(tempS(7))
            'zLimMax = NuZlimit.Value
            ' ploti = NuPlotInterval.Value
            NPontosX = Val(CDbl(tmpS(7)) * EscX / 600)
            NPontosY = NPontosX ' Val(tempS(7) * ScaletoY / 400)
            'Original function points
            ReDim Svrx(CInt(NPontosX), CInt(NPontosY))
            ReDim Svry(CInt(NPontosX), CInt(NPontosY))
            ReDim Svrz(CInt(NPontosX), CInt(NPontosY))
            ReDim Pontos(CInt(NPontosX), CInt(NPontosY))

            'Scale factors for converting grid points to x,y values
            scx = (Xmax - Xmin) / (NPontosX - 1)
            scy = (Ymax - Ymin) / (NPontosY - 1)
            ' zpl = 0.8 * Canvas3D.Width / (grxhi - 1)
            ' zpH = 0.2 * Canvas3D.Width
            EscX = Min(EscX, EscY)
            escalaX = 0.5 * EscX / (Xmax - Xmin)
            'scaleY = 0.5 * ScaletoY / (maxY - minY)
            'scaleX = Min(scaleX, scaleY)
            escalaY = escalaX
            ' Dim scfx, scfy As Single
            ' scfx = (zpH - zpl) / (Xmax - Xmin)

            For J = 0 To NPontosY
                For I = 0 To NPontosX
                    Svrx(CInt(I), CInt(J)) = (scx * (CInt(I) - 1) + Xmin) '* 200
                    Svry(CInt(I), CInt(J)) = (scy * (CInt(J) - 1) + Ymin) '* 20
                    Pontos(CInt(I), CInt(J)).X = (scx * (CInt(I) - 1) + Xmin) * escalaX
                    Pontos(CInt(I), CInt(J)).Y = (scy * (CInt(J) - 1) + Ymin) * escalaY
                Next I
            Next J

            'Dim eString As String
            'Fill svrz(i,j) with Func(x,y)
            For J = 0 To NPontosY
                For I = 0 To NPontosX
                    X = Svrx(CInt(I), CInt(J))
                    Y = Svry(CInt(I), CInt(J))
                    Select Case optshape
                        Case 0 'Put Perspec On & roll into ball  & do Y-rot only
                            Svrz(CInt(I), CInt(J)) = (X) ^ 2 + (Y) ^ 2
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 4 - 60 '* 20 ' scaleX
                        Case 1 'Saddle
                            Svrz(CInt(I), CInt(J)) = X ^ 2 - Y ^ 2
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 3 ' scaleX
                        Case 2 'Balloon Perspec On & Y-rot only
                            Svrz(CInt(I), CInt(J)) = (X ^ 2 + Y ^ 2) ^ 2
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 1.6 ' scaleX
                        Case 3 'Butterfly 1
                            Svrz(CInt(I), CInt(J)) = Raiz(Abs((X - 6) ^ 2 - (Y - 6) ^ 2))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 4 ' scaleX
                        Case 4 'Butterfly 2
                            Svrz(CInt(I), CInt(J)) = (X + Y) ^ 2
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 24 ' scaleX
                        Case 5 'Bent strip 1
                            Svrz(CInt(I), CInt(J)) = X ^ 2 + Abs(Y) ^ 0.5
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 5 ' scaleX
                        Case 6 'Bent strip 2
                            Svrz(CInt(I), CInt(J)) = Y ^ 4 - Y ^ 2 - X ^ 2
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 5 ' scaleX
                        Case 7 'cornered cone
                            Svrz(CInt(I), CInt(J)) = Raiz(X ^ 2 + Y ^ 2)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 9 ' scaleX
                        Case 8 'Cubic twisted strip
                            Svrz(CInt(I), CInt(J)) = X ^ 3 + 2 * X * Y + Y ^ 2
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 3.5 ' scaleX
                        Case 9 'Edge spike
                            Svrz(CInt(I), CInt(J)) = Exp(-(X ^ 2 + Y ^ 2))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 60 ' scaleX
                        Case 10 'Center spike 1
                            Svrz(CInt(I), CInt(J)) = 2 * Sen(Raiz((X / 4) ^ 2 + (Y / 4) ^ 2))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 30 ' scaleX
                        Case 11 'Center spike 2
                            Svrz(CInt(I), CInt(J)) = Tanh((2 * X) ^ 2 + (2 * Y) ^ 2)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 75 ' scaleX
                        Case 12 'Concentric rings
                            Svrz(CInt(I), CInt(J)) = Sen(Raiz(X ^ 2 + Y ^ 2))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 12 ' scaleX
                        Case 13 'Trig hills Egg holder
                            Svrz(CInt(I), CInt(J)) = Sen(X) + Cos(Y)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 7 ' scaleX
                        Case 14 'ledge
                            Svrz(CInt(I), CInt(J)) = Senh(X + Y)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 7 ' scaleX
                        Case 15 ' Bat wing
                            Svrz(CInt(I), CInt(J)) = Cosh(X + Y)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 7 ' scaleX
                        Case 16 'Flying sheet
                            Svrz(CInt(I), CInt(J)) = Tanh(X + Y)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 30 ' scaleX
                        Case 17 'Stingray
                            Svrz(CInt(I), CInt(J)) = Raiz(Abs(Tanh(X + Y)))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 70 ' scaleX
                        Case 18 'Sheet with bent corner
                            Svrz(CInt(I), CInt(J)) = 2 * Acos(X / 22 + Y / 22)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 50 - 100 ' scaleX
                        Case 19 ' Log sheet(ignore error)
                            Svrz(CInt(I), CInt(J)) = CDbl(IIf(X + Y > 0, Log10(X + Y), 0))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 7.5 ' scaleX
                        Case 20 'Seat
                            Svrz(CInt(I), CInt(J)) = Senh(X) + Cosh(Y)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 0.7 '* 4 ' scaleX
                        Case 21 'Corrugated sheet
                            Svrz(CInt(I), CInt(J)) = (X - Sen(X)) + (Y - Cos(X))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 13 ' scaleX
                        Case 22 'Shaver head
                            Svrz(CInt(I), CInt(J)) = Cos(X) ^ 3 + Sen(X) ^ 3
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 12 ' scaleX
                        Case 23 'Wicker chairs
                            Svrz(CInt(I), CInt(J)) = (X ^ 3 + Y ^ 3) - 12 * X * Y
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 0.35 ' scaleX
                        Case 24 'Escalators, 
                            Svrz(CInt(I), CInt(J)) = X * Cos(Y)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 6 ' scaleX
                        Case 25 '3D corner
                            Svrz(CInt(I), CInt(J)) = Exp(-2 * X * Y)
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 20 ' scaleX
                            Pontos(CInt(I), CInt(J)).X = Pontos(CInt(I), CInt(J)).X + 180
                            Pontos(CInt(I), CInt(J)).Y = Pontos(CInt(I), CInt(J)).Y + 140
                        Case 26 ' Spiked waves
                            Svrz(CInt(I), CInt(J)) = Exp(-Abs(Cos(X + Y)))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 35 ' scaleX
                            Pontos(CInt(I), CInt(J)).X = Pontos(CInt(I), CInt(J)).X + 110 '+ 260
                            Pontos(CInt(I), CInt(J)).Y = Pontos(CInt(I), CInt(J)).Y + 160 '+ 190
                        Case 27 'Domed surface
                            Svrz(CInt(I), CInt(J)) = Exp(-Abs(Cos(X) + Sen(Y)))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = -Svrz(CInt(I), CInt(J)) * 70 + 60 ' scaleX
                            Pontos(CInt(I), CInt(J)).X = Pontos(CInt(I), CInt(J)).X + 120 '+ 260
                            Pontos(CInt(I), CInt(J)).Y = Pontos(CInt(I), CInt(J)).Y + 160 '+ 190
                        Case 28 'Wavy sheet
                            Svrz(CInt(I), CInt(J)) = Atan(Cos(X + Y) + Sen(X + Y))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 50 ' scaleX
                        Case 29 'Origami?
                            Svrz(CInt(I), CInt(J)) = CInt(Cos(X + Y) + Sen(X + Y))
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim
                            Pontos(CInt(I), CInt(J)).Z = Svrz(CInt(I), CInt(J)) * 28 ' scaleX

                        Case Else
                            '  eString$ = cString
                            '  ReplaceXY(eString$, X, Y)
                            '---------------------------------------------
                            Svrz(CInt(I), CInt(J)) = Sen(X) + Cos(Y) 'sc.Eval(eString$)  'MS Script EvalCInt(Cos(X + Y) + Sin(X + Y)) 'Atan(Cos(X + Y) + Sin(X + Y)) '4 '
                            '---------------------------------------------
                            If Svrz(CInt(I), CInt(J)) < -ZLim Then Svrz(CInt(I), CInt(J)) = -ZLim
                            If Svrz(CInt(I), CInt(J)) > ZLim Then Svrz(CInt(I), CInt(J)) = ZLim

                            '(CInt(I), CInt(J)).z = Svrz(CInt(I), CInt(J)) * 12
                    End Select
                    Pontos(CInt(I), CInt(J)).Cor = CorInterpola(Color.FromArgb(79, 47, 170), Color.FromArgb(214, 242, 255), (Svrz(CInt(I), CInt(J)) + ZLim) / (2 * ZLim))
                    Pontos(CInt(I), CInt(J)).Z = Pontos(CInt(I), CInt(J)).Z * EscX / 400

                Next I
            Next J

        End Sub



        Public Function getObject3D(ByVal optCode As Integer, ByVal ScaleX As Single, ByVal ScaleY As Single, ByVal sfunction As String) As Objeto3D
            Calcular(optCode, ScaleX, ScaleY, sfunction)

            Dim Obj As New Objeto3D
            With Obj
                Obj.NPaginas = (NPontosX - 1) * (NPontosY - 1)
                ReDim Obj.Pagina(CInt(Obj.NPaginas - 1))

                .CorBorda = Color.White.ToArgb
                For i = 0 To NPontosY - 2

                    For j = 0 To NPontosX - 2
                        With .Pagina(CInt(i * (NPontosX - 1) + j))
                            ReDim .Vizinhos(8)
                            '.neighbours
                            .CorBorda = Color.Green
                            .Preenchimento = True
                            .AdicionaPonto(Pontos(CInt(j), CInt(i)))
                            .AdicionaPonto(Pontos(CInt(j + 1), CInt(i)))
                            .AdicionaPonto(Pontos(CInt(j + 1), CInt(i + 1)))
                            .AdicionaPonto(Pontos(CInt(j), CInt(i + 1)))
                            '.NumPoints = 4
                            ' ReDim .Point(4)
                            '.Point(1) = Shape3d(j, i) 'Point3D(Shape3D(, y1, z)
                            '.Point(2) = Shape3d(j + 1, i) 'Point3D(x, y, z)
                            '.Point(3) = Shape3d(j + 1, i + 1) 'Point3D(x, y, z + dZ)
                            '.Point(4) = Shape3d(j, i + 1) 'Point3D(x1, y1, z + dZ)
                            '.Point(0) = midPoint3D(midPoint3D(.Point(1), .Point(3)), midPoint3D(.Point(2), .Point(4))) 'Center point of page
                        End With
                    Next
                Next
            End With

            Return Obj
        End Function

        Public Function DyDx(ByVal optshape As Precisao, ByVal x As Precisao, ByVal y As Precisao) As Vetor3D
            Dim v As Vetor3D
            Dim dx, dy, dz As Precisao
            Select Case optshape
                Case 0 'Put Perspec On & roll into ball & do Y-rot only
                    '  Svrz(I, J) = (X) ^ 2 + (Y) ^ 2
                    dx = -2 * x
                    dy = -2 * y
                    dz = 13.35 * 13.35
                Case 1 'Saddle
                    dx = 2 * x
                    dy = 2 * y
                    dz = -1
                Case 1 'Balloon Perspec On & Y-rot only
                    ' Svrz(I, J) = (x ^ 2 + Y ^ 2) ^ 2

                Case 2 'Butterfly 1
                    ' Svrz(I, J) = Sqrt(Abs((x - 6) ^ 2 - (Y - 6) ^ 2))
                Case 3 'Butterfly 2
                    'Svrz(I, J) = (x + y) ^ 2
                    '  If Svrz(I, J) < -zlim Then Svrz(I, J) = -zlim
                    ' If Svrz(I, J) > zlim Then Svrz(I, J) = zlim
                    ' Points(I, J).z = Svrz(I, J) * 24 ' scaleX
                Case 4 'Bent strip 1
                    ' Svrz(I, J) = x ^ 2 + Abs(Y) ^ 0.5

                Case 5 'Bent strip 2
                    ' Svrz(I, J) = Y ^ 4 - Y ^ 2 - x ^ 2

                Case 6 'cornered cone
                    ' Svrz(I, J) = Sqrt(x ^ 2 + Y ^ 2)


                    dx = -(1 / Raiz(x ^ 2 + y ^ 2)) * x / 7.5
                    dy = -(1 / Raiz(x ^ 2 + y ^ 2)) * y / 7.5
                    dz = 1
                Case 7 'Cubic twisted strip
                    ' Svrz(I, J) = x ^ 3 + 2 * x * Y + Y ^ 2
                    dx = 3 * x * x + 2 * y
                    dy = 2 * y + 2 * x
                    dz = -1 / 3.5
                    ' Points(I, J).z = Svrz(I, J) * 3.5 ' scaleX
                Case 8 'Concentric rings
                    ' Svrz(I, J) = Sin(Sqrt(x ^ 2 + y ^ 2))
                    ' If Svrz(I, J) < -zlim Then Svrz(I, J) = -zlim
                    'If Svrz(I, J) > zlim Then Svrz(I, J) = zlim
                    'Points(I, J).z = Svrz(I, J) * 12 ' scaleX
                    x = x / 13.35
                    y = y / 13.35
                    dx = -Cos(Raiz(x ^ 2 + y ^ 2)) * (1 / Raiz(x ^ 2 + y ^ 2)) * x / 13.35
                    dy = -Cos(Raiz(x ^ 2 + y ^ 2)) * (1 / Raiz(x ^ 2 + y ^ 2)) * y / 13.35
                    dz = 1 / 12
                Case Else
                    '  eString$ = cString
                    '  ReplaceXY(eString$, X, Y)
                    '---------------------------------------------
                    dx = 1
                    dy = 1
                    dz = 1
            End Select
            v = New Vetor3D(dx, dy, dz) '.Normalize

            ' Debug.Print(x, y)
            Return v.getNormaliza

        End Function

    End Class


End Namespace
