﻿Option Strict On

#Region "Imports"


Imports BMath.Matriz
Imports BMath.Funcoes

Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports Precisao = System.Double


#End Region


Namespace Desenha3D


    Public Class Camera3D
        Implements IDisposable


#Region "Declaracoes"


        'POSICAO DA CAMERA NO WORLD SPACE
        '----------------------------------------------------
        'R = Raio da órbita de rotacao da camera
        'Theta = Angulo da projecao do vetor posicao da camera no plano XY, com o eixo X do World(xpitch)
        'Phi = Angulo do vetor PosicaoCamera nas coordenadas do eixo Z do World.
        Private privTitulo As String                 '   Camera1, Diretor, Birds-eye View, etc. (Opcional)
        Private privDescricao As String               '   Descricao da legenda.     (Optional)

        Private privModoDeProjecao As eProjecao
        Private privR As Precisao
        Private privTheta, privPhi As Precisao  'angulos em graus
        Private Yaw, Pitch, Roll As Precisao    'Gira no entorno dos eixos X, Y e Z, respectivamente
        Private privbPerspectiva As Boolean
        'used with perspective view only
        Private privPlanoProximo As Precisao
        Private privPlanoDistante As Precisao
        Private privFOV As Precisao                       '   Field Of View (FOV). "90 degree FOV" = "1x Zoom". If you update FOV, don't forget to update Zoom.
        Private privZoom As Precisao                      '   (The Zoom value is calculated from the FOV. Normally you define one of them, then calculate the other.)
        Private privClipFar As Precisao                   '   Don't draw Vertices further away than this value. Any value higher than 0.
        Private privClipNear As Precisao                  '   Don't draw Vertices that are this close to us (or behind us). Typically 0, but can be higher.
        Private privMatrizView As New Matriz4x4

        'HEAD OF CAMERA (UP Vector)
        '------------------------------------------------------
        'CamUPPos: represents Point in camera used to specify up direction of camera
        'UPVector: up Vector of camrea is vector Joining PV of camUPPos with PV of camCentrePOS
        'Line of Sight/Look at Vector: Vector joining CamCentrePos and Focus(targetPos)
        Public CameraPosicaoCentro As Ponto3D
        Public Foco As Ponto3D
        Public CameraPosicaoCima As Ponto3D

        Public TempCamPos As Ponto3D
        Public TempUppt As Ponto3D
        Public Tempfoco As Ponto3D


        'LOCAL AXIS OF CAMERA SPACE
        '-------------------------------------------------------
        'Z axis: or N Vector vector joining Focus and CamcentrePos 
        'Y axis: or UpVector Projection of UPvector in Projection Plane
        'X axis: Or Right vector: crossProduct(Y axis, Z axis)
        Public LocalX, LocalY, LocalZ As Vetor3D
        Public Event CameraAtualiza(ByVal Sender As Camera3D)


        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False


#End Region



#Region "Construtores e Destrutores"


        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub


        'Set defaultCamera
        Public Sub New()
            Me.privR = 10
            Me.privTheta = 0
            Me.privPhi = 0
            Me.CameraPosicaoCentro = New Ponto3D(0, 0, 400)
            Me.Foco = New Ponto3D(0, 0, 0)
            Me.CameraPosicaoCima = New Ponto3D(0, 1, 400)
            Me.privbPerspectiva = False
            Zoom = 1
            AtualizaCamera(Me.privR, Me.privTheta, Me.privPhi)
            ' Mat_View = New Matrix4x4
        End Sub



        'LookFrom : CameraCentre position in worldSpace
        'Lookat   : Focus in world Space
        'CameraTop: The position of Head of camera, to specify the upvector =Cameratop-lookfrom
        Public Sub New(ByVal LookFrom As Ponto3D, ByVal Lookat As Ponto3D, ByVal CameraTop As Ponto3D)
            Me.privR = 10
            Me.privTheta = 0
            Me.privPhi = 90
            Me.CameraPosicaoCentro = LookFrom
            Me.Foco = Lookat
            Me.CameraPosicaoCima = CameraTop
            Ponto3D.RThetaPhiDePonto3D(Me.CameraPosicaoCentro, Me.privR, Me.privTheta, Me.privPhi)
            Me.privbPerspectiva = False
            Me.Zoom = 1
            'Update localX(right), localY(up), localZ(n vector) ie. local Camera Space
            AtualizaCamera(Me.privR, Me.privTheta, Me.privPhi)
        End Sub


#End Region



#Region "Propriedades"


        'Toggles Perspctive/Parallel view
        Public Property Perspectiva As Boolean
            Get
                Return Me.privbPerspectiva
            End Get
            Set(ByVal value As Boolean)
                If (Me.Perspectiva <> value) Then
                    Me.privbPerspectiva = value
                    RaiseEvent CameraAtualiza(Me)
                End If
            End Set
        End Property


        'Set field of View of Camera
        'Field Of View (FOV). "90 degree FOV" = "1x Zoom". If you update FOV, don't forget to update Zoom.
        Public Property FOV As Precisao
            Get
                Return Me.privFOV
            End Get
            Set(ByVal value As Precisao)
                If (Me.FOV <> value) Then
                    Me.privFOV = value
                    RaiseEvent CameraAtualiza(Me)
                End If
            End Set
        End Property



        'Zoom factor
        '(The Zoom value is calculated from the FOV. Normally you define one of them, then calculate the other.)
        Public Property Zoom As Precisao
            Get
                Return Me.privZoom
            End Get
            Set(ByVal value As Precisao)
                If (Me.Zoom <> value) Then
                    Me.privZoom = value
                    RaiseEvent CameraAtualiza(Me)
                End If
            End Set
        End Property


#End Region


        'Rotate camera by specified angle
        Public Sub RotacionaCamera(ByVal dTheta As Precisao, ByVal dFi As Precisao)
            AtualizaCamera(Me.privR, Me.privTheta + dTheta, Me.privPhi + dFi)
        End Sub


        'Rotate camera by specified angle
        Public Sub RotacionaCamera(ByVal dThetaX As Precisao, ByVal dThetaY As Precisao, ByVal dThetaZ As Precisao)
            'UpdateCamera(R, Theta + dThetaX, Fi + dThetaZ)
            Dim m As Matriz4x4
            m = MatrizRotacionaZ(dThetaZ) * MatrizRotacionaX(dThetaX) * MatrizRotacionaY(dThetaY)

            'Rotate camera by theta ,fi by transforming camPos and CamUppos accordingly, Save these in temp  Variables 
            Me.TempCamPos = AplicaTransformacao3D(Me.TempCamPos, m)
            Me.TempUppt = AplicaTransformacao3D(Me.TempUppt, m)
            Ponto3D.RThetaPhiDePonto3D(Me.TempCamPos, Me.privR, Me.privTheta, Me.privPhi)
            ' Me.Theta = Me.Theta + dThetaX
            'Me.Fi = Me.Fi + dThetaZ

            'get UPvector
            LocalY = Vetor3D.ConvertePonto2Vetor3D(Me.TempCamPos, Me.TempUppt)

            'get Line of view Vector
            LocalZ = -Vetor3D.ConvertePonto2Vetor3D(Me.Foco, Me.TempCamPos).getNormaliza

            'get Right Vector
            LocalX = (LocalZ * LocalY).getNormaliza

            'ReEvaluate upVector,sothat it lies in plane of projection (in case upvector is not perpendicular to look at vector
            LocalY = LocalX * LocalZ 'The cross-product give a normalized vector, because both input vectors are normalized,  '  then we dont need to normalize.

            RaiseEvent CameraAtualiza(Me)
        End Sub


        'Update local Coordinate System in cameraSpace(It is left handed system)
        ' The local Z is the lookat vector (from the camera center to the lookat point) and is "into the screen".
        ' The local Y will be toward the up of the screen. 
        '                     If you maintain your camera up vector perpendicular to the camera lookat vector, then the up vector is also the local Y. 
        ' The local X will be normal to the two other axes (it is their cross product after their normalization).
        ' Rotating left-right is a matter of rotating the lookat vector about the local Y axis (yaw). 
        ' Rotating up-down is a matter of rotating the up vector and the lookat vector about the local X axis (pitch). 
        ' Tilting the head left-right is a matter of rotating up vector about the local Z axis (ROLL).
        ' Each time you change the lookat vector or the up vector, you need to recompute the local axes.
        Public Sub AtualizaCamera(ByVal OrbitRadius As Precisao, _
                                  ByVal sTheta As Precisao, _
                                  ByVal sFi As Precisao, _
                                  Optional ByVal dThetaY As Precisao = 0.0)

            'update Camera parameters
            Me.privR = OrbitRadius
            Me.privTheta = sTheta
            Me.privPhi = sFi
            Dim M As Matriz4x4

            'Create 4x4matric to rotate camera by theta(about z axis) and fi(about x axis)
            M = MatrizRotacionaZ(privTheta) * MatrizRotacionaX(privPhi) * MatrizRotacionaY(dThetaY)

            'Rotate camera by theta ,fi by transforming camPos and CamUppos accordingly, Save these in temp  Variables 
            Me.TempCamPos = AplicaTransformacao3D(Me.CameraPosicaoCentro, M)
            Me.TempUppt = AplicaTransformacao3D(Me.CameraPosicaoCima, M)

            'get UPvector
            LocalY = Vetor3D.ConvertePonto2Vetor3D(Me.TempCamPos, Me.TempUppt)
            'get Line of view Vector
            LocalZ = -Vetor3D.ConvertePonto2Vetor3D(Me.Foco, Me.TempCamPos).getNormaliza
            'get Right Vector
            LocalX = (LocalZ * LocalY).getNormaliza
            'ReEvaluate upVector,sothat it lies in plane of projection (in case upvector is not perpendicular to look at vector
            LocalY = LocalX * LocalZ 'The cross-product give a normalized vector, because both input vectors are normalized,  '  then we dont need to normalize.

            RaiseEvent CameraAtualiza(Me)


            'The following code is for using 4x4matrix to project
            'LocalY = (New Vector3D(0, 1, 0))
            'LocalY = Matrix4x4.applytransform(LocalY, M)
            'Dim C, F As Vector3D
            'C = New Vector3D(TempCamPos.x, TempCamPos.y, TempCamPos.z)
            'F = New Vector3D(Tempfocus.x, Tempfocus.y, Tempfocus.z)
            'Mat_View = Matrix4x4.matrix_Project3D(0, C, F, LocalY)
        End Sub



        'project point in XY plane in cameraSpace
        Public Function Projeta3DEm2D(ByVal Pt As Ponto3D) As Ponto3D
            Dim rv As New Ponto3D(0, 0, 0)

            'find the vector joining pt and focus, treating focus as origin
            Pt.X = (Pt.X - Me.Foco.X)
            Pt.Y = (Pt.Y - Me.Foco.Y)
            Pt.Z = (Pt.Z - Me.Foco.Z)

            'get Projection length of PV of point along orthogonal unit vectors localX ,localY and localZ by taking dot Product
            rv.X = Pt.X * LocalX.X + Pt.Y * LocalX.Y + Pt.Z * LocalX.Z
            rv.Y = Pt.X * LocalY.X + Pt.Y * LocalY.Y + Pt.Z * LocalY.Z
            rv.Z = Pt.X * LocalZ.X + Pt.Y * LocalZ.Y + Pt.Z * LocalZ.Z

            If Me.Perspectiva Then
                rv.X = rv.X / (1 + rv.Z / Me.privR)
                rv.Y = rv.Y / (1 + rv.Z / Me.privR)
            End If

            Return rv
        End Function



        Private Function ConverteFOV2Zoom(ByVal FOV As Precisao) As Precisao
            Dim valor As Precisao
            valor = 1 / TanG((FOV) / 2)
            ' Given a Field Of View in degree  calculate the Zoom.
            Return valor
        End Function



        Private Function ConverteZoom2FOV(ByVal Zoom As Precisao) As Precisao
            Dim valor As Precisao
            valor = 2 * AtanG(1 / Zoom)
            Return valor
        End Function
    End Class

End Namespace

