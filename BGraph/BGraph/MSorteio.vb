﻿Option Strict On


Imports Precisao = System.Double

Public Module MSorteio


    'The GNU General Public License does not apply to this module

    'sorts an array from smallest to largest
    Sub SingleSort(ByVal ArrayKey() As Single, ByVal ArrayPointer() As Integer, ByVal First As Integer, ByVal Last As Integer)

        Dim A As Integer
        Dim B As Integer
        Dim C As Integer
        Dim Split As Single

        If First < Last Then
            If Last - First = 1 Then
                If ArrayKey(First) > ArrayKey(Last) Then
                    Call SingleSwap(ArrayKey(First), ArrayKey(Last))
                    Call IntegerSwap(ArrayPointer(First), ArrayPointer(Last))
                End If
            Else
                C = IntegerRandom(First, Last)
                Call SingleSwap(ArrayKey(Last), ArrayKey(C))
                Call IntegerSwap(ArrayPointer(Last), ArrayPointer(C))
                Split = ArrayKey(Last)
                Do
                    A = First
                    B = Last
                    Do While (A < B) And ArrayKey(A) <= Split
                        A = A + 1
                    Loop
                    Do While (B > A) And ArrayKey(B) >= Split
                        B = B - 1
                    Loop
                    If A < B Then
                        Call SingleSwap(ArrayKey(A), ArrayKey(B))
                        Call IntegerSwap(ArrayPointer(A), ArrayPointer(B))
                    End If
                Loop While A < B
                Call SingleSwap(ArrayKey(A), ArrayKey(Last))
                Call IntegerSwap(ArrayPointer(A), ArrayPointer(Last))
                If (A - First) < (Last - A) Then
                    Call SingleSort(ArrayKey, ArrayPointer, First, A - 1)
                    Call SingleSort(ArrayKey, ArrayPointer, A + 1, Last)
                Else
                    Call SingleSort(ArrayKey, ArrayPointer, A + 1, Last)
                    Call SingleSort(ArrayKey, ArrayPointer, First, A - 1)
                End If
            End If
        End If

    End Sub
    Sub IntegerSwap(ByRef A As Integer, ByRef B As Integer)

        Dim C As Integer

        C = A
        A = B
        B = C

    End Sub
    Sub SingleSwap(ByRef A As Single, ByRef B As Single)

        Dim C As Single

        C = A
        A = B
        B = C

    End Sub


    Sub DoubleSwap(ByRef A As Precisao, ByRef B As Precisao)

        Dim C As Precisao

        C = A
        A = B
        B = C

    End Sub


    Function IntegerRandom(ByVal Lower As Integer, ByVal Upper As Integer) As Integer

        Randomize()

        Return CInt(Int(Rnd() * (Upper - Lower + 1)) + Lower)

    End Function

End Module
