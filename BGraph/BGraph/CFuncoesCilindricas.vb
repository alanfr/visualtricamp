﻿
Imports BMath.Constantes
Imports BMath.Funcoes


Imports BGeom.Ponto
Imports BGeom.Vetor

Imports System.Drawing
Imports Precisao = System.Double


Namespace Desenha3D


    Public Class FuncoesCilindricas

        Public Nome As String = "User Defined"
        Public min_u As Precisao = 0
        Public min_v As Precisao = 0
        Public max_u As Precisao = 10
        Public max_v As Precisao = 10
        Public xText, yText, zText As String
        Public RText, GText, BText As String
        Public zLimMax As Precisao = -50
        Public zLimMin As Precisao = 50
        Public uPontos As Precisao = 20
        Public vPontos As Precisao = 20
        Public Pontos(,) As Ponto3D

        Public Sub Calcular(ByVal optshape As Precisao, _
                                 ByVal ScaletoX As Precisao, _
                                 ByVal ScaletoY As Precisao)


            Dim x, y, z As Object
            Dim u, v, du, dv As Precisao
            Dim i, j As Precisao
            Dim c1, c2, c3, c4, c5, c6 As Precisao
            Dim scalefactor As Precisao
            scalefactor = Min(ScaletoX, ScaletoY) / 400.0
            i = 0 : j = 0
            Teste = False

            Select Case optshape
                Case 0 'Sphere

                    max_u = 2 * PI
                    max_v = PI / 2
                    min_u = 0
                    min_v = -PI / 2
                    uPontos = 20
                    vPontos = 20
                    xText = "Cos(u) / (Sqrt(2) + Sin(v))"
                    yText = "Sin(u) / (Sqrt(2) + Sin(v))"
                    zText = " 1 / (Sqrt(2) + Cos(v))"
                    RText = "(Sin(u) + 1) * 0.5*255"
                    GText = "(Cos(u) + 1) * 0.5*255"
                    BText = "(Cos(v) + 1) * 0.5*255"
                Case 1 'Ring
                    max_u = 2 * PI
                    max_v = 2 * PI
                    min_u = 0
                    min_v = 0
                    uPontos = 60
                    vPontos = 20
                    xText = "(1 + 0.4 * Cos(v)) * Cos(u)"
                    yText = "(1 + 0.4 * Cos(v)) * Sin(u)"
                    zText = "0.4 * Sin(v)"
                    RText = "(Sin(u / 2) + 1) * 0.5*255"
                    GText = "(Cos(v) + 1) * 0.5*255"
                    BText = "0 '(Cos(u) + 1) * 0.5*255)"

                Case Else 'seashell
                    max_u = 0 - 25 / 30
                    max_v = PI
                    min_u = -25
                    min_v = -PI
                    uPontos = 60
                    vPontos = 20
                    xText = "(2 + c1 * Sin(2 * PI * u)) * Sin(4 * PI * v)"
                    yText = "(2 + c1 * Sin(2 * PI * u)) * Cos(4 * PI * v)"
                    zText = "c1 * Cos(2 * PI * u) + 3 * Cos(2 * PI * v)"
                    RText = "(Sin(12 * u) + 1) * 0.5*255"
                    GText = "(Cos(12 * u) + 1) * 0.5*255"
                    BText = "0"

            End Select

            du = (max_u - min_u) / (uPontos - 1)
            dv = (max_v - min_v) / (vPontos - 1)
            ReDim Pontos(CInt(uPontos), CInt(vPontos))
            Debug.Print(CStr(UBound(Pontos)))
            For u = min_u To max_u + du Step du
                For v = min_v To max_v + dv Step dv
                    Select Case optshape
                        Case 0 'Sphere
                            x = Cos(u) * Cos(v)
                            y = Sen(u) * Cos(v)
                            z = Sen(v)
                            Pontos(i, j) = New Ponto3D(x * 100, y * 100, z * 100)
                            Pontos(i, j).tx = CInt((Sen(u) + 1) * 0.5 * 255)
                            Pontos(i, j).ty = CInt((Cos(u) + 1) * 0.5 * 225)
                            Pontos(i, j).tz = CInt((Cos(v) + 1) * 0.5 * 220)
                        Case 1 'Ring
                            x = (1 + 0.4 * Cos(v)) * Cos(u)
                            y = (1 + 0.4 * Cos(v)) * Sen(u)
                            z = 0.4 * Sen(v)
                            Pontos(i, j) = New Ponto3D(x * 100, y * 100, z * 100)
                            Pontos(i, j).tx = CInt((Sen(u / 2) + 1) * 0.5 * 255)
                            Pontos(i, j).ty = CInt((Cos(v) + 1) * 0.5 * 255)
                            Pontos(i, j).tz = 0 'CInt((Cos(u) + 1) * 0.5*255)
                        Case Else
                            c1 = 1.2
                            c2 = 2.3
                            c3 = 1.5
                            c4 = 1
                            c5 = 1.2
                            c6 = 25
                            x = (c1 ^ u) * (Cos(u) * (1 + c3 * (Cos(c5) * Cos(v) + Sen(c5) * Sen(v)))) 'c1 ^ u * (Cos(u) * (1 + c3 * (Cos(c5) * Cos(v) + c4 * Sin(c5) * Sin(v))))
                            y = (c1 ^ u) * (Sen(u) * (1 + c3 * (Cos(c5) * Cos(v) + Sen(c5) * Sen(v)))) 'Sin(u) * (1 + c3 * (Cos(c5) * Cos(v) + c4 * Sin(c5) * Sin(v)))
                            z = (c1 ^ u) * (c2 + c3 * (Cos(v) * Sen(c5) - Cos(c5) * Sen(v))) 'c2 + c3 * (Cos(v) * Sin(c5) - c4 * Cos(c5) * Sin(v))
                            Pontos(i, j) = New Ponto3D(x * 50, y * 50, 50 * z)
                            Pontos(i, j).tx = CInt((Cos(u / 8) + 1) * 0.5 * 200) 'CInt((Cos(v) + 1) * 0.5 * 255) '
                            Pontos(i, j).ty = CInt((Cos(v) + 1) * 0.5 * 215) 'CInt((Cos(u / 20) + 1) * 0.5 * 250) ''
                            Pontos(i, j).tz = 240 '0 
                    End Select

                    Pontos(CInt(i), CInt(j)).x = Pontos(CInt(i), CInt(j)).x * scalefactor
                    Pontos(CInt(i), CInt(j)).y = Pontos(CInt(i), CInt(j)).y * scalefactor
                    Pontos(CInt(i), CInt(j)).z = Pontos(CInt(i), CInt(j)).z * scalefactor
                    Pontos(CInt(i), CInt(j)).Cor = Color.FromArgb(CInt(Pontos(CInt(i), CInt(j)).tx), CInt(Pontos(CInt(i), CInt(j)).ty), CInt(Pontos(CInt(i), CInt(j)).tz))
                    j = j + 1
                Next
                i = i + 1
                j = 0
            Next
        End Sub


        Public Function getObject3D(ByVal optCode As Integer, ByVal ScaleX As Single, ByVal ScaleY As Single) As Objeto3D
            Dim Obj As New Objeto3D
            Calcular(optCode, ScaleX, ScaleY)
            With Obj
                Obj.NPaginas = (uPontos - 1) * (vPontos - 1)
                ReDim Obj.Pagina(CInt(Obj.NPaginas - 1))
                .CorBorda = Color.White.ToArgb
                .drawMode = eDesenho.SOLIDOGRADIENTE
                For i = 0 To vPontos - 2
                    For j = 0 To uPontos - 2
                        With .Pagina(CInt(i * (uPontos - 1) + j))
                            .CorBorda = Color.Cyan
                            .DesenhaBorda = False
                            ' .faceColor = Points(j, i).col
                            .Preenchimento = True
                            .AdicionaPonto(Pontos(CInt(j), CInt(i)))
                            .AdicionaPonto(Pontos(CInt(j + 1), CInt(i)))
                            .AdicionaPonto(Pontos(CInt(j + 1), CInt(i + 1)))
                            .AdicionaPonto(Pontos(CInt(j), CInt(i + 1)))
                        End With
                    Next
                Next
            End With

            If optCode = 3 Then Teste = True
            Return Obj

        End Function


    End Class


End Namespace

