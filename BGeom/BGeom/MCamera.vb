﻿Imports BGeom.Vetor
Imports BMath.Matriz
Imports BMath.Constantes

Namespace Transformacoes


    Public Module Camera3D


        ' Implementacao da funcao do OpenGL
        'https://www.opengl.org/sdk/docs/man2/xhtml/gluLookAt.xml
        'Adaptado para o sistema de coordenadas UP = -Z
        Public Function Camera(ByVal Eye As Vetor3D,
                               ByVal Center As Vetor3D,
                               ByVal Up As Vetor3D) As Matriz4x4

            Dim MT As Matriz4x4
            Dim EixoX, EixoY, EixoZ As Vetor3D

            MT = Matriz4x4.Identidade
            'EixoZ = (Eye - Center).getNormaliza  ' Vetor Forward
            EixoZ = (Eye - Center).getNormaliza  ' Vetor Forward
            EixoX = Vetor3D.ProdutoVetorial(Up, EixoZ).getNormaliza ' Vetor Direita
            EixoY = Vetor3D.ProdutoVetorial(EixoZ, EixoX) ' Vetor UP

            MT.M11 = EixoX.X : MT.M12 = EixoY.X : MT.M13 = EixoZ.X
            MT.M21 = EixoX.Y : MT.M22 = EixoY.Y : MT.M23 = EixoZ.Y
            MT.M31 = EixoX.Z : MT.M32 = EixoY.Z : MT.M33 = EixoZ.Z
            MT.M41 = -Vetor3D.ProdutoEscalar(EixoX, Eye)
            MT.M42 = -Vetor3D.ProdutoEscalar(EixoY, Eye)
            MT.M43 = -Vetor3D.ProdutoEscalar(EixoZ, Eye)

            MT = MT * MatrizTranslacao3D(-Eye.X, -Eye.Y, -Eye.Z)

            Return MT
        End Function



        Public Function FPSCamera(ByVal Eye As Vetor3D, ByVal Pitch As Double, ByVal Yaw As Double) As Matriz4x4
            'Pitch should be in the range of [-90 ... 90] degrees and yaw
            'should be in the range of [0 ... 360] degrees.
            Dim MT As Matriz4x4
            Dim EixoX, EixoY, EixoZ As Vetor3D
            Dim cosPitch As Double = Math.Cos(Pitch * DEGREE)
            Dim sinPitch As Double = Math.Sin(Pitch * DEGREE)
            Dim cosYaw As Double = Math.Cos(Yaw * DEGREE)
            Dim sinYaw As Double = Math.Sin(Yaw * DEGREE)

            MT = Matriz4x4.Identidade
            EixoX = New Vetor3D(cosYaw, 0, -sinYaw)
            EixoY = New Vetor3D(sinYaw * sinPitch, cosPitch, cosYaw * sinPitch)
            EixoZ = New Vetor3D(sinYaw * cosPitch, -sinPitch, cosPitch * cosYaw)

            MT.M11 = EixoX.X : MT.M12 = EixoY.X : MT.M13 = EixoZ.X
            MT.M21 = EixoX.Y : MT.M22 = EixoY.Y : MT.M23 = EixoZ.Y
            MT.M31 = EixoX.Z : MT.M32 = EixoY.Z : MT.M33 = EixoZ.Z
            MT.M41 = -Vetor3D.ProdutoEscalar(EixoX, Eye)
            MT.M42 = -Vetor3D.ProdutoEscalar(EixoY, Eye)
            MT.M43 = -Vetor3D.ProdutoEscalar(EixoZ, Eye)

            MT = MT * MatrizTranslacao3D(-Eye.X, -Eye.Y, -Eye.Z)

            Return MT
        End Function


        '    Public Function ArcBallCamera(

        '    Public Class arcball
        '{	
        '    private const float Epsilon = 1.0e-5f;

        '    private Vector3f StVec; //Saved click vector
        '    private Vector3f EnVec; //Saved drag vector
        '    private float adjustWidth; //Mouse bounds width
        '    private float adjustHeight; //Mouse bounds height

        '    public arcball(float NewWidth, float NewHeight)
        '    {
        '        StVec = new Vector3f();
        '        EnVec = new Vector3f();
        '        setBounds(NewWidth, NewHeight);
        '    }

        '    private void mapToSphere(Point point, Vector3f vector)
        '    {
        '        Point2f tempPoint = new Point2f(point.X, point.Y);

        '        //Adjust point coords and scale down to range of [-1 ... 1]
        '        tempPoint.x = (tempPoint.x * this.adjustWidth) - 1.0f;
        '        tempPoint.y = 1.0f - (tempPoint.y * this.adjustHeight);

        '        //Compute square of the length of the vector from this point to the center
        '        float length = (tempPoint.x * tempPoint.x) + (tempPoint.y * tempPoint.y);

        '        //If the point is mapped outside the sphere... (length > radius squared)
        '        If (length > 1.0F) Then
        '        {
        '            //Compute a normalizing factor (radius / sqrt(length))
        '            float norm = (float)(1.0 / Math.Sqrt(length));

        '            //Return the "normalized" vector, a point on the sphere
        '            vector.x = tempPoint.x * norm;
        '            vector.y = tempPoint.y * norm;
        '            vector.z = 0.0f;
        '        }
        '        //Else it's inside
        '        Else
        '        {
        '            //Return a vector to a point mapped inside the sphere sqrt(radius squared - length)
        '            vector.x = tempPoint.x;
        '            vector.y = tempPoint.y;
        '            vector.z = (float)System.Math.Sqrt(1.0f - length);
        '        }
        '    }

        '    public void setBounds(float NewWidth, float NewHeight)
        '    {
        '        //Set adjustment factor for width/height
        '        adjustWidth = 1.0f / ((NewWidth - 1.0f) * 0.5f);
        '        adjustHeight = 1.0f / ((NewHeight - 1.0f) * 0.5f);
        '    }

        '    //Mouse down
        '    public virtual void click(Point NewPt)
        '    {
        '        mapToSphere(NewPt, this.StVec);
        '    }

        '    //Mouse drag, calculate rotation
        '    public void drag(Point NewPt, Quat4f NewRot)
        '    {
        '        //Map the point to the sphere
        '        this.mapToSphere(NewPt, EnVec);

        '        //Return the quaternion equivalent to the rotation
        '        if (NewRot != null)
        '        {
        '            Vector3f Perp = new Vector3f();

        '            //Compute the vector perpendicular to the begin and end vectors
        '            Vector3f.cross(Perp, StVec, EnVec);

        '            //Compute the length of the perpendicular vector
        '                If (Perp.length() > Epsilon) Then
        '            //if its non-zero
        '            {
        '                //We're ok, so return the perpendicular vector as the transform after all
        '                NewRot.x = Perp.x;
        '                NewRot.y = Perp.y;
        '                NewRot.z = Perp.z;
        '                //In the quaternion values, w is cosine (theta / 2), where theta is the rotation angle
        '                NewRot.w = Vector3f.dot(StVec, EnVec);
        '            }
        '            //if it is zero
        '                Else
        '            {
        '                //The begin and end vectors coincide, so return an identity transform
        '                NewRot.x = NewRot.y = NewRot.z = NewRot.w = 0.0f;
        '            }
        '        }
        '    }
        '}



    End Module

End Namespace
