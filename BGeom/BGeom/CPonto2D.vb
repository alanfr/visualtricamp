﻿Option Strict On

Imports BGeom.Vetor
Imports Precisao = System.Double



Namespace Ponto

    Public Class Ponto2D

        Implements ICloneable, IDisposable, IComparable

#Region "Declaracoes"

        Private protX As Precisao
        Private protY As Precisao

        'Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False

#End Region


#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub



        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub




        Public Sub New()
            Me.protX = 0.0
            Me.protY = 0.0
        End Sub


        'Cria um novo ponto 2D
        Public Sub New(ByVal x As Precisao, ByVal y As Precisao)
            Me.protX = x
            Me.protY = y
        End Sub


#End Region



#Region "Propriedades"

        Public Property X() As Precisao
            Get
                Return Me.protX
            End Get
            Set(value As Precisao)
                Me.protX = value
            End Set
        End Property

        Public Property Y() As Precisao
            Get
                Return Me.protY
            End Get
            Set(value As Precisao)
                Me.protY = value
            End Set
        End Property


#End Region


#Region "Métodos"


        Public Shared Function Origem() As Ponto2D
            Return New Ponto2D(0, 0)
        End Function


        ' Copia um ponto 2D.
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Ponto2D(Me.X, Me.Y)
        End Function


        'Pega o ponto 2D de um vetor (used for foot,image etc).
        Public Shared Function PontoDeVetor(ByVal V As Vetor3D) As Ponto2D
            Return New Ponto2D(V.X, V.Y)
        End Function


        ' Converte um ponto 2D em 3D z=0.
        Public Shared Function VetorDePonto2D(ByVal pt As Ponto2D) As Vetor3D
            Return New Vetor3D(pt.X, pt.Y, 0)
        End Function


        ' Retorna o vetor entre dois pontos (usado para a linha de direção).
        Public Shared Function VetorDe2Pontos(ByVal p1 As Ponto2D, ByVal p2 As Ponto2D) As Vetor3D
            Return New Vetor3D(p2.X - p1.X, p2.Y - p1.Y, 0)
        End Function


        ' Retorna as coordenadas do ponto de um vetor na forma polar
        Public Shared Function PontoPolar(ByVal r As Precisao, ByVal theta As Precisao) As Ponto2D
            Return New Ponto2D(r * Math.Cos(theta), r * Math.Sin(theta))
        End Function


        ' Retorna o ponto intermediário
        Public Shared Function PontoMedio(ByVal p1 As Ponto2D, ByVal p2 As Ponto2D) As Ponto2D
            Return New Ponto2D((p2.X + p1.X) / 2, (p2.Y + p1.Y) / 2)
        End Function



        'Obtem a interseção entre os pontos e divide pela razão da interpolação: 1
        '1>interpolação>0==> divisão interna   eg=1/2 ==> ponto médio
        'interpolação>1  ==> divisão externa   pt está além de ptB  eg=2 ==> ptb é o ponto médio de ptA e retorna o ponto
        'interpolação<0  ==> divisão externa   pt está além de ptA  eg=-1 ==> ptA é o ponto médio de ptB e retorna o ponto
        Public Shared Function PontoInterpolado(ByVal ptA As Ponto2D, _
                                                ByVal ptB As Ponto2D, _
                                                ByVal FatorInterpolacao As Precisao) As Ponto2D

            Return New Ponto2D(ptA.X + (ptB.X - ptA.X) * FatorInterpolacao, ptA.Y + (ptB.Y - ptA.Y) * FatorInterpolacao)
        End Function


        Public Sub RthetaPonto(ByVal Pt As Ponto2D, ByRef r As Precisao, ByRef theta As Precisao)
            r = Math.Sqrt(Pt.X * Pt.X + Pt.Y * Pt.Y)
            theta = Math.Acos(Pt.X / r)
        End Sub


        Public Shared Function DistanciaPonto(ByVal pt1 As Ponto2D, ByVal pt2 As Ponto2D) As Precisao
            Return Math.Sqrt((pt1.X - pt2.X) ^ 2 + (pt1.Y - pt2.Y) ^ 2)
        End Function


        Public Shared Function PontoColinear(ByVal pt1 As Ponto2D, ByVal pt2 As Ponto2D, ByVal pt3 As Ponto2D) As Boolean
            Return VetorDe2Pontos(pt1, pt2).SeColinear(VetorDe2Pontos(pt2, pt3))
        End Function

        ' Retorna o ponto no formato texto (x,y,x)
        Public Overrides Function ToString() As String
            Return "(" & Me.X.ToString() & "," & Me.Y.ToString() & ")"
        End Function


        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Ponto2D) Then Return 0
            Dim ponto As Ponto2D = CType(obj, Ponto2D)
            Return Me.CompareTo(ponto)
        End Function


#End Region


    End Class



End Namespace
