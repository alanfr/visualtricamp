﻿Option Strict On

Imports BGeom.Ponto

Imports BMath.Matriz
Imports BMath.Funcoes

Imports System.Drawing
Imports Precisao = System.Double


Namespace Vetor



    Public Class Linha3D
        Implements ICloneable, IDisposable, IComparable


#Region "Declaracoes"

        Private privPonto3D As Ponto3D  'Ponto por onde a linha passa
        Private privVetorDiretor3D As Vetor3D 'Vetor diretor
        Private privCor As Color
        Private privNormal As Vetor3D


        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False


#End Region


#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub



        Public Sub New()
            Me.privPonto3D = New Ponto3D
            Me.privVetorDiretor3D = New Vetor3D
            Me.privCor = Color.White
            Me.privNormal = New Vetor3D
        End Sub



        Public Sub New(ByVal pt As Ponto3D, ByVal vetor As Vetor3D)
            Me.privPonto3D = pt
            Me.privVetorDiretor3D = vetor
        End Sub



        Public Sub New(ByVal p1 As Ponto3D, ByVal p2 As Ponto3D) ' formado por dois pontos
            Me.privPonto3D = p1
            Me.privVetorDiretor3D = New Vetor3D(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z)
        End Sub



#End Region



#Region "Propriedades"


        Public Property Ponto3D() As Ponto3D
            Get
                Return Me.privPonto3D
            End Get
            Set(value As Ponto3D)
                Me.privPonto3D = value
            End Set
        End Property


        Public Property VetorDiretor3D() As Vetor3D
            Get
                Return Me.privVetorDiretor3D
            End Get
            Set(value As Vetor3D)
                Me.privVetorDiretor3D = value
            End Set
        End Property

        Public Property Cor() As Color
            Get
                Return Me.privCor
            End Get
            Set(value As Color)
                Me.privCor = value
            End Set
        End Property

        Public Property Normal() As Vetor3D
            Get
                Return Me.privNormal
            End Get
            Set(value As Vetor3D)
                Me.privNormal = value
            End Set
        End Property


#End Region




#Region "Metodos"



#Region "Eixos"


        Public Shared Function eixoX() As Linha3D
            Return New Linha3D(Ponto3D.Origem3D, New Vetor3D(1, 0, 0))
        End Function

        Public Shared Function eixoY() As Linha3D
            Return New Linha3D(Ponto3D.Origem3D, New Vetor3D(0, 1, 0))
        End Function

        Public Shared Function eixoZ() As Linha3D
            Return New Linha3D(Ponto3D.Origem3D, New Vetor3D(0, 0, 1))
        End Function


#End Region




        ' Copia Linha3D.
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Linha3D(Me.Ponto3D, Me.VetorDiretor3D)
        End Function



        'Verifica se a linha contem ponto 
        Public Function SeContemPonto(ByVal ponto As Ponto3D) As Boolean
            If Me.VetorDiretor3D.SeColinear(Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, ponto)) Then
                Return True
            Else
                Return False
            End If
        End Function


        'Verifica se a linha e perpendicular ao vetor dado
        Public Function SePerpendicular(ByVal vetor As Vetor3D) As Boolean
            If Vetor3D.ProdutoEscalar(vetor, Me.VetorDiretor3D) = 0 Then
                Return True
            Else
                Return False
            End If
        End Function


        'Verifica se a linha é perpendicular a outra linha
        Public Function SePerpendicular(ByVal linha As Linha3D) As Boolean
            Return SePerpendicular(linha.VetorDiretor3D)
        End Function


        'Verifica se a linha e paralela ao vetor 
        Public Function SeParalelo(ByVal vetor As Vetor3D) As Boolean
            Return Me.VetorDiretor3D.SeColinear(vetor)
        End Function


        'Verifica se a linha e paralela a outra linha 
        Public Function SeParalelo(ByVal linha As Linha3D) As Boolean
            Return Me.VetorDiretor3D.SeColinear(linha.VetorDiretor3D)
        End Function


        'Calcula o "Foot Of perpendicular of point" na linha
        'http://mathworld.wolfram.com/PerpendicularFoot.html

        Public Function FootOfPoint(ByVal ponto As Ponto3D) As Ponto3D

            Dim n As Vetor3D

            If Ponto3D.Compara(Me.Ponto3D, ponto) Then
                Return Me.Ponto3D
            End If

            'Encontra o vetor que une os pontos
            n = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, ponto)

            'Faz a projeção do vetor na linha
            n = n.ComponenteParalela(Me.VetorDiretor3D)

            'Adiciona o vetor projetado ao ponto e retorna o "foot point"
            Return Ponto3D.ConverteVetor2Ponto3D(Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D) + n)
        End Function


        Public Function ImagemPonto(ByVal ponto As Ponto3D) As Ponto3D
            'pA.....FooOfPoint(A).....Imagem
            'InterpolaPonto3D(ponto, ponto, fator)
            Return Ponto3D.InterpolaPonto3D(ponto, Me.FootOfPoint(ponto), 2)
        End Function


        Public Function DistanciaPonto(ByVal ponto As Ponto3D) As Precisao
            Dim n As Vetor3D

            'Encontra o vetor que une os pontos
            n = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, ponto)

            'Retorna a projecao perpendicular do vetor ao longo da linha
            n = n.ComponentePerpendicular(Me.VetorDiretor3D)
            Return n.getComprimento
        End Function



        'Verifica se as linhas sao iguais
        Public Function SeIgual(ByVal linha As Linha3D) As Boolean
            '[n1,n1,(p1-p2)] =0 então coplanar
            If Me.SeParalelo(linha) And Me.SeContemPonto(linha.Ponto3D) Then
                Return True
            Else
                Return False
            End If
        End Function

        'Verifica se é coplanar
        Public Function SeCoplanar(ByVal linha As Linha3D) As Boolean
            '[n1,n1,(p1-p2)] =0 então coplanar
            If Vetor3D.ProdutoMisto(Me.VetorDiretor3D, linha.VetorDiretor3D, Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, linha.Ponto3D)) = 0 Then
                Return True
            Else
                Return False
            End If
        End Function



        'Verifica se é obliquo
        Public Function SeObliquo(ByVal linha As Linha3D) As Boolean
            '[n1,n1,(p1-p2)] <>0 então oblíquo
            If Vetor3D.ProdutoMisto(Me.VetorDiretor3D, linha.VetorDiretor3D, Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, linha.Ponto3D)) <> 0 Then
                Return True
            Else
                Return False
            End If
        End Function



        'Verifica se as linhas se encontram em algum ponto
        Public Function SeIntercede(ByVal linha As Linha3D) As Boolean
            '[n1,n1,(p1-p2)] <> 0  e linhas não paralelas = ocorre interseção
            If Me.SeCoplanar(linha) And Not Me.SeParalelo(linha) Then
                Return True
            Else
                Return False
            End If
        End Function



        'Verifica se ocorre intersecao
        'return 0: não ocorre
        '       1: em um unico ponto
        '       2: linhas coincidentes
        Public Function PontoIntersecao(ByVal line As Linha3D, ByVal P_return As Ponto3D) As Precisao
            '[n1,n1,(p1-p2)] <>0  e linhas não paralelas = ocorre interseção
            Dim Lambda, Mu As Precisao
            Dim v As Vetor3D
            If Me.SeCoplanar(line) = False Then
                Return 0 'Linhas não se encontram
            Else
                If Me.SeParalelo(line) Then
                    If Me.SeContemPonto(line.Ponto3D) Then
                        Return 2 'Linhas coincidentes
                    Else
                        Return 0 'Linhas paralelas mas não são ocincidentes
                    End If
                Else 'Linhas não paralelas e coplanares, então, devem se encontrar
                    'Resolve o sistema linear para encontrar o ponto de interseção
                    Linear2(Me.VetorDiretor3D.X, -line.VetorDiretor3D.X, line.Ponto3D.X - Me.Ponto3D.X, _
                            Me.VetorDiretor3D.Y, -line.VetorDiretor3D.Y, line.Ponto3D.Y - Me.Ponto3D.Y, _
                            Lambda, Mu)
                    v = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D) + Lambda * Me.VetorDiretor3D
                    P_return = Ponto3D.ConverteVetor2Ponto3D(v)
                    Return 1
                End If
            End If

        End Function



        'Encontrar a menor distância até o ponto de interseção
        'Para linhas oblíquoas   AB.(n1xn2)
        '                        ----------
        '                          |n1xn2|

        Public Function DistanciaLinha(ByVal linha As Linha3D) As Precisao
            '[n1,n1,(p1-p2)] <>0  e linhas não paralelas = interseção
            'Linhas iguais ==> distancia=0
            'Linhas.. paralelas ===> Projeção de AB é perpendicular a qualquer linhas
            'Linhas....oblíquoas  =====> Projeção ao longo da normal comum a ambas
            Dim n1xn2, AB As Vetor3D
            If Me.SeObliquo(linha) Then
                n1xn2 = Me.VetorDiretor3D * linha.VetorDiretor3D
                AB = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, linha.Ponto3D)
                AB = AB.ComponenteParalela(n1xn2)
                Return AB.getComprimento
            ElseIf Me.SeIgual(linha) Then
                Return 0
            Else 'Linhas paralelas não se intercedem
                AB = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, linha.Ponto3D)
                Return AB.ComponentePerpendicular(Me.VetorDiretor3D).getComprimento
            End If
        End Function



        'Retorna o ângulo entre as linhas
        Public Function AnguloLinha(ByVal linha As Linha3D) As Precisao
            Return Vetor3D.AnguloVetor(Me.VetorDiretor3D, linha.VetorDiretor3D)
        End Function



        'Retorna a equação de linhas na forma simétrica
        '   (x-x1)/a=(y-y1)/b=(z-z1)/c
        Public Overrides Function ToString() As String
            Dim c As Precisao
            Dim temp As String
            Dim s As String

            temp = ""
            c = -Me.Ponto3D.X
            If c <> 0 Then
                temp = CStr(IIf(c > 0, "+" + c.ToString, c.ToString))
            End If
            s = CStr(IIf(c = 0, "x/", "(x" + temp + ")/")) + Me.VetorDiretor3D.X.ToString + " = "


            temp = ""
            c = -Me.Ponto3D.Y
            If c <> 0 Then
                temp = CStr(IIf(c > 0, "+" + c.ToString, c.ToString))
            End If
            s = s + CStr(IIf(c = 0, "y/", "(y" + temp + ")/")) + Me.VetorDiretor3D.Y.ToString + " = "


            temp = ""
            c = -Me.Ponto3D.Z
            If c <> 0 Then
                temp = CStr(IIf(c > 0, "+" + c.ToString, c.ToString))
            End If
            s = s + CStr(IIf(c = 0, "z/", "(z" + temp + ")/")) + Me.VetorDiretor3D.Z.ToString

            Return s
        End Function


        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Linha3D) Then Return 0
            Dim matriz As Linha3D = CType(obj, Linha3D)
            Return Me.CompareTo(matriz)
        End Function


#End Region


    End Class


End Namespace

