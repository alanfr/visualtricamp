﻿Option Strict On

Imports BGeom.Ponto

Imports BMath.Funcoes
Imports BMath.Enumera

Imports System.Drawing
Imports Precisao = System.Double

Namespace Vetor

    Public Class Plano3D
        Implements IDisposable, ICloneable, IComparable


#Region "Declaracoes"

        Private privPonto3D As Ponto3D  'Ponto por onde a linha passa
        Private privVetorDiretor3D As Vetor3D 'Vetor direcao ou linha

        Private privCor As Color
        Private privNormal As Vetor3D

        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False

#End Region



#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub


        Public Sub New()
            Me.privPonto3D = New Ponto3D
            Me.privVetorDiretor3D = New Vetor3D
            Me.privCor = Color.White
            Me.privNormal = New Vetor3D
        End Sub


        Public Sub New(ByVal pt As Ponto3D, ByVal dir As Vetor3D)
            Me.Ponto3D = pt
            Me.VetorDiretor3D = dir
        End Sub


        'Cria um plano a partir de 3 pontos
        Public Sub New(ByVal pt1 As Ponto3D, ByVal pt2 As Ponto3D, ByVal pt3 As Ponto3D)
            'Se Ponto3D.SeColinear(pt1, pt2, pt3) = False Then Exit Sub
            Dim n As Vetor3D
            n = Vetor3D.ConvertePonto2Vetor3D(pt1, pt2) * Vetor3D.ConvertePonto2Vetor3D(pt1, pt3)
            Me.Ponto3D = pt1
            Me.VetorDiretor3D = n
        End Sub


        'Cria um plano a partir de 1 ponto e dois vetores
        Public Sub New(ByVal pt As Ponto3D, ByVal vetor3d1 As Vetor3D, ByVal vetor3d2 As Vetor3D)
            ' Se Point3D.CollinearPts(pt1, pt2, pt3) = False Then Exit Sub
            ' n = u x v
            Dim n As Vetor3D
            n = vetor3d1 * vetor3d2
            Me.Ponto3D = pt
            Me.VetorDiretor3D = n
        End Sub




        'Cria um plano que contem a linha e passa pelo ponto dado
        Public Sub New(ByVal pt As Ponto3D, ByVal linha As Linha3D)
            Dim n As Vetor3D
            n = Vetor3D.ConvertePonto2Vetor3D(pt, linha.Ponto3D) * linha.VetorDiretor3D
            Me.Ponto3D = pt
            Me.VetorDiretor3D = n
        End Sub




        'Cria um plano na forma ax+by+cz+d=0.
        Public Sub New(ByVal a As Precisao, ByVal b As Precisao, ByVal c As Precisao, ByVal d As Precisao)

            Dim pt As Ponto3D
            Dim n As Vetor3D
            n = New Vetor3D(a, b, c)
            If a <> 0 Then ' Eixo x corta o plano
                pt = New Ponto3D(-d / a, 0, 0)
            ElseIf b <> 0 Then 'Eixo y corta o plano
                pt = New Ponto3D(0, -d / b, 0)
            Else ' Eixo z corta o plano
                pt = New Ponto3D(0, 0, -d / b)
            End If

            Me.Ponto3D = pt
            Me.VetorDiretor3D = n
        End Sub
#End Region


#Region "Propriedades"


        Public Property Ponto3D() As Ponto3D
            Get
                Return Me.privPonto3D
            End Get
            Set(value As Ponto3D)
                Me.privPonto3D = value
            End Set
        End Property


        Public Property VetorDiretor3D() As Vetor3D
            Get
                Return Me.privVetorDiretor3D
            End Get
            Set(value As Vetor3D)
                Me.privVetorDiretor3D = value
            End Set
        End Property

        'Public Property Cor() As Color
        '    Get
        '        Return Me.privCor
        '    End Get
        '    Set(value As Color)
        '        Me.privCor = value
        '    End Set
        'End Property

        Public Property Normal() As Vetor3D
            Get
                Return Me.privNormal
            End Get
            Set(value As Vetor3D)
                Me.privNormal = value
            End Set
        End Property


#End Region



#Region "Planos"

        Public Shared Function planoXY() As Plano3D
            Return New Plano3D(Ponto3D.Origem3D, New Vetor3D(0, 0, 1))
        End Function


        Public Shared Function planoYZ() As Plano3D
            Return New Plano3D(Ponto3D.Origem3D, New Vetor3D(1, 0, 0))
        End Function


        Public Shared Function planoXZ() As Plano3D
            Return New Plano3D(Ponto3D.Origem3D, New Vetor3D(0, 1, 0))
        End Function

#End Region




#Region "Metodos"

        'verifica se o plano contem o ponto
        Public Function SeContemPonto(ByVal ponto As Ponto3D) As Boolean
            If Ponto3D.Compara(Me.Ponto3D, ponto) Then Return True : Exit Function
            Return Me.VetorDiretor3D.SePerpendicular(Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, ponto))
        End Function

        'verifica se o plano contem a linha
        Public Function SeContemLinha(ByVal linha As Linha3D) As Boolean
            Return Me.SeContemPonto(linha.Ponto3D) And Me.VetorDiretor3D.SePerpendicular(linha.VetorDiretor3D)
        End Function


        'Verifica se os planos são iguais
        Public Function SeIgual(ByVal P As Plano3D) As Boolean
            If Me.SeContemPonto(P.Ponto3D) And Me.VetorDiretor3D.SeColinear(P.VetorDiretor3D) Then
                Return True
            Else
                Return False
            End If
        End Function


        'Verifica se os planos sao paralelos
        Public Function SeParalelo(ByVal P As Plano3D) As Boolean
            If Me.VetorDiretor3D.SeColinear(P.VetorDiretor3D) Then
                Return True
            Else
                Return False
            End If
        End Function


        'Verifica se os planos sao perpendiculares
        Public Function SePerpendicular(ByVal P As Plano3D) As Boolean
            If Me.VetorDiretor3D.SePerpendicular(P.VetorDiretor3D) Then
                Return True
            Else
                Return False
            End If
        End Function


        'Retorna o plano paralelo ao plano atual que passa por um dado ponto.
        Public Function getPlanoParalelo(ByVal pt As Ponto3D) As Plano3D
            Return New Plano3D(pt, Me.VetorDiretor3D)
        End Function


        'Retorna 0: se a linha de projecao nao existe(linha perpendicular ao plano)
        '        1: se a linha de projecao existir
        Public Function getProjecaoLinha(ByVal linha As Linha3D, ByRef retorno As Linha3D) As Precisao

            'se a linha for paralela as coordenadas do plano, a projeção sera a propria linha
            If linha.SePerpendicular(Me.VetorDiretor3D) Then retorno = linha : Return 1 : Exit Function

            'se a linha for perpendicular ao plano, sua projecao sera um ponto
            If linha.SeParalelo(Me.VetorDiretor3D) Then Return 0 : Exit Function
            Dim pt1 As Ponto3D
            Dim pt2 As Ponto3D = Nothing

            'verifica se pt1 já se encontra no plano.
            ' caso afirmativo, deve escolher outro ponto
            pt1 = CType(IIf(Me.SeContemPonto(linha.Ponto3D), Ponto3D.TransladaPonto3D(linha.Ponto3D, linha.VetorDiretor3D), linha.Ponto3D), Ponto3D)
            Call Me.getPontoIntersecao(linha, pt2)
            retorno = New Linha3D(pt1, Me.getFootOfPt(pt2))
            Return 1
        End Function




        Public Function getImagemLinha(ByVal linha As Linha3D) As Linha3D
            ' Se Plano1.VetorDiretor3D.SeColinear(Plano2.VetorDiretor3D) Então => Exit Sub
            Dim pt1 As Ponto3D
            Dim pt2 As Ponto3D = Nothing
            pt1 = CType(IIf(Me.SeContemPonto(linha.Ponto3D), Ponto3D.TransladaPonto3D(linha.Ponto3D, linha.VetorDiretor3D), linha.Ponto3D), Ponto3D)
            Call Me.getPontoIntersecao(linha, pt2)
            Return New Linha3D(pt2, Me.getImagemPonto(pt1))
        End Function



        Public Function getAngulo(ByVal Plano As Plano3D) As Precisao
            Return Vetor3D.AnguloVetor(Me.VetorDiretor3D, Plano.VetorDiretor3D)
        End Function



        Public Function getAngulo(ByVal dir As Vetor3D) As Precisao
            Return Vetor3D.AnguloVetor(Me.VetorDiretor3D, dir)
        End Function



        Public Function getAngulo(ByVal linha As Linha3D) As Precisao
            Return Vetor3D.AnguloVetor(Me.VetorDiretor3D, linha.VetorDiretor3D)
        End Function



        'get o plano normal aos planos que passam pelo ponto.
        Public Function getDistanciaPonto(ByVal pt As Ponto3D) As Precisao
            Dim n As Vetor3D
            'encontra o vetor normal ao ponto no plano 
            n = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, pt)
            'retorna a projeção desse vetor ao longo do vetor normal ao plano
            n = n.ComponenteParalela(Me.VetorDiretor3D)
            Return n.getComprimento
        End Function



        'Verifica se dois pontos estão no mesmo plano
        'returns 0: se os dois pontos estão no plano
        '        1: se um ponto está no plano
        '        2: se ambos estão no mesmo lado
        '        3: se ambos estão em lados opostos ao plano
        Public Function PontosNoMesmoLado(ByVal P1 As Ponto3D, ByVal P2 As Ponto3D) As Precisao
            Dim A, B, C As Vetor3D
            Dim V1, V2 As Precisao
            A = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D)
            B = Vetor3D.ConvertePonto2Vetor3D(P1)
            C = Vetor3D.ConvertePonto2Vetor3D(P2)

            V1 = Vetor3D.ProdutoEscalar(Me.VetorDiretor3D, B) - Vetor3D.ProdutoEscalar(Me.VetorDiretor3D, A)
            V2 = Vetor3D.ProdutoEscalar(Me.VetorDiretor3D, C) - Vetor3D.ProdutoEscalar(Me.VetorDiretor3D, A)
            If V1 = 0 And V2 = 0 Then Return 0 : Exit Function
            If V1 * V2 = 0 Then
                Return 1 ' exatamente um ponto sobre o plano
            ElseIf V1 * V2 > 0 Then
                Return 2 'pontos estão no mesmo lado
            Else
                Return 3 ' pontos estão em lados opostos ao plano e ao segmento que corta o plano
            End If

        End Function


        Public Function getFootOfPt(ByVal pt As Ponto3D) As Ponto3D
            Dim n As Vetor3D
            'encontrar o vetor que passa pelo ponto no plano 
            n = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, pt)
            'retorna a projeção desse vetor ao longo do vetor normal ao plano
            n = Vetor3D.ConvertePonto2Vetor3D(pt) + n.ComponenteParalela(Me.VetorDiretor3D)
            Return Ponto3D.ConverteVetor2Ponto3D(n)
        End Function



        Public Function getImagemPonto(ByVal pt As Ponto3D) As Ponto3D
            Dim n As Vetor3D
            'encontrar o vetor que passa pelo ponto no plano
            n = Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D, pt)
            'retorna a projeção desse vetor ao longo do vetor normal ao plano
            n = Vetor3D.ConvertePonto2Vetor3D(pt) + 2 * n.ComponenteParalela(Me.VetorDiretor3D)
            Return Ponto3D.ConverteVetor2Ponto3D(n)
        End Function



        'Retorna o ponto de interseção
        'Return  0: Se a linha é paralela ao plano => não existe interseção
        '        1: Se a linha corta o plano em um unico ponto
        '        2: Se a linha não corta o plano
        Public Function getPontoIntersecao(ByVal line As Linha3D, ByRef P_return As Ponto3D) As Precisao
            'Se Linha.VetorDiretor3D.SePerpendicular(Plano.VetorDiretor3D) Então => exit sub
            Dim r As Vetor3D
            Dim m As Precisao
            If line.SeParalelo(Me.VetorDiretor3D) Then Return 0 : Exit Function
            If Me.SeContemLinha(line) Then Return 2 : Exit Function
            r = Vetor3D.ConvertePonto2Vetor3D(line.Ponto3D, Me.Ponto3D)
            m = r.getProdutoEscalar(Me.VetorDiretor3D) / Me.VetorDiretor3D.getProdutoEscalar(line.VetorDiretor3D)
            P_return = Ponto3D.ConverteVetor2Ponto3D(Vetor3D.ConvertePonto2Vetor3D(line.Ponto3D) + m * line.VetorDiretor3D)
            Return 1
        End Function


        'Verifica o ponto de interseção entre planos na forma ax+by+cz=d
        'Return  0: Se os planos não se encontram
        '        1: Se os planos se encontram em uma unica linha
        '        2: Se os planos são coincidentes
        Public Function getIntersecaoLinha(ByVal plano As Plano3D, ByRef retorno As Linha3D) As Precisao

            If Me.SeIgual(plano) Then
                Return 2 'Infinitas linhas
            ElseIf Me.SeParalelo(plano) Then
                Return 0 'Planos não se encontram
            Else 'Planos se encontram em uma linha
                Dim n1xn2 As Vetor3D
                Dim pt As Ponto3D
                n1xn2 = Me.VetorDiretor3D * plano.VetorDiretor3D
                Dim x, y, z As Precisao
                If Linear2(Me.VetorDiretor3D.X, Me.VetorDiretor3D.Y, Me.VetorDiretor3D.getProdutoEscalar(Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D)), _
                           plano.VetorDiretor3D.X, plano.VetorDiretor3D.Y, plano.VetorDiretor3D.getProdutoEscalar(Vetor3D.ConvertePonto2Vetor3D(plano.Ponto3D)), x, y) = TiposDeSolucao.UNICA_SOLUCAO Then
                    pt = New Ponto3D(x, y, 0)
                Else
                    Linear2(Me.VetorDiretor3D.X, Me.VetorDiretor3D.Z, Me.VetorDiretor3D.getProdutoEscalar(Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D)), _
                            plano.VetorDiretor3D.X, plano.VetorDiretor3D.Z, plano.VetorDiretor3D.getProdutoEscalar(Vetor3D.ConvertePonto2Vetor3D(plano.Ponto3D)), x, z)
                    pt = New Ponto3D(x, 0, z)
                End If
                Return 1
                retorno = New Linha3D(pt, n1xn2)
            End If
        End Function




        'Retorna a equação do plano na forma cartesiana
        Public Overrides Function ToString() As String
            Dim builder As New System.Text.StringBuilder()
            Dim c As Precisao

            Dim s As String
            s = ""
            c = Me.VetorDiretor3D.X
            If c <> 0 Then s = CStr(IIf(Abs(c) = 1, "x", c.ToString & "x"))
            c = Me.VetorDiretor3D.Y
            If c <> 0 Then s = s + CStr(IIf(c > 0, IIf(s = "", "", "+"), "-")) + CStr(IIf(Abs(c) = 1, "y", Abs(c).ToString & "y"))
            c = Me.VetorDiretor3D.Z
            If c <> 0 Then s = s + CStr(IIf(c > 0, IIf(s = "", "", "+"), "-")) + CStr(IIf(Abs(c) = 1, "z", Abs(c).ToString & "z"))
            c = Vetor3D.ProdutoEscalar(Vetor3D.ConvertePonto2Vetor3D(Me.Ponto3D), Me.VetorDiretor3D)
            If c <> 0 Then s = s + CStr(IIf(c < 0, "+", "-")) + Abs(c).ToString
            s = s + "=0"
            Return s
        End Function


        ' Copia Linha3D.
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Plano3D(Me.Ponto3D, Me.VetorDiretor3D)
        End Function


        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Plano3D) Then Return 0
            Dim plano As Plano3D = CType(obj, Plano3D)
            Return Me.CompareTo(plano)
        End Function

#End Region

    End Class

End Namespace
