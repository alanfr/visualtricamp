﻿Option Strict On

Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes

Imports BGeom.Ponto
Imports BGeom.Vetor

Imports System.Windows.Forms

Imports Precisao = System.Double



Namespace Transformacoes


    Public Module Homogenea3D


#Region "Matrizes Transformacao"

        'Matriz que faz a translacao para o local correspondente aos pontos dados
        Public Function MatrizTranslacao3D(ByVal Tx As Precisao, ByVal Ty As Precisao, ByVal Tz As Precisao) As Matriz4x4
            '          | 1   0   0   0 |
            '          | 0   1   0   0 |
            '[x,y,z,1] | 0   0   0   1 |=[x+Tx, y+Ty, z+Tz, 1]
            '          | Tx  Ty  Tz  1 |
            '
            MatrizTranslacao3D = Matriz4x4.Identidade
            MatrizTranslacao3D.M41 = Tx
            MatrizTranslacao3D.M42 = Ty
            MatrizTranslacao3D.M43 = Tz

            Return MatrizTranslacao3D
        End Function

        'Matriz que faz a translacao para o local correspondente aos pontos dados
        Public Function MatrizTranslacao3D(ByVal P1 As Ponto3D) As Matriz4x4
            '          | 1   0   0   0 |
            '          | 0   1   0   0 |
            '[x,y,z,1] | 0   0   0   1 |=[x+Tx, y+Ty, z+Tz, 1]
            '          | Tx  Ty  Tz  1 |
            '
            MatrizTranslacao3D = Matriz4x4.Identidade
            MatrizTranslacao3D.M41 = P1.x
            MatrizTranslacao3D.M42 = P1.y
            MatrizTranslacao3D.M43 = P1.z

            Return MatrizTranslacao3D
        End Function


        ' Cria a matriz para escalonar de acordo com os fatores dados em relacao a origem
        Public Function MatrizEscala3D(ByVal Sx As Precisao, ByVal Sy As Precisao, ByVal Sz As Precisao) As Matriz4x4
            '          | Sx  0   0   0 |
            '          | 0   Sy  0   0 |
            '[x,y,z,1] | 0   0   Sz  1 |=[Sx, Sy, Sz, 1]
            '          | 0   0   0   1 |
            '            
            MatrizEscala3D = Matriz4x4.Identidade()
            MatrizEscala3D.M11 = Sx
            MatrizEscala3D.M22 = Sy
            MatrizEscala3D.M33 = Sz

            Return MatrizEscala3D
        End Function



        'Escalona mantendo o ponto dado fixo (Para zoom considerando a origem)
        Public Function MatrizEscala3D(ByVal Sx As Precisao, ByVal Sy As Precisao, ByVal Sz As Precisao, ByVal Pt As Vetor3D) As Matriz4x4
            '1: Faz a translacao do ponto(centro do objeto) ate a origem)===> pt*T
            '2: Escalona o ponto pelo fator dado ===> pt*(T*S)
            '3: Faz a translacao do centro de volta para a posicao inicial ===> pt*(T*S)*T-inv
            Dim T, S, T_inv As Matriz4x4
            T = MatrizTranslacao3D(-Pt.X, -Pt.Y, -Pt.Z)
            S = MatrizEscala3D(Sx, Sy, Sz)
            T_inv = MatrizTranslacao3D(Pt.X, Pt.Y, Pt.Z)
            Return (T * S) * T_inv
        End Function



        'Multiplica a matriz por um vetor
        Public Function MatrizMultiplicaVetor3D(ByVal Vec As Vetor3D, ByVal Mat As Matriz4x4) As Vetor3D
            Dim rv As Vetor3D
            rv = New Vetor3D(0, 0, 0)
            rv.x = (Mat.M11 * Vec.x) + (Mat.M12 * Vec.y) + (Mat.M13 * Vec.z) + (Mat.M14)
            rv.y = (Mat.M21 * Vec.x) + (Mat.M22 * Vec.y) + (Mat.M23 * Vec.z) + (Mat.M24)
            rv.z = (Mat.M31 * Vec.x) + (Mat.M32 * Vec.y) + (Mat.M33 * Vec.z) + (Mat.M34)
            Return rv
        End Function


        'Reflete a matriz em relacao ao plano XY
        Public Function MatrizRefleteXY() As Matriz4x4
            '          | 1   0   0   0 |
            '          | 0   1   0   0 |
            '[x,y,z,1] | 0   0   -1  1 |=[x, y, -z, 1]
            '          | 0   0   0   1 |
            '            
            Return Matriz4x4.Identidade() * MatrizEscala3D(1, 1, -1)
        End Function



        'Reflete a matriz em relacao ao plano YZ
        Public Function MatrizRefleteYZ() As Matriz4x4
            '          | -1  0   0   0 |
            '          | 0   1   0   0 |
            '[x,y,z,1] | 0   0   1   1 |=[-x, y, z, 1]
            '          | 0   0   0   1 |
            '            
            Return Matriz4x4.Identidade() * MatrizEscala3D(-1, 1, 1)
        End Function

        'Reflete a matriz em relacao ao plano XZ
        Public Function MatrizRefleteXZ() As Matriz4x4
            '          | 1   0   0   0 |
            '          | 0   -1  0   0 |
            '[x,y,z,1] | 0   0   1   1 |=[x, -y, z, 1]
            '          | 0   0   0   1 |
            '            
            Return Matriz4x4.Identidade() * MatrizEscala3D(1, -1, 1)
        End Function


        'Matriz cisalhamento
        Public Function MatrizCisalhamento3D(ByVal Sx As Precisao, ByVal Sy As Precisao, ByVal Sz As Precisao) As Matriz4x4
            MatrizCisalhamento3D = Matriz4x4.Identidade()
            MatrizCisalhamento3D.M13 = Sx
            MatrizCisalhamento3D.M23 = Sy
            MatrizCisalhamento3D.M33 = Sz

            Return MatrizCisalhamento3D
        End Function

        'Matriz que rotaciona em torno do eixo x
        Public Function MatrizRotacionaX(ByVal AnguloGraus As Precisao) As Matriz4x4
            '          | 1    0     0   0 |
            '          | 0   cos   sin  0 |
            '[x,y,z,1] | 0   -sin  cos  1 |=[x, ycos-zsin, zcos+ycos, 1]
            '          | 0    0     0   1 |

            MatrizRotacionaX = Matriz4x4.Identidade()

            With MatrizRotacionaX
                .M22 = CosG(AnguloGraus)
                .M23 = SenG(AnguloGraus)
                .M32 = -.M23
                .M33 = .M22
            End With

            Return MatrizRotacionaX

        End Function

        'Matriz que rotaciona entorno do eixo y
        Public Function MatrizRotacionaY(ByVal AnguloGraus As Precisao) As Matriz4x4
            '          | cos   0  -sin  0 |
            '          |  0    1    0   0 |
            '[x,y,z,1] | sin   0   cos  1 |=[xcos+zsin, y, zcos-xsin, 1]
            '          |  0    0    0   1 |

            MatrizRotacionaY = Matriz4x4.Identidade()
            With MatrizRotacionaY
                .M11 = CosG(AnguloGraus)
                .M13 = -SenG(AnguloGraus)
                .M31 = -.M13
                .M33 = .M11
            End With
            Return MatrizRotacionaY
        End Function


        'Matriz que rotaciona entorno do eixo z
        Public Function MatrizRotacionaZ(ByVal AnguloGraus As Precisao) As Matriz4x4
            '          |  cos   sin   0   0 |
            '          | -sin   cos   0   0 |
            '[x,y,z,1] |   0     0    1   0 |=[xcos-ysin, ycos+xsin, z, 1]
            '          |   0     0    0   1 |

            MatrizRotacionaZ = Matriz4x4.Identidade()
            With MatrizRotacionaZ
                .M11 = CosG(AnguloGraus)
                .M12 = SenG(AnguloGraus)
                .M21 = -.M12
                .M22 = .M11
                .M33 = 1
                .M44 = 1
            End With
            Return MatrizRotacionaZ
        End Function




        'Rotaciona o plano XY ao redor do eixo Z, de forma que o ponto pertenca ao plano de rotacao
        Public Function MatrizRotacionaXY(ByVal pt As Vetor3D) As Matriz4x4
            '          |  cos   sin   0   0 | |  x/d   -y/d  0   0 |
            '          | -sin   cos   0   0 | |  y/d   x/d   0   0 |
            '[x,y,z,1] |   0     0    1   0 |=|   0     0    1   0 | = [d, 0, z, 1]
            '          |   0     0    0   1 | |   0     0    0   1 |
            'd=sqrt(x^2+y^2)
            MatrizRotacionaXY = Matriz4x4.Identidade()
            Dim d As Precisao
            If pt.Z = 0 Then Return MatrizRotacionaXY : Exit Function 'ponto realmente no plano xy
            d = Raiz(pt.Y ^ 2 + pt.Z ^ 2)
            Return MatrizRotacionaX(AcosG(pt.Y / d))
        End Function

        'Rotaciona o plano XZ ao redor do eixo Y, de forma que o ponto pertenca ao plano de rotacao
        Public Function MatrizRotacionaXZ(ByVal pt As Vetor3D) As Matriz4x4
            '          |  cos   sin   0   0 | |  x/d   -y/d  0   0 |
            '          | -sin   cos   0   0 | |  y/d   x/d   0   0 |
            '[x,y,z,1] |   0     0    1   0 |=|   0     0    1   0 | = [d, 0, z, 1]
            '          |   0     0    0   1 | |   0     0    0   1 |
            'd=sqrt(y^2+z^2)
            MatrizRotacionaXZ = Matriz4x4.Identidade()
            Dim d As Precisao
            If pt.Y = 0 Then Return MatrizRotacionaXZ : Exit Function 'ponto realmente pertence ao plano XZ
            d = Raiz(pt.X ^ 2 + pt.Y ^ 2)
            With MatrizRotacionaXZ
                .M11 = pt.X / d
                .M12 = -pt.Y / d
                .M21 = pt.Y / d
                .M22 = pt.X / d
            End With
            Return MatrizRotacionaXZ
        End Function



        Public Function MatrizRotacionaYZ(ByVal pt As Vetor3D) As Matriz4x4
            '          |  cos   sin   0   0 | |  y/d   x/d   0   0 |
            '          | -sin   cos   0   0 | | -x/d   y/d   0   0 |
            '[x,y,z,1] |   0     0    1   0 |=|   0     0    1   0 | = [d, 0, z, 1]
            '          |   0     0    0   1 | |   0     0    0   1 |
            'd=sqrt(x^2+y^2)
            MatrizRotacionaYZ = Matriz4x4.Identidade()
            Dim d As Precisao
            If pt.X = 0 Then Return MatrizRotacionaYZ : Exit Function 'ponto no plano yz
            d = Raiz(pt.X ^ 2 + pt.Y ^ 2)
            With MatrizRotacionaYZ
                .M11 = pt.Y / d
                .M12 = pt.X / d
                .M21 = -pt.X / d
                .M22 = pt.Y / d
            End With
            Return MatrizRotacionaYZ
        End Function



        'Matriz que rotaciona o vetor no plano segundo a normal
        Public Function MatrizRefleteNoPlano(ByVal pt As Vetor3D, ByVal n As Vetor3D) As Matriz4x4
            '1:Translada o ponto para a origem                             ===> pt*T
            '2:Rotaciona N no plano YZ                                     ===> pt*(T*R1)
            '3:Rotaciona novamente em X, deixando paralelo ao eixo X       ===> pt*(T*R1)*R2
            '4:Passa a ser o plano XZ (x,y,z)-->(x,-y,z)                   ===> pt(*(T*R1)*R2)*S
            '5:Reverte a operacao para R2                                  ===> pt((*(T*R1)*R2)*S*)R2i
            '5:Reverte a operacao para R1                                  ===> pt(((*(T*R1)*R2)*S*)*R2i)*R1i
            '6:Reverte a translacao                                        ===> pt(((*(T*R1)*R2)*S*)*R2i)*R1i*Ti

            Dim T, R1, R2, S, R1_inv, R2_inv, T_inv As Matriz4x4
            Dim d, L As Precisao

            'Faz a translacao para a origem
            T = MatrizTranslacao3D(-pt.X, -pt.Y, -pt.Z)
            T_inv = MatrizTranslacao3D(pt.X, pt.Y, pt.Z)

            'Rotaciona a normal para o plano YZ
            R1 = MatrizRotacionaYZ(n)
            
            R1_inv = Matriz4x4.Identidade()
            With R1_inv
                .M11 = R1.M11
                .M12 = -R1.M12
                .M21 = -R1.M21
                .M22 = R1.M22
            End With

            R2 = Matriz4x4.Identidade()
            d = Raiz(n.X ^ 2 + n.Y ^ 2)
            L = n.getComprimento
            With R2
                .M22 = d / L
                .M23 = -n.Z / L
                .M32 = -.M23
                .M33 = .M22
            End With

            R2_inv = Matriz4x4.Identidade()
            With R2_inv
                .M22 = R2.M22
                .M23 = -R2.M23
                .M32 = -R2.M32
                .M33 = R2.M33
            End With

            'Reflete no plano XZ
            S = MatrizRefleteXZ()

            'Combina as tranformacoes
            Return T * R1 * R2 * S * R2_inv * R1_inv * T_inv

        End Function


        ' Cria a matriz transformacao para rotacionar um angulo no entorno da linha
        ' no ponto pt e na direcao do vetor n.
        Public Function MatrizRotacionaLinha(ByVal pt As Vetor3D, ByVal n As Vetor3D, ByVal theta_D As Precisao) As Matriz4x4
            ' Theta is measured counterclockwise as you look down the line opposite the line's direction.
            '1. Translate the line to the origin.
            '2. Rotate around the Z axis until the line lies in the Y-Z plane.
            '3. Rotate around the X axis until the line lies along the Y axis.
            '4. Rotate around the Y axis.
            '5. Reverse the second rotation.
            '6. Reverse the first rotation.
            '7. Reverse the translation.
            Dim T, R1, R2, R3, R2_inv, R1_inv, T_inv As Matriz4x4
            Dim d, L As Precisao
            ' Translate the line to the origin.
            T = MatrizTranslacao3D(-pt.X, -pt.Y, -pt.Z)
            T_inv = MatrizTranslacao3D(-pt.X, -pt.Y, -pt.Z)
            ' Rotate around Z-axis until the line is in
            ' the Y-Z plane.
            d = Raiz(n.X ^ 2 + n.Y ^ 2)
            R1 = MatrizRotacionaYZ(n)
            '///////////////////////////
            ' R1 = MatrixRotateIntoYZ(n) can be implemented in following way also
            '            '  d = Sqrt(n.x ^ 2 + n.y ^ 2)
            '               With R1
            '                 .M11 = n.y / d
            '                 .M12 = n.x / ds
            '                 .M21 = -.M12
            '                 .M22 = .M11
            '                End With
            '///////////////////////////////////////
            R1_inv = Matriz4x4.Identidade()
            With R1_inv
                .M11 = R1.M11
                .M12 = -R1.M12
                .M21 = -R1.M21
                .M22 = R1.M22
            End With
            ' Rotate around the X-axis until the line
            ' lies along the Y axis.
            R2 = Matriz4x4.Identidade()
            L = n.getComprimento
            With R2
                .M22 = d / L
                .M23 = -n.Z / L
                .M32 = -.M23
                .M33 = .M22
            End With

            R2_inv = Matriz4x4.Identidade()
            With R2_inv
                .M22 = R2.M22
                .M23 = -R2.M23
                .M32 = -R2.M32
                .M33 = R2.M33
            End With

            ' Rotate around the line (Y axis).
            R3 = MatrizRotacionaY(theta_D)

            'combine the matrices and return
            Return T * R1 * R2 * R3 * R2_inv * R1_inv * T_inv  'left to right precedence
        End Function



        'Vectors-based matrix rotation, rotate from a free point to another free point (arbitrary rotation or planar rotation).
        Public Function MatrizRotacionaPorVetores(ByVal VecFrom As Vetor3D, ByVal VecTo As Vetor3D) As Matriz4x4
            'This is very important function in this program, because we can use it for:
            ' - Orienting the cameras
            ' - Making the Primitives (the most of theme)
            Dim rv As Matriz4x4 = Matriz4x4.Zeros()
            Dim N As Vetor3D, U As Vetor3D, V As Vetor3D
            N = VecTo.getNormaliza
            U = (Vetor3D.ProdutoVetorial(VecFrom.getNormaliza, N)).getNormaliza
            V = Vetor3D.ProdutoVetorial(N, U) 'The cross-product gives a normalized vector,
            'because both input vectors are normalized,
            'so we don't need to normalize.
            With rv
                .M11 = U.X : .M12 = U.Y : .M13 = U.Z
                .M21 = N.X : .M22 = N.Y : .M23 = N.Z
                .M31 = V.X : .M32 = V.Y : .M33 = V.Z
                .M44 = 1
            End With
            Return rv
        End Function



        'Create a 3-D transformation matrix for a perspective projection into the X-Z plane with center of projection at the origin and 
        'the plane of projection at distance D.
        Public Function MatrizPerspectivaXZ(ByVal D As Precisao) As Matriz4x4
            Dim rv As Matriz4x4
            rv = Matriz4x4.Identidade()
            If D <> 0 Then
                rv.M34 = -1 / D
                rv.M33 = 0
            End If
            Return rv
        End Function


        'Aplica a matriz transformação ao Ponto3D (translacao, reflexao etc)
        Public Function AplicaTransformacao3D(ByVal Pt As Ponto3D, ByVal T As Matriz4x4) As Ponto3D

            Dim pt1 As New Ponto3D

            pt1.X = Pt.X * T.M11 + Pt.Y * T.M21 + Pt.Z * T.M31 + T.M41
            pt1.Y = Pt.X * T.M12 + Pt.Y * T.M22 + Pt.Z * T.M32 + T.M42
            pt1.Z = Pt.X * T.M13 + Pt.Y * T.M23 + Pt.Z * T.M33 + T.M43

            Return pt1

        End Function


        'Aplica a matriz transformação ao Vetor3D
        Public Function AplicaTransformacao3D(ByVal V As Vetor3D, ByVal T As Matriz4x4) As Vetor3D
            Dim rv As Vetor3D
            rv = New Vetor3D(0, 0, 0)
            rv.x = V.x * T.M11 + V.y * T.M21 + V.z * T.M31 + T.M41
            rv.y = V.x * T.M12 + V.y * T.M22 + V.z * T.M32 + T.M42
            rv.z = V.x * T.M13 + V.y * T.M23 + V.z * T.M33 + T.M43
            Return rv
        End Function

#End Region




#Region "Matriz Euler e projecao"



        Public Function ProjetaTela(ByVal Ponto As Ponto3D, ByVal Local As PictureBox) As Ponto3D
            Dim xn As Double
            Dim yn As Double

            xn = Ponto.x + Local.Width * 0.5 'Coloca o centro da tela como o centro das coordenadas
            yn = Ponto.y + Local.Height * 0.5

            Return New Ponto3D(xn, yn, Ponto.z)
        End Function



        'Matriz Euler
        ' Referencia: Classical Mechanics, 3ed - Goldstein, Poole e Safko
        ' Pg 150 - Euler Angles
        Public Function MatrizEulerPos(ByVal P1 As Ponto3D, ByVal P2 As Ponto3D, ByVal Tipo As Integer) As Matriz4x4
            'Tipo:
            '0 = Cilindro
            '1 = Toroide
            '2 = Esfera
            '3 = Placa
            Dim V As Vetor3D
            Dim Rz As Matriz4x4 = MatrizRotacionaX(0)
            Dim YAW, PITCH, ROLL As Double

            V = Vetor3D.ConvertePonto2Vetor3D(P2 - P1)
            YAW = Math.Atan2(V.Z, V.Y) * RADIAN ' Converte para graus
            PITCH = Math.Atan2(V.Z, V.X) * RADIAN
            ROLL = Math.Atan2(V.Y, V.X) * RADIAN

            Dim xMat As Matriz4x4 = MatrizRotacionaX(YAW)
            Dim yMat As Matriz4x4 = MatrizRotacionaY(PITCH)
            Dim zMat As Matriz4x4 = MatrizRotacionaZ(ROLL)

            'Corrige Orientacao do eixo Z
            If (Tipo = 0) Then
                If (V.Z > ZERO) Then
                    Rz = MatrizRotacionaX(180)
                ElseIf (V.Z < 0) Then
                    Rz = MatrizRotacionaX(-180)
                End If
            ElseIf (Tipo = 1) Or (Tipo = 2) Then
                Rz = MatrizRotacionaX(-90) * MatrizRotacionaZ(90)
            ElseIf (Tipo = 3) Then
                Rz = MatrizRotacionaZ(0)
            End If


            Return xMat * (yMat * zMat) * Rz * MatrizTranslacao3D(P1)

        End Function





        'Matriz Euler
        ' Referencia: Classical Mechanics, 3ed - Goldstein, Poole e Safko
        ' Pg 150 - Euler Angles
        Public Function MatrizEulerPos(ByVal P1 As Ponto3D, ByVal P2 As Ponto3D) As Matriz4x4

            Dim V As Vetor3D
            Dim YAW, PITCH, ROLL As Double

            V = Vetor3D.ConvertePonto2Vetor3D(P2 - P1)
            YAW = Math.Atan2(V.Z, V.Y) * RADIAN ' Converte para graus
            PITCH = Math.Atan2(V.Z, V.X) * RADIAN
            ROLL = Math.Atan2(V.Y, V.X) * RADIAN

            Dim xMat As Matriz4x4 = MatrizRotacionaX(YAW)
            Dim yMat As Matriz4x4 = MatrizRotacionaY(PITCH)
            Dim zMat As Matriz4x4 = MatrizRotacionaZ(ROLL)

            Return xMat * (yMat * zMat)

        End Function




        Public Function MatrizEuler(ByVal V As Vetor3D) As Matriz4x4
            Dim YAW, PITCH, ROLL As Double

            V.Z = -V.Z  'Colocar o eixo Z saindo da tela

            YAW = Math.Atan2(V.Z, V.Y) * RADIAN ' Converte para graus
            PITCH = Math.Atan2(V.Z, V.X) * RADIAN
            ROLL = Math.Atan2(V.Y, V.X) * RADIAN


            Dim xMat As Matriz4x4 = MatrizRotacionaX(YAW)
            Dim yMat As Matriz4x4 = MatrizRotacionaY(PITCH)
            Dim zMat As Matriz4x4 = MatrizRotacionaZ(ROLL)

            Return xMat * (yMat * zMat)
        End Function



        Public Function MatrizEuler(ByVal yaw As Double, ByVal pitch As Double, ByVal roll As Double) As Matriz4x4
            Dim xMat As Matriz4x4 = MatrizRotacionaX(yaw)
            Dim yMat As Matriz4x4 = MatrizRotacionaY(pitch)
            Dim zMat As Matriz4x4 = MatrizRotacionaZ(roll)

            Return xMat * (yMat * zMat)
        End Function



#End Region


    End Module


End Namespace

