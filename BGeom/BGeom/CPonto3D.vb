﻿Option Strict On

Imports BGeom.Vetor

Imports BMath.Funcoes
Imports BMath.Matriz
Imports BMath.Constantes


Imports System.Drawing
Imports Precisao = System.Double

Namespace Ponto

    Public Class Ponto3D
        Implements IDisposable, ICloneable, IComparable



#Region "Declaracoes"


        'Point3D: stores Point in 3D
        Private privX As Precisao
        Private privY As Precisao
        Private privZ As Precisao

        Private privTX As Precisao
        Private privTY As Precisao
        Private privTZ As Precisao

        Private privCor As Color
        Private privNormal As Vetor3D


        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False
        'Public Normal As Vector3D

#End Region


#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub



        Public Sub New()
            Me.privX = 0.0
            Me.privY = 0.0
            Me.privZ = 0.0
            Me.privTX = 0.0
            Me.privTY = 0.0
            Me.privTZ = 0.0
            Me.Cor = Color.White
            Me.Normal = New Vetor3D
        End Sub


        Public Sub New(ByVal x As Precisao, ByVal y As Precisao, ByVal z As Precisao)
            Me.privX = x
            Me.privY = y
            Me.privZ = z
        End Sub


#End Region


#Region "Propriedades"

        Public Property X() As Precisao
            Get
                Return privX
            End Get
            Set(value As Precisao)
                privX = value
            End Set
        End Property

        Public Property Y() As Precisao
            Get
                Return privY
            End Get
            Set(value As Precisao)
                privY = value
            End Set
        End Property

        Public Property Z() As Precisao
            Get
                Return privZ
            End Get
            Set(value As Precisao)
                privZ = value
            End Set
        End Property




        Public Property TX() As Precisao
            Get
                Return privTX
            End Get
            Set(value As Precisao)
                privTX = value
            End Set
        End Property

        Public Property TY() As Precisao
            Get
                Return privTY
            End Get
            Set(value As Precisao)
                privTY = value
            End Set
        End Property

        Public Property TZ() As Precisao
            Get
                Return privTZ
            End Get
            Set(value As Precisao)
                privTZ = value
            End Set
        End Property

        Public Property Cor() As Color
            Get
                Return privCor
            End Get
            Set(value As Color)
                privCor = value
            End Set
        End Property


        Public Property Normal() As Vetor3D
            Get
                Return privNormal
            End Get
            Set(value As Vetor3D)
                privNormal = value
            End Set
        End Property
#End Region



#Region "Métodos"


        'Aplica a matriz transformação ao Ponto3D (rotacao etc)
        Public Sub Transforma(ByVal T As Matriz4x4)

            With Me
                .TX = (.X * T.M11) + (.Y * T.M21) + (.Z * T.M31) + T.M41
                .TY = (.X * T.M12) + (.Y * T.M22) + (.Z * T.M32) + T.M42
                .TZ = (.X * T.M13) + (.Y * T.M23) + (.Z * T.M33) + T.M43
                .X = .TX
                .Y = .TY
                .Z = .TZ
            End With

        End Sub

        ' Retorna o ponto na forma de texto (x,y,x)
        Public Overrides Function ToString() As String
            Return "(" & Me.X.ToString() & "," & Me.Y.ToString() & "," & Me.Z.ToString() & ")"
        End Function


        Public Shared Function Origem3D() As Ponto3D
            Return New Ponto3D()
        End Function


        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Ponto3D) Then Return 0
            Dim ponto As Ponto3D = CType(obj, Ponto3D)
            Return Me.CompareTo(ponto)
        End Function


        Public Shared Function Compara(ByVal P1 As Ponto3D, ByVal P2 As Ponto3D) As Boolean
            Return (P1.X = P2.X And P1.Y = P2.Y And P1.Z = P2.Z)
        End Function


        'Point3D_FromVector
        Public Shared Function ConverteVetor2Ponto3D(ByVal V As Vetor3D) As Ponto3D
            Return New Ponto3D(V.X, V.Y, V.Z)
        End Function


        ' Retorna o ponto em coordenadas esféricas
        ' Point3D_FromSpherical
        Public Shared Function Ponto3DCoordenadaEsferica(ByVal r As Precisao, ByVal theta As Precisao, ByVal fi As Precisao) As Ponto3D
            Return New Ponto3D(r * Math.Sin(fi) * Math.Sin(theta), r * Math.Sin(fi) * Math.Cos(theta), r * Math.Cos(fi))
        End Function


        ' retorna o ponto médio entre dois pontos
        ' midPoint3D
        Public Shared Function Ponto3DMedio(ByVal p1 As Ponto3D, ByVal p2 As Ponto3D) As Ponto3D
            Return New Ponto3D((p2.X + p1.X) / 2, (p2.Y + p1.Y) / 2, (p2.Z + p1.Z) / 2)
        End Function


        'translate_Point3D
        Public Shared Function TransladaPonto3D(ByVal pt As Ponto3D, ByVal TransVector As Vetor3D) As Ponto3D
            Return New Ponto3D(pt.X + TransVector.X, pt.Y + TransVector.Y, pt.Z + TransVector.Z)
        End Function




        ' Obtem a interseção entre os pontos e divide pela razão da interpolação: 1
        '1>interpolação>0==> divisão interna   eg=1/2 ==> ponto médio
        'interpolação>1  ==> divisão externa   pt está além de ptB  eg=2 ==> ptb é o ponto médio de ptA e retorna o ponto
        'interpolação<0  ==> divisão externa   pt está além de ptA  eg=-1 ==> ptA é o ponto médio de ptB e retorna o ponto
        'Interpolate_Point3d
        Public Shared Function InterpolaPonto3D(ByVal ptA As Ponto3D, ByVal ptB As Ponto3D, ByVal InterpolationFactor As Precisao) As Ponto3D
            Return New Ponto3D(ptA.X + (ptB.X - ptA.X) * InterpolationFactor, _
                               ptA.Y + (ptB.Y - ptA.Y) * InterpolationFactor, _
                               ptA.Z + (ptB.Z - ptA.Z) * InterpolationFactor)
        End Function


        ' Retorna o valor de R-Theta-Phi para o ponto correspondente
        ' get_ThetaFi_OfPoint3D
        Public Shared Sub RThetaPhiDePonto3D(ByVal Pt As Ponto3D, ByRef r As Precisao, ByRef theta As Precisao, ByRef fi As Precisao)
            r = Math.Sqrt(Pt.X * Pt.X + Pt.Y * Pt.Y + Pt.Z * Pt.Z)
            fi = AcosG(Pt.Z / r)
            If Math.Abs(fi) > ZERO Then  'Define um valor minimo, abaixo disso será considerado zero 
                theta = AcosG(Pt.X / (r * SenG(fi)))
            Else
                theta = 0
            End If
        End Sub


        ' Retorna as coordenadas do ponto para um dado R-Theta-Phi
        ' get_point_of_RThetaFi
        Public Shared Function PontoDeRThetaPhi(ByVal R As Precisao, ByVal theta As Precisao, ByVal fi As Precisao) As Ponto3D
            Return New Ponto3D(R * SenG(fi) * CosG(theta), R * SenG(fi) * SenG(theta), R * CosG(fi))
        End Function



        ' Retorna a distância entre dois pontos
        ' Distance_2Point3D
        Public Shared Function DistanciaDePonto3D(ByVal pt1 As Ponto3D, ByVal pt2 As Ponto3D) As Precisao
            Return Math.Sqrt((pt1.X - pt2.X) ^ 2 + (pt1.Y - pt2.Y) ^ 2 + (pt1.Z - pt2.Z) ^ 2)
        End Function




        'Checa se os pontos são colineares
        'Collinear_Points3D
        Public Shared Function SeColinear(ByVal pt1 As Ponto3D, ByVal pt2 As Ponto3D, ByVal pt3 As Ponto3D) As Boolean
            Return Vetor3D.ConvertePonto2Vetor3D(pt1, pt2).SeColinear(Vetor3D.ConvertePonto2Vetor3D(pt2, pt3))
        End Function


        'CentroidPoint3D
        Public Shared Function CentroidePonto3D(ByVal p() As Ponto3D, ByVal nfinish As Precisao) As Ponto3D
            'x=sigma(x)/n  , y=sigma(y)/n  ,  z=sigma(z)/n

            Dim cent As New Ponto3D()

            Dim i As Precisao

            Dim x As Precisao
            Dim y As Precisao
            Dim z As Precisao

            For i = 0 To nfinish
                x = x + p(CInt(i)).X
                y = y + p(CInt(i)).Y
                z = z + p(CInt(i)).Z
            Next

            With cent
                .X = x / nfinish
                .Y = y / nfinish
                .Z = z / nfinish
            End With

            Return cent

        End Function

        ' Dados 3 pontos, retorna a área do triângulo
        'Area3Angular
        Public Shared Function getAreaTriangulo(ByVal p1 As Ponto3D, ByVal p2 As Ponto3D, ByVal p3 As Ponto3D) As Precisao

            Dim a As Precisao
            Dim b As Precisao
            Dim c As Precisao
            Dim p As Precisao
            Dim area As Precisao

            a = DistanciaDePonto3D(p1, p2)
            b = DistanciaDePonto3D(p2, p3)
            c = DistanciaDePonto3D(p3, p1)
            p = (a + b + c) / 2 'meio perimetro

            area = Raiz(Abs(p * (p - a) * (p - b) * (p - c))) 'don't remove ABS function
            'If Left(CStr(Area3Angular), 4) = "-1.#" Then MsgBox CStr(Area3Angular) + "  =>   " + CStr(Area3Angular)

            Return area

        End Function




        ' Copia Ponto3D.
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Ponto3D(Me.X, Me.Y, Me.Z)
        End Function



#End Region


    End Class


End Namespace

