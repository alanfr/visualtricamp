﻿Option Strict On

Imports BGeom.Ponto

Imports BMath.Matriz

Imports Precisao = System.Double


Namespace Vetor

    Public Class Segmento3D
        Implements IDisposable, ICloneable, IComparable

#Region "Declaracoes"

        Private privP1 As Ponto3D
        Private privP2 As Ponto3D

        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False

#End Region


#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub


        Public Sub New()
            Me.privP1 = New Ponto3D(0, 0, 0)
            Me.privP2 = New Ponto3D(1, 1, 1)
        End Sub


        Public Sub New(ByVal p1 As Ponto3D, ByVal p2 As Ponto3D)
            Me.privP1 = p1
            Me.privP2 = p2
        End Sub


#End Region



#Region "Propriedades"


        Public Property P1() As Ponto3D
            Get
                Return privP1
            End Get
            Set(value As Ponto3D)
                privP1 = value
            End Set
        End Property


        Public Property P2() As Ponto3D
            Get
                Return privP2
            End Get
            Set(value As Ponto3D)
                privP2 = value
            End Set
        End Property


#End Region




#Region "Metodos"

        'Retorna True/False se iguais/diferentes
        Public Function Compara(ByVal S1 As Segmento3D, ByVal S2 As Segmento3D) As Boolean
            Return (S1.P1.X = S2.P1.X And S1.P1.Y = S2.P1.Y And S1.P1.Z = S2.P1.Z) And (S1.P2.X = S2.P2.X And S1.P2.Y = S2.P2.Y And S1.P2.Z = S2.P2.Z) Or _
                   (S1.P1.X = S2.P2.X And S1.P1.Y = S2.P2.Y And S1.P1.Z = S2.P2.Z) And (S1.P2.X = S2.P1.X And S1.P2.Y = S2.P1.Y And S1.P2.Z = S2.P1.Z)
        End Function



        'Retorna o comprimento do segmento
        Public Function Comprimento(ByVal S As Segmento3D) As Precisao
            'Distance_2Point3D
            Return Ponto3D.DistanciaDePonto3D(S.P1, S.P2)
        End Function

        'VectorOf_Seg3D
        Public Function Segmento3D2Vetor(ByVal S As Segmento3D) As Vetor3D
            'VectorOf_2Points3D
            Return Vetor3D.ConvertePonto2Vetor3D(S.P1, S.P2)
        End Function


        ''Gives Line3d of segment
        ''Line3DOf_Seg3D
        Public Function Segmento2Linha3D(ByVal S As Segmento3D) As Linha3D
            Return New Linha3D(S.P1, S.P2)
        End Function



        '    inSegment(): determine if a point is inside a segment
        '    Input:  a point P, and a  segment S
        '    Return: true = P is inside S
        '            false = P is not inside S
        Public Function SePertenceSegmento(ByVal S As Segmento3D, ByVal P As Ponto3D) As Boolean

            'If VectorOf_2Points3D(P, S.pt1).IsCollinearto(VectorOf_2Points3D(P, S.pt2)) = False Then 'check if P is Colenear with S
            'Return 0 : Exit Function
            'Else 'P is collinear withS
            'If (S.pt1.x <> S.pt2.x) Then ' S is not vertical, test x coordinate
            'If (S.pt1.x <= P.x And P.x <= S.pt2.x) Then Return 1 : Exit Function
            'If (S.pt1.x >= P.x And P.x >= S.pt2.x) Then Return 1 : Exit Function
            'Else 'S is vertical, so test y coordinate
            ' If (S.pt1.y <= P.y And P.y <= S.pt2.y) Then Return 1 : Exit Function
            ' If (S.pt1.y >= P.y And P.y >= S.pt2.y) Then Return 1 : Exit Function
            ' End If
            ' End If
            'VectorOf_2Points3D(S.pt1, P).IsParallelto(VectorOf_2Points3D(P, S.pt2)) Then 'Pt1...P....Pt2

            If Vetor3D.ConvertePonto2Vetor3D(S.P1, P).SeParalelo(Vetor3D.ConvertePonto2Vetor3D(P, S.P2)) Then 'Pt1...P....Pt2
                Return True
            Else
                Return False
            End If

        End Function


        '    SegmentOverlap(): determine if a segments overlap or not
        '    Input:  2 segments S
        Private Function SeSobrepoeSegmento(ByVal S1 As Segmento3D, ByVal S2 As Segmento3D) As Boolean
            If (SePertenceSegmento(S1, S2.P1) Or SePertenceSegmento(S1, S2.P2)) Or (SePertenceSegmento(S2, S1.P1) Or SePertenceSegmento(S1, S1.P2)) Then
                Return True
            Else
                Return False
            End If
        End Function

        'intersect3D_SegmentPlane(): intersect a segment and a plane
        '    Input:  S = a segment, and Pn = a plane = {Point V0; Vector n;}
        '    Output: *I0 = the intersect point (when it exists)
        '    Return: 0 = disjoint (no intersection)
        '            1 = intersection in the unique point *I0
        '            2 = the segment lies in the plane
        Public Function SeIntersecaoSegmentoPlano(ByVal S As Segmento3D, ByVal P As Plano3D, ByRef P_return As Ponto3D) As Precisao
            Dim m As Precisao
            m = CDbl(P.PontosNoMesmoLado(S.P1, S.P2) = 0.0)
            If m = 0 Then 'both points lie in plane 
                Return 2
            ElseIf m = 1 Then 'exactly one lies in plane
                If P.SeContemPonto(S.P1) Then
                    P_return = S.P1 'Pt1 lies in plane
                Else
                    P_return = S.P2 'pt2 lies in plane
                End If
                Return 1
            ElseIf m = 2 Then  'both are on same side of plane
                Return 0
            Else  '(m=3)'both pts are on opposite sides of plane, so segment will meet the plane
                Dim L As New Linha3D(S.P1, S.P2)
                P.getPontoIntersecao(L, P_return)
                Return 1
            End If
        End Function



        ''intersect3D_SegmentLine(): intersect a segment and a Line
        ''    Input:  S = a segment, and L = a LINE3D
        ''    Output: *I0 = the intersect point (when it exists)
        ''    Return: 0 = disjoint (no intersection)
        ''            1 = intersection in the unique point *I0
        ''            2 = the segment lies in the Line
        Public Function getIntersecaoSegmentoLinha(ByVal S As Segmento3D, ByVal L As Linha3D, ByRef P_return As Ponto3D) As Precisao
            Dim m1, m2 As Boolean
            m1 = L.SeContemPonto(S.P1) : m2 = L.SeContemPonto(S.P2)

            If (m1 And m2) = True Then
                Return 2
            ElseIf m1 = True Then
                P_return = S.P1
                Return 1
            ElseIf m2 = True Then
                P_return = S.P2
                Return 1
            Else
                L.PontoIntersecao(Segmento2Linha3D(S), P_return)
                If (S.P1.X - P_return.X) * (S.P2.X - P_return.X) < 0 _
                         Or (S.P1.Y - P_return.Y) * (S.P2.Y - P_return.Y) < 0 _
                         Or (S.P1.Z - P_return.Z) * (S.P2.Z - P_return.Z) < 0 Then
                    Return 1 'Line divides segment internally
                Else
                    Return 0 'Line divides segment externally
                End If
            End If
        End Function


        'intersect2D_2Segments(): the intersection of 2 finite 2D segments
        ''   Input:  two finite segments S1 and S2
        '    Output: P_return = intersect point (when it exists)
        '            S_return =  intersect segment  (when it exists)
        '    Return:0=disjoint (no intersect)
        '           1=intersect in unique point 
        '           2=overlap in segment 
        '
        Public Function getIntersecaoSegmentos(ByVal S1 As Segmento3D, ByVal S2 As Segmento3D, ByRef P_return As Ponto3D, ByRef S_return As Segmento3D) As Precisao
            Dim u, v, w As Vetor3D
            u = Segmento3D2Vetor(S1)
            v = Segmento3D2Vetor(S2)
            w = Vetor3D.ConvertePonto2Vetor3D(S1.P1, S2.P1)
            'if segments are not caplanar they can't intersect
            If Vetor3D.ProdutoMisto(u, v, w) <> 0 Then Return 0 : Exit Function 'segments do not lie in same plane

            'check if either segment is of zero length Deal separatately to avoid null vectors
            If Comprimento(S1) = 0 Then
                If Comprimento(S2) = 0 Then
                    If Compara(S1, S2) Then
                        P_return = S1.P1
                        Return 1 : Exit Function
                    Else
                        Return 0 : Exit Function
                    End If
                Else
                    If SePertenceSegmento(S2, S1.P1) Then
                        P_return = S1.P1
                        Return 1 : Exit Function
                    Else
                        Return 0 : Exit Function
                    End If
                End If
            Else
                If Comprimento(S2) = 0 Then
                    If SePertenceSegmento(S1, S2.P1) Then
                        P_return = S1.P1
                        Return 1 : Exit Function
                    Else
                        Return 0 : Exit Function
                    End If
                End If
            End If

            If SeSobrepoeSegmento(S1, S2) Then
                If SePertenceSegmento(S1, S2.P1) Then
                    If SePertenceSegmento(S1, S2.P2) Then
                        S_return = S2
                        Return 2 : Exit Function
                    ElseIf SePertenceSegmento(S2, S1.P1) Then
                        S_return = New Segmento3D(S1.P1, S2.P1)
                        Return 2 : Exit Function
                    ElseIf SePertenceSegmento(S2, S1.P2) Then
                        S_return = New Segmento3D(S1.P2, S2.P1)
                        Return 2 : Exit Function
                    End If
                Else
                    If SePertenceSegmento(S1, S2.P2) Then
                        S_return = S2
                        Return 2 : Exit Function
                    ElseIf SePertenceSegmento(S2, S1.P1) Then
                        S_return = New Segmento3D(S1.P1, S2.P2)
                        Return 2 : Exit Function
                    ElseIf SePertenceSegmento(S2, S1.P2) Then
                        S_return = New Segmento3D(S1.P2, S2.P2)
                        Return 2 : Exit Function
                    End If
                End If
            ElseIf u.SeColinear(v) Then
                Return 0 : Exit Function
            Else
                If getIntersecaoSegmentoLinha(S2, Segmento2Linha3D(S1), P_return) = 1 Then
                    If SePertenceSegmento(S1, P_return) Then Return 1 : Exit Function
                End If
            End If

            ''Now both segments are of finite non zero length
            ''We will check for common point If any
            If Compara(S1, S2) Then
                S_return = S1
                Return 2 : Exit Function
            ElseIf Ponto3D.Compara(S1.P1, S2.P2) Or Ponto3D.Compara(S1.P1, S2.P2) Then 'S1.pt1 is common
                P_return = S1.P1 : Return 0 : Exit Function
            ElseIf Ponto3D.Compara(S1.P2, S2.P2) Or Ponto3D.Compara(S1.P2, S2.P2) Then 'S1.pt2 is common
                P_return = S1.P2 : Return 0 : Exit Function
            ElseIf Ponto3D.Compara(S2.P1, S1.P1) Or Ponto3D.Compara(S2.P1, S1.P2) Then ''S2.pt1 is common
                P_return = S2.P1 : Return 0 : Exit Function
            ElseIf Ponto3D.Compara(S2.P2, S1.P1) Or Ponto3D.Compara(S2.P2, S1.P2) Then ''S2.pt2 is common
                P_return = S2.P2 : Return 0 : Exit Function
            End If

            Return 0

        End Function



        ' Copia Linha3D.
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Segmento3D(Me.P1, Me.P2)
        End Function


        ' Compara dois numeros complexos (permite organização por array).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Segmento3D) Then Return 0
            Dim matriz As Segmento3D = CType(obj, Segmento3D)
            Return Me.CompareTo(matriz)
        End Function


#End Region



    End Class

End Namespace

