﻿Option Strict On

Imports BMath.Matriz
Imports BMath.Funcoes

Imports BGeom.Ponto

Imports Precisao = System.Double



Namespace Vetor


    Public Class Vetor3D
        Implements ICloneable, IComparable, IDisposable


#Region "Declarações comuns"

        'Coordenadas
        Private privX As Precisao
        Private privY As Precisao
        Private privZ As Precisao

        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False


#End Region


#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub



        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub


        ' Cria novo vetor 3D.
        Public Sub New()
            Me.privX = 0.0
            Me.privY = 0.0
            Me.privZ = 0.0
        End Sub


        ' Cria novo vetor 3D.
        Public Sub New(ByVal x As Precisao, ByVal y As Precisao, ByVal z As Precisao)
            Me.privX = x
            Me.privY = y
            Me.privZ = z
        End Sub



#End Region 'New(x,  y , z),  VetorZeros(), VetorPosicao(x,y,z)




#Region "Propriedades"

        Public Property X() As Precisao
            Get
                Return privX
            End Get
            Set(value As Precisao)
                privX = value
            End Set
        End Property

        Public Property Y() As Precisao
            Get
                Return privY
            End Get
            Set(value As Precisao)
                privY = value
            End Set
        End Property

        Public Property Z() As Precisao
            Get
                Return privZ
            End Get
            Set(value As Precisao)
                privZ = value
            End Set
        End Property




#End Region




#Region "Operações"

        Public Shared Operator *(ByVal vetor1 As Vetor3D, ByVal vetor2 As Vetor3D) As Vetor3D
            Return ProdutoVetorial(vetor1, vetor2)
        End Operator


        Public Shared Operator *(ByVal vetor As Vetor3D, ByVal Precisao As Precisao) As Vetor3D
            Return vetor.Multiplica(Precisao)
        End Operator


        Public Shared Operator *(ByVal Precisao As Precisao, ByVal vetor As Vetor3D) As Vetor3D
            Return vetor.Multiplica(Precisao)
        End Operator


        Public Shared Operator +(ByVal esquerda As Vetor3D, ByVal direita As Vetor3D) As Vetor3D
            Return SomaVetores(esquerda, direita)
        End Operator


        Public Shared Operator -(ByVal esquerda As Vetor3D, ByVal direita As Vetor3D) As Vetor3D
            Return SubtraiVetores(esquerda, direita)
        End Operator


        Public Shared Operator -(ByVal vetor1 As Vetor3D) As Vetor3D
            Return vetor1.Multiplica(-1)
        End Operator

        Public Shared Operator /(ByVal vetor1 As Vetor3D, ByVal valor As Double) As Vetor3D
            Return New Vetor3D(vetor1.X / valor, vetor1.Y / valor, vetor1.Z / valor)
        End Operator


        Public Shared Operator =(ByVal esquerda As Vetor3D, ByVal direita As Vetor3D) As Boolean
            Return (esquerda.X = direita.X AndAlso esquerda.Y = direita.Y AndAlso esquerda.Z = direita.Z)
        End Operator


        Public Shared Operator <>(ByVal esquerda As Vetor3D, ByVal direita As Vetor3D) As Boolean
            Return Not (esquerda = direita)
        End Operator



#End Region 'Sobrecarga de operadores *,+,-,=,<>



#Region "Metodos"

        'RadianToDegree
        Private Function Radiano2Grau(ByVal Radiano As Precisao) As Precisao
            Radiano2Grau = CSng(Radiano * 180 / Math.PI)
        End Function

        'DegreeToRadian
        Private Function Grau2Radiano(ByVal Grau As Precisao) As Precisao
            Grau2Radiano = CSng(Grau * Math.PI / 180)
        End Function

        'NullVector()
        Public Shared Function VetorZeros() As Vetor3D
            Return New Vetor3D(0, 0, 0)
        End Function


        'positionVector
        Public Shared Function VetorPosicao(ByVal x As Precisao, ByVal y As Precisao, ByVal z As Precisao) As Vetor3D
            Return New Vetor3D(x, y, z)
        End Function


#Region "Algebra Vetorial"

        Public Overloads Shared Function Compara(ByVal objA As Object, ByVal objB As Object) As Boolean
            If Not (TypeOf objA Is Vetor3D) Or Not (TypeOf objB Is Vetor3D) Then Return False

            'convert object types to vector type
            Dim Vetor3D_A As Vetor3D = CType(objA, Vetor3D)
            Dim Vetor3D_B As Vetor3D = CType(objB, Vetor3D)
            Return (Vetor3D_A.X = Vetor3D_B.X And Vetor3D_A.Y = Vetor3D_B.Y And Vetor3D_A.Z = Vetor3D_B.Z)

        End Function


        'find modulus of vector v
        'Modulus 
        Public Shared Function Modulo(ByVal v As Vetor3D) As Precisao
            Return Math.Sqrt(v.X * v.X + v.Y * v.Y + v.Z * v.Z)
        End Function


        ' Checks if Vetor3D is null vector or not
        ' IsZeroVector
        Public Shared Function SeVetorZero(ByVal v As Vetor3D) As Boolean
            With v
                SeVetorZero = .X = 0 And .Y = 0 And .Z = 0
            End With
        End Function


        ' get unit vector along given vector
        ' unitVector
        Public Shared Function VetorUnitario(ByVal v As Vetor3D) As Vetor3D
            Dim nLen As Precisao
            nLen = Modulo(v)
            If nLen = 0 Then Throw New DivideByZeroException()
            Return New Vetor3D(v.X / nLen, v.Y / nLen, v.Z / nLen)
        End Function


        ' Adiciona um Vetor3D ao Vetor3D original.
        ' AddVectors
        Public Shared Function SomaVetores(ByVal Vetor3D_1 As Vetor3D, ByVal Vetor3D_2 As Vetor3D) As Vetor3D
            Return New Vetor3D(Vetor3D_1.X + Vetor3D_2.X, Vetor3D_1.Y + Vetor3D_2.Y, Vetor3D_1.Z + Vetor3D_2.Z)
        End Function


        ' Subtrai um Vetor3D do Vetor3D original.
        ' SubtractVectors
        Public Shared Function SubtraiVetores(ByVal Vetor3D_1 As Vetor3D, ByVal Vetor3D_2 As Vetor3D) As Vetor3D
            Return New Vetor3D(Vetor3D_1.X - Vetor3D_2.X, Vetor3D_1.Y - Vetor3D_2.Y, Vetor3D_1.Z - Vetor3D_2.Z)
        End Function


        ' Multiply the current Vetor3D by a scalar.
        ' ScaleVector
        Public Shared Function VetorEscala(ByVal vetor1 As Vetor3D, ByVal fatorescala As Precisao) As Vetor3D
            Return New Vetor3D(vetor1.X * fatorescala, vetor1.Y * fatorescala, vetor1.Z * fatorescala)
        End Function


        ' Returns cross product of 2 vectors.
        ' CrossProduct
        Public Shared Function ProdutoVetorial(ByVal Vetor3D_1 As Vetor3D, ByVal Vetor3D_2 As Vetor3D) As Vetor3D
            Return New Vetor3D(Vetor3D_1.Y * Vetor3D_2.Z - Vetor3D_1.Z * Vetor3D_2.Y, _
                              -Vetor3D_1.X * Vetor3D_2.Z + Vetor3D_1.Z * Vetor3D_2.X, _
                               Vetor3D_1.X * Vetor3D_2.Y - Vetor3D_1.Y * Vetor3D_2.X)
        End Function


        ' Returns dot product of 2 vectors.
        ' DotProduct
        Public Shared Function ProdutoEscalar(ByVal Vetor3D_1 As Vetor3D, ByVal Vetor3D_2 As Vetor3D) As Precisao
            Return (Vetor3D_1.X * Vetor3D_2.X + Vetor3D_1.Y * Vetor3D_2.Y + Vetor3D_1.Z * Vetor3D_2.Z)
        End Function

#End Region 'ProdutoEscalar, ProdutoVetorial,  Soma, subtract, mod, scale, unit, isequal Test for value equality between two Vector3Ds.



#Region "Geometria Vetorial"

        ' get distance between pos vectors
        ' VectorDistance
        Public Shared Function DistanciaVetor(ByVal vetor1 As Vetor3D, ByVal vetor2 As Vetor3D) As Precisao
            DistanciaVetor = Modulo(SubtraiVetores(vetor1, vetor2))
        End Function


        ' get angle between free vectors
        ' VectorAngle
        Public Shared Function AnguloVetor(ByVal vetor1 As Vetor3D, ByVal vetor2 As Vetor3D) As Precisao
            If (vetor1 = VetorZeros()) = False And (vetor2 = VetorZeros()) = False Then
                AnguloVetor = CSng(Math.Acos(ProdutoEscalar(VetorUnitario(vetor1), VetorUnitario(vetor2))))

            Else
                Debug.Print("Cls:Vector3D || Function:VectorAngle || 'One of the vector is NULL vector so Angle is not defined'")
                Return Nothing
            End If
        End Function


        ' get distance between points
        ' get_thetafiOfVector
        Public Shared Sub getThetaPhi0Vetor(ByVal V As Vetor3D, ByRef r As Precisao, ByRef theta As Precisao, ByRef phi As Precisao)
            r = CSng(Math.Sqrt(V.X * V.X + V.Y * V.Y + V.Z * V.Z))
            theta = CSng(Math.Acos(V.Z / r))
            phi = CSng(Math.Acos(V.X / (r * Math.Sin(phi))))
        End Sub


        ' get pos vector of vector joining given points(pv) and dividing in ratio
        ' VectorInterpolate
        Function InterpolaVetor(ByVal vetor1 As Vetor3D, ByVal vetor2 As Vetor3D, ByVal FatorInterpolacao As Precisao) As Vetor3D
            Return (vetor1 + (vetor2 - vetor1) * FatorInterpolacao)
        End Function


        ' Retorna a componente paralela do vetor1 ao longo da direcao.
        ' parallel_Component
        Public Shared Function ComponenteParalela(ByVal vetor1 As Vetor3D, ByVal direcao As Vetor3D) As Vetor3D
            Return VetorEscala(VetorUnitario(direcao), ProdutoEscalar(vetor1, VetorUnitario(direcao)))
        End Function

        ' Retorna a componente paralela do vetor instanciado no vetor direcao.
        ' Get_Parallel_Component
        Public Function ComponenteParalela(ByVal direcao As Vetor3D) As Vetor3D
            Return VetorEscala(VetorUnitario(direcao), getProdutoVetorial(VetorUnitario(direcao)))
        End Function


        ' Retorna a projecao do vetor1 na componente perpendicular da direcao.
        ' Perpendicular_Component
        Public Shared Function ComponentePerpendicular(ByVal vetor1 As Vetor3D, ByVal direcao As Vetor3D) As Vetor3D
            Return SubtraiVetores(vetor1, ComponenteParalela(vetor1, direcao))
        End Function

        ' Retorna a componente perpendicular do vetor.
        ' Get_Perpendicular_Component
        Public Function ComponentePerpendicular(ByVal direcao As Vetor3D) As Vetor3D
            Return SubtraiVetor3D(ComponenteParalela(direcao))
        End Function


        'Returns normal to plane formed by pv represented by points A,B,C
        ' NormalVectorofPlane
        Public Shared Function VetorNormalPlano(ByVal vetor1 As Vetor3D, ByVal vetor2 As Vetor3D, ByVal vetor3 As Vetor3D) As Vetor3D
            Return (vetor1 - vetor2) * (vetor3 - vetor2)
        End Function


        ' Returns Box [a b c]
        ' ScalarTripleProduct
        Public Shared Function ProdutoMisto(ByVal Vetor3D_1 As Vetor3D, ByVal Vetor3D_2 As Vetor3D, ByVal vetor3d_3 As Vetor3D) As Precisao
            Return ProdutoEscalar(Vetor3D_1, Vetor3D_2 * vetor3d_3)
        End Function


        ' Returns  ax(bxc)
        ' VectorTripleProduct
        Public Shared Function ProdutoTriploVetorial(ByVal Vetor3D_1 As Vetor3D, ByVal Vetor3D_2 As Vetor3D, ByVal vetor3d_3 As Vetor3D) As Vetor3D
            Return Vetor3D_1 * (Vetor3D_2 * vetor3d_3)
        End Function


        ' gives the vector obtained by inputvector by rotating it by given about coordinate axis specified
        ' VectorRotate
        Public Shared Function RotacionaVetor(ByVal vetor1 As Vetor3D, ByVal Eixo As Precisao, ByVal Angulo As Precisao) As Vetor3D
            'Basic rotations (without matrices)
            Dim rv As New Vetor3D(0, 0, 0)
            Select Case Eixo
                Case 0
                    rv.X = vetor1.X
                    rv.Y = (Math.Cos(Angulo) * vetor1.Y) - (Math.Sin(Angulo) * vetor1.Z)
                    rv.Z = (Math.Sin(Angulo) * vetor1.Y) + (Math.Cos(Angulo) * vetor1.Z)
                Case 1
                    rv.X = (Math.Cos(Angulo) * vetor1.X) + (Math.Sin(Angulo) * vetor1.Z)
                    rv.Y = vetor1.Y
                    rv.Z = -(Math.Sin(Angulo) * vetor1.X) + (Math.Cos(Angulo) * vetor1.Z)
                Case 2
                    rv.X = (Math.Cos(Angulo) * vetor1.X) - (Math.Sin(Angulo) * vetor1.Y)
                    rv.Y = (Math.Sin(Angulo) * vetor1.X) + (Math.Cos(Angulo) * vetor1.Y)
                    rv.Z = vetor1.Z
                Case Else
                    rv = Nothing
                    Debug.Print("cls: Vector3d|| Function: VectorRotate || 'wrong axes spaecified it should be 0,1,2'")
            End Select
            Return rv
        End Function


        ' returns reflection of A off of B
        ' VectorReflect
        Public Shared Function VetorRefletido(ByVal RaioIncidente As Vetor3D, ByVal Normal As Vetor3D) As Vetor3D
            If AnguloVetor(RaioIncidente, Normal) < 0 Then
                VetorRefletido = RaioIncidente + RaioIncidente.ComponenteParalela(Normal) * -2
            Else
                Return Nothing
                Debug.Print(" IncidentRay and normal are along same sense ")
            End If
        End Function

#End Region



#End Region ' Funções compartilhadas

        '*************************'INSTANCE FUNCTIONS (accessible only through object/instance)****************************************************************************************
#Region "Instance Function"

        'VectorOf_Point3D
        Public Shared Function ConvertePonto2Vetor3D(ByVal pt As Ponto3D) As Vetor3D
            Return New Vetor3D(pt.X, pt.Y, pt.Z)
        End Function


        'VectorOf_2Points3D
        Public Shared Function ConvertePonto2Vetor3D(ByVal pt1 As Ponto3D, ByVal pt2 As Ponto3D) As Vetor3D
            Return New Vetor3D(pt2.X - pt1.X, pt2.Y - pt1.Y, pt2.Z - pt1.Z)
        End Function


        'gives length of instance vector
        ' Length
        Public Function getComprimento() As Precisao
            Return Math.Sqrt(Me.X * Me.X + Me.Y * Me.Y + Me.Z * Me.Z)
        End Function


        'gives unit vector of vector
        ' Normalize
        Public Function getNormaliza() As Vetor3D
            Dim nLen As Precisao
            nLen = getComprimento()
            If nLen = 0 Then Throw New DivideByZeroException()
            Return New Vetor3D(Me.X / nLen, Me.Y / nLen, Me.Z / nLen)
        End Function


        ' Add a Vector3D to the current Vector3D.
        ' Add
        Public Function AdicionaVetor3D(ByVal Vetor3D As Vetor3D) As Vetor3D
            Return New Vetor3D(Me.X + Vetor3D.X, Me.Y + Vetor3D.Y, Me.Z + Vetor3D.Z)
        End Function


        ' Subtract a Vector3D from the current Vector3D.
        ' Subtract
        Public Function SubtraiVetor3D(ByVal Vetor3D As Vetor3D) As Vetor3D
            Return New Vetor3D(Me.X - Vetor3D.X, Me.Y - Vetor3D.Y, Me.Z - Vetor3D.Z)
        End Function


        ' Multiply the current Vector3D by a scalar.
        ' Multiply
        Public Function Multiplica(ByVal n As Precisao) As Vetor3D
            Return New Vetor3D(Me.X * n, Me.Y * n, Me.Z * n)
        End Function


        'multiply matrix with vector : transform Vector with given Matrix
        Public Shared Function Multiplica(ByVal Vec As Vetor3D, ByVal Mat As Matriz4x4) As Vetor3D
            Dim rv As Vetor3D
            rv = New Vetor3D(0, 0, 0)
            rv.X = (Mat.M11 * Vec.X) + (Mat.M12 * Vec.Y) + (Mat.M13 * Vec.Z) + (Mat.M14)
            rv.Y = (Mat.M21 * Vec.X) + (Mat.M22 * Vec.Y) + (Mat.M23 * Vec.Z) + (Mat.M24)
            rv.Z = (Mat.M31 * Vec.X) + (Mat.M32 * Vec.Y) + (Mat.M33 * Vec.Z) + (Mat.M34)
            Return rv
        End Function




        ' Divide the current Vector3D by a scalar.
        ' DivideBy
        Public Function DividePor(ByVal n As Precisao) As Vetor3D
            Return New Vetor3D(Me.X / n, Me.Y / n, Me.Z / n)
        End Function


        ' Define some helper methods.
        ' GetCrossProduct
        Public Function getProdutoVetorial(ByVal Vetor3D As Vetor3D) As Vetor3D
            Return New Vetor3D(Me.Y * Vetor3D.Z - Me.Z * Vetor3D.Y, _
                              -Me.X * Vetor3D.Z + Me.Z * Vetor3D.X, _
                               Me.X * Vetor3D.Y - Me.Y * Vetor3D.X)
        End Function


        'Gives dot product of instance vector with input vector
        ' GetDotProduct
        Public Function getProdutoEscalar(ByVal vetor1 As Vetor3D) As Precisao
            Return (Me.X * vetor1.X + Me.Y * vetor1.Y + Me.Z * vetor1.Z)
        End Function










        ' checks if vector is perpendicualr to given vector.
        ' IsPerpendicularto
        Public Function SePerpendicular(ByVal direcao As Vetor3D) As Boolean
            If getProdutoEscalar(direcao) = 0 Then
                Return True
            Else
                Return False
            End If
        End Function


        ' checks if vector is parallel to given vector.  ............>  and .........>
        ' IsParallelto
        Public Function SeParalelo(ByVal vetor1 As Vetor3D) As Boolean
            If (ProdutoEscalar(Me, Vetor)) = Me.getComprimento * vetor1.getComprimento Then
                Return True
            Else
                Return False
            End If
        End Function


        ' Collinear means parallel or antiparallel. ......> and  .......> or ..........> and <...........
        ' IsCollinearto
        Public Function SeColinear(ByVal vetor1 As Vetor3D) As Boolean
            If Abs(ProdutoEscalar(Me, vetor1)) = Me.getComprimento * vetor1.getComprimento Then
                Return True
            Else
                Return False
            End If
        End Function


        ' Test for value equality between the current Vector3D and another.
        Public Overloads Function Equals(ByVal obj As Object) As Boolean
            If Not (TypeOf obj Is Vetor3D) Then Return False
            Dim Compare As Vetor3D = CType(obj, Vetor3D)
            Return (Me.X = Compare.X And Me.Y = Compare.Y And Me.Z = Compare.Z)
        End Function


        ' returns vector in text form
        Public Function ToStrings() As String
            Return Me.X.ToString() & "i +" & _
            Me.Y.ToString() & "j +" & Me.Z.ToString() & "k"
        End Function


        Public Overrides Function tostring() As String
            Dim builder As New System.Text.StringBuilder()
            Dim c As Precisao

            Dim s As String
            s = ""
            c = CSng(Me.X)
            If c <> 0 Then s = CStr(IIf(Math.Abs(c) = 1, "i", c.ToString & "i"))
            c = CSng(Me.Y)
            If c <> 0 Then s = s + CStr(IIf(c > 0, "+", "-")) + CStr(IIf(Math.Abs(c) = 1, "j", Math.Abs(c).ToString & "j"))
            c = CSng(Me.Z)
            If c <> 0 Then s = s + CStr(IIf(c > 0, "+", "-")) + CStr(IIf(Math.Abs(c) = 1, "k", Math.Abs(c).ToString & "k"))
            Return s
        End Function


        ' Copy a Vector3D.
        Public Function Clone() As Object Implements System.ICloneable.Clone
            Return New Vetor3D(Me.X, Me.Y, Me.Z)
        End Function


        ' Compare two Vector3Ds (allows array sorting).
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If Not (TypeOf obj Is Vetor3D) Then Return 0
            Dim Compare As Vetor3D = CType(obj, Vetor3D)
            Return Me.getComprimento().CompareTo(Compare.getComprimento())
        End Function

        '/****************************************************************************************************************



        Private Function VetorEscala(vetor3D As Vetor3D, vetor3D1 As Vetor3D) As Vetor3D
            Throw New NotImplementedException
        End Function

        Private Function Vetor() As Vetor3D
            Throw New NotImplementedException
        End Function


#End Region




    End Class



End Namespace
