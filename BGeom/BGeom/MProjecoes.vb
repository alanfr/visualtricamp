﻿Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes

Namespace Transformacoes


    Public Module MProjecoes

        'Recebe o vetor que indica o plano paralelo
        ' Positivo entrando na tela
        Public Function ProjParalela() As Matriz4x4 ' Z = 0
            Dim rv As Matriz4x4
            rv = Matriz4x4.Identidade()
            rv.M11 = 1
            rv.M22 = 1
            rv.M33 = 0  'Plano Z = 0
            Return rv
        End Function


        Public Function ProjParalelaIso() As Matriz4x4
            Dim rv As Matriz4x4
            rv = Matriz4x4.Identidade

            rv.M13 = -1
            rv.M23 = -1
            rv.M33 = 0
            Return rv
        End Function

        Public Function ProjPerspectiva() As Matriz4x4
            Dim rv As Matriz4x4
            rv = Matriz4x4.Identidade
            rv.M43 = 1
            Return rv
        End Function




        Public Function ProjCavaleira() As Matriz4x4
            Dim rv As Matriz4x4
            rv = Matriz4x4.Identidade()
            rv.M13 = CosG(45) '0.52532198881772973 ' CosG(45)
            rv.M23 = SenG(45) '0.85090352453411844 ' SenG(45)
            rv.M33 = 0
            Return rv
        End Function

        Public Function ProjCabinet() As Matriz4x4
            Dim rv As Matriz4x4
            rv = Matriz4x4.Identidade()
            rv.M13 = 0.26266099440886487 '0.5 * CosG(45)
            rv.M23 = 0.89442719099991586 'SenG(Math.Atan(2))
            rv.M33 = 0.001
            Return rv
        End Function


    End Module


End Namespace
