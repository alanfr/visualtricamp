﻿
Imports BMath.Matriz
Imports BMath.Funcoes


Imports Precisao = System.Double



Namespace Transformacoes


    Public Module Homogenea2D

#Region "Matrizes Transformacao"


        'Cria a matriz rotacao correspondente ao angulo em radianos
        Public Function MatrizRotacao2D(ByVal radiano As Precisao) As Matriz3x3

            Dim MatrizSaida As Matriz3x3
            MatrizSaida = Matriz3x3.Identidade()

            With MatrizSaida
                .M11 = Math.Cos(radiano)
                .M12 = -Math.Sin(radiano)
                .M21 = Math.Sin(radiano)
                .M22 = Math.Cos(radiano)
            End With

            Return MatrizSaida

        End Function


        Public Function MatrizTranslacao2D(ByVal tx As Precisao, ByVal ty As Precisao) As Matriz3x3

            Dim matriz As Matriz3x3 = Matriz3x3.Identidade()

            matriz.M31 = tx
            matriz.M32 = ty
            Return matriz

        End Function


        ' Cria a matriz escala de fator sx e sy com relação a origem
        Public Function MatrizEscala2D(ByVal Sx As Precisao, ByVal Sy As Precisao) As Matriz3x3

            Dim rv As Matriz3x3 = Matriz3x3.Identidade()
            rv.M11 = Sx
            rv.M22 = Sy

            Return rv

        End Function


        ' Cria a matriz escala de fator sx e sy no ponto (x, y) a partir da origem.
        Public Function MatrizEscala2D(ByVal sx As Precisao, ByVal sy As Precisao, ByVal x As Precisao, ByVal y As Precisao) As Matriz3x3

            'Dim rv As Matriz3x3
            Dim T As Matriz3x3
            Dim T_inv As Matriz3x3
            Dim S As Matriz3x3

            T = MatrizTranslacao2D(-x, -y)
            T_inv = MatrizTranslacao2D(x, y)
            S = MatrizEscala2D(sx, sy)
            'rv = Multiply(T, S)
            'rv = Multiply(rv, T_inv)

            Return (T * S) * T_inv

        End Function


        ' Cria a matriz de rotação entorno do ponto (x, y,1) no eixo X.
        Public Function MatrizRotacionaX2D(ByVal x As Precisao, ByVal y As Precisao) As Matriz3x3

            Dim MatrizSaida As Matriz3x3 = Matriz3x3.Identidade()
            Dim d As Precisao

            d = Raiz(x * x + y * y)

            MatrizSaida.M11 = x / d
            MatrizSaida.M12 = -y / d
            MatrizSaida.M21 = -MatrizSaida.M12
            MatrizSaida.M22 = MatrizSaida.M11

            Return MatrizSaida

        End Function


        ' Cria a matriz para reflexão através de uma normal
        ' passando por (x, y) na direção <dx, dy>.
        Public Function MatrizReflexao2D(ByVal x As Precisao, ByVal y As Precisao, ByVal dx As Precisao, ByVal dy As Precisao) As Matriz3x3

            Dim T, R, S, T_inv, R_inv As Matriz3x3

            ' Translada o ponto para a origem.
            T = MatrizTranslacao2D(-x, -y)

            ' Calcula a inversa da matriz de translacao.
            T_inv = MatrizTranslacao2D(x, y)

            ' Rotaciona na direção do eixo Y.
            R = MatrizRotacionaX2D(dx, dy)

            ' Rotacao Inversa.
            R_inv = MatrizRotacionaX2D(dx, -dy)

            ' Reflete atraves do eixo X.
            S = MatrizEscala2D(1, -1)

            ' Combina as transformacoes.
            Return (T * R) * (S * R_inv) * T_inv

        End Function


        ' Cria a matriz de rotacao de um angulo theta entorno do ponto (x, y).
        Public Function MatrizRotacionaXY2D(ByVal theta As Precisao, ByVal x As Precisao, ByVal y As Precisao) As Matriz3x3

            Dim T, r, t_inv As Matriz3x3

            ' Translada o ponto para a origem.
            T = MatrizTranslacao2D(-x, -y)

            ' Translacao Inversa.
            t_inv = MatrizTranslacao2D(x, y)

            ' Rotacao.
            r = MatrizRotacao2D(theta)
            'r = FromRotate2D(theta)

            ' Combina as transformacoes.
            Return (T * r) * t_inv

        End Function


        Public Function MatrizCisalhamento2D(ByVal Sx As Precisao, ByVal Sy As Precisao) As Matriz3x3

            Dim rv As Matriz3x3 = Matriz3x3.Identidade()
            rv.M13 = Sx
            rv.M23 = Sy
            Return rv

        End Function



#End Region


    End Module


End Namespace
