﻿
Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes

Imports BGeom.Vetor


Imports System.Drawing
Imports Precisao = System.Double

Namespace Ponto


    Public Module EPonto3D

        Public Structure Ponto3D

            Public x As Precisao
            Public y As Precisao
            Public z As Precisao
            Public tx As Precisao
            Public ty As Precisao
            Public tz As Precisao
            Public Cor As Color
            Public Normal As Vetor3D

            Public Sub New(ByVal x As Precisao, ByVal y As Precisao, ByVal z As Precisao)
                Me.x = x
                Me.y = y
                Me.z = z
            End Sub


            Public Shared Operator +(ByVal P1 As Ponto3D, ByVal P2 As Ponto3D) As Ponto3D
                Return New Ponto3D(P1.x + P2.x, P1.y + P2.y, P1.z + P2.z)
            End Operator

            Public Shared Operator -(ByVal PF As Ponto3D, ByVal PI As Ponto3D) As Ponto3D
                Return New Ponto3D(PF.x - PI.x, PF.y - PI.y, PF.z - PI.z)
            End Operator




            ' Retorna o ponto na forma de texto (x,y,x)
            Public Overrides Function ToString() As String
                Return "(" & Me.x.ToString() & "," & Me.y.ToString() & "," & Me.z.ToString() & ")"
            End Function



            'Aplica a matrix transformação ao Ponto3D (rotacao etc)
            Public Sub Transforma(ByVal T As Matriz4x4)
                tx = x * T.M11 + y * T.M21 + z * T.M31 + T.M41
                ty = x * T.M12 + y * T.M22 + z * T.M32 + T.M42
                tz = x * T.M13 + y * T.M23 + z * T.M33 + T.M43
                Me.x = tx
                Me.y = ty
                Me.z = tz
            End Sub



            Public Shared Function Origem3D() As Ponto3D
                Return New Ponto3D()
            End Function


            Public Shared Function Compara(ByVal P1 As Ponto3D, ByVal P2 As Ponto3D) As Boolean
                Return (P1.x = P2.x And P1.y = P2.y And P1.z = P2.z)
            End Function


            Public Shared Function ConverteVetor2Ponto3D(ByVal V As Vetor3D) As Ponto3D
                Return New Ponto3D(V.X, V.Y, V.Z)
            End Function


            ' Retorna o ponto em coordenadas esféricas
            Public Shared Function Ponto3DCoordenadaEsferica(ByVal r As Precisao, ByVal theta As Precisao, ByVal fi As Precisao) As Ponto3D
                Return New Ponto3D(r * Math.Sin(fi) * Math.Sin(theta), r * Math.Sin(fi) * Math.Cos(theta), r * Math.Cos(fi))
            End Function


            ' retorna o ponto médio entre dois pontos
            Public Shared Function Ponto3DMedio(ByVal p1 As Ponto3D, ByVal p2 As Ponto3D) As Ponto3D
                Return New Ponto3D((p2.x + p1.x) / 2, (p2.y + p1.y) / 2, (p2.z + p1.z) / 2)
            End Function


            'translada Ponto3D
            Public Shared Function TransladaPonto3D(ByVal pt As Ponto3D, ByVal TransVector As Vetor3D) As Ponto3D
                Return New Ponto3D(pt.x + TransVector.X, pt.y + TransVector.Y, pt.z + TransVector.Z)
            End Function

            'translada Ponto3D
            Public Shared Function TransladaPonto3D(ByVal pt As Ponto3D, ByVal TransPonto As Ponto3D) As Ponto3D
                Return New Ponto3D(pt.x + TransPonto.x, pt.y + TransPonto.y, pt.z + TransPonto.z)
            End Function




            ' Obtem a interseção entre os pontos e divide pela razão da interpolação: 1
            '1>interpolação>0==> divisão interna   eg=1/2 ==> ponto médio
            'interpolação>1  ==> divisão externa   pt está além de ptB  eg=2 ==> ptb é o ponto médio de ptA e retorna o ponto
            'interpolação<0  ==> divisão externa   pt está além de ptA  eg=-1 ==> ptA é o ponto médio de ptB e retorna o ponto
            Public Shared Function InterpolaPonto3D(ByVal ptA As Ponto3D, ByVal ptB As Ponto3D, ByVal InterpolationFactor As Precisao) As Ponto3D
                Return New Ponto3D(ptA.x + (ptB.x - ptA.x) * InterpolationFactor, _
                                   ptA.y + (ptB.y - ptA.y) * InterpolationFactor, _
                                   ptA.z + (ptB.z - ptA.z) * InterpolationFactor)
            End Function


            ' Retorna o valor de R-Theta-Phi para o ponto correspondente
            Public Shared Sub RThetaPhiDePonto3D(ByVal Pt As Ponto3D, ByRef r As Precisao, ByRef theta As Precisao, ByRef fi As Precisao)
                r = Math.Sqrt(Pt.x * Pt.x + Pt.y * Pt.y + Pt.z * Pt.z)
                fi = AcosG(Pt.z / r)
                If Math.Abs(fi) > ZERO Then  'Define um valor minimo, abaixo disso será considerado zero 
                    theta = AcosG(Pt.x / (r * SenG(fi)))
                Else
                    theta = 0
                End If
            End Sub


            ' Retorna as coordenadas do ponto para um dado R-Theta-Phi
            Public Shared Function PontoDeRThetaPhi(ByVal R As Precisao, ByVal theta As Precisao, ByVal fi As Precisao) As Ponto3D
                Return New Ponto3D(R * SenG(fi) * CosG(theta), R * SenG(fi) * SenG(theta), R * CosG(fi))
            End Function

            'Retorna Angulos em graus
            Public Shared Sub AlphaXYZ(ByVal P1 As Ponto3D, ByVal P2 As Ponto3D,
                                       ByRef AlphaXY As Precisao,
                                       ByRef AlphaXZ As Precisao,
                                       ByRef AlphaYZ As Precisao)

                'Se o numero > ~0 ou negativo
                If ((P2.x - P1.x) > ZERO) Or ((P2.x - P1.x) < 0) Then
                    If ((P2.y - P1.y) <= ZERO) Then
                        AlphaXY = 90
                    Else
                        AlphaXY = AtanG((P2.y - P1.y) / (P2.x - P1.x))
                    End If

                    If ((P2.z - P1.z) <= ZERO) Then
                        AlphaXZ = 90
                    Else
                        AlphaXZ = AtanG((P2.z - P1.z) / (P2.x - P1.x))
                    End If
                Else
                    AlphaXY = 0
                    AlphaXZ = 0
                End If

                If ((P2.y - P1.y) > ZERO) Or ((P2.y - P1.y) < 0) Then
                    If ((P2.z - P1.z) <= ZERO) Then
                        AlphaYZ = 90
                    Else
                        AlphaYZ = AtanG((P2.z - P1.z) / (P2.y - P1.y))
                    End If
                Else
                    AlphaYZ = 0
                End If



            End Sub



            ' Retorna a distância entre dois pontos
            Public Shared Function DistanciaDePonto3D(ByVal pt1 As Ponto3D, ByVal pt2 As Ponto3D) As Precisao
                Return Math.Sqrt((pt1.x - pt2.x) ^ 2 + (pt1.y - pt2.y) ^ 2 + (pt1.z - pt2.z) ^ 2)
            End Function




            'Checa se os pontos são colineares
            Public Shared Function SeColinear(ByVal pt1 As Ponto3D, ByVal pt2 As Ponto3D, ByVal pt3 As Ponto3D) As Boolean
                Return Vetor3D.ConvertePonto2Vetor3D(pt1, pt2).SeColinear(Vetor3D.ConvertePonto2Vetor3D(pt2, pt3))
            End Function


            Public Shared Function CentroidePonto3D(ByVal p() As Ponto3D, ByVal nfinish As Precisao) As Ponto3D
                'x=sigma(x)/n  , y=sigma(y)/n  ,  z=sigma(z)/n

                Dim cent As New Ponto3D()

                Dim i As Precisao

                Dim x As Precisao
                Dim y As Precisao
                Dim z As Precisao

                For i = 0 To nfinish
                    x = x + p(CInt(i)).x
                    y = y + p(CInt(i)).y
                    z = z + p(CInt(i)).z
                Next

                With cent
                    .x = CSng(x / nfinish)
                    .y = CSng(y / nfinish)
                    .z = CSng(z / nfinish)
                End With

                Return cent

            End Function

            ' Dados 3 pontos, retorna a área do triângulo
            Public Shared Function getAreaTriangulo(ByVal p1 As Ponto3D, ByVal p2 As Ponto3D, ByVal p3 As Ponto3D) As Precisao

                Dim a As Precisao
                Dim b As Precisao
                Dim c As Precisao
                Dim p As Precisao
                Dim area As Precisao

                a = DistanciaDePonto3D(p1, p2)
                b = DistanciaDePonto3D(p2, p3)
                c = DistanciaDePonto3D(p3, p1)
                p = (a + b + c) / 2 'meio perimetro

                area = Raiz(Abs(p * (p - a) * (p - b) * (p - c))) 'valor absoluto


                Return area

            End Function


        End Structure

    End Module


End Namespace

