﻿Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes

Imports System.Windows.Forms
Imports System.Drawing

Namespace Objetos


    Public Class Toroide
        Inherits Objeto

        Protected _Toroide(,) As Ponto3D
        Protected _RaioToro As Double
        Protected _RaioTubo As Double


#Region "Construtor e Destrutor"

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub

        Public Sub New()
            MyBase.New()
            _Toroide(0, 0) = New Ponto3D
            _RaioToro = 1.0
            _RaioTubo = 1.0
        End Sub


        Public Sub New(ByVal P1 As Ponto3D, _
                       ByVal P2 As Ponto3D, _
                       ByVal raio_toro As Double, _
                       ByVal raio_tubo As Double, _
                       ByVal divisoes_comprimento As Integer, _
                       ByVal divisoes_circulo As Integer)

            If (divisoes_comprimento <= 2) Then
                divisoes_comprimento = 2
                Me._Status = Erro.a100
            ElseIf (divisoes_circulo <= 2) Then
                divisoes_circulo = 2
                Me._Status = Erro.a101
            End If


            If (raio_toro <= ZERO) Then
                Me._Status = Erro.e102
                Exit Sub
            ElseIf (raio_tubo <= ZERO) Then
                Me._Status = Erro.e103
                Exit Sub
            Else
                With Me
                    ._P1 = P1
                    ._P2 = P2
                    ._RaioToro = raio_toro
                    ._RaioTubo = raio_tubo
                    ._NR = divisoes_circulo
                    ._NL = divisoes_comprimento
                    ._Angulo = Angulo
                End With
                Me._Status = Erro.ok
            End If

            Me._Tipo = "Toroide"
        End Sub


#End Region




#Region "Propriedades"

        'Implementar -> Comprimento: toro, tubo
        'Volume toro

        '############### Get-Set ###############

        Public Property RaioToro() As Double
            Get
                Return Me._RaioToro
            End Get
            Set(value As Double)
                Me._RaioToro = value
            End Set
        End Property

        Public Property RaioTubo() As Double
            Get
                Return Me._RaioTubo
            End Get
            Set(value As Double)
                Me._RaioTubo = value
            End Set
        End Property

        Public Overrides Property Cor() As Color
            Get
                Return Me._Cor
            End Get
            Set(value As Color)
                Me._Cor = value
            End Set
        End Property

        Public Overrides Property Angulo() As Double
            Get
                Return _Angulo
            End Get
            Set(value As Double)
                Me._Angulo = value
            End Set
        End Property


        Public Overrides Property NumDivCirc() As Integer
            Get
                Return Me._NR
            End Get
            Set(value As Integer)
                Me._NR = value
            End Set
        End Property


        Public Overrides Property NumDivComp() As Integer
            Get
                Return Me._NL
            End Get
            Set(value As Integer)
                Me._NL = value
            End Set
        End Property

        Public Overrides Property ID() As Integer
            Get
                Return _ID
            End Get
            Set(value As Integer)
                _ID = value
            End Set
        End Property

        Public Overrides Property Wireframe() As Boolean
            Get
                Return Me._Wireframe
            End Get
            Set(value As Boolean)
                Me._Wireframe = value
            End Set
        End Property

        Public Overrides Property AntiAlias As Boolean
            Get
                Return Me._AntiAlias
            End Get
            Set(value As Boolean)
                Me._AntiAlias = value
            End Set
        End Property

        Public Overrides Property BoundBox As Boolean
            Get
                Return Me._BoundBox
            End Get
            Set(value As Boolean)
                Me._BoundBox = value
            End Set
        End Property


        Public Overrides Property MT As Matriz4x4
            Get
                Return Me._MT
            End Get
            Set(value As Matriz4x4)
                Me._MT = MT
            End Set
        End Property

        Public Overrides Property Zoom As Double
            Get
                Return Me._Zoom
            End Get
            Set(value As Double)
                Me._Zoom = value
            End Set
        End Property


        Public Overrides Property MostraXYZ() As Boolean
            Get
                Return Me._MostraXYZ
            End Get
            Set(value As Boolean)
                Me._MostraXYZ = value
            End Set
        End Property


        '####### Read Only #########

        Public ReadOnly Property Toroide() As Ponto3D(,)
            Get
                Return Me._Toroide
            End Get
        End Property

        Public Overrides ReadOnly Property Box() As Ponto3D(,)
            Get
                Return Me._Box
            End Get
        End Property

        Public Overrides ReadOnly Property Circulo() As Ponto3D()
            Get
                Return Me._Circulo
            End Get
        End Property

        Public Overrides ReadOnly Property Faces() As Integer(,)
            Get
                Return Me._Faces
            End Get
        End Property


        Public Overrides ReadOnly Property Vertices() As Ponto3D()
            Get
                Return Me._Vertices
            End Get
        End Property


        Public Overrides ReadOnly Property Status() As String
            Get
                Return Me._Status
            End Get
        End Property


        Public Overrides ReadOnly Property Tipo() As String
            Get
                Return Me._Tipo
            End Get
        End Property

        Public Overrides ReadOnly Property P1() As Ponto3D
            Get
                Return Me._P1
            End Get
        End Property

        Public Overrides ReadOnly Property P2() As Ponto3D
            Get
                Return Me._P2
            End Get
        End Property



#End Region


        Public Overrides Sub GeraCasca3D()
            GC.Collect() 'Limpar recursos não utilizados
            Dim MT As New Matriz4x4
            Dim PassoToroide As Double = 0.0
            'Dim V1, V2, N As Vetor3D

            Dim i, j As Integer

            With Me

                ReDim ._Circulo(NumDivCirc)
                ReDim ._Vertices((NumDivComp + 1) * NumDivCirc)
                ReDim ._Faces(NumDivComp, NumDivCirc)
                ReDim ._Toroide(NumDivComp, NumDivCirc)


                'Gera a primeira circunferência centrada na origem
                'Com o numero de divisoes
                ._Circulo = PtosCircunferencia(NumDivCirc, RaioTubo) ' Circulo com as coordenadas dos pontos

                '' #### Gera Toroide ####
                PassoToroide = 360 / NumDivComp
                'V1 = Vetor3D.ConvertePonto2Vetor3D(_P1)
                'V2 = Vetor3D.ConvertePonto2Vetor3D(_P2)
                'N = (V2 - V1) / _NL
                'MT = MatrizTranslacao3D(N.X, N.Y, N.Z) ' * MatrizRotacionaX(30)

                'Transporta o circulo inicial para a posição
                MT = MatrizTranslacao3D(0, RaioToro, 0)
                For j = 0 To NumDivCirc
                    ._Circulo(j).Transforma(MT)
                Next

                'Gera o toroide distribuindo os circulos ao longo do comprimento do toroide
                MT = MatrizRotacionaZ(PassoToroide)
                For i = 0 To NumDivComp
                    For j = 0 To NumDivCirc
                        ._Circulo(j).Transforma(MT)
                        ._Toroide(i, j) = ._Circulo(j)
                    Next
                Next

                '########################

                'Mapeia os vértices
                ._Vertices = PtosVertices(._Toroide)

                'Mapeia faces: face(nface,ncirculo)
                ._Faces = MapeiaFaces(NumDivComp, NumDivCirc)

            End With

        End Sub




        'Public Overrides Sub Desenha(ByVal Local As PictureBox, ByVal camera As Camera.Camera3D, ByVal MT As Matriz4x4, Optional ByVal Zoom As Integer = 200)
        Public Overrides Sub Desenha(ByVal Local As PictureBox, ByVal MT As Matriz4x4)

            If Me._Wireframe = True Then
                GC.Collect() 'Limpar recursos não utilizados
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me.Cor)

                If Me._AntiAlias = True Then
                    _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
                End If

                Dim angulo As Double = Me.Angulo
                Dim NL As Integer = Me.NumDivComp
                Dim NR As Integer = Me.NumDivCirc
                Dim vertices() As Ponto3D = Me.Vertices()
                Dim faces(,) As Integer = Me.Faces()
                Dim NLxNR As Integer = NR * NL

                Dim t(NLxNR) As Ponto3D 'NL*NR
                Dim f(NL + 1) As Integer ' NL + 1
                Dim indf(NR - 1) As Integer
                Dim MTF As Matriz4x4

                'Matriz de transformacao final
                MTF = MatrizEulerPos(P1, P2, 1) * MT

                If (Me.MostraXYZ = True) Then
                    _Eixo = New Eixos(_RaioToro * 0.3)
                    _Eixo.Desenha(Local, MTF)
                End If

                'Gera indices para relacao de fechamento da malha
                Parallel.For(0, NR - 1, Sub(i1)
                                            If i1 <> NR - 1 Then
                                                indf(i1) = i1 + 1
                                            Else
                                                indf(NR - 1) = 0
                                            End If
                                        End Sub)


                ' Transforma os pontos e salva no array t.
                Dim accept As Boolean = True
                For i = 1 To NLxNR
                    t(i) = ProjetaTela(AplicaTransformacao3D(Me.Vertices(i), MTF), Local)
                    't(i) = AplicaTransformacao3D(Me.Vertices(i), MT)
                Next



                ' ''Desenha Toroide
                'For i = 0 To NL - 1
                '    For j = 0 To NR - 1
                '        If (i <= NL - 2) Then
                '            _GL.DrawLine(_Pen, CInt(t(faces(i, j)).x), CInt(t(faces(i, j)).y), CInt(t(faces(i + 1, j)).x), CInt(t(faces(i + 1, j)).y)) 'Desenha linhas
                '        End If
                '        _GL.DrawLine(_Pen, CInt(t(faces(i, j)).x), CInt(t(faces(i, j)).y), CInt(t(faces(i, indf(j))).x), CInt(t(faces(i, indf(j))).y)) ' Desenha bordas(faces)
                '        _GL.DrawLine(_Pen, CInt(t(faces(NL - 1, j)).x), CInt(t(faces(NL - 1, j)).y), CInt(t(faces(0, j)).x), CInt(t(faces(0, j)).y)) ' fecha o toroide
                '    Next
                'Next

                Dim fechaToroPt(1) As Ponto3D

                'Checar motivo de pontos estourarem no zoom
                For i = 0 To NL - 1
                    For j = 0 To NR - 1
                        If (i <= NL - 2) Then
                            'Clipping
                            _linhaPt = Clip3D(t(faces(i, j)).x, t(faces(i, j)).y, t(faces(i, j)).z,
                                              t(faces(i + 1, j)).x, t(faces(i + 1, j)).y, t(faces(i + 1, j)).z, _Zoom)
                            'Desenha Linhas
                            _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linhas
                        End If '(i <= NL - 2)

                        'Clipping
                        _facePt = Clip3D(t(faces(i, j)).x, t(faces(i, j)).y, t(faces(i, j)).z,
                                         t(faces(i, indf(j))).x, t(faces(i, indf(j))).y, t(faces(i, indf(j))).z, _Zoom)

                        fechaToroPt = Clip3D(t(faces(NL - 1, j)).x, t(faces(NL - 1, j)).y, t(faces(NL - 1, j)).z,
                                             t(faces(0, j)).x, t(faces(0, j)).y, t(faces(0, j)).z, _Zoom)

                        ' Desenha faces
                        _GL.DrawLine(_Pen, CInt(_facePt(0).x), CInt(_facePt(0).y), CInt(_facePt(1).x), CInt(_facePt(1).y)) ' Desenha faces
                        _GL.DrawLine(_Pen, CInt(fechaToroPt(0).x), CInt(fechaToroPt(0).y), CInt(fechaToroPt(1).x), CInt(fechaToroPt(1).y)) ' fecha o toroide
                    Next
                Next




            End If 'If Wireframe True



        End Sub





    End Class

End Namespace

