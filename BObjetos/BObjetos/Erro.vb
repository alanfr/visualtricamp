﻿Namespace Objetos

    Public Structure Erro

        Public Shared ReadOnly ok As String = "OK!"
        Public Shared ReadOnly e100 As String = "Erro 100: comprimento menor ou igual a zero"
        Public Shared ReadOnly e101 As String = "Erro 101: raio menor ou igual a zero"
        Public Shared ReadOnly e102 As String = "Erro 102: raio do toroide menor ou igual a zero"
        Public Shared ReadOnly e103 As String = "Erro 103: raio do tubo menor ou igual a zero"

        Public Shared ReadOnly a100 As String = "Alerta 100: NL mínimo deve ser 2. Fazendo NL = 2"
        Public Shared ReadOnly a101 As String = "Alerta 101: NR mínimo deve ser 2. Fazendo NR = 2"
    End Structure


End Namespace


