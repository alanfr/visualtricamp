﻿Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes


Imports System.Windows.Forms
Imports System.Drawing


Namespace Objetos


    Public Class Plano

        Protected _LPatch As Double  'Largura do Patch
        Protected _CPatch As Double  'Comprimento do Patch
        Protected _LPlano As Double
        Protected _CPlano As Double
        Protected _XYZ_Inicial As Ponto3D
        Protected _Tipo As String
        Protected _Cor As Color
        Protected _Mostra As Boolean
        Protected _ID As Integer
        Protected _Zoom As Double

        Protected _GL As Graphics
        Protected _Pen As Pen

        'Clipping
        'Protected _facePt(1) As Ponto3D
        Protected _linhaPt(1) As Ponto3D



#Region "Construtor e Destrutor"

        Public Sub New()
            Me.LPatch = 1.0
            Me.CPatch = 1.0
            Me.LPlano = 10.0
            Me.CPlano = 10.0
            Me._XYZ_Inicial = New Ponto3D(0, 0, 0)
            Me.Cor = Color.LightGray
            Me._Mostra = True

            Me._Tipo = "Plano"

        End Sub


        Public Sub New(ByVal XYZ As Ponto3D, ByVal CPatch As Double, ByVal CPlano As Double, ByVal LPatch As Double, ByVal LPlano As Double)
            Me.LPatch = LPatch
            Me.CPatch = CPatch
            Me.LPlano = LPlano
            Me.CPlano = CPlano
            Me.XYZ = XYZ
            Me.Cor = Color.LightGray
            Me.Mostra = True

            Me._Tipo = "Plano"

        End Sub


#End Region


#Region "Propriedades"


        '############### Get-Set ###############

        Public Property Cor() As Color
            Get
                Return Me._Cor
            End Get
            Set(value As Color)
                Me._Cor = value
            End Set
        End Property

        Public Property Mostra() As Boolean
            Get
                Return Me._Mostra
            End Get
            Set(value As Boolean)
                Me._Mostra = value
            End Set
        End Property


        Public Property ID() As Integer
            Get
                Return _ID
            End Get
            Set(value As Integer)
                _ID = value
            End Set
        End Property


        Public Property Zoom() As Double
            Get
                Return Me._Zoom
            End Get
            Set(value As Double)
                Me._Zoom = value
            End Set
        End Property


        Public Property LPatch() As Double
            Get
                Return Me._LPatch
            End Get
            Set(value As Double)
                Me._LPatch = value
            End Set
        End Property

        Public Property CPatch() As Double
            Get
                Return Me._CPatch
            End Get
            Set(value As Double)
                Me._CPatch = value
            End Set
        End Property


        Public Property LPlano() As Double
            Get
                Return _LPlano
            End Get
            Set(value As Double)
                _LPlano = value
            End Set
        End Property

        Public Property CPlano() As Double
            Get
                Return _CPlano
            End Get
            Set(value As Double)
                _CPlano = value
            End Set
        End Property



        Public Property XYZ() As Ponto3D
            Get
                Return _XYZ_Inicial
            End Get
            Set(value As Ponto3D)
                _XYZ_Inicial = value
            End Set
        End Property


        '####### Read Only #########

        Public ReadOnly Property Tipo() As String
            Get
                Return Me._Tipo
            End Get
        End Property


#End Region


        Public Sub Desenha(ByVal Local As PictureBox, ByVal MT As Matriz4x4)

            GC.Collect() 'Limpar recursos não utilizados
            'Desenha o patch
            If Me._Mostra = True Then
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me._Cor)

                _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

                Dim vertices(4) As Ponto3D
                Dim face(2) As Integer
                Dim t(4) As Ponto3D
                Dim v As Ponto3D

                ' Cria o patch quadrado de lado LPatch.
                vertices = New Ponto3D() {
                           New Ponto3D(0, 0, 0),
                           New Ponto3D(CPatch, 0, 0),
                           New Ponto3D(CPatch, LPatch, 0),
                           New Ponto3D(0, LPatch, 0),
                           New Ponto3D(CPatch / 2, LPatch / 2, 0)}

                '  3 ________ 2
                '   |        |
                '   |        |
                '   |        |
                '  0|________|1

                'Face conecta os pontos 0->1, 1->2, 2->3, 3->0, 0->5...
                face = New Integer() {0, 1, 2, 3, 0} '13 pontos

                For i1 = XYZ.x To CPlano Step CPatch
                    For i2 = XYZ.y To LPlano Step LPatch

                        For i = 0 To 3
                            v = vertices(i)
                            t(i) = ProjetaTela(AplicaTransformacao3D(v, MatrizTranslacao3D(i1, i2, XYZ.z) * MT), Local)
                        Next


                        For i = 0 To 3  '12 pontos + 1
                            'Clipping
                            _linhaPt = Clip3D(t(face(i)).x, t(face(i)).y, t(face(i)).z,
                                              t(face(i + 1)).x, t(face(i + 1)).y, t(face(i + 1)).z, _Zoom)
                            'Desenha Linhas
                            _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        Next
                    Next
                Next

            End If ' MostraX

        End Sub


    End Class

End Namespace

