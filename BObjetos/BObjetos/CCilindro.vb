﻿Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes


Imports System.Windows.Forms
Imports System.Drawing


Namespace Objetos


    Public Class Cilindro
        Inherits Objeto

        Protected _Raio As Double = 0.0
        Protected _Cilindro(,) As Ponto3D
        Protected _L As Double = 0.0




#Region "Construtor e Destrutor"

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub

        Public Sub New()
            MyBase.New()
            _Cilindro(0, 0) = New Ponto3D
        End Sub


        Public Sub New(ByVal P1 As Ponto3D, _
                       ByVal P2 As Ponto3D, _
                       ByVal R As Double,
                       ByVal NL As Integer, _
                       ByVal NR As Integer)

            Dim absval As Double

            If (NL < 2) Then
                NL = 2
                Me._Status = Erro.a100
            ElseIf (NR < 2) Then
                NR = 2
                Me._Status = Erro.a101
            End If

            absval = Math.Abs(Ponto3D.DistanciaDePonto3D(P1, P2))
            If (absval <= ZERO) Then
                Me._Status = Erro.e100
                Exit Sub
            ElseIf (R <= ZERO) Then
                Me._Status = Erro.e101
                Exit Sub
            Else
                With Me
                    ._P1 = P1
                    ._P2 = P2
                    ._Raio = R
                    ._NR = NR
                    ._NL = NL
                    ._L = Ponto3D.DistanciaDePonto3D(P2, P1)
                End With
                Me._Status = Erro.ok
            End If

            Me._Tipo = "Cilindro"

        End Sub


#End Region


#Region "Propriedades"


        '############### Get-Set ###############

        Public Property Comprimento() As Double
            Get
                Return Me._L
            End Get
            Set(value As Double)
                Me._L = value
            End Set
        End Property


        Public Property Raio() As Double
            Get
                Return Me._Raio
            End Get
            Set(value As Double)
                Me._Raio = value
            End Set
        End Property


        Public Overrides Property Cor() As Color
            Get
                Return Me._Cor
            End Get
            Set(value As Color)
                Me._Cor = value
            End Set
        End Property



        Public Overrides Property Angulo() As Double
            Get
                Return _Angulo
            End Get
            Set(value As Double)
                Me._Angulo = value
            End Set
        End Property


        Public Overrides Property NumDivCirc() As Integer
            Get
                Return Me._NR
            End Get
            Set(value As Integer)
                Me._NR = value
            End Set
        End Property


        Public Overrides Property NumDivComp() As Integer
            Get
                Return Me._NL
            End Get
            Set(value As Integer)
                Me._NL = value
            End Set
        End Property

        Public Overrides Property ID() As Integer
            Get
                Return _ID
            End Get
            Set(value As Integer)
                _ID = value
            End Set
        End Property


        Public Overrides Property Wireframe() As Boolean
            Get
                Return Me._Wireframe
            End Get
            Set(value As Boolean)
                Me._Wireframe = value
            End Set
        End Property

        Public Overrides Property AntiAlias As Boolean
            Get
                Return Me._AntiAlias
            End Get
            Set(value As Boolean)
                Me._AntiAlias = value
            End Set
        End Property


        Public Overrides Property BoundBox As Boolean
            Get
                Return Me._BoundBox
            End Get
            Set(value As Boolean)
                Me._BoundBox = value
            End Set
        End Property


        Public Overrides Property MT As Matriz4x4
            Get
                Return Me._MT
            End Get
            Set(value As Matriz4x4)
                Me._MT = MT
            End Set
        End Property

        Public Overrides Property Zoom As Double
            Get
                Return Me._Zoom
            End Get
            Set(value As Double)
                Me._Zoom = value
            End Set
        End Property

        Public Overrides Property MostraXYZ() As Boolean
            Get
                Return Me._MostraXYZ
            End Get
            Set(value As Boolean)
                Me._MostraXYZ = value
            End Set
        End Property


        '####### Read Only #########

        Public ReadOnly Property Cilindro As Ponto3D(,)
            Get
                Return Me._Cilindro
            End Get
        End Property

        Public Overrides ReadOnly Property Box As Ponto3D(,)
            Get
                Return Me._Box
            End Get
        End Property

        Public Overrides ReadOnly Property Circulo As Ponto3D()
            Get
                Return Me._Circulo
            End Get
        End Property

        Public Overrides ReadOnly Property Faces As Integer(,)
            Get
                Return Me._Faces
            End Get
        End Property


        Public Overrides ReadOnly Property Vertices As Ponto3D()
            Get
                Return Me._Vertices
            End Get
        End Property


        Public Overrides ReadOnly Property Status As String
            Get
                Return Me._Status
            End Get
        End Property


        Public Overrides ReadOnly Property Tipo As String
            Get
                Return Me._Tipo
            End Get
        End Property


        Public Overrides ReadOnly Property P1() As Ponto3D
            Get
                Return Me._P1
            End Get
        End Property

        Public Overrides ReadOnly Property P2() As Ponto3D
            Get
                Return Me._P2
            End Get
        End Property



#End Region




        Public Overrides Sub GeraCasca3D()
            GC.Collect() 'Limpar recursos não utilizados
            Dim Passo As Double = 0.0
            Dim MT As New Matriz4x4


            With Me

                ReDim ._Circulo(NumDivCirc)
                ReDim ._Vertices((NumDivComp + 2) * NumDivCirc)
                ReDim ._Faces((NumDivComp + 1), NumDivCirc)
                ReDim ._Cilindro((NumDivComp + 1), NumDivCirc)

                Passo = Comprimento / (NumDivComp)

                'Gera a primeira circunferência centrada na origem
                'Com o numero de divisoes

                ._Circulo = PtosCircunferencia(NumDivCirc, Raio) ' Circulo com as coordenadas dos pontos

                'Gera o Cilindro

                ''Transporta o circulo inicial para a posição
                'MT = MatrizTranslacao3D(0, 0, 0)
                'For j = 0 To NumDivCirc
                '    ._Circulo(j).Transforma(MT)
                'Next

                'Gera o cilindro distribuindo os circulos ao longo do comprimento
                MT = MatrizTranslacao3D(Passo, 0, 0)
                For i = 0 To (NumDivComp + 1) 'NumDivComp + 1
                    For j = 0 To NumDivCirc
                        If i = 0 Then
                            ._Cilindro(i, j) = ._Circulo(j) 'Joga a primeira circunferencia para referencia 0
                        Else
                            ._Circulo(j).Transforma(MT) 'Aplica a transformacao
                            ._Cilindro(i, j) = ._Circulo(j) ' O circulo atual sera a fatia inicial apos a transformacao
                        End If
                    Next
                Next

                'Mapeia os vértices
                ._Vertices = PtosVertices(._Cilindro)


                'Mapeia faces: face(nface,ncirculo)
                ._Faces = MapeiaFaces(NumDivComp + 1, NumDivCirc) 'NumDivComp + 1




            End With

        End Sub




        Public Overrides Sub Desenha(ByVal Local As PictureBox, ByVal MT As Matriz4x4)



            If Me._Wireframe = True Then
                GC.Collect() 'Limpar recursos não utilizados
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me.Cor)

                If Me._AntiAlias = True Then
                    _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
                End If

                Dim angulo As Double = Me.Angulo
                Dim NL As Integer = (Me.NumDivComp + 1) 'NumDivComp + 1
                Dim NR As Integer = Me.NumDivCirc
                Dim vertices() As Ponto3D = Me.Vertices()
                Dim faces(,) As Integer = Me.Faces()
                Dim NLxNR As Integer = NR * NL

                Dim t(NLxNR) As Ponto3D 'NL*NR
                Dim p(1) As Ponto3D
                Dim f(NL + 1) As Integer ' NL + 1
                Dim indf(NR - 1) As Integer
                Dim v As Ponto3D
                Dim MTF As Matriz4x4



                'Matriz de transformacao final
                MTF = MatrizEulerPos(P1, P2, 0) * MT

                If (Me.MostraXYZ = True) Then
                    _Eixo = New Eixos(Me._Raio * 0.5) 'dobro do raio
                    _Eixo.Desenha(Local, MTF)
                End If

                'Gera indices para relacao de fechamento da malha
                Parallel.For(0, NR - 1, Sub(i1)
                                            If i1 <> NR - 1 Then
                                                indf(i1) = i1 + 1
                                            Else
                                                indf(NR - 1) = 0
                                            End If
                                        End Sub)



                ' Transforma os pontos e salva no array t.
                Dim accept As Boolean = True

                For i = 1 To NLxNR
                    v = _Vertices(i)
                    t(i) = ProjetaTela(AplicaTransformacao3D(v, MTF), Local)
                Next


                For i = 0 To NL - 1
                    For j = 0 To NR - 1
                        If (i <= NL - 2) Then
                            'Clipping
                            _linhaPt = Clip3D(t(faces(i, j)).x, t(faces(i, j)).y, t(faces(i, j)).z,
                                              t(faces(i + 1, j)).x, t(faces(i + 1, j)).y, t(faces(i + 1, j)).z, _Zoom)
                            'Desenha Linhas
                            _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linhas
                        End If '(i <= NL - 2)

                        'Clipping
                        _facePt = Clip3D(t(faces(i, j)).x, t(faces(i, j)).y, t(faces(i, j)).z,
                                         t(faces(i, indf(j))).x, t(faces(i, indf(j))).y, t(faces(i, indf(j))).z, _Zoom)

                        ' Desenha faces
                        _GL.DrawLine(_Pen, CInt(_facePt(0).x), CInt(_facePt(0).y), CInt(_facePt(1).x), CInt(_facePt(1).y)) ' Desenha faces
                    Next
                Next


            End If ' Wireframe


        End Sub



    End Class

End Namespace

