﻿Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes

Imports System.Drawing
Imports System.Windows.Forms

Namespace Objetos


    Public Class Esfera
        Inherits Objeto

        Protected _Raio As Double
        Protected _Esfera(,) As Ponto3D
        Protected VEsfera(,) As Ponto3D



#Region "Construtor e Destrutor"

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub


        Public Sub New()
            MyBase.New()
            _Esfera(0, 0) = New Ponto3D(0.0, 0.0, 0.0)
            _Raio = 1
        End Sub


        Public Sub New(ByVal P1 As Ponto3D, _
                       ByVal P2 As Ponto3D, _
                       ByVal raio As Double, _
                       ByVal divisoes_comprimento As Integer, _
                       ByVal divisoes_circulo As Integer)

            If (divisoes_comprimento < 2) Then
                divisoes_comprimento = 2
                Me._Status = Erro.a100
            ElseIf (divisoes_circulo < 2) Then
                divisoes_circulo = 2
                Me._Status = Erro.a101
            End If

            If (raio <= ZERO) Then
                Me._Status = Erro.e101
                Exit Sub
            Else
                With Me
                    ._P1 = P1
                    ._P2 = P2
                    ._Raio = raio
                    ._NR = divisoes_circulo
                    ._NL = divisoes_comprimento
                    ._Angulo = Angulo
                    ._Status = Erro.ok
                End With
            End If

            Me._Tipo = "Esfera"
        End Sub


#End Region


#Region "Propriedades"

        'Implementar -> Comprimento: toro, tubo
        'Volume toro

        '####### Get-Set #######
        Public Property Raio() As Double
            Get
                Return Me._Raio
            End Get
            Set(value As Double)
                Me._Raio = value
            End Set
        End Property


        Public Overrides Property Angulo() As Double
            Get
                Return _Angulo
            End Get
            Set(value As Double)
                Me._Angulo = value
            End Set
        End Property


        Public Overrides Property Cor() As Color
            Get
                Return _Cor
            End Get
            Set(value As Color)
                Me._Cor = value
            End Set
        End Property

        Public Overrides Property NumDivCirc() As Integer
            Get
                Return Me._NR
            End Get
            Set(value As Integer)
                Me._NR = value
            End Set
        End Property

        Public Overrides Property NumDivComp() As Integer
            Get
                Return Me._NL
            End Get
            Set(value As Integer)
                Me._NL = value
            End Set
        End Property


        Public Overrides Property ID() As Integer
            Get
                Return Me._ID
            End Get
            Set(value As Integer)
                Me._ID = value
            End Set
        End Property

        Public Overrides Property Wireframe() As Boolean
            Get
                Return Me._Wireframe
            End Get
            Set(value As Boolean)
                Me._Wireframe = value
            End Set
        End Property

        Public Overrides Property AntiAlias As Boolean
            Get
                Return Me._AntiAlias
            End Get
            Set(value As Boolean)
                Me._AntiAlias = value
            End Set
        End Property



        Public Overrides Property BoundBox As Boolean
            Get
                Return Me._BoundBox
            End Get
            Set(value As Boolean)
                Me._BoundBox = value
            End Set
        End Property


        Public Overrides Property MT As Matriz4x4
            Get
                Return Me._MT
            End Get
            Set(value As Matriz4x4)
                Me._MT = MT
            End Set
        End Property

        Public Overrides Property Zoom As Double
            Get
                Return Me._Zoom
            End Get
            Set(value As Double)
                Me._Zoom = value
            End Set
        End Property

        Public Overrides Property MostraXYZ() As Boolean
            Get
                Return Me._MostraXYZ
            End Get
            Set(value As Boolean)
                Me._MostraXYZ = value
            End Set
        End Property


        '############# Read Only #############


        Public ReadOnly Property Esfera() As Ponto3D(,)
            Get
                Return Me._Esfera
            End Get
        End Property


        Public Overrides ReadOnly Property Box() As Ponto3D(,)
            Get
                Return Me._Box
            End Get
        End Property

        Public Overrides ReadOnly Property Circulo() As Ponto3D()
            Get
                Return Me._Circulo
            End Get
        End Property

        Public Overrides ReadOnly Property Faces() As Integer(,)
            Get
                Return Me._Faces
            End Get
        End Property

        Public Overrides ReadOnly Property Vertices() As Ponto3D()
            Get
                Return Me._Vertices
            End Get
        End Property

        Public Overrides ReadOnly Property Status() As String
            Get
                Return Me._Status
            End Get
        End Property

        Public Overrides ReadOnly Property Tipo() As String
            Get
                Return Me._Tipo
            End Get
        End Property


        Public Overrides ReadOnly Property P1() As Ponto3D
            Get
                Return Me._P1
            End Get
        End Property

        Public Overrides ReadOnly Property P2() As Ponto3D
            Get
                Return Me._P2
            End Get
        End Property


#End Region


        Public Overrides Sub GeraCasca3D()
            GC.Collect() 'Limpar recursos não utilizados
            Dim MT As New Matriz4x4
            Dim Passo As Double = 0.0
            'Dim P As Ponto3D

            Dim i, j As Integer

            With Me

                ReDim ._Circulo(.NumDivCirc)
                ReDim ._Vertices((.NumDivComp + 1) * .NumDivCirc)
                ReDim ._Faces(.NumDivComp, .NumDivCirc)
                ReDim ._Esfera(.NumDivComp, .NumDivCirc)


                'Gera a primeira circunferência centrada na origem
                'Com o numero de divisoes
                Me._Circulo = PtosCircunferencia(NumDivCirc, Raio) ' Circulo com as coordenadas dos pontos

                '' #### Gera Esfera ####
                Passo = 360 / NumDivComp

                'Transporta o centro do circulo para a posição
                'P = Ponto3D.Ponto3DMedio(_P1, _P2)
                MT = MatrizTranslacao3D(0, 0, 0)
                For j = 0 To NumDivCirc
                    ._Circulo(j).Transforma(MT)
                Next

                MT = MatrizRotacionaZ(Passo)
                For i = 0 To NumDivComp
                    For j = 0 To NumDivCirc
                        Me._Circulo(j).Transforma(MT)
                        Me._Esfera(i, j) = Me._Circulo(j)
                    Next
                Next

                '########################

                'Mapeia os vértices
                Me._Vertices = PtosVertices(Esfera)

                'Mapeia faces: face(nface,ncirculo)
                Me._Faces = MapeiaFaces(NumDivComp, NumDivCirc)


            End With

        End Sub




        Public Overrides Sub Desenha(ByVal Local As PictureBox, ByVal MT As Matriz4x4)


            If Me._Wireframe = True Then
                GC.Collect() 'Limpar recursos não utilizados
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me.Cor)

                If Me._AntiAlias = True Then
                    _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
                End If


                Dim angulo As Double = Me.Angulo
                Dim NL As Integer = Me.NumDivComp
                Dim NR As Integer = Me.NumDivCirc
                Dim vertices() As Ponto3D = Me.Vertices()
                Dim faces(,) As Integer = Me.Faces()
                Dim NLxNR As Integer = NR * NL

                Dim t(NLxNR) As Ponto3D 'NL*NR
                Dim f(NL + 1) As Integer ' NL + 1
                Dim indf(NR - 1) As Integer
                Dim MTF As Matriz4x4


                'Matriz de transformacao final
                MTF = MatrizEulerPos(P1, P2, 2) * MT

                If (Me.MostraXYZ = True) Then
                    _Eixo = New Eixos(Me._Raio * 0.3)
                    _Eixo.Desenha(Local, MTF)
                End If

                'Gera indices para relacao de fechamento da malha
                Parallel.For(0, NR - 1, Sub(i1)
                                            If i1 <> NR - 1 Then
                                                indf(i1) = i1 + 1
                                            Else
                                                indf(NR - 1) = 0
                                            End If
                                        End Sub)



                ' Transforma os pontos e salva no array t.
                Dim accept As Boolean = True

                For i = 1 To NLxNR
                    t(i) = ProjetaTela(AplicaTransformacao3D(Me.Vertices(i), MTF), Local)
                    't(i) = AplicaTransformacao3D(Me.Vertices(i), MT)
                Next

                For i = 0 To NL - 1
                    For j = 0 To NR - 1
                        If (i <= NL - 2) Then
                            'Clipping
                            _linhaPt = Clip3D(t(faces(i, j)).x, t(faces(i, j)).y, t(faces(i, j)).z,
                                              t(faces(i + 1, j)).x, t(faces(i + 1, j)).y, t(faces(i + 1, j)).z, _Zoom)
                            'Desenha Linhas
                            _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linhas horizontais
                        End If '(i <= NL - 2)

                        'Clipping
                        _facePt = Clip3D(t(faces(i, j)).x, t(faces(i, j)).y, t(faces(i, j)).z,
                                         t(faces(i, indf(j))).x, t(faces(i, indf(j))).y, t(faces(i, indf(j))).z, _Zoom)

                        ' Desenha faces
                        _GL.DrawLine(_Pen, CInt(_facePt(0).x), CInt(_facePt(0).y), CInt(_facePt(1).x), CInt(_facePt(1).y)) ' Desenha faces (linhas verticais)
                    Next
                Next



            End If ' Wireframe

        End Sub



    End Class

End Namespace

