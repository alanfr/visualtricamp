﻿Imports BGeom.Ponto
Imports BMath.Funcoes

Imports BMath.Constantes


Namespace Objetos

    Module Clipping

        Const INSIDE As Byte = 0 '0000
        Const LEFT As Byte = 1   '0001
        Const RIGHT As Byte = 2  '0010
        Const BOTTOM As Byte = 4 '0100
        Const TOP As Byte = 8    '1000

        '#################### LINE CLIPPING ####################



        Public Function CLIPt(ByVal denom As Double, ByVal num As Double, ByRef tE As Double, ByRef tL As Double) As Boolean
            ' Foley & Van Dam - pg 122 (Computer Graphics, 2ed in C)

            Dim t As Double

            If (denom > ZERO) Then         ' PE intersection
                t = num / denom         ' value of t at intersection
                If (t > tL) Then        ' tE and tL crossover
                    Return False        ' so prepare to reject line
                ElseIf (t > tE) Then    ' A new tE has been found
                    tE = t
                End If
            ElseIf (denom < ZERO) Then     ' PL intersection
                t = num / denom         ' Value of t at the intersection
                If (t < tE) Then        ' tE and tL crossover
                    Return False        ' so prepare to reject line
                Else                    ' A new tL has been found
                    tL = t
                End If
            ElseIf (num > ZERO) Then       ' Line on outside of edge
                Return False
            End If
            Return True

        End Function


        'Liang-Barsky 2D clipping extended to the 3D canonical perspective-projection view volume
        Public Function Clip3D(ByVal x0 As Double, ByVal y0 As Double, ByVal z0 As Double,
                               ByVal x1 As Double, ByVal y1 As Double, ByVal z1 As Double, ByVal zmin As Double) As Ponto3D()

            ' Foley & Van Dam - pg 274 (Computer Graphics, 2ed in C)

            Dim tmin, tmax As Double
            Dim dx, dy, dz As Double
            Dim pts(1) As Ponto3D

            tmin = ZERO : tmax = 1.0
            dx = x1 - x0 : dz = z1 - z0
            'accept = False

            If (CLIPt(-dx - dz, x0 + z0, tmin, tmax)) Then                      ' Right side
                If (CLIPt(dx - dz, -x0 + z0, tmin, tmax)) Then '{               ' Left side
                    ' If get this far, part of line is in -z <= x <= z
                    dy = y1 - y0
                    If (CLIPt(dy - dz, -y0 + z0, tmin, tmax)) Then              ' Bottom
                        If (CLIPt(-dy - dz, y0 + z0, tmin, tmax)) Then          ' Top
                            ' if get this far, part of line is in 
                            '-z <= x <= z, -z <= y <= z
                            If (CLIPt(-dz, z0 - zmin, tmin, tmax)) Then         ' Front     z0 - zmin
                                If (CLIPt(dz, -z0 - 1, tmin, tmax)) Then
                                    ' If get here, part of line is visible in -z <= x <= z,
                                    ' -z <= y <= z, -1 <= z <= zmin
                                    'accept = True
                                    'if endpoint 1 (t=1) is not in the region, compute intersection
                                    If (tmax < 1.0) Then
                                        x1 = x0 + tmax * dx
                                        y1 = y0 + tmax * dy
                                        z1 = z0 + tmax * dz
                                    End If
                                    ' if endpoint 0 (t=0) is not in the region, compute intersection
                                    If (tmin > ZERO) Then
                                        x0 += tmin * dx
                                        y0 += tmin * dy
                                        z0 += tmin * dz
                                    End If
                                End If ' Calcula a intersecao
                            End If
                        End If
                    End If
                End If
            End If 'Clip3D
            pts(0) = New Ponto3D(x0, y0, z0) ' Ponto inicial
            pts(1) = New Ponto3D(x1, y1, z1) ' Ponto final

            Return pts

        End Function 'Sub Clip3D




        '##################################################################




        ' Cohen-Sutherland line-clipping algorithm
        Public Function CompOutCode(ByVal x As Double, ByVal y As Double,
                                    ByVal xmin As Double, ByVal xmax As Double,
                                    ByVal ymin As Double, ByVal ymax As Double) As Byte
            ' Foley & Van Dam - pg 117 (Computer Graphics, 2ed in C)

            Dim code As Byte = INSIDE


            If (x < xmin) Then           'to the left of clip window
                code = code Or LEFT
            ElseIf (x > xmax) Then      'to the right of clip window
                code = code Or RIGHT
            ElseIf (y < ymin) Then           'below the clip window
                code = code Or BOTTOM
            ElseIf (y > ymax) Then      'above the clip window
                code = code Or TOP
            End If

            Return code
        End Function


        ' Cohen-Sutherland line-clipping algorithm
        Public Function CohenSutherland(ByVal x0 As Double, ByVal y0 As Double, _
                                        ByVal x1 As Double, ByVal y1 As Double, _
                                        ByVal xmin As Double, ByVal xmax As Double, _
                                        ByVal ymin As Double, ByVal ymax As Double) As Ponto2D()
            ' Foley & Van Dam - pg 116 (Computer Graphics, 2ed in C)

            Dim outcode0, outcode1 As Byte
            'Dim accept As Boolean = False
            Dim pts(1) As Ponto2D
            Dim accept As Boolean = False

            outcode0 = CompOutCode(x0, y0, xmin, xmax, ymin, ymax)
            outcode1 = CompOutCode(x1, y1, xmin, xmax, ymin, ymax)


            Do While True
                If (Not (CBool(outcode0 Or outcode1))) Then
                    accept = True
                    Exit Do
                ElseIf CBool((outcode0 And outcode1)) Then
                    Exit Do
                Else

                    ' Failed both tests, so calculate the line segment clip:
                    ' from an outside point to an intersection with clip edge
                    Dim x, y As Double
                    Dim outcodeOut As Byte
                    outcodeOut = If(CBool(outcode0), outcode0, outcode1) ' outcodeOut = outcode0 ? outcode0:outcode1;
                    If CBool((outcodeOut And TOP)) Then
                        x = x0 + (x1 - x0) * (ymax - y0) / (y1 - y0)
                        y = ymax
                    ElseIf CBool((outcodeOut And BOTTOM)) Then
                        x = x0 + (x1 - x0) * (ymin - y0) / (y1 - y0)
                        y = ymin
                    ElseIf CBool((outcodeOut And RIGHT)) Then
                        y = y0 + (y1 - y0) * (xmax - x0) / (x1 - x0)
                        x = xmax
                    ElseIf CBool((outcodeOut And LEFT)) Then
                        y = y0 + (y1 - y0) * (xmin - x0) / (x1 - x0)
                        x = xmin
                    End If

                    ' Now we move outside point to intersection point to clip
                    ' and get ready for next pass
                    If (outcodeOut = outcode0) Then
                        x0 = x : y0 = y
                        outcode0 = CompOutCode(x0, y0, xmin, xmax, ymin, ymax)
                    Else
                        x1 = x : y1 = y
                        outcode1 = CompOutCode(x1, y1, xmin, xmax, ymin, ymax)
                    End If
                End If
            Loop

            If accept Then
                pts(0) = New Ponto2D(x0, y0)
                pts(1) = New Ponto2D(x1, y1)
            End If
            
            Return pts
        End Function


    End Module

End Namespace

