﻿Imports BGeom.Ponto
Imports BMath.Matriz
Imports BMath.Constantes

Imports System.Windows.Forms
Imports System.Drawing

Namespace Objetos

    Public MustInherit Class Objeto ' Classe Abstrata

        Implements IDisposable

        Protected _Pen As Pen
        Protected _GL As Graphics
        Protected _Cor As Color

        Protected _AngX As Double
        Protected _AngY As Double
        Protected _AngZ As Double
        Protected _Zoom As Double
        Protected _MT As Matriz4x4
        Protected _P1 As Ponto3D
        Protected _P2 As Ponto3D

        Protected _Angulo As Double
        Protected _NR As Integer
        Protected _NL As Integer
        Protected _Tipo As String
        Protected _Status As String
        Protected _ID As Integer
        Protected _Wireframe As Boolean
        Protected _AntiAlias As Boolean
        Protected _BoundBox As Boolean

        Protected _Circulo() As Ponto3D
        Protected _CirculoP() As Ponto3D
        Protected _Vertices() As Ponto3D
        Protected _Faces(,) As Integer

        'Clipping
        Protected _facePt(1) As Ponto3D
        Protected _linhaPt(1) As Ponto3D

        'BoundBox
        Protected _CBox(3) As Ponto3D '0,1,2,3
        Protected _VBox(7) As Ponto3D '0,1,2,3,4,5,6,7
        Protected _FBox(1, 3) As Integer '0,1 ; 0,1,2,3
        Protected _QuadBox(3) As Ponto3D
        Protected _Box(1, 3) As Ponto3D '2 faces e 4 pontos

        'Mostrar Eixos
        Protected _Eixo As Eixos
        Protected _MostraXYZ As Boolean


        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False


#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub


        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub


        Public Sub New()
            _AngX = ZERO
            _AngY = ZERO
            _AngZ = ZERO
            _Zoom = ZERO
            _Angulo = ZERO
            _NR = 2
            _NL = 2
            _Tipo = ""
            _Status = ""
            _ID = 0
            _Cor = Color.Black
            _Wireframe = True
            _AntiAlias = True
            _BoundBox = False
            _MT = Matriz4x4.Identidade
            _MostraXYZ = True

            '_Circulo(0) = New Ponto3D
            '_Vertices(0) = New Ponto3D
            '_Faces(0, 0) = 0
            '_BoundBox(0, 0) = New Ponto3D

        End Sub


#End Region



#Region "Propriedades"

        '######## Get-Set ###########
        Public MustOverride Property Angulo() As Double
        Public MustOverride Property Cor() As Color
        Public MustOverride Property NumDivCirc() As Integer
        Public MustOverride Property NumDivComp() As Integer
        Public MustOverride Property ID() As Integer
        Public MustOverride Property Wireframe() As Boolean
        Public MustOverride Property AntiAlias() As Boolean
        Public MustOverride Property BoundBox() As Boolean
        Public MustOverride Property MT() As Matriz4x4
        Public MustOverride Property Zoom() As Double
        Public MustOverride Property MostraXYZ() As Boolean



        '####### Read Only #########
        Public MustOverride ReadOnly Property Box As Ponto3D(,)
        Public MustOverride ReadOnly Property Circulo As Ponto3D()
        Public MustOverride ReadOnly Property Faces As Integer(,)
        Public MustOverride ReadOnly Property Vertices As Ponto3D()
        Public MustOverride ReadOnly Property Status As String
        Public MustOverride ReadOnly Property Tipo As String
        Public MustOverride ReadOnly Property P1 As Ponto3D
        Public MustOverride ReadOnly Property P2 As Ponto3D


        '############## Métodos ##############
        Public MustOverride Sub GeraCasca3D()
        Public MustOverride Sub Desenha(ByVal Local As PictureBox, _
                                        ByVal MT As Matriz4x4)


#End Region



    End Class

End Namespace

