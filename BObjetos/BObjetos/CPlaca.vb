﻿Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes


Imports System.Windows.Forms
Imports System.Drawing


Namespace Objetos


    Public Class Placa
        Inherits Objeto

        Protected _P3 As Ponto3D
        Protected _H As Double





#Region "Construtor e Destrutor"

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub

        Public Sub New()
            MyBase.New()
            _P1 = New Ponto3D(0.0, 0.0, 0.0)
            _P2 = New Ponto3D(0.0, 1.0, 0.0)
            _P3 = New Ponto3D(0.0, 0.0, 1.0)
            _H = 1.0
        End Sub


        Public Sub New(ByVal P1 As Ponto3D, _
                       ByVal P2 As Ponto3D, _
                       ByVal P3 As Ponto3D, _
                       ByVal H As Double)


            Me._P1 = P1
            Me._P2 = P2
            Me._P3 = P3
            Me._H = H
            Me._Status = Erro.ok
            Me._Tipo = "Placa"

        End Sub


#End Region


#Region "Propriedades"


        '############### Get-Set ###############

        Public Overrides Property Cor() As Color
            Get
                Return Me._Cor
            End Get
            Set(value As Color)
                Me._Cor = value
            End Set
        End Property

        Public Overrides Property Angulo() As Double
            Get
                Return _Angulo
            End Get
            Set(value As Double)
                Me._Angulo = value
            End Set
        End Property


        Public Overrides Property NumDivCirc() As Integer
            Get
                Return Me._NR
            End Get
            Set(value As Integer)
                Me._NR = value
            End Set
        End Property


        Public Overrides Property NumDivComp() As Integer
            Get
                Return Me._NL
            End Get
            Set(value As Integer)
                Me._NL = value
            End Set
        End Property


        Public Overrides Property ID() As Integer
            Get
                Return _ID
            End Get
            Set(value As Integer)
                _ID = value
            End Set
        End Property


        Public Overrides Property Wireframe() As Boolean
            Get
                Return Me._Wireframe
            End Get
            Set(value As Boolean)
                Me._Wireframe = value
            End Set
        End Property



        Public Overrides Property AntiAlias As Boolean
            Get
                Return Me._AntiAlias
            End Get
            Set(value As Boolean)
                Me._AntiAlias = value
            End Set
        End Property




        Public Overrides Property BoundBox As Boolean
            Get
                Return Me._BoundBox
            End Get
            Set(value As Boolean)
                Me._BoundBox = value
            End Set
        End Property



        Public Overrides Property MT As Matriz4x4
            Get
                Return Me._MT
            End Get
            Set(value As Matriz4x4)
                Me._MT = MT
            End Set
        End Property




        Public Overrides Property Zoom As Double
            Get
                Return Me._Zoom
            End Get
            Set(value As Double)
                Me._Zoom = value
            End Set
        End Property




        Public Overrides Property MostraXYZ() As Boolean
            Get
                Return Me._MostraXYZ
            End Get
            Set(value As Boolean)
                Me._MostraXYZ = value
            End Set
        End Property




        '####### Read Only #########

        Public Overrides ReadOnly Property Box As Ponto3D(,)
            Get
                Return Me._Box
            End Get
        End Property



        Public Overrides ReadOnly Property Circulo As Ponto3D()
            Get
                Return Me._Circulo
            End Get
        End Property



        Public Overrides ReadOnly Property Faces As Integer(,)
            Get
                Return Me._Faces
            End Get
        End Property


        Public Overrides ReadOnly Property Vertices As Ponto3D()
            Get
                Return Me._Vertices
            End Get
        End Property


        Public Overrides ReadOnly Property Status As String
            Get
                Return Me._Status
            End Get
        End Property


        Public Overrides ReadOnly Property Tipo As String
            Get
                Return Me._Tipo
            End Get
        End Property



        Public Overrides ReadOnly Property P1() As Ponto3D
            Get
                Return Me._P1
            End Get
        End Property


        Public Overrides ReadOnly Property P2() As Ponto3D
            Get
                Return Me._P2
            End Get
        End Property



        Public ReadOnly Property P3() As Ponto3D
            Get
                Return Me._P3
            End Get
        End Property


        Public ReadOnly Property H() As Double
            Get
                Return Me._H
            End Get
        End Property




#End Region



        Public Overrides Sub GeraCasca3D()
            GC.Collect() 'Limpar recursos não utilizados


            ' Cria o array com os 8 pontos/vertices.
            _Vertices = New Ponto3D() {
                        P1,
                        New Ponto3D(P1.x + H, P1.y, P1.z),
                        New Ponto3D(P2.x + H, P2.y, P2.z),
                        P2,
                        P3,
                        New Ponto3D(P3.x + H, P3.y, P3.z),
                        New Ponto3D(P2.x + H, P2.y, P3.z),
                        New Ponto3D(P2.x, P2.y, P3.z)}

            ' Cria um array com a representação das 6 faces.
            ' Cada face é composta pelo índice do array de vertices
            _Faces = New Integer(,) {{0, 1, 5, 4}, _
                                     {1, 2, 6, 5}, _
                                     {2, 3, 7, 6}, _
                                     {3, 0, 4, 7}, _
                                     {4, 5, 6, 7}, _
                                     {0, 3, 2, 1}}
            '     7 __________6
            '      |\        |\
            '      | \_______|_\5
            '      |4|       | |
            '      |_|_______|2|
            '     3\ |        \|
            '       \|_________\
            '        0          1
            '
        End Sub




        Public Overrides Sub Desenha(ByVal Local As PictureBox, ByVal MT As Matriz4x4)



            If Me._Wireframe = True Then
                GC.Collect() 'Limpar recursos não utilizados

                Dim t(8) As Ponto3D
                Dim f(4) As Integer
                Dim v As Ponto3D
                Dim indf(6) As Integer
                Dim MTF As Matriz4x4

                indf = New Integer() {1, 2, 3, 0}

                GC.Collect() 'Limpar recursos não utilizados
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me.Cor)


                If Me._AntiAlias = True Then
                    _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
                End If

                'Matriz de transformacao final
                MTF = MatrizEulerPos(P1, P2, 3) * MT


                If (Me.MostraXYZ = True) Then
                    _Eixo = New Eixos(0.5)
                    _Eixo.Desenha(Local, MTF)
                End If


                ' Transforma os pontos e salva no array t.
                For i = 0 To 7
                    v = Vertices(i)
                    t(i) = ProjetaTela(AplicaTransformacao3D(v, MT), Local)
                Next


                ' Desenha o cubo em wireframe. 
                ' Pega o array "faces" para encontrar os vertices que compoem cada face.
                For i = 0 To 5 '6 faces
                    For j = 0 To 3
                        _linhaPt = Clip3D(t(Faces(i, j)).x, t(Faces(i, j)).y, t(Faces(i, j)).z,
                                              t(Faces(i, indf(j))).x, t(Faces(i, indf(j))).y, t(Faces(i, indf(j))).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linhas
                    Next
                Next
            End If ' Wireframe


        End Sub



    End Class

End Namespace




























'Namespace Objetos



'    Public Class Placa

'        Protected pen As Pen

'        'Objeto CUBO
'        Protected vertices(8) As Ponto3D
'        Protected faces(6, 4) As Integer '6 faces com 4 vertices
'        Protected angulo As Integer


'        Public Sub New()

'        End Sub



'        'Objeto Path
'        Public Sub CriaCubo()
'            ' Cria o array com os 8 pontos/vertices.
'            vertices = New Ponto3D() {
'                       New Ponto3D(0, 10, 0),
'                       New Ponto3D(10, 10, 0),
'                       New Ponto3D(10, 10, 10),
'                       New Ponto3D(0, 10, 10),
'                       New Ponto3D(10, 0, 0),
'                       New Ponto3D(10, 0, 10),
'                       New Ponto3D(0, 0, 10),
'                       New Ponto3D(0, 0, 0)}

'            ' Cria um array com a representação das 6 faces.
'            ' Cada face é composta pelo índice do array de vertices
'            faces = New Integer(,) {{0, 1, 2, 3, 3, 3}, _
'                                    {1, 4, 5, 2, 2, 2}, _
'                                    {4, 7, 6, 5, 5, 5}, _
'                                    {7, 0, 3, 6, 6, 6}, _
'                                    {0, 1, 4, 7, 7, 7}, _
'                                    {3, 2, 5, 6, 6, 6}}
'            '     7 __________6
'            '      |\        |\
'            '      | \_______|_\2
'            '      |3|       | |
'            '      |_|_______|5|
'            '     4\ |        \|
'            '       \|_________\
'            '        0          1
'            '
'        End Sub


'        Public Sub Desenha(ByVal Local As PictureBox, ByVal MT As Matriz4x4)
'            Dim t(8) As Ponto3D
'            Dim f(4) As Integer
'            Dim v As Ponto3D
'            Dim Pen As Pen
'            Dim GL As Graphics

'            GC.Collect() 'Limpar recursos não utilizados
'            GL = Local.CreateGraphics
'            Pen = New Pen(Color.Black)

'            GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias


'            ' Transforma os pontos e salva no array t.
'            For i = 0 To 7
'                v = vertices(i)
'                t(i) = ProjetaTela(AplicaTransformacao3D(v, MT), Local)
'            Next


'            ' Desenha o cubo em wireframe. 
'            ' Pega o array "faces" para encontrar os vertices que compoem cada face.

'            For i = 0 To 5 '6 faces
'                GL.DrawLine(Pen, CInt(t(faces(i, 0)).x), CInt(t(faces(i, 0)).y), CInt(t(faces(i, 1)).x), CInt(t(faces(i, 1)).y))
'                GL.DrawLine(Pen, CInt(t(faces(i, 1)).x), CInt(t(faces(i, 1)).y), CInt(t(faces(i, 2)).x), CInt(t(faces(i, 2)).y))
'                GL.DrawLine(Pen, CInt(t(faces(i, 2)).x), CInt(t(faces(i, 2)).y), CInt(t(faces(i, 3)).x), CInt(t(faces(i, 3)).y))
'                GL.DrawLine(Pen, CInt(t(faces(i, 3)).x), CInt(t(faces(i, 3)).y), CInt(t(faces(i, 4)).x), CInt(t(faces(i, 4)).y))
'                GL.DrawLine(Pen, CInt(t(faces(i, 4)).x), CInt(t(faces(i, 4)).y), CInt(t(faces(i, 5)).x), CInt(t(faces(i, 5)).y))
'                GL.DrawLine(Pen, CInt(t(faces(i, 5)).x), CInt(t(faces(i, 5)).y), CInt(t(faces(i, 0)).x), CInt(t(faces(i, 0)).y))
'            Next

'        End Sub


'    End Class


'End Namespace

