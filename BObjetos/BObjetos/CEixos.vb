﻿Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes


Imports System.Windows.Forms
Imports System.Drawing


Namespace Objetos


    Public Class Eixos

        Protected _L As Double
        Protected _Tipo As String
        Protected _CorX, _CorY, _CorZ As Color
        Protected _MostraX, _MostraY, _MostraZ As Boolean
        Protected _ID As Integer
        Protected _Zoom As Double

        Protected _GL As Graphics
        Protected _Pen As Pen

        'Clipping
        'Protected _facePt(1) As Ponto3D
        Protected _linhaPt(1) As Ponto3D



#Region "Construtor e Destrutor"


        Public Sub New(ByVal L As Double)
            Me._L = L
            Me._CorX = Color.Red
            Me._CorY = Color.Green
            Me._CorZ = Color.Blue
            Me._MostraX = True
            Me._MostraY = True
            Me._MostraZ = True

            Me._Tipo = "Linha"

        End Sub


#End Region


#Region "Propriedades"


        '############### Get-Set ###############

        Public Property CorX() As Color
            Get
                Return Me._CorX
            End Get
            Set(value As Color)
                Me._CorX = value
            End Set
        End Property

        Public Property CorY() As Color
            Get
                Return Me._CorY
            End Get
            Set(value As Color)
                Me._CorY = value
            End Set
        End Property

        Public Property CorZ() As Color
            Get
                Return Me._CorZ
            End Get
            Set(value As Color)
                Me._CorZ = value
            End Set
        End Property

        Public Property MostraX() As Boolean
            Get
                Return Me._MostraX
            End Get
            Set(value As Boolean)
                Me._MostraX = value
            End Set
        End Property

        Public Property MostraY() As Boolean
            Get
                Return Me._MostraY
            End Get
            Set(value As Boolean)
                Me._MostraY = value
            End Set
        End Property

        Public Property MostraZ() As Boolean
            Get
                Return Me._MostraZ
            End Get
            Set(value As Boolean)
                Me._MostraZ = value
            End Set
        End Property


        Public Property ID() As Integer
            Get
                Return _ID
            End Get
            Set(value As Integer)
                _ID = value
            End Set
        End Property


        Public Property Zoom() As Double
            Get
                Return Me._Zoom
            End Get
            Set(value As Double)
                Me._Zoom = value
            End Set
        End Property

        '####### Read Only #########

        Public ReadOnly Property Tipo As String
            Get
                Return Me._Tipo
            End Get
        End Property


#End Region


        Public Sub Desenha(ByVal Local As PictureBox, ByVal MT As Matriz4x4)

            GC.Collect() 'Limpar recursos não utilizados
            'Desenha eixo X
            If Me._MostraX = True Then
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me._CorX)

                _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

                Dim t(6) As Ponto3D
                Dim X(3) As Ponto3D

                ' Transforma os pontos e salva no array t.
                t(0) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0, 0, 0), MT), Local)
                t(1) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L, 0, 0), MT), Local)

                'seta
                'planoXZ
                t(2) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L - (0.05 * _L), 0.0, (0.01 * _L)), MT), Local)
                t(3) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L - (0.05 * _L), 0.0, -(0.01 * _L)), MT), Local)
                'planoXY
                t(4) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L - (0.05 * _L), (0.01 * _L), 0.0), MT), Local)
                t(5) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L - (0.05 * _L), -(0.01 * _L), 0.0), MT), Local)


                For i = 0 To 2
                    'Clipping
                    _linhaPt = Clip3D(t(i).x, t(i).y, t(i).z,
                                      t(i + 1).x, t(i + 1).y, t(i + 1).z, _Zoom)
                    'Desenha Linhas
                    _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                    If i = 2 Then
                        'desenha triangulo no plano XZ
                        _linhaPt = Clip3D(t(3).x, t(3).y, t(3).z,
                                      t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        'desenha triangulo no plano XY
                        _linhaPt = Clip3D(t(1).x, t(1).y, t(1).z,
                                          t(4).x, t(4).y, t(4).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        _linhaPt = Clip3D(t(4).x, t(4).y, t(4).z,
                                          t(5).x, t(5).y, t(5).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha


                        _linhaPt = Clip3D(t(5).x, t(5).y, t(5).z,
                                          t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha
                    End If

                Next

                'Escreve X na ponta do eixo no plano XY
                X(0) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L + (0.03 * _L), (0.01 * _L), 0), MT), Local)
                X(1) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L + (0.07 * _L), -(0.01 * _L), 0), MT), Local)

                X(2) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L + (0.03 * _L), -(0.01 * _L), 0), MT), Local)
                X(3) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(_L + (0.07 * _L), (0.01 * _L), 0), MT), Local)

                _linhaPt = Clip3D(X(0).x, X(0).y, X(0).z,
                                  X(1).x, X(1).y, X(1).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                _linhaPt = Clip3D(X(2).x, X(2).y, X(2).z,
                                  X(3).x, X(3).y, X(3).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha


            End If ' MostraX






            'Desenha eixo Y
            If Me._MostraY = True Then
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me._CorY)

                _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

                Dim t(6) As Ponto3D
                Dim Y(3) As Ponto3D

                ' Transforma os pontos e salva no array t.
                t(0) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0, 0, 0), MT), Local)
                t(1) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0, _L, 0), MT), Local)

                'seta
                'planoYZ
                t(2) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0.0, _L - (0.05 * _L), (0.01 * _L)), MT), Local)
                t(3) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0.0, _L - (0.05 * _L), -(0.01 * _L)), MT), Local)

                'planoXY
                t(4) = ProjetaTela(AplicaTransformacao3D(New Ponto3D((0.01 * _L), _L - (0.05 * _L), 0.0), MT), Local)
                t(5) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), _L - (0.05 * _L), 0.0), MT), Local)


                For i = 0 To 2
                    'Clipping
                    _linhaPt = Clip3D(t(i).x, t(i).y, t(i).z,
                                      t(i + 1).x, t(i + 1).y, t(i + 1).z, _Zoom)
                    'Desenha Linhas
                    _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                    If i = 2 Then
                        'desenha triangulo no plano XZ
                        _linhaPt = Clip3D(t(3).x, t(3).y, t(3).z,
                                      t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        'desenha triangulo no plano XY
                        _linhaPt = Clip3D(t(1).x, t(1).y, t(1).z,
                                          t(4).x, t(4).y, t(4).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        _linhaPt = Clip3D(t(4).x, t(4).y, t(4).z,
                                          t(5).x, t(5).y, t(5).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha


                        _linhaPt = Clip3D(t(5).x, t(5).y, t(5).z,
                                          t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha
                    End If

                Next


                'Escreve Y na ponta do eixo no plano XY
                Y(0) = ProjetaTela(AplicaTransformacao3D(New Ponto3D((0.01 * _L), _L + (0.02 * _L), 0), MT), Local)
                Y(1) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0, _L + (0.05 * _L), 0), MT), Local)

                Y(2) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), _L + (0.02 * _L), 0), MT), Local)
                Y(3) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0, _L + (0.07 * _L), 0), MT), Local)

                _linhaPt = Clip3D(Y(0).x, Y(0).y, Y(0).z,
                                  Y(1).x, Y(1).y, Y(1).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                _linhaPt = Clip3D(Y(1).x, Y(1).y, Y(1).z,
                                  Y(2).x, Y(2).y, Y(2).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                _linhaPt = Clip3D(Y(1).x, Y(1).y, Y(1).z,
                                  Y(3).x, Y(3).y, Y(3).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

            End If ' MostraY



            'Desenha eixo Z
            If Me._MostraZ = True Then
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me._CorZ)

                _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

                Dim t(6) As Ponto3D
                Dim Z(3) As Ponto3D

                ' Transforma os pontos e salva no array t.
                t(0) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0, 0, 0), MT), Local)
                t(1) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0, 0, _L), MT), Local)

                'seta
                'planoYZ
                t(2) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0.0, (0.01 * _L), _L - (0.05 * _L)), MT), Local)
                t(3) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(0.0, -(0.01 * _L), _L - (0.05 * _L)), MT), Local)

                'planoXZ
                t(4) = ProjetaTela(AplicaTransformacao3D(New Ponto3D((0.01 * _L), 0.0, _L - (0.05 * _L)), MT), Local)
                t(5) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), 0.0, _L - (0.05 * _L)), MT), Local)


                For i = 0 To 2
                    'Clipping
                    _linhaPt = Clip3D(t(i).x, t(i).y, t(i).z,
                                      t(i + 1).x, t(i + 1).y, t(i + 1).z, _Zoom)
                    'Desenha Linhas
                    _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                    If i = 2 Then
                        'desenha triangulo no plano XZ
                        _linhaPt = Clip3D(t(3).x, t(3).y, t(3).z,
                                      t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        'desenha triangulo no plano XY
                        _linhaPt = Clip3D(t(1).x, t(1).y, t(1).z,
                                          t(4).x, t(4).y, t(4).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        _linhaPt = Clip3D(t(4).x, t(4).y, t(4).z,
                                          t(5).x, t(5).y, t(5).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha


                        _linhaPt = Clip3D(t(5).x, t(5).y, t(5).z,
                                          t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha
                    End If

                Next

                'Escreve Y na ponta do eixo no plano XY

                Z(0) = ProjetaTela(AplicaTransformacao3D(New Ponto3D((0.01 * _L), -(0.01 * _L), _L + (0.03 * _L)), MT), Local)
                Z(1) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), (0.01 * _L), _L + (0.03 * _L)), MT), Local)

                Z(2) = ProjetaTela(AplicaTransformacao3D(New Ponto3D((0.01 * _L), -(0.01 * _L), _L + (0.07 * _L)), MT), Local)
                Z(3) = ProjetaTela(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), (0.01 * _L), _L + (0.07 * _L)), MT), Local)


                For i = 0 To 2
                    _linhaPt = Clip3D(Z(i).x, Z(i).y, Z(i).z,
                                  Z(i + 1).x, Z(i + 1).y, Z(i + 1).z, _Zoom)
                    _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                Next
                

            End If ' MostraZ

        End Sub





        Public Function ProjetaEixoCanto(ByVal Ponto As Ponto3D, ByVal Local As PictureBox) As Ponto3D
            Dim xn As Double
            Dim yn As Double

            xn = Ponto.x + Local.Width * 0.05 'Coloca o centro da tela como o centro das coordenadas
            yn = Ponto.y + Local.Height * 0.95

            Return New Ponto3D(xn, yn, Ponto.z)
        End Function



        Public Sub DesenhaCantoTela(ByVal Local As PictureBox, ByVal MT As Matriz4x4)

            GC.Collect() 'Limpar recursos não utilizados
            'Desenha eixo X
            If Me._MostraX = True Then
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me._CorX)

                _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

                Dim t(6) As Ponto3D
                Dim X(3) As Ponto3D

                ' Transforma os pontos e salva no array t.
                t(0) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0, 0, 0), MT), Local)
                t(1) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L, 0, 0), MT), Local)

                'seta
                'planoXZ
                t(2) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L - (0.05 * _L), 0.0, (0.01 * _L)), MT), Local)
                t(3) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L - (0.05 * _L), 0.0, -(0.01 * _L)), MT), Local)
                'planoXY
                t(4) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L - (0.05 * _L), (0.01 * _L), 0.0), MT), Local)
                t(5) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L - (0.05 * _L), -(0.01 * _L), 0.0), MT), Local)


                For i = 0 To 2
                    'Clipping
                    _linhaPt = Clip3D(t(i).x, t(i).y, t(i).z,
                                      t(i + 1).x, t(i + 1).y, t(i + 1).z, _Zoom)
                    'Desenha Linhas
                    _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                    If i = 2 Then
                        'desenha triangulo no plano XZ
                        _linhaPt = Clip3D(t(3).x, t(3).y, t(3).z,
                                      t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        'desenha triangulo no plano XY
                        _linhaPt = Clip3D(t(1).x, t(1).y, t(1).z,
                                          t(4).x, t(4).y, t(4).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        _linhaPt = Clip3D(t(4).x, t(4).y, t(4).z,
                                          t(5).x, t(5).y, t(5).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha


                        _linhaPt = Clip3D(t(5).x, t(5).y, t(5).z,
                                          t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha
                    End If

                Next

                'Escreve X na ponta do eixo no plano XY
                X(0) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L + (0.03 * _L), (0.01 * _L), 0), MT), Local)
                X(1) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L + (0.07 * _L), -(0.01 * _L), 0), MT), Local)

                X(2) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L + (0.03 * _L), -(0.01 * _L), 0), MT), Local)
                X(3) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(_L + (0.07 * _L), (0.01 * _L), 0), MT), Local)

                _linhaPt = Clip3D(X(0).x, X(0).y, X(0).z,
                                  X(1).x, X(1).y, X(1).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                _linhaPt = Clip3D(X(2).x, X(2).y, X(2).z,
                                  X(3).x, X(3).y, X(3).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha


            End If ' MostraX






            'Desenha eixo Y
            If Me._MostraY = True Then
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me._CorY)

                _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

                Dim t(6) As Ponto3D
                Dim Y(3) As Ponto3D

                ' Transforma os pontos e salva no array t.
                t(0) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0, 0, 0), MT), Local)
                t(1) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0, _L, 0), MT), Local)

                'seta
                'planoYZ
                t(2) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0.0, _L - (0.05 * _L), (0.01 * _L)), MT), Local)
                t(3) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0.0, _L - (0.05 * _L), -(0.01 * _L)), MT), Local)

                'planoXY
                t(4) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D((0.01 * _L), _L - (0.05 * _L), 0.0), MT), Local)
                t(5) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), _L - (0.05 * _L), 0.0), MT), Local)


                For i = 0 To 2
                    'Clipping
                    _linhaPt = Clip3D(t(i).x, t(i).y, t(i).z,
                                      t(i + 1).x, t(i + 1).y, t(i + 1).z, _Zoom)
                    'Desenha Linhas
                    _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                    If i = 2 Then
                        'desenha triangulo no plano XZ
                        _linhaPt = Clip3D(t(3).x, t(3).y, t(3).z,
                                      t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        'desenha triangulo no plano XY
                        _linhaPt = Clip3D(t(1).x, t(1).y, t(1).z,
                                          t(4).x, t(4).y, t(4).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        _linhaPt = Clip3D(t(4).x, t(4).y, t(4).z,
                                          t(5).x, t(5).y, t(5).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha


                        _linhaPt = Clip3D(t(5).x, t(5).y, t(5).z,
                                          t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha
                    End If

                Next


                'Escreve Y na ponta do eixo no plano XY
                Y(0) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D((0.01 * _L), _L + (0.02 * _L), 0), MT), Local)
                Y(1) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0, _L + (0.05 * _L), 0), MT), Local)

                Y(2) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), _L + (0.02 * _L), 0), MT), Local)
                Y(3) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0, _L + (0.07 * _L), 0), MT), Local)

                _linhaPt = Clip3D(Y(0).x, Y(0).y, Y(0).z,
                                  Y(1).x, Y(1).y, Y(1).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                _linhaPt = Clip3D(Y(1).x, Y(1).y, Y(1).z,
                                  Y(2).x, Y(2).y, Y(2).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                _linhaPt = Clip3D(Y(1).x, Y(1).y, Y(1).z,
                                  Y(3).x, Y(3).y, Y(3).z, _Zoom)
                _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

            End If ' MostraY



            'Desenha eixo Z
            If Me._MostraZ = True Then
                _GL = Local.CreateGraphics
                _Pen = New Pen(Me._CorZ)

                _GL.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias

                Dim t(6) As Ponto3D
                Dim Z(3) As Ponto3D

                ' Transforma os pontos e salva no array t.
                t(0) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0, 0, 0), MT), Local)
                t(1) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0, 0, _L), MT), Local)

                'seta
                'planoYZ
                t(2) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0.0, (0.01 * _L), _L - (0.05 * _L)), MT), Local)
                t(3) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(0.0, -(0.01 * _L), _L - (0.05 * _L)), MT), Local)

                'planoXZ
                t(4) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D((0.01 * _L), 0.0, _L - (0.05 * _L)), MT), Local)
                t(5) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), 0.0, _L - (0.05 * _L)), MT), Local)


                For i = 0 To 2
                    'Clipping
                    _linhaPt = Clip3D(t(i).x, t(i).y, t(i).z,
                                      t(i + 1).x, t(i + 1).y, t(i + 1).z, _Zoom)
                    'Desenha Linhas
                    _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                    If i = 2 Then
                        'desenha triangulo no plano XZ
                        _linhaPt = Clip3D(t(3).x, t(3).y, t(3).z,
                                      t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        'desenha triangulo no plano XY
                        _linhaPt = Clip3D(t(1).x, t(1).y, t(1).z,
                                          t(4).x, t(4).y, t(4).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                        _linhaPt = Clip3D(t(4).x, t(4).y, t(4).z,
                                          t(5).x, t(5).y, t(5).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha


                        _linhaPt = Clip3D(t(5).x, t(5).y, t(5).z,
                                          t(1).x, t(1).y, t(1).z, _Zoom)

                        _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha
                    End If

                Next

                'Escreve Y na ponta do eixo no plano XY

                Z(0) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D((0.01 * _L), -(0.01 * _L), _L + (0.03 * _L)), MT), Local)
                Z(1) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), (0.01 * _L), _L + (0.03 * _L)), MT), Local)

                Z(2) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D((0.01 * _L), -(0.01 * _L), _L + (0.07 * _L)), MT), Local)
                Z(3) = ProjetaEixoCanto(AplicaTransformacao3D(New Ponto3D(-(0.01 * _L), (0.01 * _L), _L + (0.07 * _L)), MT), Local)


                For i = 0 To 2
                    _linhaPt = Clip3D(Z(i).x, Z(i).y, Z(i).z,
                                  Z(i + 1).x, Z(i + 1).y, Z(i + 1).z, _Zoom)
                    _GL.DrawLine(_Pen, CInt(_linhaPt(0).x), CInt(_linhaPt(0).y), CInt(_linhaPt(1).x), CInt(_linhaPt(1).y)) 'Desenha linha

                Next


            End If ' MostraZ

        End Sub



    End Class

End Namespace

