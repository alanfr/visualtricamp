﻿Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports BMath.Matriz
Imports BMath.Funcoes
Imports BMath.Constantes


Imports System.Windows.Forms
Imports System.Drawing


Namespace Camera

    Public Class Camera3D
        Implements IDisposable

        Private _Eye As Vetor3D
        Private _Center As Vetor3D
        Private _UP As Vetor3D
        Private _Raio As Double
        Private _Proj As Integer


        Private _Tipo As String
        Private _Nome As String
        Protected _Status As String
        Protected _ID As Integer
        Protected _Liga As Boolean

        Private _Yaw As Double    'Angulo de rotacao em relacao ao eixo Y (coordenadas do objeto)
        Private _Pitch As Double  'Angulo de rotacao em relacao ao eixo X (coordenadas do objeto)


        Private _Pan As Double   'Angulo de rotacao em relacao ao eixo Y (coordenadas da camera)
        Private _Tilt As Double  'Angulo de rotacao em relacao ao eixo X (coordenadas da camera)
        Private _Roll As Double  'Angulo de rotacao em relacao ao eixo Z (coordenadas da camera)

        'Efeitos de camera de cinema
        Private _Dolly As Double
        Private _Truck As Double
        Private _Pedestal As Double

        'Private _Rotacao As Matriz4x4
        'Private _Translacao As Matriz4x4


        ' Keep track of when the object is disposed.
        ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
        Protected disposed As Boolean = False



#Region "Construtor e Destrutor"

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposed Then
                If disposing Then
                End If
            End If
            Me.disposed = True
        End Sub


        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Dispose(False)
            MyBase.Finalize()
        End Sub


        Public Sub New()
            _Eye = New Vetor3D(1.0, 1.0, 1.0)
            _Center = New Vetor3D(0.0, 0.0, 0.0)
            _UP = New Vetor3D(0.0, 0.0, 1.0)
            _Raio = 1
            _Liga = True
            _Tipo = "Camera"
            _Nome = "Camera 1"
            _Status = "OK"
            _ID = 0
        End Sub

        Public Sub New(ByVal Eye As Vetor3D, ByVal Center As Vetor3D, ByVal UP As Vetor3D, ByVal Raio As Double)
            _Eye = Eye
            _Center = Center
            _UP = UP
            _Raio = Raio
            _Liga = True
            _Tipo = "Camera"
            _Nome = "Camera 1"
            _Status = "OK"
            _ID = 0
        End Sub



#End Region


#Region "Propriedades"


        Public Property Eye() As Vetor3D
            Get
                Return Me._Eye
            End Get
            Set(value As Vetor3D)
                Me._Eye = value
            End Set
        End Property

        Public Property Center() As Vetor3D
            Get
                Return Me._Center
            End Get
            Set(value As Vetor3D)
                Me._Center = value
            End Set
        End Property

        Public Property Up() As Vetor3D
            Get
                Return Me._UP
            End Get
            Set(value As Vetor3D)
                Me._UP = value
            End Set
        End Property


        Public Property Raio() As Double
            Get
                Return Me._Raio
            End Get
            Set(value As Double)
                Me._Raio = value
            End Set
        End Property

        Public Property Liga() As Boolean
            Get
                Return Me._Liga
            End Get
            Set(value As Boolean)
                Me._Liga = value
            End Set
        End Property

        Public Property Nome() As String
            Get
                Return Me._Nome
            End Get
            Set(value As String)
                Me._Nome = value
            End Set
        End Property

        Public Property Projecao() As Integer
            Get
                Return Me._Proj
            End Get
            Set(value As Integer)
                Me._Proj = value
            End Set
        End Property

        Public Property Yaw() As Double
            Get
                Return Me._Yaw
            End Get
            Set(value As Double)
                Me._Yaw = value
            End Set
        End Property

        Public Property Pitch() As Double
            Get
                Return Me._Pitch
            End Get
            Set(value As Double)
                Me._Pitch = value
            End Set
        End Property



        Public ReadOnly Property Tipo() As String
            Get
                Return Me._Tipo
            End Get
        End Property



#End Region

        Private Function MProj(ByVal Tipo As Integer) As Matriz4x4
            Dim MT As New Matriz4x4

            Select Case Tipo
                Case 0
                    MT = BGeom.Transformacoes.ProjParalela
                Case 1
                    MT = BGeom.Transformacoes.ProjParalelaIso
                Case 2
                    MT = BGeom.Transformacoes.ProjPerspectiva
                Case Else
                    MT = BGeom.Transformacoes.ProjParalela
            End Select

            Return MT
        End Function



        Public Function Camera() As Matriz4x4

            Dim MT As Matriz4x4
            Dim EixoX, EixoY, EixoZ As Vetor3D

            MT = Matriz4x4.Identidade
            EixoZ = (Eye - Center).getNormaliza  ' Vetor Forward
            EixoX = Vetor3D.ProdutoVetorial(Up, EixoZ).getNormaliza ' Vetor Direita
            EixoY = Vetor3D.ProdutoVetorial(EixoZ, EixoX) ' Vetor UP

            MT.M11 = EixoX.X : MT.M12 = EixoY.X : MT.M13 = EixoZ.X
            MT.M21 = EixoX.Y : MT.M22 = EixoY.Y : MT.M23 = EixoZ.Y
            MT.M31 = EixoX.Z : MT.M32 = EixoY.Z : MT.M33 = EixoZ.Z
            MT.M41 = -Vetor3D.ProdutoEscalar(EixoX, Eye)
            MT.M42 = -Vetor3D.ProdutoEscalar(EixoY, Eye)
            MT.M43 = -Vetor3D.ProdutoEscalar(EixoZ, Eye)

            MT = MT * MatrizTranslacao3D(-Eye.X, -Eye.Y, -Eye.Z) * MProj(Projecao)

            Return MT
        End Function



        Public Function FPSCamera() As Matriz4x4
            'Pitch should be in the range of [-90 ... 90] degrees and yaw
            'should be in the range of [0 ... 360] degrees.
            Dim MT As Matriz4x4
            Dim EixoX, EixoY, EixoZ As Vetor3D
            Dim cosPitch As Double = Math.Cos(Pitch * DEGREE)
            Dim sinPitch As Double = Math.Sin(Pitch * DEGREE)
            Dim cosYaw As Double = Math.Cos(Yaw * DEGREE)
            Dim sinYaw As Double = Math.Sin(Yaw * DEGREE)

            MT = Matriz4x4.Identidade
            EixoX = New Vetor3D(cosYaw, 0, -sinYaw)
            EixoY = New Vetor3D(sinYaw * sinPitch, cosPitch, cosYaw * sinPitch)
            EixoZ = New Vetor3D(sinYaw * cosPitch, -sinPitch, cosPitch * cosYaw)

            MT.M11 = EixoX.X : MT.M12 = EixoY.X : MT.M13 = EixoZ.X
            MT.M21 = EixoX.Y : MT.M22 = EixoY.Y : MT.M23 = EixoZ.Y
            MT.M31 = EixoX.Z : MT.M32 = EixoY.Z : MT.M33 = EixoZ.Z
            MT.M41 = -Vetor3D.ProdutoEscalar(EixoX, Eye)
            MT.M42 = -Vetor3D.ProdutoEscalar(EixoY, Eye)
            MT.M43 = -Vetor3D.ProdutoEscalar(EixoZ, Eye)

            MT = MT * MatrizTranslacao3D(-Eye.X, -Eye.Y, -Eye.Z) * MProj(Projecao)

            Return MT
        End Function




        'Rotaciona a camera
        Public Sub Rotacao(ByVal Rx As Double, ByVal Ry As Double, ByVal Rz As Double)
            Dim MT As Matriz4x4

            MT = MatrizRotacionaX(Rx) * MatrizRotacionaY(Ry) * MatrizRotacionaZ(Rz) * Camera()
            _Eye = AplicaTransformacao3D(_Eye, MT)
            _Center = AplicaTransformacao3D(_Center, MT)

        End Sub


        'Rotaciona a camera
        Public Sub Translacao(ByVal Tx As Double, ByVal Ty As Double, ByVal Tz As Double)
            Dim MT As Matriz4x4

            MT = Camera() * MatrizTranslacao3D(Tx, Ty, Tz)
            _Eye = AplicaTransformacao3D(_Eye, MT)
            _Center = AplicaTransformacao3D(_Center, MT)
            _UP = AplicaTransformacao3D(_UP, MT)

        End Sub


        'Rotaciona a camera
        Public Sub Zoom(ByVal Zoom As Double)
            Dim MT As Matriz4x4

            MT = Camera() * MatrizEscala3D(Zoom, Zoom, Zoom)
            _Eye = AplicaTransformacao3D(_Eye, MT)
            _Center = AplicaTransformacao3D(_Center, MT)
            _UP = AplicaTransformacao3D(_UP, MT)
        End Sub



        'Faz o efeito Camera Dolly
        'Faz o movimento do eye junto com o centro
        Public Sub Dolly(ByVal Translacao As Vetor3D, ByVal Rx As Double, ByVal Ry As Double, ByVal Rz As Double)
            Dim MT As Matriz4x4

            MT = Camera() * MatrizTranslacao3D(Translacao.X, Translacao.Y, Translacao.Z) * MatrizRotacionaX(Rx) * MatrizRotacionaY(Ry) * MatrizRotacionaZ(Rz)
            _Eye = AplicaTransformacao3D(_Eye, MT)
            _Center = AplicaTransformacao3D(_Center, MT)

        End Sub



    End Class

End Namespace
