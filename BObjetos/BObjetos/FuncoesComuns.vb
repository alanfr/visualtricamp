﻿Imports BGeom.Ponto
Imports BMath.Funcoes
Imports BMath.Constantes
Imports BGeom.Transformacoes

Imports BMath.Matriz
Imports BGeom.Vetor

Imports System.Windows.Forms

Namespace Objetos

    Module FuncoesComuns


        Public Function PtosCircunferencia(ByVal NR As Integer, _
                                           ByVal raio As Double) As Ponto3D()

            Dim ptCirc(NR) As Ponto3D
            Dim PassoAngulo As Double = 360 / NR ' Graus


            'Gera a primeira circunferência centrada na origem
            'Com o numero de divisoes
            Parallel.For(1, NR + 1, _
                         Sub(j1)
                             ptCirc(j1) = New Ponto3D(0.0, raio * CosG(PassoAngulo * (j1 - 1)),
                                                      raio * SenG(PassoAngulo * (j1 - 1)))
                         End Sub)

            'For j = 1 To NR
            'ptCirc(j) = New Ponto3D(0, raio * CosG(PassoAngulo * (j - 1)), raio * SenG(PassoAngulo * (j - 1)))
            'Next

            Return ptCirc

        End Function


        Public Function PtosSemiCircunferencia(ByVal NR As Integer, _
                                   ByVal raio As Double) As Ponto3D()

            Dim ptCirc(NR) As Ponto3D
            Dim PassoAngulo As Double = 360 / NR ' Graus

            NR = CInt(NR / 2)
            'Gera a primeira circunferência centrada na origem
            'Com o numero de divisoes
            Parallel.For(1, NR + 1, _
                         Sub(j1)
                             ptCirc(j1) = New Ponto3D(0.0, raio * CosG(PassoAngulo * (j1 - 1)),
                                                      raio * SenG(PassoAngulo * (j1 - 1)))
                         End Sub)

            'For j = 1 To NR
            'ptCirc(j) = New Ponto3D(0, raio * CosG(PassoAngulo * (j - 1)), raio * SenG(PassoAngulo * (j - 1)))
            'Next

            Return ptCirc

        End Function





        Public Function PtosVertices(ByVal cil(,) As Ponto3D) As Ponto3D()

            Dim ii, i, j As Integer
            Dim vert((UBound(cil, 1) + 1) * UBound(cil, 2)) As Ponto3D

            'Mapeia os vértices
            ii = 1
            For i = 0 To UBound(cil, 1)
                For j = 1 To UBound(cil, 2)
                    vert(ii) = cil(i, j)
                    ii = ii + 1
                Next
            Next

            Return vert

        End Function


        Public Function MapeiaFaces(ByVal NL As Integer, ByVal NR As Integer) As Integer(,)

        'Mapeia faces: face(nface,ncirculo)
            Dim temp(NL, NR) As Integer
            Dim ii As Integer = 1


            For i = 0 To (NL - 1)
                For j = 0 To (NR - 1)
                    temp(i, j) = ii
                    ii = ii + 1
                Next
            Next

            Return temp

        End Function


    End Module


End Namespace

