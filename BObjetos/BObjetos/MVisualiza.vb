﻿'Imports BMath.Matriz
'Imports BGeom.Ponto
'Imports BGeom.Vetor
'Imports BMath.Constantes


'Imports System.Windows.Forms

'Namespace Objetos

'    'Fundamentals of Computer Graphics - Third Edition
'    'Peter Shirley Steve Marschner
'    'Cap 7 - Pg 142

'    Public Module MVisualiza

'        'Matriz View Port
'        Public Function Mvp(ByVal Local As PictureBox) As Matriz4x4
'            Mvp = Matriz4x4.Identidade
'            Mvp.M11() = Local.Width * 0.5 'Nx/2
'            Mvp.M14() = (Local.Width - 1) * 0.5 ' (Nx - 1)/2
'            Mvp.M22() = Local.Height * 0.5 'Ny/2
'            Mvp.M24() = (Local.Height - 1) * 0.5 '(Ny - 1)/2

'            Return Mvp

'        End Function

'        'Matriz Orthographic Projection Transformation
'        Public Function Morth(ByVal Local As PictureBox) As Matriz4x4
'            Dim l, r, b, t, n, f As Double

'            l = Local.Left
'            r = Local.Right
'            b = Local.Bottom
'            t = Local.Top
'            n = ZERO
'            f = 1

'            Morth = Matriz4x4.Identidade
'            Morth.M11() = 2 / (r - l)
'            Morth.M14() = -((r + l) / (r - l))
'            Morth.M22() = 2 / (t - b)
'            Morth.M24() = -((t + b) / (t - b))
'            Morth.M33() = -2 / (f - n)
'            Morth.M34() = -((f + n) / (f - n))

'            Return Morth

'        End Function



'        'Matriz Perspectiva
'        Public Function Mper(ByVal Local As PictureBox, Optional ByVal Near As Double = ZERO, Optional ByVal Far As Double = 1) As Matriz4x4

'            Mper = Matriz4x4.Identidade
'            Mper.M11() = 1 / (Local.Width * 0.5)
'            Mper.M22() = 1 / (Local.Height * 0.5)
'            Mper.M33() = 1 / Far
'            Mper.M44() = 1


'            Return Mper

'        End Function


'        ' Matriz Camera
'        Public Function Mcam(ByVal Eye As Vetor3D, ByVal Focus As Vetor3D, ByVal ViewUP As Vetor3D) As Matriz4x4
'            Dim MFV As Matriz4x4
'            Dim U, V, W As Vetor3D

'            W = -(Focus / Vetor3D.Modulo(Focus))
'            U = Vetor3D.ProdutoVetorial(ViewUP, W) / Vetor3D.Modulo(Vetor3D.ProdutoVetorial(ViewUP, W))
'            V = Vetor3D.ProdutoVetorial(W, U)

'            MFV = Matriz4x4.Identidade
'            MFV.M11 = U.X : MFV.M12 = U.Y : MFV.M13 = U.Z : MFV.M14 = -Eye.X * U.X - Eye.Y * U.Y - Eye.Z * U.Z
'            MFV.M21 = V.X : MFV.M22 = V.Y : MFV.M23 = V.Z : MFV.M24 = -Eye.X * V.X - Eye.Y * V.Y - Eye.Z * V.Z
'            MFV.M31 = W.X : MFV.M32 = W.Y : MFV.M33 = W.Z : MFV.M34 = -Eye.X * W.X - Eye.Y * W.Y - Eye.Z * W.Z
'            MFV.M44 = 1.0

'            Return MFV

'        End Function

'        'construct M vp
'        'construct M orth
'        'M = M vp M orth
'        'for each line segment (a i , b i ) do
'        'p = Ma i
'        'q = Mb i
'        'drawline(x p , y p , x q , y q )

'        Public Function Tela(ByVal Pt As Ponto3D, ByVal Local As PictureBox, ByVal MT As Matriz4x4) As Ponto3D
'            Dim MP As New Matriz4x4
'            MP.M11() = Pt.x : MP.M22() = Pt.y : MP.M33() = Pt.z : MP.M14() = 1
'            MP = MT * MP
'            Return New Ponto3D(MP.M11(), MP.M22(), MP.M33())
'        End Function


'    End Module


'End Namespace