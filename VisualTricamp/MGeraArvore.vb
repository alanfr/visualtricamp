﻿Module MGeraArvore

    Public Function GeraArvore(ByRef Eletrodos() As Integer) As Boolean

        Try

            Principal.TreeView1.Nodes.Clear()
            Principal.TreeView1.TreeViewNodeSorter = New NodeSorter()
            'Principal.TreeView1.CheckBoxes = True

            ' No futuro adicionar os outros Nos (Eletrodo flutuante, perfis de medicao...)
            Dim objetos As TreeNode = Principal.TreeView1.Nodes.Add("Eletrodos")
            objetos.SelectedImageIndex() = 0

            Dim cil As Integer = 1
            Dim tor As Integer = 1
            Dim esf As Integer = 1
            Dim pla As Integer = 1

            For i = 0 To (Eletrodos.Length - 2)

                Select Case Eletrodos(i)
                    Case 1, 2, 6
                        objetos.Nodes.Add("Cilindro " & CStr(cil), "Cilindro " & CStr(cil), 0, 2)
                        cil += 1
                    Case 3
                        objetos.Nodes.Add("Placa " & CStr(pla), "Placa " & CStr(pla), 0, 6)
                        pla += 1
                    Case 4
                        objetos.Nodes.Add("Toróide " & CStr(tor), "Toróide " & CStr(tor), 0, 8)
                        tor += 1
                    Case 5
                        objetos.Nodes.Add("Esfera " & CStr(esf), "Esfera " & CStr(esf), 0, 4)
                        esf += 1
                    Case Else

                End Select
            Next

            Principal.TreeView1.BorderStyle = BorderStyle.FixedSingle
            Principal.TreeView1.BackColor = SystemColors.Window

        Catch ex As Exception
            MessageBox.Show("Erro ao gerar a árvore de objetos do arquivo Tricamp.")
            Return False
            Exit Function
        End Try

        Return True

    End Function

End Module
