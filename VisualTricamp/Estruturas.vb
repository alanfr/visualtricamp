﻿Imports BGeom.Ponto
Imports BObjetos.Objetos

Imports System.Xml

Public Module Estruturas

    Public Structure Trixl
        Dim Linha() As Integer
        Dim Px() As Double
        Dim Py() As Double
        Dim Pz() As Double
        Dim Ez() As Double
        Dim Perfil As String
        Public Sub Define(ByVal d1 As Integer)
            ReDim Linha(d1)
            ReDim Px(d1)
            ReDim Py(d1)
            ReDim Pz(d1)
            ReDim Ez(d1)
        End Sub
    End Structure


    Public Structure ObjetosDesenho
        Public Codigo() As Integer
        Public Potencial() As Integer
        Public LinhasCarga() As Integer
        Public TipoDist() As Integer
        Public P1() As Ponto3D
        Public P2() As Ponto3D
        Public RAB() As Ponto3D 'Para aproveitar a estrura dos dados

        Public NEletrodos As Integer
        Public NEeletrodos_Flutuantes As Integer
        Public NPerfis_Medicao As Integer
        Public setor4 As Integer
        Public setor5 As Integer
        '--------------------------------------------

        '-------- Objetos ---------------
        Public Cilindro() As Cilindro
        Public Esfera() As Esfera
        Public Toroide() As Toroide

        Public Sub Inicializa()
            ReDim Codigo(0)
            ReDim Potencial(0)
            ReDim LinhasCarga(0)
            ReDim TipoDist(0)
            ReDim P1(0)
            ReDim P2(0)
            ReDim RAB(0)

            ReDim Cilindro(0)
            ReDim Esfera(0)
            ReDim Toroide(0)

            NEletrodos = 0
            NEeletrodos_Flutuantes = 0
            NPerfis_Medicao = 0
            setor4 = 0
            setor5 = 0
        End Sub

    End Structure




    Public Function CriaConfiguracoes() As String

        Dim AbrirArquivo As New OpenFileDialog()
        Dim Path As String = ""

        AbrirArquivo.InitialDirectory = "" 'Abre o ultimo diretorio utilizado
        AbrirArquivo.Filter = "Tricamp.exe (*.exe)|*.exe|" +
                              "Todos os arquivos (*.*)|*.*"
        AbrirArquivo.FilterIndex = 1
        AbrirArquivo.RestoreDirectory = True

        If AbrirArquivo.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try

                Path = AbrirArquivo.FileName

                If Path = Nothing Then
                    Path = "C:\Tricamp\Tricamp.exe"
                End If

                'Checa se ja existe o arquivo
                'If IO.File.Exists("ConfVisualTricamp.xml") = False Then

                'declare our xmlwritersettings object
                Dim settings As New XmlWriterSettings()

                'lets tell to our xmlwritersettings that it must use indention for our xml
                settings.Indent = True

                'lets create the MyXML.xml document, the first parameter was the Path/filename of xml file
                ' the second parameter was our xml settings
                Dim XmlWrt As XmlWriter = XmlWriter.Create("ConfVisualTricamp.xml", settings)

                With XmlWrt

                    ' Write the Xml declaration.
                    .WriteStartDocument()

                    ' Write a comment.
                    .WriteComment("Configurações do Visual Tricamp")

                    ' Write the root element.
                    .WriteStartElement("Configuracoes")

                    ' The person nodes.
                    .WriteStartElement("LocalTricamp")
                    .WriteString(Path)
                    .WriteEndElement()

                    ' Close the XmlTextWriter.
                    .WriteEndDocument()
                    .Close()

                End With

                MessageBox.Show("Configurações salvas!")
                'End If


            Catch Ex As Exception
                MessageBox.Show("Não é possivel ler o arquivo. Original error: " & Ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If

        Return Path

    End Function


    Public Sub LeConfiguracoes(ByRef saida As String)

        Try
            'Cria uma instância de um documento XML
            Dim oXML As New XmlDocument

            'Define o caminho do arquivo XML 
            Dim ArquivoXML As String = "ConfVisualTricamp.xml"
            'carrega o arquivo XML
            oXML.Load(ArquivoXML)

            'Lê o filho de um No Pai específico 
            saida = oXML.SelectSingleNode("Configuracoes").ChildNodes(0).InnerText

        Catch Ex As Exception
            MessageBox.Show("Não é possivel executar o Tricamp. Original error: " & Ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub




End Module
