﻿Imports BGeom.Ponto

Imports BObjetos.Objetos

Module MGeraObjetos

    Public Function GeraObjetos(ByVal CodigoObjetos() As Integer,
                                ByVal P1() As Ponto3D,
                                ByVal P2() As Ponto3D,
                                ByVal P3() As Ponto3D,
                                ByVal RAB() As Ponto3D,
                                ByVal H() As Double,
                                ByRef Cilindro() As Cilindro,
                                ByRef Esfera() As Esfera,
                                ByRef Toroide() As Toroide,
                                ByRef Placa() As Placa,
                                Optional NL As Integer = 8,
                                Optional NR As Integer = 8) As Boolean

        'Garbage Collector - Remove objetos nao mais referenciados na memoria
        GC.Collect()
        Try

            Dim cil(), tor(), esf(), pla() As Integer
            Dim ncil, nesf, ntor, npla As Integer  'quantidade de cada objeto

            ncil = 0 : nesf = 0 : ntor = 0 : npla = 0

            '---------- Conta quantos eletrodos de cada tipo -------
            For i = 0 To (CodigoObjetos.Length - 2)
                Select Case CodigoObjetos(i)
                    Case 1, 2, 6
                        ncil += 1
                    Case 3
                        npla += 1
                    Case 4
                        ntor += 1
                    Case 5
                        nesf += 1
                    Case Else
                End Select
            Next
            '----------------------------------------------------



            '----------- Aloca objetos na memoria ---------
            If (ncil > 0) Then
                ReDim Cilindro(ncil)
                ReDim cil(ncil)
            Else
                ReDim Cilindro(0)
                ReDim cil(0)
                Cilindro(0) = New Cilindro(New Ponto3D(0, 0, 0), New Ponto3D(0, 0, 0), 1, 1, 1)
                Cilindro(0).ID = 9999
                cil(0) = 0
            End If

            If (ntor > 0) Then
                ReDim Toroide(ntor)
                ReDim tor(ntor)
            Else
                ReDim Toroide(0)
                ReDim tor(0)
                Toroide(0) = New Toroide(New Ponto3D(0, 0, 0), New Ponto3D(0, 0, 0), 0.01, 0.01, 1, 1)
                Toroide(0).ID = 9999
                tor(0) = 0
            End If

            If (nesf > 0) Then
                ReDim Esfera(nesf)
                ReDim esf(nesf)
            Else

                ReDim Esfera(0)
                ReDim esf(0)

                Esfera(0) = New Esfera(New Ponto3D(0, 0, 0), New Ponto3D(0, 0, 0), 0.01, 1, 1)
                Esfera(0).ID = 9999
                esf(0) = 0
            End If

            If (npla > 0) Then
                ReDim Placa(npla)
                ReDim pla(npla)
            Else
                ReDim Placa(0)
                ReDim pla(0)

                Placa(0) = New Placa(New Ponto3D(0, 0, 0), New Ponto3D(0, 0, 0), New Ponto3D(0, 0, 0), 1)
                Placa(0).ID = 9999
                pla(0) = 0
            End If
            '----------------------------------------



            ''---------- Associa a posicao do eletrodo ao objeto -------------
            Dim c1, c2, c3, c4 As Integer
            c1 = 0 : c2 = 0 : c3 = 0 : c4 = 0
            For i = 0 To (CodigoObjetos.Length - 2)

                If (CodigoObjetos(i) = 1) Or (CodigoObjetos(i) = 2) Or (CodigoObjetos(i) = 6) Then 'Cilindros
                    cil(c1) = i
                    c1 += 1
                End If
                If (CodigoObjetos(i) = 3) Then 'Placas
                    pla(c2) = i
                    c2 += 1
                End If
                If (CodigoObjetos(i) = 4) Then 'Toroide
                    tor(c3) = i
                    c3 += 1
                End If

                If (CodigoObjetos(i) = 5) Then
                    esf(c4) = i
                    c4 += 1
                End If


            Next

            '------------------------------------------------------------------


            'Gera cilindros
            If (ncil > 0) Then
                For i = 0 To (ncil - 1)
                    Cilindro(i) = New Cilindro(P1(cil(i)), P2(cil(i)), RAB(cil(i)).x, NL, NR)
                    Cilindro(i).GeraCasca3D()
                Next
            End If

            'Gera esferas
            If (nesf > 0) Then
                For i = 0 To (nesf - 1)
                    Esfera(i) = New Esfera(P1(esf(i)), P2(esf(i)), RAB(esf(i)).x, NL, NR)
                    Esfera(i).GeraCasca3D()
                Next
            End If


            'Gera Toroide
            If (ntor > 0) Then
                For i = 0 To (ntor - 1)
                    Toroide(i) = New Toroide(P1(tor(i)), P2(tor(i)), RAB(tor(i)).x, RAB(tor(i)).y, NL, NR)
                    Toroide(i).GeraCasca3D()
                Next
            End If

            'Gera Placa
            If (npla > 0) Then
                For i = 0 To (npla - 1)
                    Placa(i) = New Placa(P1(pla(i)), P2(pla(i)), P3(pla(i)), H(pla(i)))
                    Placa(i).GeraCasca3D()
                Next
            End If

        Catch ex As Exception
            MessageBox.Show("Não foi possivel gerar os objetos: " & ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
            Exit Function
        End Try

        Return True


    End Function


End Module
