﻿Option Strict On
Imports System.Windows.Forms.DataVisualization.Charting 'Para tratar Chart


Public Class PlotaChart

    Implements IDisposable

    Private _Local As Chart
    Private _X() As Double
    Private _Y() As Double
    Private _SobrePoe As Boolean
    Private _MostraX As Boolean
    Private _MostraY As Boolean
    Private _MesmaCor As Boolean
    Private _Teste As Boolean
    Private _Serie As String
    Private _Titulo As String
    Private _EixoX As String
    Private _EixoY As String

    ' Keep track of when the object is disposed.
    ' https://msdn.microsoft.com/en-us/library/2z08e49e%28v=vs.90%29.aspx
    Protected disposed As Boolean = False



#Region "Construtor e Destrutor"


    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
            End If
        End If
        Me.disposed = True
    End Sub


    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub


    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub



    Public Sub New()
        With Me
            .SobreporGraficos = True
            .MostraX = True
            .MostraY = True
            .MesmaCor = False
            .Teste = True
            .Serie = "Aleatoria"
            .Titulo = ""
            .EixoX = "Eixo X"
            .EixoY = "Eixo Y"
        End With
    End Sub

    Sub New(ByVal chart As Chart)

        SobreporGraficos = True
        MostraX = True
        MostraY = True
        MesmaCor = False
        Teste = True
        Serie = "Aleatoria"
        Titulo = ""
        EixoX = "Eixo X"
        EixoY = "Eixo Y"

        Local = chart

    End Sub


    Sub New(ByVal chart As Chart,
            ByVal X() As Double,
            ByVal Y() As Double)

        SobreporGraficos = True
        MostraX = True
        MostraY = True
        MesmaCor = False
        Teste = True
        Serie = "Aleatoria"
        Titulo = ""
        EixoX = "Eixo X"
        EixoY = "Eixo Y"

        Me.Local = chart
        Me.X = X
        Me.Y = Y

    End Sub




#End Region


#Region "Propriedades"

    '############### Get-Set ###############

    Public Property Local() As Chart
        Get
            Return _Local
        End Get
        Set(ByVal value As Chart)
            _Local = value
        End Set
    End Property


    Public Property X As Double()
        Get
            Return _X
        End Get
        Set(value As Double())
            _X = value
        End Set
    End Property


    Public Property Y As Double()
        Get
            Return _Y
        End Get
        Set(value As Double())
            _Y = value
        End Set
    End Property


    Public Property Serie As String
        Get
            Return _Serie
        End Get
        Set(value As String)
            _Serie = value
        End Set
    End Property


    Public Property Titulo As String
        Get
            Return _Titulo
        End Get
        Set(value As String)
            _Titulo = value
        End Set
    End Property


    Public Property EixoX As String
        Get
            Return _EixoX
        End Get
        Set(value As String)
            _EixoX = value
        End Set
    End Property


    Public Property EixoY As String
        Get
            Return _EixoY
        End Get
        Set(value As String)
            _EixoY = value
        End Set
    End Property


    Public Property SobreporGraficos As Boolean
        Get
            Return _SobrePoe
        End Get
        Set(value As Boolean)
            _SobrePoe = value
        End Set
    End Property


    Public Property MostraX As Boolean
        Get
            Return _MostraX
        End Get
        Set(value As Boolean)
            _MostraX = value
        End Set
    End Property


    Public Property MostraY As Boolean
        Get
            Return _MostraY
        End Get
        Set(value As Boolean)
            _MostraY = value
        End Set
    End Property


    Public Property MesmaCor As Boolean
        Get
            Return _MesmaCor
        End Get
        Set(value As Boolean)
            _MesmaCor = value
        End Set
    End Property


    Public Property Teste As Boolean
        Get
            Return _Teste
        End Get
        Set(value As Boolean)
            _Teste = value
        End Set
    End Property



#End Region



    Public Sub Plot()

        If (Teste = True) Then

            Dim i As Integer = 0

            With Me

                ' Verifica e habilita ou nao a sobreposicao dos gráficos
                If .SobreporGraficos = True Then
                    .Serie = _Serie ' + CStr(i)
                    .Local.Titles.Clear()
                    .Local.Series.Add(_Serie)
                Else
                    .Local.Series.Clear()
                    .Local.Titles.Clear()
                    .Local.Series.Add(_Serie)
                    .Local.Series(_Serie).Points.Clear() 'Clear all points
                End If


                ' Mostra ou nao o grid
                .Local.ChartAreas(0).AxisX.MajorGrid.Enabled = .MostraX
                .Local.ChartAreas(0).AxisY.MajorGrid.Enabled = .MostraY

                ' Define os titulos
                .Local.Titles.Add(.Titulo)
                .Local.ChartAreas(0).AxisX.Title = EixoX
                .Local.ChartAreas(0).AxisX.TitleFont = New Font("Verdana", 14)
                .Local.ChartAreas(0).AxisX.LabelAutoFitMinFontSize = 14

                .Local.ChartAreas(0).AxisY.Title = EixoY
                .Local.ChartAreas(0).AxisY.TitleFont = New Font("Verdana", 14)
                .Local.ChartAreas(0).AxisY.LabelAutoFitMinFontSize = 14

                .Local.Series(_Serie).BorderWidth = 3

                If (MesmaCor = True) Then

                    Dim dado As New Random 'Gera numeros aleatorios
                    Dim Cor As Drawing.Color

                    ' A cor sera uma combinação aleatoria
                    Cor = Drawing.Color.FromArgb(dado.Next(0, 256), dado.Next(0, 256), dado.Next(0, 256))

                    .Local.Series(_Serie).Color = Cor
                End If



                'Inicializa e plota o gráfico
                .Local.Series(_Serie).ChartType = SeriesChartType.Line 'DataVisualization.Charting.SeriesChartType.Line
                For k = 0 To CInt(.X.Length - 1)
                    .Local.Series(_Serie).Points.AddXY(.X(k), .Y(k)) 'Adds the data from the array to the first series
                Next

                'Retorna os Valores de (X,Y)
                _Local.Series(_Serie).ToolTip = "X: #VALX{F1}\nY: #VALY{F1}"
            End With


        ElseIf (Teste = False) Then
            Exit Sub


        End If


    End Sub




End Class
