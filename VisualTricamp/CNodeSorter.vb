﻿Public Class NodeSorter
    Implements IComparer

    ' Compare the length of the strings, or the strings
    ' themselves, if they are the same length.
    Public Function Compare(ByVal x As Object, ByVal y As Object) _
        As Integer Implements IComparer.Compare
        Dim tx As TreeNode = CType(x, TreeNode)
        Dim ty As TreeNode = CType(y, TreeNode)

        If tx.Text.Length <> ty.Text.Length Then
            Return tx.Text.Length - ty.Text.Length
        End If
        Return String.Compare(tx.Text, ty.Text)

    End Function
End Class
