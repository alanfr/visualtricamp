﻿Imports BMath.Matriz
Imports BMath.Constantes

Imports BGeom.Vetor
Imports BGeom.Ponto
Imports BGeom.Transformacoes

Imports BObjetos.Objetos

Imports System.Drawing

Module MDesenha

    'Desenha os objetos
    Public Function Desenha(ByVal Local As PictureBox,
                            ByVal Cilindro As Cilindro(),
                            ByVal Esfera As Esfera(),
                            ByVal Toroide As Toroide(),
                            ByVal Placa As Placa(),
                            ByVal Zoom As Double,
                            ByVal angx As Integer,
                            ByVal angy As Integer) As Boolean

        'Garbage Collector - Remove objetos nao mais referenciados na memoria
        GC.Collect()


        Try
            '----- Habilita Menus ---------
            Local.Enabled = True

            Camera.Fx.Enabled = True
            Camera.Fy.Enabled = True
            Camera.Fz.Enabled = True

            Camera.Ex.Enabled = True
            Camera.Ey.Enabled = True
            Camera.Ez.Enabled = True

            Principal.Button1.Enabled = True
            '-----------------------------
            Dim MT, MROT, MCAM As New Matriz4x4
            Dim Eixos As New Eixos(10)
            Dim Plano As New Plano(New Ponto3D(-250, -250, 0), 10, 250, 10, 250)
            Dim _GL As Graphics
            Dim Cam1 As New BObjetos.Camera.Camera3D


            _GL = Local.CreateGraphics

            ' Limpa a janela
            _GL.Clear(SystemColors.Window) '

            'Vetores iniciais da camera
            Dim Eye As New Vetor3D(10, 10, 10)  'Z = 0 >> Frontal
            Dim Centro As New Vetor3D(0, 0, 0) 'Localizacao da camera
            Dim Up As New Vetor3D(ZERO, ZERO, 1)


            'Matriz Transformacao
            'Pela regra do produto vetorial positivo (mão direita)
            'Entrando na tela = Camera(Eye, Centro, Up)
            'Saindo da tela = Camera(Centro, Eye, -Up)
            If Principal.CheckBox2.Checked = True Then
                Eye = AplicaTransformacao3D(Eye, MatrizTranslacao3D(angx, angy, 0))
                Centro = AplicaTransformacao3D(Centro, MatrizTranslacao3D(angx, angy, 0))
                MROT = MatrizRotacionaZ(0)
            Else
                MROT = MatrizRotacionaZ(angx)
            End If

            'Parâmetros da câmera
            Cam1.Eye = Centro
            Cam1.Center = Eye
            Cam1.Up = -Up
            Cam1.Projecao = 0
            Cam1.Yaw = angx
            Cam1.Pitch = angy

            'Matriz Camera
            MCAM = MROT * Cam1.Camera
            MT = MatrizEscala3D(Zoom, Zoom, Zoom, Centro) * MCAM ' 0 = Projecao

            If Principal.CheckBox1.Checked = True Then
                Eixos.Desenha(Local, MT)
            End If

            If Principal.CheckBox3.Checked = True Then
                Plano.Desenha(Local, MT)
            End If

            Eixos.DesenhaCantoTela(Local, MCAM)


            '----------------- Desenha objetos ---------------------
            'No paralelo deve ser Length - 1 ao inves de Length - 2
            If ((Cilindro.Length - 1) > 0) Then
                Parallel.For(0, Cilindro.Length - 1,
                             Sub(i)
                                 Cilindro(i).Desenha(Local, MT)
                                 'Cilindro(i).MostraXYZ = Principal.CheckBox1.Checked
                             End Sub)
            End If


            If ((Esfera.Length - 1) > 0) Then
                Parallel.For(0, Esfera.Length - 1,
                             Sub(i)
                                 Esfera(i).Desenha(Local, MT)
                                 'Esfera(i).MostraXYZ = Principal.CheckBox1.Checked
                             End Sub)
            End If

            If ((Toroide.Length - 1) > 0) Then
                Parallel.For(0, Toroide.Length - 1,
                             Sub(i)
                                 Toroide(i).Desenha(Local, MT)
                                 'Toroide(i).MostraXYZ = Principal.CheckBox1.Checked
                             End Sub)
            End If

            If ((Placa.Length - 1) > 0) Then
                Parallel.For(0, Placa.Length - 1,
                            Sub(i)
                                    Placa(i).Desenha(Local, MT)
                                    'Toroide(i).MostraXYZ = Principal.CheckBox1.Checked
                                End Sub)
                End If
                '-----------------------------------------------------


        Catch ex As Exception
            MessageBox.Show("Não foi possivel desenhar os objetos: " & ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
            Exit Function
        End Try

        Return True

    End Function

End Module




