﻿Imports System.IO
Imports System.Windows.Forms

Module MLeTrixlOut

    Public Function LeTrixlOut(ByRef saida As Trixl()) As Boolean

        Dim PL() As Trixl
        Dim cperfil As Integer = 0
        Dim clinha As Integer = 0

        Dim TextoStream As Stream = Nothing
        Dim AbrirArquivo As New OpenFileDialog()
        Dim file_num As Integer = FreeFile()
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim kin As Integer = 0



        AbrirArquivo.InitialDirectory = "" 'Abre o ultimo diretorio utilizado
        AbrirArquivo.Filter = "Trixl (trixl.out)|trixl.out|" +
                              "Saida (*.out)|*.out|" +
                              "Perfis (*.perf)|*.perf|" +
                              "Todos os arquivos (*.*)|*.*"
        AbrirArquivo.FilterIndex = 0
        AbrirArquivo.RestoreDirectory = True


        If AbrirArquivo.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                TextoStream = AbrirArquivo.OpenFile()
                If (TextoStream IsNot Nothing) Then
                    FileOpen(file_num, AbrirArquivo.FileName, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

                    Dim input() As String = File.ReadAllLines(AbrirArquivo.FileName)

                    'Le o arquivo e checa quantidade de perfis
                    cperfil = 0
                    For Each m In input
                        'trixl.out
                        If (Mid(m, 3, 17).Trim = "INICIO DO PERFIL-") Then
                            cperfil += 1 'Conta perfis
                        End If
                    Next

                    'Define a quantidade de perfis
                    ReDim PL(cperfil - 1)

                    cperfil = 0
                    clinha = 0
                    'Dimensiona arrays para o numero de linhas que cada perfil possui
                    'Forma burra funcional emergencial
                    For Each m In input
                        If (Mid(m, 3, 17).Trim = "INICIO DO PERFIL-") Then
                            cperfil += 1
                            clinha = 0
                        Else
                            clinha += 1
                            PL(cperfil - 1).Define(clinha - 1)
                        End If
                    Next

                    cperfil = 0
                    clinha = 0
                    i = 1
                    For Each m In input
                        If (Mid(m, 3, 17).Trim = "INICIO DO PERFIL-") Then
                            cperfil += 1
                            clinha = 0
                            PL(cperfil - 1).Perfil = Mid(m, 13, 40).Trim()
                        Else
                            PL(cperfil - 1).Linha(clinha) = i
                            PL(cperfil - 1).Px(clinha) = Val(Mid(m, 1, 12).Trim)
                            PL(cperfil - 1).Py(clinha) = Val(Mid(m, 16, 12).Trim)
                            PL(cperfil - 1).Pz(clinha) = Val(Mid(m, 31, 12).Trim)
                            PL(cperfil - 1).Ez(clinha) = Val(Mid(m, 46, 12).Trim)
                            clinha += 1
                        End If
                        i += 1
                    Next

                    saida = PL

                    ' Close the file.
                    FileClose(file_num)
                End If
            Catch Ex As Exception
                MessageBox.Show("Não é possivel ler o arquivo. Original error: " & Ex.Message)
                Return False
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
                If (TextoStream IsNot Nothing) Then
                    TextoStream.Close()
                End If
            End Try
        Else
            FileClose(file_num)
            Return False 'Houve algum erro
        End If

        Return True


    End Function


End Module
