﻿
Imports System.IO
Imports System.Windows.Forms
Imports System.Text.RegularExpressions

Module MLeTricampOut

    'Retorna True se tudo Ok. Caso contrario, False.
    Public Function TricampOut(ByRef Px As Double(), ByRef Py As Double(), ByRef Pz As Double(), _
                               ByRef PotkV As Double(), ByRef Pang As Double(), _
                               ByRef Epx As Double(), ByRef Epy As Double(), ByRef Epz As Double(), _
                               ByRef Eangx As Double(), Eangy As Double(), Eangz As Double(), _
                               ByRef NomeArq As String) As Boolean


        Dim TextoStream As Stream = Nothing
        Dim AbrirArquivo As New OpenFileDialog()
        Dim inicio As Integer = 0
        Dim zeros As Integer = 0
        Dim file_num As Integer = FreeFile()
        Dim i As Integer = 0
        Dim k_input As Integer = 0

        AbrirArquivo.InitialDirectory = "" 'Abre o ultimo diretorio utilizado
        AbrirArquivo.Filter = "Saída Tricamp (*.out)|*.out|" +
                              "Texto (*.txt)|*.txt|" +
                              "Todos os arquivos (*.*)|*.*"
        AbrirArquivo.FilterIndex = 1
        AbrirArquivo.RestoreDirectory = True


        If AbrirArquivo.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                TextoStream = AbrirArquivo.OpenFile()
                If (TextoStream IsNot Nothing) Then
                    FileOpen(file_num, AbrirArquivo.FileName, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

                    NomeArq = AbrirArquivo.FileName
                    'Expressao regular para identificar o padrao de dados do eletrodo
                    Dim padrao_saida As String = "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                 "([+-]?\d+\.\d+|[+-]?\.\d+)"

                    Dim padrao_saida2 As String = "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                             "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                             "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                             "([+-]?\d+\.\d+|[+-]?\.\d+)"




                    Dim input() As String = File.ReadAllLines(AbrirArquivo.FileName)
                    Dim matches As MatchCollection


                    'Le o arquivo e checa quantidades
                    For Each m In input

                        'Tricamp.out
                        If (Mid(m, 14, 105).Trim = "(M)       (M)       (M)       (KV)    (GRAUS)    (KV/M)   (GRAUS)    (KV/M)   (GRAUS)    (KV/M)   (GRAUS)") Then
                            inicio = 1
                        End If

                        If (inicio = 1) Then
                            i += 1
                        End If

                    Next

                    If (inicio = 0) Then
                        MessageBox.Show("Arquivo não reconhecido como válido.", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    Else

                        ReDim Px(CInt(i / 2) - 1)
                        ReDim Py(CInt(i / 2) - 1)
                        ReDim Pz(CInt(i / 2) - 1)
                        ReDim PotkV(CInt(i / 2) - 1)
                        ReDim Pang(CInt(i / 2) - 1)

                        ReDim Epx(CInt(i / 2) - 1)
                        ReDim Epy(CInt(i / 2) - 1)
                        ReDim Epz(CInt(i / 2) - 1)
                        ReDim Eangx(CInt(i / 2) - 1)
                        ReDim Eangy(CInt(i / 2) - 1)
                        ReDim Eangz(CInt(i / 2) - 1)

                        inicio = 0
                        zeros = 0
                        i = 0

                        For Each m In input 'Para cada linha no arquivo de entrada
                            If (Mid(m, 11, 37).Trim = "CALCULO DE POTENCIAL E CAMPO ELETRICO") Then
                                inicio = 1
                            End If
                            'Inicia a atribuicao de parametros
                            If (inicio = 1) Then ' Se estiver iniciado e na primeira categoria
                                If (Mid(m, 12, 8).Trim <> Nothing) Then ' Se nao for vazio
                                    matches = Regex.Matches(m, padrao_saida, RegexOptions.Compiled) ' Aplica o filtro na linha - Regex compilado ao inves de interpretado
                                    For Each m2 As Match In matches
                                        Px(i) = Val(m2.Groups(1).Value)
                                        Py(i) = Val(m2.Groups(2).Value)
                                        Pz(i) = Val(m2.Groups(3).Value)
                                        PotkV(i) = Val(m2.Groups(4).Value)
                                        Pang(i) = Val(m2.Groups(5).Value)

                                        Epx(i) = Val(m2.Groups(6).Value)
                                        Epy(i) = Val(m2.Groups(8).Value)
                                        Epz(i) = Val(m2.Groups(10).Value)

                                        Eangx(i) = Val(m2.Groups(7).Value)
                                        Eangy(i) = Val(m2.Groups(9).Value)
                                        Eangz(i) = Val(m2.Groups(11).Value)


                                        i += 1 'incrementa o numero de linhas
                                    Next
                                End If
                            End If
                            k_input += 1
                        Next

                        '    eletrodos = eletrodo
                        '    flutuantes = eletrodo_flutuante
                        '    perfilmed = perfil_med
                    End If

                    ' Close the file.
                    FileClose(file_num)
                End If
            Catch Ex As Exception
                MessageBox.Show("Não é possivel ler o arquivo." & vbNewLine & Ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
                If (TextoStream IsNot Nothing) Then
                    TextoStream.Close()
                End If
            End Try
        Else
            FileClose(file_num)
            Return False 'Houve algum erro
        End If



        Return True 'Tudo Ok

    End Function

End Module
