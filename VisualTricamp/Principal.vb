﻿Imports BGeom.Ponto
Imports BGeom.Vetor
Imports BGeom.Transformacoes

Imports BObjetos.Objetos
Imports BMath.Matriz
Imports BMath.Constantes

Imports System.Drawing
Imports System.Windows.Forms
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Text.RegularExpressions
Imports System.Drawing.Imaging



Public Class Principal

#Region "Variaveis do programa"

    '------- Leitura do arquivo Tricamp.dat ----------
    ' Requsito para: DadosTricamp(), GeraArvore()
    Private _Codigo() As Integer
    Private _Potencial() As Integer
    Private _LinhasCarga() As Integer
    Private _TipoDist() As Integer
    Private _P1() As Ponto3D
    Private _P2() As Ponto3D
    Private _P3() As Ponto3D
    Private _RAB() As Ponto3D 'Para aproveitar a estrura dos dados
    Private _H() As Double  'Para as placas
    Private _Temp As String


    Protected Friend _NEletrodos As Integer = 0
    Protected Friend _NEeletrodos_Flutuantes As Integer = 0
    Protected Friend _NPerfis_Medicao As Integer = 0
    Protected Friend _arquivo As String = " "
    '--------------------------------------------

    '------- Leitura do arquivo tricamp.out ----------
    Protected _Px() As Double
    Protected _Py() As Double
    Protected _Pz() As Double
    Protected _PotkV() As Double
    Protected _Pang() As Double

    Protected _Epx() As Double
    Protected _Epy() As Double
    Protected _Epz() As Double

    Protected _Eangx() As Double
    Protected _Eangy() As Double
    Protected _Eangz() As Double

    Private _NomeArq As String

    '--------------------------------------------


    '------- Leitura do arquivo trixl.out ----------
    Private _TrixlOut() As Trixl
    Private _Perfil() As String
    Private _TesteTrixl As Boolean

    '--------------------------------------------


    '-------- Objetos ---------------
    Private _Cilindro() As Cilindro
    Private _Esfera() As Esfera
    Private _Toroide() As Toroide
    Private _Placa() As Placa
    '------------------------------


    '------- Mouse ---------
    Private flag As Boolean = False
    Private dx, dy As Integer
    Private mousepoint As Point
    Private shift As Integer

    Private IsDragging As Boolean = False
    Private IsClick As Boolean = False
    Private StartPoint, FirstPoint, LastPoint As Point

    Private Zoom As Double = 50
    Private angx As Integer = 10
    Private angy As Integer = 10
    '---------------------------------

    Protected Event Atualiza(ByVal i As Integer)


#End Region



    'Constroi objetos
    Public Sub Constroi(ByRef flag As Boolean)
        GC.Collect()
        flag = GeraObjetos(_Codigo,
                           _P1,
                           _P2,
                           _P3,
                           _RAB,
                           _H,
                           _Cilindro,
                           _Esfera,
                           _Toroide,
                           _Placa,
                           CInt(TextBox1.Text),
                           CInt(TextBox2.Text))

        If (flag = True) Then
            flag = Desenha(PictureBox1,
                           _Cilindro,
                           _Esfera,
                           _Toroide,
                           _Placa,
                           Zoom, angx, angy)
        End If

    End Sub

    Public Sub Redesenha()
        GC.Collect()
        Desenha(PictureBox1, _Cilindro, _Esfera, _Toroide, _Placa, Zoom, angx, angy)
    End Sub


    Public Sub Limpa()
        GC.Collect()
        Chart1.Series.Clear()
        Chart1.Titles.Clear()
        TreeView1.BorderStyle = BorderStyle.None
        TreeView1.BackColor = SystemColors.Control
        PictureBox1.Enabled = False
        Chart1.Enabled = False
        Camera.Fx.Enabled = False
        Camera.Fy.Enabled = False
        Camera.Fz.Enabled = False

        Camera.Ex.Enabled = False
        Camera.Ey.Enabled = False
        Camera.Ez.Enabled = False

        Button1.Enabled = False
    End Sub


    Public Function LeTrixl() As Boolean
        GC.Collect()
        Dim teste As Boolean
        'teste = TricampOut(_Px, _Py, _Pz, _PotkV, _Pang, _
        '             _Epx, _Epy, _Epz, _Eangx, _Eangy, _Eangz, _NomeArq)

        teste = LeTrixlOut(_TrixlOut) 'Redim saida

        If (teste = True) Then
            ReDim _Perfil(_TrixlOut.Length - 1)

            For i = 0 To (_Perfil.Length - 1)
                _Perfil(i) = _TrixlOut(i).Perfil
            Next

            ComboBox1.DataSource = _Perfil
        End If

        Return teste

    End Function


    Public Function PlotaGrafico(ByVal teste As Boolean, ByVal perfil As String) As Boolean
        GC.Collect()
        If teste = True Then
            Dim Grafico As New PlotaChart(Chart1)

            Grafico.EixoX = "Distância [m]"
            Grafico.EixoY = "Campo Elétrico [kV/m]"

            Select Case perfil
                Case "XEz"
                    Grafico.X = _TrixlOut(ComboBox1.SelectedIndex).Px
                    Grafico.Y = _TrixlOut(ComboBox1.SelectedIndex).Ez
                Case "YEz"
                    Grafico.X = _TrixlOut(ComboBox1.SelectedIndex).Py
                    Grafico.Y = _TrixlOut(ComboBox1.SelectedIndex).Ez
                Case "ZEz"
                    Grafico.X = _TrixlOut(ComboBox1.SelectedIndex).Pz
                    Grafico.Y = _TrixlOut(ComboBox1.SelectedIndex).Ez
            End Select

            Grafico.SobreporGraficos = CheckBox4.Checked
            Grafico.Serie = _TrixlOut(ComboBox1.SelectedIndex).Perfil
            Grafico.Plot()

            Label1.Text = "Potencial máximo: " & Grafico.Y.Max.ToString
        End If

        Return teste

    End Function




    'Opcao Abrir do menu
    Private Sub AbrirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirToolStripMenuItem.Click

        'Le o arquivo tricamp e retorna os dados
        flag = DadosTricamp(_Codigo,
                            _Potencial,
                            _LinhasCarga,
                            _TipoDist,
                            _P1,
                            _P2,
                            _P3,
                            _RAB,
                            _H,
                            _NEletrodos,
                            _NEeletrodos_Flutuantes,
                            _NPerfis_Medicao, _arquivo)

        Me.Text = "Visual Tricamp - " & _arquivo
        If (flag = True) Then
            StatusCarrega.ShowDialog()
            flag = GeraArvore(_Codigo)
        End If

        If (flag = True) Then
            Constroi(flag)
        End If

    End Sub


    'Opcao Sair do menu
    Private Sub SairToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SairToolStripMenuItem.Click
        Me.Close()
    End Sub


    'Eventos ao carregar a janela
    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Limpa()
    End Sub



    'TODO checar o motivo do zoom funcionar somente apos acionar o botao
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Constroi(True)
        Zoom = 50
    End Sub



    Private Sub ConfiguraçõesDeExibiçãoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConfiguraçõesDeExibiçãoToolStripMenuItem.Click
        Opcoes.ShowDialog()
    End Sub



    Private Sub LimparToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LimparToolStripMenuItem.Click
        GC.Collect()
        TreeView1.Nodes.Clear()
        PictureBox1.Image = Nothing
        PictureBox1.Enabled = False
    End Sub


    Private Sub CâmeraToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CâmeraToolStripMenuItem.Click
        Camera.Show()
    End Sub


#Region "TreeView"


    Private Sub TreeView1_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        If e.Node.Text.Contains("Toróide") Then
            'Label1.Text = TreeView1.SelectedNode.FullPath.ToString
            'Label1.Text = "Toróide index = " + CStr(e.Node.Index)
        Else

            'Label1.Text = CStr(e.Node.GetNodeCount(True)) 'TreeView1.SelectedNode.FullPath.ToString
        End If

    End Sub

    Sub treeView1_NodeMouseClick(ByVal sender As Object, ByVal e As TreeNodeMouseClickEventArgs) Handles TreeView1.NodeMouseDoubleClick

        Dim padrao As String = "\d+"
        Dim matches As MatchCollection
        matches = Regex.Matches(e.Node.Text, padrao) ', RegexOptions.Compiled)
        'match = Regex.Matches(e.Node.Text, padrao) ', RegexOptions.Compiled)


        If (e.Button = MouseButtons.Left) Then
            'ContextMenuStrip1.Show()
            EditaNode.Text = "Visual Tricamp - Opções " & e.Node.Text
            EditaNode.ShowDialog()
            EditaNode.TextBox1.Text = _Cilindro(0).NumDivCirc.ToString
            '_Cilindro(Convert.ToInt16(matches(0).Value)).NumDivCirc = CInt(EditaNode.TextBox1.Text)
            Constroi(True)
            'Label2.Text = "Botão direito " + e.Node.Text
        End If


    End Sub 'treeView1_NodeMouseClick


#End Region






#Region "Eventos do mouse"

    Private Sub PictureBox1_MouseWheel(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseWheel

        If e.Delta > 0 Then
            If Zoom > 10 Then 'Se zoom maior que 10
                Zoom += 5     ' faz o incremento de 5
                If Zoom > 1000 Then 'Se zoom maior que 1000, mantem
                    Zoom = 1000
                End If
            Else
                Zoom += 0.5 'Se zoom menor que 10, incrementa de 0.5
            End If
        ElseIf e.Delta < 0 Then
            If Zoom <= 10 Then
                Zoom -= 0.5
                If Zoom < 1 Then
                    Zoom = 1
                End If
            Else
                Zoom -= 5
            End If
        End If
        Redesenha()

    End Sub

    Private Sub PictureBox1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseDown
        If e.Button = MouseButtons.Left Then
            StartPoint = PictureBox1.PointToScreen(New Point(e.X, e.Y))
            FirstPoint = StartPoint
            IsDragging = True
        End If
    End Sub

    Private Sub PictureBox1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseMove
        If IsDragging Then
            Dim EndPoint As Point = PictureBox1.PointToScreen(New Point(e.X, e.Y))
            IsClick = False
            angx += (EndPoint.X - StartPoint.X)
            angy += (EndPoint.Y - StartPoint.Y)
            StartPoint = EndPoint
            LastPoint = EndPoint
            Redesenha()
        End If
    End Sub

    Private Sub PictureBox1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox1.MouseUp
        IsDragging = False
        If LastPoint = StartPoint Then IsClick = True Else IsClick = False
    End Sub


    ' Dim toolTip1 As New ToolTip()

    Sub OnPictureMouseHover(ByVal sender As Object, ByVal e As MouseEventArgs) Handles PictureBox1.MouseClick
        toolTip1.Show("(" & e.X & ", " & e.Y & ")", PictureBox1)
        Redesenha()
    End Sub

    'Sub OnPictureMouseLeave(ByVal sender As Object, ByVal e As EventArgs) Handles PictureBox1.MouseLeave
    '    tt.Hide()
    'End Sub






#End Region



    'Abrir trixl.out ou *.perf
    Private Sub AbriroutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbriroutToolStripMenuItem.Click
        _TesteTrixl = LeTrixl()
        PlotaGrafico(_TesteTrixl, "XEz")
    End Sub


    Private Sub ExecutarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExecutarToolStripMenuItem.Click
        Dim Path As String = Nothing
        LeConfiguracoes(Path)
        Try
            Process.Start("cmd", String.Format("/k {0} & {1} & {2}", Path, "pause", "exit"))
            'Process.Start("C:\Tricamp\Tricamp.exe")
        Catch ex As Exception
            MessageBox.Show("Não é possivel executar o Tricamp." & vbNewLine & "Tricamp.exe não encontrado em c:/Tricamp" & vbNewLine & ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        If RadioButton1.Checked = True Then
            PlotaGrafico(_TesteTrixl, "XEz")
        ElseIf RadioButton2.Checked = True Then
            PlotaGrafico(_TesteTrixl, "YEz")
        ElseIf RadioButton3.Checked = True Then
            PlotaGrafico(_TesteTrixl, "ZEz")
        End If
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Limpa()
    End Sub



    Private Sub ConfiguraçõesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConfiguraçõesToolStripMenuItem.Click
        CriaConfiguracoes()
    End Sub



    Private Function GetChartImage(ByVal chartObj As Chart, _
        ByVal dpiX As Integer, ByVal dpiY As Integer) As Bitmap

        ' Bitmap where chart will be printed
        Dim image As Bitmap
        ' The width of the bitmap.
        Dim width As Integer = 0
        ' The height of the bitmap.
        Dim height As Integer = 0

        ' Obtain chart's graphics to determine screen DPI.
        Using chartGraphics As Graphics = chartObj.CreateGraphics()
            ' Calculate bitmap witdh as chart width multiplied by ratio
            ' of desired DPI divided by screen DPI.
            width = CInt((chartObj.Width * dpiX / chartGraphics.DpiX))
            ' Calculate bitmap height.
            height = CInt((chartObj.Height * dpiX / chartGraphics.DpiY))

            ' Create new bitmap.
            image = New Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb)

            ' Set bitmap DPI to user specified values.
            image.SetResolution(dpiX, dpiY)

            ' Create graphics object from bitmap.
            Using imageGraphics As Graphics = Graphics.FromImage(image)
                ' Print chart to the bitmap object.
                chartObj.Printing.PrintPaint(imageGraphics, _
                        New Rectangle(0, 0, image.Width, image.Height))
            End Using
        End Using

        ' Return chart image.
        GetChartImage = image
    End Function


    'Salvar grafico
    Private Sub SalvarGráficoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalvarGráficoToolStripMenuItem.Click

        Dim salvar As New SaveFileDialog()
        'Dim bm As Bitmap = GetChartImage(Chart1, 2400, 2400)
        'bm.SetResolution(300, 300)

        'Clonando o chart1
        Dim myStream As System.IO.MemoryStream = New System.IO.MemoryStream()
        Dim g As New Chart
        Chart1.Serializer.Save(myStream)
        g.Serializer.Load(myStream)

        salvar.Filter = "PNG (*.png)|*.png"
        If salvar.ShowDialog = Windows.Forms.DialogResult.OK _
        Then
            'bm.Save(salvar.FileName, ImageFormat.Png)
            g.AntiAliasing = AntiAliasingStyles.All
            g.TextAntiAliasingQuality = TextAntiAliasingQuality.High
            g.Height = 1080
            g.Width = 1920
            g.SaveImage(salvar.FileName, ChartImageFormat.Png)
            g.Dispose()
        End If
        GC.Collect()
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            PlotaGrafico(_TesteTrixl, "XEz")
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            PlotaGrafico(_TesteTrixl, "YEz")
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            PlotaGrafico(_TesteTrixl, "ZEz")
        End If
    End Sub

End Class
