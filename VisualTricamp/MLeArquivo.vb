﻿
Imports System.IO
Imports System.Windows.Forms
Imports System.Text.RegularExpressions

Imports BGeom.Ponto

Module LeTricamp

    'Retorna True se tudo Ok. Caso contrario, False.
    Public Function DadosTricamp(ByRef Codigo As Integer(),
                                 ByRef Potencial As Integer(),
                                 ByRef LinhasCarga As Integer(),
                                 ByRef TipoDist As Integer(),
                                 ByRef P1 As Ponto3D(),
                                 ByRef P2 As Ponto3D(),
                                 ByRef P3 As Ponto3D(),
                                 ByRef RAB As Ponto3D(),
                                 ByRef H As Double(),
                                 ByRef eletrodos As Integer,
                                 ByRef flutuantes As Integer,
                                 ByRef perfilmed As Integer,
                                 ByRef NomeArq As String) As Boolean


        Dim TextoStream As Stream = Nothing
        Dim AbrirArquivo As New OpenFileDialog()
        Dim inicio As Integer = 0
        Dim zeros As Integer = 0
        Dim file_num As Integer = FreeFile()
        Dim i As Integer = 0
        Dim k_input As Integer = 0

        Dim eletrodo As Integer = 0
        Dim eletrodo_flutuante As Integer = 0
        Dim perfil_med As Integer = 0
        Dim setor4 As Integer = 0
        Dim setor5 As Integer = 0

        AbrirArquivo.InitialDirectory = "" 'Abre o ultimo diretorio utilizado
        AbrirArquivo.Filter = "Tricamp (*.tric,*.dat)|*.tric;*.dat|" +
                              "Texto (*.txt)|*.txt|" +
                              "Todos os arquivos (*.*)|*.*"
        AbrirArquivo.FilterIndex = 1
        AbrirArquivo.RestoreDirectory = True


        If AbrirArquivo.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                TextoStream = AbrirArquivo.OpenFile()
                If (TextoStream IsNot Nothing) Then
                    FileOpen(file_num, AbrirArquivo.FileName, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)

                    NomeArq = AbrirArquivo.FileName
                    'Expressao regular para identificar o padrao de dados do eletrodo
                    Dim padrao_eletrodos As String = "(\d)\s+" +
                                                     "(\d+)\s+" +
                                                     "(\d+)\s+" +
                                                     "(\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                     "([+-]?\d+\.\d+|[+-]?\.\d+)"

                    Dim padrao_p3placa As String = "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                   "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                   "([+-]?\d+\.\d+|[+-]?\.\d+)\s+" +
                                                   "([+-]?\d+\.\d+|[+-]?\.\d+)"


                    Dim input() As String = File.ReadAllLines(AbrirArquivo.FileName)
                    Dim matches As MatchCollection


                    'Le o arquivo e checa quantidades
                    For Each m In input

                        If (Mid(m, 1, 1).Trim = "0") Then
                            inicio += 1
                        End If

                        'Procura pela mudanca de categoria de entrada
                        ' zeros = 1 => Eletrodo
                        ' zeros = 2 => Eletrodo Flutuante
                        ' zeros = 3 => Perfil de medicao
                        ' zeros = 4 => Caso 4
                        If (inicio = 1) And (Mid(m, 2, 2).Trim = "0") Then
                            zeros += 1
                        End If

                        If (inicio = 1) And (zeros = 0) Then ' Se estiver iniciado e em eletrodos
                            If (Mid(m, 2, 2).Trim <> "0") And (Mid(m, 2, 2).Trim <> Nothing) Then ' Se nao for mudanca ou vazio
                                eletrodo += 1  ' Conta Eletrodos
                            End If
                        End If

                        If (inicio = 1) And (zeros = 1) Then ' Se estiver iniciado e em eletrodos flutuantes
                            If (Mid(m, 2, 2).Trim <> "0") And (Mid(m, 2, 2).Trim <> Nothing) Then ' Se nao for mudanca ou vazio
                                eletrodo_flutuante += 1  ' Conta Eletrodos flutuantes
                            End If
                        End If

                        If (inicio = 1) And (zeros = 2) Then ' Se estiver iniciado e em eletrodos flutuantes
                            If (Mid(m, 2, 2).Trim <> "0") And (Mid(m, 2, 2).Trim <> Nothing) Then ' Se nao for mudanca ou vazio
                                perfil_med += 1  ' Conta perfil medicao
                            End If
                        End If

                        If (inicio = 1) And (zeros = 3) Then ' Se estiver iniciado e em eletrodos flutuantes
                            If (Mid(m, 2, 2).Trim <> "0") And (Mid(m, 2, 2).Trim <> Nothing) Then ' Se nao for mudanca ou vazio
                                setor4 += 1  ' Conta setor 4
                            End If
                        End If

                        If (inicio = 1) And (zeros = 4) Then ' Se estiver iniciado e em eletrodos flutuantes
                            If (Mid(m, 2, 2).Trim <> "0") And (Mid(m, 2, 2).Trim <> Nothing) Then ' Se nao for mudanca ou vazio
                                setor5 += 1  ' Conta setor 4
                            End If
                        End If


                    Next

                    If (inicio = 0) And (zeros = 0) Then
                        MessageBox.Show("Arquivo não reconhecido como válido.", "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return False
                    Else

                        ReDim Codigo(eletrodo)
                        ReDim Potencial(eletrodo)
                        ReDim LinhasCarga(eletrodo)
                        ReDim TipoDist(eletrodo)
                        ReDim P1(eletrodo)
                        ReDim P2(eletrodo)
                        ReDim P3(eletrodo)
                        ReDim RAB(eletrodo)
                        ReDim H(eletrodo)

                        inicio = 0
                        zeros = 0

                        For Each m In input 'Para cada linha no arquivo de entrada
                            If (Mid(m, 1, 1).Trim = "0") Then 'Checa o zero que inicia os grupos
                                inicio += 1
                            End If

                            If (inicio = 1) And (Mid(m, 2, 2).Trim = "0") Then 'Faz a contagem de zeros que efetuam a mudanca de grupos
                                zeros += 1
                            End If

                            'Inicia a atribuicao de parametros
                            If (inicio = 1) And (zeros = 0) Then ' Se estiver iniciado e na primeira categoria
                                If (Mid(m, 2, 2).Trim <> "0") And (Mid(m, 2, 2).Trim <> Nothing) Then ' Se nao for mudanca ou vazio
                                    matches = Regex.Matches(m, padrao_eletrodos, RegexOptions.Compiled) ' Aplica o filtro na linha - Regex compilado ao inves de interpretado
                                    For Each m2 As Match In matches
                                        Codigo(i) = Convert.ToInt16(m2.Groups(1).Value)
                                        Potencial(i) = Convert.ToInt16(m2.Groups(2).Value)
                                        LinhasCarga(i) = Convert.ToInt16(m2.Groups(3).Value)
                                        TipoDist(i) = Convert.ToInt16(m2.Groups(4).Value)
                                        P1(i) = New Ponto3D(Val(m2.Groups(5).Value), Val(m2.Groups(6).Value), Val(m2.Groups(7).Value))
                                        P2(i) = New Ponto3D(Val(m2.Groups(8).Value), Val(m2.Groups(9).Value), Val(m2.Groups(10).Value))
                                        RAB(i) = New Ponto3D(Val(m2.Groups(11).Value), Val(m2.Groups(12).Value), Val(m2.Groups(13).Value)) 'Val() converte o valor string para um integer ou double real

                                        If (Codigo(i) = 3) Then 'Se for placa, le a proxima linha (k_input + 1)
                                            matches = Regex.Matches(input(k_input + 1), padrao_p3placa, RegexOptions.Compiled) 'Aplica o filtro na linha seguinte
                                            For Each m3 As Match In matches
                                                P3(i) = New Ponto3D(Val(m3.Groups(1).Value), Val(m3.Groups(2).Value), Val(m3.Groups(3).Value))
                                                H(i) = Val(m3.Groups(4).Value)
                                            Next
                                        End If
                                        i += 1 'incrementa o numero de linhas
                                    Next
                                End If
                            End If
                            k_input += 1
                        Next

                        eletrodos = eletrodo
                        flutuantes = eletrodo_flutuante
                        perfilmed = setor4
                    End If

                    ' Close the file.
                    FileClose(file_num)
                End If
            Catch Ex As Exception
                MessageBox.Show("Não é possivel ler o arquivo. Original error: " & Ex.Message, "ERRO", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open.
                If (TextoStream IsNot Nothing) Then
                    TextoStream.Close()
                End If
            End Try
        Else
            FileClose(file_num)
            Return False 'Houve algum erro
        End If

        Return True 'Tudo Ok

    End Function



End Module

